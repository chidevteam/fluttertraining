
- Adding setup parameters for line and circle graph.
  
- Position the graphs correctly (dial should be in the center etc.)
  > y- Positioning for line
  > margins for circular graph
  
- Drawing the current value (circular or line marker)
  > theta position on dial
  > x-position on line.

- Low priority: 
    > Add animation to dial

- Think about BP (systolic, distolic)
- 
- Get Code from Atiq w.r.t table
- 
- Study Atiq's code of circles
- How about using Inherited widgets for data passing
- ValueNotifier study, probably no need since repaint should work

- General Table and General form, and General Page

4 ways to implement data transfer from Parent to child view (based on view hierarchy)
- Via constructor, (Parent updates the variable and child can observe in a timer)  
- Inherited widget
- provider
- Scoped Model
===================================================================================
===================================================================================
TEAM
=====
1. Ahmad  
2. Daniyal
3. Junaid
4. Kashif
5. Hamza Mahmood
6. Sara
7. Ali Hassan
8. Salman
9. Ahsan
10. Umair


 
THE RULES OF PROGRAMMING
========================
- Plan a strategy
- Divide and conquer, no task more than 15min
  > you will immediately realize in 15min that you are going slow
- Think systems, inputs and output 
- Code in english
- Design proper architecture - might need iterations
- No function more than screen height
- Need to go 10x faster
- No hit and trial programming
- Fast iteration cycle
- Know your language and tool
 


