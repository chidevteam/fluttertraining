// import 'dart:ui';

import 'package:flutter/material.dart';
import 'dart:math' as math;

class MyPainter extends StatefulWidget {
  final bool isTest;
  final bool secondTest;

  MyPainter(this.isTest, this.secondTest);

  @override
  _MyPainterState createState() => _MyPainterState(isTest, secondTest);
}

class _MyPainterState extends State<MyPainter>
    with SingleTickerProviderStateMixin {
  final bool isTest;
  final bool secondTest;

  var _radius = 100.0;

  late Animation<double> animation;
  late AnimationController controller;

  late List<double> ecgBuffer = [];

  _MyPainterState(this.isTest, this.secondTest);

  @override
  void initState() {
    super.initState();

    controller = AnimationController(
      vsync: this,
      duration: Duration(seconds: 2),
    );

    Tween<double> _rotationTween = Tween(begin: -math.pi, end: math.pi);

    animation = _rotationTween.animate(controller)
      ..addListener(() {
        setState(() {});
      })
      ..addStatusListener((status) {
        if (status == AnimationStatus.completed) {
          controller.repeat();
        } else if (status == AnimationStatus.dismissed) {
          controller.forward();
        }
      });

    controller.forward();

    double T = 0;
    for (int i = 0; i < 1500; i += 1) {
      T = T + (1.0 / 256.0);
      ecgBuffer.add(math.sin(2 * 3.141592654 * 1 * T)); //2*Pie*f*T
    }
  }

  @override
  Widget build(BuildContext context) {
    return isTest
        ? ClipRect(
            child: CustomPaint(
              painter: TestPainter(animation.value, isTest, ecgBuffer),
              child: Container(),
            ),
          )
        : Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Expanded(
                child: CustomPaint(
                  foregroundPainter:
                      PointPainter(_radius, animation.value, secondTest),
                  painter: CirclePainter(_radius),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 16.0),
                child: Text('Size'),
              ),
              Slider(
                value: _radius,
                min: 10.0,
                max: MediaQuery.of(context).size.width / 2,
                onChanged: (value) {
                  setState(() {
                    _radius = value;
                  });
                },
              ),
            ],
          );
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }
}

// FOR PAINTING THE CIRCLE
class CirclePainter extends CustomPainter {
  final double radius;

  CirclePainter(this.radius);

  @override
  void paint(Canvas canvas, Size size) {
    var paint = Paint()
      ..color = Colors.purpleAccent
      ..strokeWidth = 3
      ..style = PaintingStyle.stroke
      ..strokeCap = StrokeCap.round;

    var path = Path();
    path.addOval(Rect.fromCircle(
      center: Offset(size.width / 2, size.height / 2),
      radius: radius,
    ));
    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}

// FOR PAINTING THE TRACKING POINT
class PointPainter extends CustomPainter {
  final double radius;
  final double radians;
  final bool secondTest;

  PointPainter(this.radius, this.radians, this.secondTest);

  @override
  void paint(Canvas canvas, Size size) {
    var paint = Paint()
      ..color = Colors.teal
      ..strokeWidth = 2
      ..style = PaintingStyle.stroke
      ..strokeCap = StrokeCap.round;

    var pointPaint = Paint()
      ..color = Colors.black
      ..strokeWidth = 1
      ..style = PaintingStyle.fill
      ..strokeCap = StrokeCap.round;

    var innerCirclePaint = Paint()
      ..color = Colors.red
      ..strokeWidth = 2
      ..style = PaintingStyle.stroke
      ..strokeCap = StrokeCap.round;

    final textSpan = TextSpan(
      text:
          "(${(radius * math.cos(radians)).round()}, ${(radius * math.sin(radians)).round()})",
      style: TextStyle(color: Colors.black, fontSize: 16),
    );
    final textPainter = TextPainter(
      text: textSpan,
      textDirection: TextDirection.ltr,
    );
    textPainter.layout(
      minWidth: 0,
      maxWidth: 100,
    );

    var path = Path();

    Offset center = Offset(size.width / 2, size.height / 2);

    path.moveTo(center.dx, center.dy);

    Offset pointOnCircle = Offset(
      radius * math.cos(radians) + center.dx,
      radius * math.sin(radians) + center.dy,
    );

    // For showing the point moving on the circle
    canvas.drawCircle(pointOnCircle, 10, pointPaint);

    // For drawing the inner circle
    if (math.cos(radians) < 0.0) {
      canvas.drawCircle(center, -radius * math.cos(radians), innerCirclePaint);
      textPainter.paint(
        canvas,
        pointOnCircle + Offset(-100, 10),
      );
    } else {
      canvas.drawCircle(center, radius * math.cos(radians), innerCirclePaint);
      textPainter.paint(
        canvas,
        pointOnCircle + Offset(10, 10),
      );
    }

    if (secondTest) {
      for (int i = 0; i < 1500; i++) {
        // For drawing the inner circle
        // if (math.sin(radians) < 0.0) {
        //   canvas.drawCircle(
        //       center, (-radius * math.sin(radians) + i),
        //       innerCirclePaint); //(i*i) -i
        // } else {
        //   canvas.drawCircle(
        //       center, (radius * math.sin(radians) + i), innerCirclePaint);
        // }

        Offset center = Offset((i - 750) * math.Random().nextDouble(),
            (size.height) * math.Random().nextDouble());
        canvas.drawCircle(center, 3, innerCirclePaint);
      }
    }

    path.lineTo(pointOnCircle.dx, pointOnCircle.dy);
    path.lineTo(pointOnCircle.dx, center.dy);

    path.close();
    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}

class TestPainter extends CustomPainter {
  final bool isTest;
  final double radius;
  List<double> ecgBuffer;

  TestPainter(this.radius, this.isTest, this.ecgBuffer);

  @override
  void paint(Canvas canvas, Size size) {
    computePointToPixelAndDrawData(canvas, size);
  }

  void computePointToPixelAndDrawData(Canvas canvas, Size size) {
    var paint = Paint()
      ..color = Colors.black
      ..strokeWidth = 1
      ..strokeCap = StrokeCap.round;

    double canvWidth = size.width;

    double xScale = (1.0 / (256 * 8)) * canvWidth; //TODOx: BUFFER SIZE IN SEC

    double ymax = 1; //1;
    double ymin = -1; //-1;

    double m = (0 - 300) / (ymax - ymin);

    for (int i = 0; i < ecgBuffer.length - 1; i++) {
      double x1 = i.toDouble();
      double y1 = ecgBuffer[i].toDouble();
      double x2 = (i + 1).toDouble();
      double y2 = ecgBuffer[i + 1].toDouble();
      if (y1 == -999 || y2 == -999) continue;
      double X1 = x1 * xScale;
      double Y1 = 300 + m * (y1 - ymin);
      double X2 = x2 * xScale;
      double Y2 = 300 + m * (y2 - ymin);

      Offset startingPoint = Offset(X1 * radius, Y1);
      Offset endingPoint = Offset(X2 * radius, Y2);
      canvas.drawLine(startingPoint, endingPoint, paint);
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
