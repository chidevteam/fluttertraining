import 'dots.dart';
import 'dart:math' as math;
import 'package:flutter/material.dart';

class LiveLineChart1 extends StatefulWidget {
  final bool isTest;
  const LiveLineChart1(this.isTest);

  @override
  State<LiveLineChart1> createState() => _LiveLineChartState(isTest);
}

class _LiveLineChartState extends State<LiveLineChart1>
    with SingleTickerProviderStateMixin {
  late Animation<double> animation;
  late AnimationController controller;

  final ChartController chartController = ChartController();

  final bool isTest;

  late List<double> ecgBuffer = [];

  _LiveLineChartState(this.isTest);

  @override
  void initState() {
    super.initState();
    print("circles created");

    for (int i = 0; i < 10000; i++) {
      double sx = math.Random().nextDouble() * 4.0;
      double sy = math.Random().nextDouble() * 2.0;
      final circle = AnimCircle(
        slopeX: sx,
        slopeY: sy,
        fill: math.Random().nextBool(),
        reverse: math.Random().nextBool(),
        color: Colors.primaries[math.Random().nextInt(Colors.primaries.length)],
        // .withOpacity(0.7),
      );
      chartController.circles.add(circle);
    }

    controller = AnimationController(
      vsync: this,
      duration: const Duration(seconds: 8),
    );

    Tween<double> _tween = Tween(begin: 0, end: 1);

    animation = _tween.animate(controller)
      ..addListener(() {
        // setState(() {});
        if (chartController.changeRadius != null) {
          chartController.changeRadius!(animation.value);
        }
      })
      ..addStatusListener((status) {
        if (status == AnimationStatus.completed) {
          controller.repeat();
        } else if (status == AnimationStatus.dismissed) {
          controller.forward();
        }
      });

    controller.forward();


    double T = 0;
    for (int i = 0; i < 1500; i += 1) {
      T = T + (1.0 / 256.0);
      ecgBuffer.add(math.sin(2 * 3.141592654 * 1 * T)); //2*Pie*f*T
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.blue,
      child: ClipRect(
        child: Dots(
          constroller: chartController,
          isTest: isTest,
          ecgBuffer: ecgBuffer,
        ),
      ),
    );
  }

  @override
  void dispose() {

    controller.dispose();
    super.dispose();

  }
}
