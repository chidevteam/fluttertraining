import 'package:flutter/material.dart';
import 'live_line_chart.dart';

class ChartHolder1App extends StatelessWidget {
  const ChartHolder1App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Chart Demo 1a',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: ChartHolder1(),
    );
  }
}

class ChartHolder1 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Chart Demo 1a'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: const <Widget>[
            SizedBox(
              width: double.infinity,
              height: 300,
              child: LiveLineChart1(false),
            ),
          ],
        ),
      ),
    );
  }
}
