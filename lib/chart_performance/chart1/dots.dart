import 'dart:math' as math;
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
// import 'package:flutter/widgets.dart';

class AnimCircle {
  final double slopeX;
  final double slopeY;
  final double radius;
  final double yIntercept;
  final Paint paint;

  final bool reverse;

  AnimCircle({
    required this.slopeX,
    required this.slopeY,
    required Color color,
    bool fill = false,
    this.reverse = false,
  })  : paint = Paint()
          ..strokeWidth = 2
          ..color = color
          ..style = fill ? PaintingStyle.fill : PaintingStyle.stroke,
        yIntercept = math.Random().nextDouble(),
        radius = (math.Random().nextDouble() * 30 + 20.0);

  Offset getCenter(Offset offset, Size size, double anim) {
    double dx = slopeX * anim - yIntercept;
    double dy = slopeY * anim + yIntercept;

    if (reverse) {
      return Offset((size.width - (dx * size.width / 2.0)) + offset.dx,
          (dy * size.height / 2.0) + offset.dy);
    }
    return Offset((dx * size.width / 2.0) + offset.dx,
        (dy * size.height / 2.0) + offset.dy);
  }
}

class RenderDots extends RenderConstrainedBox {
  double animState = 0.0;
  ChartController constroller;
  final bool isTest;
  List<double> ecgBuffer;

  RenderDots(
      {required this.constroller,
      required this.isTest,
      required this.ecgBuffer})
      : super(additionalConstraints: const BoxConstraints.expand()) {
    constroller.changeRadius = changeRadius;
  }

  @override
  bool hitTestSelf(Offset position) => false;

  @override
  void paint(PaintingContext context, Offset offset) {
    final Canvas canvas = context.canvas;
    canvas.drawRect(offset & size, Paint()..color = const Color(0xFF0000FF));

    if (!isTest) {
      Offset point;
      for (AnimCircle circle in constroller.circles) {
        point = circle.getCenter(offset, size, animState);
        canvas.drawCircle(point, animState * circle.radius, circle.paint);
      }
    } else
      computePointToPixelAndDrawData(canvas, size);

    super.paint(context, offset);
  }

  changeRadius(double value) {
    animState = value;
    markNeedsPaint();
  }

  void computePointToPixelAndDrawData(Canvas canvas, Size size) {
    var paint = Paint()
      ..color = Colors.black
      ..strokeWidth = 1
      ..strokeCap = StrokeCap.round;

    double canvWidth = size.width;

    double xScale = (1.0 / (256 * 8)) * canvWidth; //TODOx: BUFFER SIZE IN SEC

    double ymax = 1; //1;
    double ymin = -1; //-1;

    double m = (0 - 300) / (ymax - ymin);

    for (int i = 0; i < ecgBuffer.length - 1; i++) {
      double x1 = i.toDouble();
      double y1 = ecgBuffer[i].toDouble();
      double x2 = (i + 1).toDouble();
      double y2 = ecgBuffer[i + 1].toDouble();
      if (y1 == -999 || y2 == -999) continue;
      double X1 = x1 * xScale;
      double Y1 = 300 + m * (y1 - ymin);
      double X2 = x2 * xScale;
      double Y2 = 300 + m * (y2 - ymin);

      Offset startingPoint = Offset(X1 * animState, Y1);
      Offset endingPoint = Offset(X2 * animState, Y2);
      canvas.drawLine(startingPoint, endingPoint, paint);
    }
  }
}

class Dots extends SingleChildRenderObjectWidget {
  final ChartController constroller;
  final bool isTest;
  final List<double> ecgBuffer;
  const Dots({
    Key? key,
    required this.constroller,
    required this.isTest,
    required this.ecgBuffer,
    Widget? child,
  }) : super(key: key, child: child);

  @override
  RenderDots createRenderObject(BuildContext context) => RenderDots(
      constroller: constroller, isTest: isTest, ecgBuffer: ecgBuffer);
}

typedef RadiusTypedef = Function(double value);

class ChartController {
  RadiusTypedef? changeRadius;

  final List<AnimCircle> circles = [];

  void dispose() {
    changeRadius = null;
  }
}
