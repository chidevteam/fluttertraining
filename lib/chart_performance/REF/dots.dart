import 'dart:math' as math;
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';

class AnimCircle {
  final double slopeX;
  final double slopeY;
  final double radius;
  final double yIntercept;
  final Paint paint;

  final bool reverse;

  AnimCircle({
    required this.slopeX,
    required this.slopeY,
    required Color color,
    bool fill = false,
    this.reverse = false,
  })  : paint = Paint()
          ..strokeWidth = 2
          ..color = color
          ..style = fill ? PaintingStyle.fill : PaintingStyle.stroke,
        yIntercept = math.Random().nextDouble(),
        radius = 1; //(math.Random().nextDouble() * 30 + 20.0);

  Offset getCenter(Offset offset, Size size, double anim) {
    double dx = slopeX * anim - yIntercept;
    double dy = slopeY * anim + yIntercept;

    if (reverse) {
      return Offset((size.width - (dx * size.width / 2.0)) + offset.dx,
          (dy * size.height / 2.0) + offset.dy);
    }
    return Offset((dx * size.width / 2.0) + offset.dx,
        (dy * size.height / 2.0) + offset.dy);
  }
}
