import 'dart:async';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'dart:ui' as ui;

void main() => runApp(MyApp());

int xorshift32(int x) {
  x ^= x << 13;
  x ^= x >> 17;
  x ^= x << 5;
  return x;
}

int seed = 0xDEADBEEF;

const kImageDimension = 300;

Future<ui.Image> makeImage() {
  final c = Completer<ui.Image>();
  final pixels = Int32List(kImageDimension * kImageDimension);
  for (int i = 0; i < pixels.length; i++) {
    seed = pixels[i] = xorshift32(seed);
  }
  ui.decodeImageFromPixels(
    pixels.buffer.asUint8List(),
    kImageDimension,
    kImageDimension,
    ui.PixelFormat.rgba8888,
    c.complete,
  );
  return c.future;
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  final String title;
  MyHomePage({Key? key, required this.title}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    print("Wwidgt built");
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: GridView.builder(
        gridDelegate:
            SliverGridDelegateWithMaxCrossAxisExtent(maxCrossAxisExtent: 128.0),
        itemBuilder: (context, index) {
          return FutureBuilder<ui.Image>(
            future: makeImage(),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return RawImage(
                  image: snapshot.data,
                );
              } else {
                return Center(child: CircularProgressIndicator());
              }
            },
          );
        },
      ),
    );
  }
}
