import 'dart:ui' as ui;

import 'package:flutter/material.dart';
import 'matrix_gesture_detector.dart';

void main() => runApp(GestureApp2());

class GestureApp2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'MatrixGestureDetector Demo1',
      home: CustomPainterDemo(),
    );
  }
}

class CustomPainterDemo extends StatefulWidget {
  @override
  _CustomPainterDemoState createState() => _CustomPainterDemoState();
}

class _CustomPainterDemoState extends State<CustomPainterDemo> {
  Matrix4 matrix = Matrix4.identity();
  ValueNotifier<Matrix4>? notifier;

  @override
  void initState() {
    super.initState();
    notifier = ValueNotifier(matrix);
  }

  @override
  Widget build(BuildContext context) {
    print("BUILD OF _CustomPainterDemoState ...............");
    return Scaffold(
      appBar: AppBar(
        title: Text('CustomPainter Demo'),
      ),
      body: MatrixGestureDetector(
        onMatrixUpdate: (m, tm, sm, rm) => notifier!.value = m,
        child: CustomPaint(
          foregroundPainter: TestCustomPainter(context, notifier),
          child: Container(
            color: Colors.blueGrey,
          ),
        ),
      ),
    );
  }
}

class TestCustomPainter extends CustomPainter {
  ValueNotifier<Matrix4>? notifier;
  Paint shapesPaint = Paint();
  Paint backgroundPaint = Paint();
  late Path path;
  late ui.Paragraph paragraph;
  Size currentSize = Size.zero;

  TestCustomPainter(BuildContext context, this.notifier)
      : super(repaint: notifier) {
    var _ = MediaQuery.of(context).size.shortestSide / 40;
    shapesPaint.strokeWidth = _;
    shapesPaint.style = PaintingStyle.stroke;
  }

  @override
  void paint(Canvas canvas, Size size) {
    print("pAINT OF CALLED ...............");

    if (size != currentSize) {
      currentSize = size;
      RRect rr = RRect.fromLTRBR(size.width * 0.2, 100, size.width * 0.8,
          100 + size.height / 3, Radius.circular(shapesPaint.strokeWidth * 2));
      Offset offset = Offset(0, 100 + size.height / 3);
      path = Path();
      for (double i = 0; i < 3; i += 0.01) {
        path.addRRect(rr.shift(offset * i));
      }
      backgroundPaint.shader = LinearGradient(
        colors: [
          Color(0xffFF0044),
          Color(0xff000022),
        ],
        stops: [0.5, 1.0],
        begin: Alignment.topCenter,
        end: Alignment.bottomCenter,
      ).createShader(Offset.zero & size);
    }

    canvas.drawPaint(backgroundPaint);

    shapesPaint.color = Color(0xff880000);
    canvas.drawPath(path, shapesPaint);

    shapesPaint.color = Color(0xffbbFF00);
    Matrix4 inverted = Matrix4.zero();
    inverted.copyInverse(notifier!.value);
    canvas.save();
    canvas.transform(inverted.storage);
    canvas.drawPath(path, shapesPaint);
    canvas.restore();

    shapesPaint.color = Color(0xff008800);
    canvas.drawPath(path.transform(notifier!.value.storage), shapesPaint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => true;
}
