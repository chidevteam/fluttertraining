import 'package:flutter/material.dart';
import 'matrix_gesture_detector.dart';

class TransformDemo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final ValueNotifier<Matrix4> notifier = ValueNotifier(Matrix4.identity());
    return Scaffold(
      backgroundColor: Colors.grey,
      appBar: AppBar(
        title: Text('Transform Demo'),
      ),
      body: MatrixGestureDetector(
        onMatrixUpdate: (m, tm, sm, rm) {
          notifier.value = m;
        },
        child: getValueListenableBuilder(context, notifier), //DO NOT REMOVE,
        // child: getAnimatedBuilder(context, notifier), //DO NOT REMOVE,
        shouldRotate: false,
      ),
    );
  }
}

Widget getValueListenableBuilder(ctx, notifier) {
  return ValueListenableBuilder<Matrix4>(
    builder: (BuildContext context, Matrix4 value, Widget? complexWgt) {
      return getBox(notifier);
    },
    valueListenable: notifier,
  );
}

Widget getAnimatedBuilder(ctx, notifier) {
  return AnimatedBuilder(
      animation: notifier,
      builder: (ctx, child) {
        return getBox(notifier);
      });
}

Widget getHeart(notifier) {
  return Container(
    width: double.infinity,
    height: double.infinity,
    transform: notifier.value,
    color: Colors.red,
    child: FittedBox(
      fit: BoxFit.contain,
      child: Icon(
        Icons.favorite,
        color: Colors.white,
      ),
    ),
  );
}

Widget getBox(notifier) {
  return Container(
    width: double.infinity,
    height: double.infinity,
    transform: notifier!.value,
    color: Colors.white,
    child: Container(
      transform: notifier!.value,
      width: double.infinity,
      height: double.infinity,
      color: Colors.red,
    ),
  );
}
