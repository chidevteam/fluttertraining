import 'dart:ui' as ui;
import 'dart:math' as math;
import 'package:flutter/material.dart';
import 'matrix_gesture_detector.dart';
import 'dots.dart';

void main() => runApp(GestureApp2());

class GestureApp2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'MatrixGestureDetector Demo1',
      home: ChartHolder2(),
    );
  }
}

class ChartHolder2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Chart Demo 2a'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            SizedBox(
              width: double.infinity,
              height: 300,
              child: CustomPainterDemo(),
            ),
          ],
        ),
      ),
    );
  }
}

class CustomPainterDemo extends StatefulWidget {
  @override
  _CustomPainterDemoState createState() => _CustomPainterDemoState();
}

class _CustomPainterDemoState extends State<CustomPainterDemo>
    with SingleTickerProviderStateMixin {
  Matrix4 matrix = Matrix4.identity();
  ValueNotifier<Matrix4>? notifier;

  //
  final List<AnimCircle> circles = [];
  late Animation<double> animation;
  late AnimationController controller;

  double animState = 0.0;

  @override
  void initState() {
    super.initState();

    for (int i = 0; i < 10000; i++) {
      double sx = math.Random().nextDouble() * 4.0;
      double sy = math.Random().nextDouble() * 2.0;
      final circle = AnimCircle(
        slopeX: sx,
        slopeY: sy,
        fill: math.Random().nextBool(),
        reverse: math.Random().nextBool(),
        color: Colors.primaries[math.Random().nextInt(Colors.primaries.length)]
            .withOpacity(0.7),
      );

      circles.add(circle);
    }

    controller = AnimationController(
      vsync: this,
      duration: const Duration(seconds: 4),
    );

    Tween<double> _tween = Tween(begin: 0, end: 1);

    animation = _tween.animate(controller)
      ..addListener(() {
        setState(() {
          animState = animation.value;
        });
      })
      ..addStatusListener((status) {
        if (status == AnimationStatus.completed) {
          controller.repeat();
        } else if (status == AnimationStatus.dismissed) {
          controller.forward();
        }
      });

    controller.forward();

    super.initState();
    notifier = ValueNotifier(matrix);
  }

  @override
  Widget build(BuildContext context) {
    // print("BUILD OF _CustomPainterDemoState ...............");
    return Scaffold(
      appBar: AppBar(
        title: Text('CustomPainter Demo'),
      ),
      body: MatrixGestureDetector(
        onMatrixUpdate: (m, tm, sm, rm) => notifier!.value = m,
        child: CustomPaint(
          foregroundPainter:
              TestCustomPainter(context, circles, animState, notifier),
          child: Container(
            color: Colors.blueGrey,
          ),
        ),
      ),
    );
  }
}

class TestCustomPainter extends CustomPainter {
  ValueNotifier<Matrix4>? notifier;
  Paint shapesPaint = Paint();
  Paint backgroundPaint = Paint();
  late Path path;
  late ui.Paragraph paragraph;
  Size currentSize = Size.zero;

  final List<AnimCircle> circles;
  final double animState;

  TestCustomPainter(
      BuildContext context, this.circles, this.animState, this.notifier)
      : super(repaint: notifier) {
    var _ = MediaQuery.of(context).size.shortestSide / 40;
    shapesPaint.strokeWidth = _;
    shapesPaint.style = PaintingStyle.stroke;
  }

  @override
  void paint(Canvas canvas, Size size) {
    // print("pAINT OF CALLED ...............");
    const Offset offset = Offset(0, 0);

    canvas.drawRect(offset & size, Paint()..color = const Color(0xFF00FF00));

    Offset point;
    for (AnimCircle circle in circles) {
      point = circle.getCenter(offset, size, animState);
      canvas.drawCircle(point, 2 * animState * circle.radius, circle.paint);
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => true;
}
