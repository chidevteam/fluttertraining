import 'dart:math' as math;
// import 'dart:ui';

import 'package:flutter/material.dart';
// import 'package:flutter/rendering.dart';

import 'dots.dart';

class LiveLineChart2 extends StatefulWidget {
  final bool isTest;
  const LiveLineChart2(this.isTest);

  @override
  _LiveLineChartState createState() => _LiveLineChartState(isTest);
}

class _LiveLineChartState extends State<LiveLineChart2>
    with SingleTickerProviderStateMixin {
  final bool isTest;
  final List<AnimCircle> circles = [];
  late Animation<double> animation;
  late AnimationController controller;

  late List<double> ecgBuffer = [];

  double animState = 0.0;

  _LiveLineChartState(this.isTest);

  @override
  void initState() {
    super.initState();

    for (int i = 0; i < 1000; i++) {
      double sx = math.Random().nextDouble() * 4.0;
      double sy = math.Random().nextDouble() * 2.0;
      final circle = AnimCircle(
        slopeX: sx,
        slopeY: sy,
        fill: math.Random().nextBool(),
        reverse: math.Random().nextBool(),
        color: Colors.primaries[math.Random().nextInt(Colors.primaries.length)]
            .withOpacity(0.7),
      );

      circles.add(circle);
    }
    double T = 0;
    for (int i = 0; i < 1500; i += 1) {
      T = T + (1.0 / 256.0);
      ecgBuffer.add(math.sin(2 * 3.141592654 * 1 * T)); //2*Pie*f*T
    }

    controller = AnimationController(
      vsync: this,
      duration: const Duration(seconds: 10),
    );

    Tween<double> _tween = Tween(begin: 0, end: 1);

    animation = _tween.animate(controller)
      ..addListener(() {
        setState(() {
          animState = animation.value;
        });
      })
      ..addStatusListener((status) {
        if (status == AnimationStatus.completed) {
          controller.repeat();
        } else if (status == AnimationStatus.dismissed) {
          controller.forward();
        }
      });

    controller.forward();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.blue,
      child: ClipRect(
        child: CustomPaint(
          painter: _LiveTracePainter(
              circles: circles,
              animState: animState,
              ecgBuffer: ecgBuffer,
              isTest: isTest),
        ),
      ),
    );
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }
}

class _LiveTracePainter extends CustomPainter {
  final List<AnimCircle> circles;
  final double animState;
  List<double> ecgBuffer;
  bool isTest;

  _LiveTracePainter(
      {required this.circles,
      required this.animState,
      required this.ecgBuffer,
      required this.isTest});

  @override
  void paint(Canvas canvas, Size size) {
    if (!isTest) {
      const Offset offset = Offset(0, 0);

      canvas.drawRect(offset & size, Paint()..color = const Color(0xFF0000FF));

      Offset point;
      for (AnimCircle circle in circles) {
        point = circle.getCenter(offset, size, animState);
        canvas.drawCircle(point, 2 * animState * circle.radius, circle.paint);
      }
    } else
      computePointToPixelAndDrawData(canvas, size);
  }

  void computePointToPixelAndDrawData(Canvas canvas, Size size) {
    var paint = Paint()
      ..color = Colors.black
      ..strokeWidth = 1
      ..strokeCap = StrokeCap.round;

    double canvWidth = size.width;
    // double canvHeight = size.height; // y1 = 300

    // print(ecgGrid.printDesc());

    double xScale = (1.0 / (256 * 8)) * canvWidth; //TODOx: BUFFER SIZE IN SEC

    double ymax = 1; //1;
    double ymin = -1; //-1;

    double m = (0 - 300) / (ymax - ymin);

    // double xindex = 0;
    for (int i = 0; i < ecgBuffer.length - 1; i++) {
      double x1 = i.toDouble();
      double y1 = ecgBuffer[i].toDouble();
      double x2 = (i + 1).toDouble();
      double y2 = ecgBuffer[i + 1].toDouble();
      if (y1 == -999 || y2 == -999) continue;
      double X1 = x1 * xScale;
      double Y1 = 300 + m * (y1 - ymin);
      double X2 = x2 * xScale;
      double Y2 = 300 + m * (y2 - ymin);

      Offset startingPoint = Offset(X1 * animState, Y1);
      Offset endingPoint = Offset(X2 * animState, Y2);
      canvas.drawLine(startingPoint, endingPoint, paint);
      // xindex = X1;

    }
    // print('Xindex++++++++++++++++++++++++');
    // Offset s1 = Offset(xindex, 0);
    // Offset e1 = Offset(xindex, size.height);
    // canvas.drawLine(s1, e1, paint);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}
