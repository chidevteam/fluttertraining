import 'dart:math';
import "dart:io";

void printx(v, {dynamic end = ''}) {
  stdout.write(v + ' ');
}

String f2(double v) {
  // return "{:.3f}".format(v)
  return (v).toStringAsFixed(3);
}

List<double> computeNorm(double vmin, double vmax, double n) {
  double dx = vmax - vmin;
  double d = dx / n;
  double lg = log(d) / log(10);
  double flog = lg.floorToDouble();
  double v = pow(10.0, flog).toDouble();
  double sc = d / v;

  //sc is in range 1.xx - 9.999
  double scv = 0;
  if (sc < 1.5) {
    scv = v * 1;
  } else if (sc < 2.25) {
    scv = v * 2.0;
  } else if (sc < 3.75) {
    scv = v * 2.5;
  } else if (sc < 7.5) {
    scv = v * 5.0;
  } else {
    scv = v * 10.0;
  }

  // exit(0);
  double vvmin = (vmin / scv).ceil() * scv;
  double vvmax = (vmax / scv).floor() * scv;

  // print("-----------------------------------");
  // print("Total Delta = ${vmax} - ${vmin} = ${dx}");
  // print("-----------------------------------");
  // print("Number of Gridlines = ${n}");
  // print("Approx grid spacing = ${f2(d)}");
  // print("[1.x-9.x range]  ${f2(sc)} ");
  // print("Round and scale back ${f2(scv)} ");
  // print("Upper and lower limits : [${vvmin}, ${vvmax}] -- delta = $scv");
  return [scv, vvmin, vvmax];
}

void main() {
  double nGridLines = 8;
  double scale = 1;
  double vmin = 1 * scale;
  double vmax = 3 * scale;
  List<double> results = computeNorm(vmin, vmax, nGridLines);
  double scv = results[0];
  double vvmin = results[1];
  double vvmax = results[2];

  for (double f = vvmin; f < vvmax + .01; f += scv) {
    printx(f2(f), end: '');
  }
  print('');
}
