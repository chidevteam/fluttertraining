// import 'package:flutter/material.dart';
// import 'graph_painter.dart';
// import 'chi_gesture_detector.dart';
// import 'ref/matrix_gesture_detector.dart';
// import 'dart:math' as math;

// // ignore: must_be_immutable
// class ScichartDemo extends StatelessWidget {
//   late ValueNotifier<Matrix4> notifier = ValueNotifier(Matrix4.identity());
//   // late Offset m_focalPoint;
//   double sc = 0.0;
//   Offset m_f1Pt = Offset.infinite;
//   Offset m_f2Pt = Offset.infinite;
//   Offset m_cPt = Offset.infinite;
//   Offset m_dxy = Offset(0, 0);
//   double m_scale = 1.0;
//   int m_nF = -1;
//   late Map<String, List<double>> m_data = {"x": [], "y": []};

//   ScichartDemo() {
//     print("Scichart demo ............oooooooooooooooo ");

//     for (double t = 1; t < 3; t += 0.001) {
//       double x = t;
//       double y = 3 + 1 * math.sin(2 * math.pi * 1 * t);
//       // 1 * math.sin(2 * math.pi * 3 * t);
//       m_data["x"]!.add(x);
//       m_data["y"]!.add(y);
//     }
//   }

//   initVars() {
//     notifier = ValueNotifier(Matrix4.identity());
//     m_f1Pt = Offset.infinite;
//     m_f2Pt = Offset.infinite;
//     m_cPt = Offset.infinite;
//     m_dxy = Offset(0, 0);
//     m_nF = -1;
//   }

//   @override
//   Widget build(BuildContext context) {
//     initVars();
//     print("Scichart demo ............ooo ");

//     return Scaffold(
//       backgroundColor: Color.fromARGB(255, 80, 80, 80),
//       appBar: AppBar(
//         title: Text('Scicharts Zoom/Pan'),
//         backgroundColor: Colors.green.shade600,
//       ),
//       body: Padding(
//         padding: const EdgeInsets.fromLTRB(5.0, 10, 5.0, 30),
//         child: MatrixGestureDetector(
//           onMatrixUpdate: (m, tm, sm, rm, dxy, scale) {
//             notifier.value = m;
//           },
//           child: getValueListenableBuilder(context, notifier),
//         ),

//         // child: CHIGestureDetector(
//         //   onUpdate: (Matrix4 m, Offset f1Pt, Offset f2Pt, Offset cPt, int nF,
//         //       Offset dxy, double scale) {
//         //     m_f1Pt = f1Pt;
//         //     m_f2Pt = f2Pt;
//         //     m_cPt = cPt;
//         //     m_nF = nF;
//         //     m_dxy = dxy;
//         //     m_scale = scale;
//         //     notifier.value = m;
//         //   },
//         //   child: getValueListenableBuilder(context, notifier),
//         // ),
//       ),
//     );
//   }

//   String toTwoDecimal(double v) {
//     return v.toStringAsFixed(2);
//   }

//   Widget getValueListenableBuilder(ctx, notifier) {
//     print('======================++++++++');
//     return ValueListenableBuilder<Matrix4>(
//       builder: (BuildContext context, Matrix4 value, Widget? complexWgt) {
//         return ClipRect(
//           child: Container(
//             // width: double.infinity,
//             // height: double.infinity,
//             width: 400,
//             height: 750,

//             color: Colors.grey.shade100,
//             child: Container(
//               transform: notifier!.value,
//               child: CustomPaint(
//                 painter: GraphPainter(
//                     m_data, m_f1Pt, m_f2Pt, m_cPt, m_nF, m_dxy, m_scale),
//               ),
//             ),
//           ),
//         );
//         // return getBox(notifier);
//       },
//       valueListenable: notifier,
//     );
//   }
// }

// // void main() {
// //   runApp(MyApp());
// // }

// // class MyApp extends StatelessWidget {
// //   @override
// //   Widget build(BuildContext context) {
// //     return MaterialApp(
// //       title: 'Flutter Art',
// //       theme: ThemeData.dark(),
// //       debugShowCheckedModeBanner: false,
// //       home: ScichartDemo(),
// //     );
// //   }
// // }
