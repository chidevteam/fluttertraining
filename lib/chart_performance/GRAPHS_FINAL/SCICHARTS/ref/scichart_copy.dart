// import 'package:flutter/material.dart';
// import 'graph_painter.dart';

// // ignore: must_be_immutable
// class ScichartDemo extends StatelessWidget {
//   final ValueNotifier<Matrix4> notifier = ValueNotifier(Matrix4.identity());
//   double sc = 1.0;
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       backgroundColor: Colors.grey,
//       appBar: AppBar(
//         title: Text('Transform Demo'),
//       ),
//       body: Padding(
//         padding: const EdgeInsets.all(20.0),
//         child: GestureDetector(
//           onScaleStart: onScaleStart,
//           onScaleUpdate: onScaleUpdate,
//           onScaleEnd: onScaleEnd,

//           child: getValueListenableBuilder(context, notifier), //DO NOT REMOVE,
//           // child: getAnimatedBuilder(context, notifier), //DO NOT REMOVE,
//         ),
//       ),
//     );
//   }

//   String toTwoDecimal(double v) {
//     return v.toStringAsFixed(2);
//   }

//   Matrix4 _scale(double scale, Offset focalPoint) {
//     var dx = (1 - scale) * focalPoint.dx;
//     var dy = (1 - scale) * focalPoint.dy;

//     return Matrix4(scale, 0, 0, 0, 0, scale, 0, 0, 0, 0, 1, 0, dx, dy, 0, 1);
//   }

//   void onScaleStart(ScaleStartDetails details) {
//     translationUpdater.value = details.focalPoint;
//     scaleUpdater.value = 1.0;
//     rotationUpdater.value = 0.0;
//     touchDownValue = details.focalPoint;
//     print(
//         "start === ${details.focalPoint}  ${details.localFocalPoint}  ${details.pointerCount} ");
//   }

//   void onScaleUpdate(ScaleUpdateDetails details) {
//     sc = sc + .002;
//     notifier.value = _scale(details.scale, details.focalPoint);
//     // print('''update1 === S = ${details.scale.toStringAsFixed(3)}
//     //   SX = ${details.horizontalScale.toStringAsFixed(3)}
//     //   SY = ${details.verticalScale.toStringAsFixed(3)} ''');
//   }

//   void onScaleEnd(ScaleEndDetails details) {
//     print("end ===${details.velocity}   ${details.pointerCount} ");
//   }
// }

// Widget getValueListenableBuilder(ctx, notifier) {
//   print('======================++++++++');
//   return ValueListenableBuilder<Matrix4>(
//     builder: (BuildContext context, Matrix4 value, Widget? complexWgt) {
//       return getLiveGraph(notifier);
//       // return getBox(notifier);
//     },
//     valueListenable: notifier,
//   );
// }

// Widget getLiveGraph(notifier) {
//   return ClipRect(
//     child: Container(
//       width: 350,
//       height: 700,
//       color: Colors.grey.shade100,
//       child: Container(
//         transform: notifier!.value,
//         child: CustomPaint(
//           painter: GraphPainter(),
//         ),
//       ),
//     ),
//   );
// }

// Widget getLiveGraph2(notifier) {
//   return ClipRect(
//     child: Container(
//       width: 350,
//       height: 700,
//       color: Colors.yellow.shade800,
//       child: Container(
//         transform: notifier!.value,
//         child: ClipRect(
//           child: CustomPaint(
//             painter: GraphPainter(),
//           ),
//         ),
//       ),
//     ),
//   );
// }

// ///////////////////////////////////////////////////////////////////////
// ///////////////////////////////////////////////////////////////////////
// //////////////////////////////////////////////////////////////////////////
// ///
// typedef _OnUpdate<T> = T Function(T oldValue, T newValue);

// class _ValueUpdater<T> {
//   final _OnUpdate<T> onUpdate;
//   T value;

//   _ValueUpdater({
//     required this.value,
//     required this.onUpdate,
//   });

//   T update(T newValue) {
//     T updated = onUpdate(value, newValue);
//     value = newValue;
//     return updated;
//   }
// }

// _ValueUpdater<Offset> translationUpdater = _ValueUpdater(
//   value: Offset.zero,
//   onUpdate: (oldVal, newVal) => newVal - oldVal,
// );
// _ValueUpdater<double> scaleUpdater = _ValueUpdater(
//   value: 1.0,
//   onUpdate: (oldVal, newVal) => newVal / oldVal,
// );
// _ValueUpdater<double> rotationUpdater = _ValueUpdater(
//   value: 0.0,
//   onUpdate: (oldVal, newVal) => newVal - oldVal,
// );

// Offset touchDownValue = Offset(0, 0);

// void onScaleStart1(ScaleStartDetails details) {
//   translationUpdater.value = details.focalPoint;
//   scaleUpdater.value = 1.0;
//   rotationUpdater.value = 0.0;
//   touchDownValue = details.focalPoint;
// }

// void onScaleUpdate1(ScaleUpdateDetails details) {
//   // Offset translationDelta = translationUpdater.update(details.focalPoint);

//   // final focalPointAlignment = widget.focalPointAlignment;
//   // final focalPoint = focalPointAlignment == null
//   //     ? details.localFocalPoint
//   //     : focalPointAlignment.alongSize(context.size!);

//   // double scaleDelta = scaleUpdater.update(details.scale);

//   // double rotationDelta = rotationUpdater.update(details.rotation);

//   // print("${details.focalPoint - touchDownValue} ${details.focalPoint}");
//   print("${details.localFocalPoint} ${details.focalPoint}");
//   //  ${details.focalPoint}

//   // widget.onMatrixUpdate(
//   //     matrix, translationDeltaMatrix, scaleDeltaMatrix, rotationDeltaMatrix);
// }

// void main() {
//   runApp(MyApp());
// }

// class MyApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       title: 'Flutter Art',
//       theme: ThemeData.dark(),
//       debugShowCheckedModeBanner: false,
//       home: ScichartDemo(),
//     );
//   }
// }
