import 'package:flutter/material.dart';
// import 'dart:math' as math;
// import 'dart:ui';
import 'utils/colors.dart';
import 'utils/scale_design.dart';

class GraphPainter extends CustomPainter {
  final Offset f1Pt;
  final Offset f2Pt;
  final Offset cPt;
  final int nF;
  final Map<String, List<double>> data;
  late Offset dxy;
  late double scale1;
  late double sx;
  late double sy;

  late double width = -1;
  late double height = -1;

  double leftMargin = 40;
  double topMargin = 10;
  double rightMargin = 10;
  double bottomMargin = 30;
  late double mx = 1.0, my = 1.0, cx, cy;

  late double yMinPx, yMaxPx, yDeltaPx;
  late double xMinPx, xMaxPx, xDeltaPx;

  late Offset revFocal;

  late double newXmin, newXmax, newYmin, newYmax;

  GraphPainter(this.data, this.f1Pt, this.f2Pt, this.cPt, this.nF, this.dxy,
      this.scale1, this.sx, this.sy) {
    // print("GraphPainter ........................ here");
  }

  @override
  bool shouldRepaint(GraphPainter oldDelegate) => true;

  void initVars(double w, double h) {
    double ymin = minimum(data["y"]!);
    double ymax = maximum(data["y"]!);
    double xmin = minimum(data["x"]!);
    double xmax = maximum(data["x"]!);
    computeVars(w, h, xmin, xmax, ymin, ymax);
  }

  List<double> solveLinear(
      double x1, double x2, double y1, double y2, d, scale) {
    double mx = (y2 - y1) / (x2 - x1);
    double cx = y1 - mx * x1;
    mx = mx * scale;
    cx = cx * scale + d;
    double xmin_new = (y1 - cx) / mx;
    double xmax_new = (y2 - cx) / mx;
    return [mx, cx, xmin_new, xmax_new];
  }

  void computeVars(
      double w, double h, double xmin, double xmax, double ymin, double ymax) {
    List<double> mxcx =
        solveLinear(xmin, xmax, leftMargin, w - rightMargin, dxy.dx, sx);
    mx = mxcx[0];
    cx = mxcx[1];
    List<double> resultsX = computeNorm(mxcx[2], mxcx[3], 4);
    xDeltaPx = resultsX[0] * mx;
    xMinPx = resultsX[1] * mx + cx;
    xMaxPx = resultsX[2] * mx + cx;

    List<double> mycy =
        solveLinear(ymin, ymax, h - bottomMargin, topMargin, dxy.dy, sy);
    my = mycy[0];
    cy = mycy[1];

    List<double> resultsY = computeNorm(mycy[2], mycy[3], 4);
    yDeltaPx = resultsY[0] * my;
    yMinPx = resultsY[1] * my + cy;
    yMaxPx = resultsY[2] * my + cy;
  }

  @override
  void paint(Canvas canvas, Size size) {
    // if (this.nF == 1) return;

    if (size.width != width || size.height != height) {
      width = size.width;
      height = size.height;
    }
    initVars(size.width, size.height);

    Paint paintOuter = makePaint(Color(0xFF141414), style: PaintingStyle.fill);
    Paint paintInner = makePaint(Color(0xFF1B1B1B), style: PaintingStyle.fill);
    Paint paintBorder =
        makePaint(Color(0xFF555555), style: PaintingStyle.stroke);

    Rect outerRect = Rect.fromLTRB(0, 0, size.width, size.height);
    Rect innerRect = Rect.fromLTRB(leftMargin, topMargin,
        size.width - rightMargin, size.height - bottomMargin);

    canvas.drawRect(outerRect, paintOuter);
    canvas.drawRect(innerRect, paintInner);
    drawGrid(canvas, size);
    canvas.drawRect(innerRect, paintBorder);
    canvas.clipRect(innerRect);

    paintPathFromPTS(canvas, size);
    // drawDebugCircles(canvas, f1Pt, f2Pt, cPt);
  }

  void paintPathFromPTS(Canvas canvas, Size size) {
    Paint paint0 = makePaint(Colors.red, strokeWidth: 1.0);

    Path path = Path();
    List<double> x = data["x"]!;
    List<double> y = data["y"]!;
    // path.moveTo(mx * x[0] + cx, my * y[0] + cy);

    path.moveTo(mx * x[0] + cx, my * y[0] + cy);
    for (int i = 1; i < x.length; i++) {
      path.lineTo(mx * x[i] + cx, my * y[i] + cy);
    }
    canvas.drawPath(path, paint0);
  }

  void drawGrid(Canvas canvas, Size size) {
    // return;
    Path path = Path();
    Paint paint = makePaint(Colors.grey.shade800);

    for (double x = xMinPx; x <= xMaxPx + 1e-3; x += xDeltaPx) {
      path.moveTo(x, topMargin);
      path.lineTo(x, size.height - bottomMargin);
      double xn = (x - cx) / mx;
      textPaint(canvas, size, xn.toStringAsFixed(2), x - 10,
          size.height - bottomMargin + 5);
    }

    for (double y = yMaxPx; y <= yMinPx + 1e-3; y -= yDeltaPx) {
      path.moveTo(leftMargin, y);
      path.lineTo(size.width - rightMargin, y);
      double yn = (y - cy) / my;
      textPaint(canvas, size, yn.toStringAsFixed(2), 15, y - 6);
    }
    canvas.drawPath(path, paint);
  }
}
