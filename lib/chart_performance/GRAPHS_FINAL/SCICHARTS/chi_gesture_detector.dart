import 'package:flutter/material.dart';
import 'dart:math';

// ignore: must_be_immutable
class CHIGestureDetector extends StatelessWidget {
  final Widget child;

  final void Function(Matrix4 m, Offset f1Pt, Offset f2Pt, Offset cPt,
      int pointCount, Offset dxy, double scale, double sx, double sy) onUpdate;

  double dxx = 0.0;
  double dyy = 0.0;
  int pointerCount = -1;

  Offset finger1Pt = Offset.zero;
  Offset finger2Pt = Offset.zero;
  Offset centerPt = Offset.zero;

  // the scales based on current touch processs (from touch down to touch up)
  double currScale = 1.0;
  double currScaleX = 1.0;
  double currScaleY = 1.0;
  double currRotation = 0.0;
  Offset currTrans = Offset(0.0, 0.0);

  // The net scales upto the last touch up.
  double prevScale = 1.0;
  double prevScaleX = 1.0;
  double prevScaleY = 1.0;
  double prevRotation = 0.0;
  Offset prevTrans = Offset(0.0, 0.0);

  Offset memoryPoint = Offset.infinite;

  CHIGestureDetector({Key? key, required this.onUpdate, required this.child}) {}

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onScaleStart: onScaleStart,
      onScaleUpdate: onScaleUpdate,
      onScaleEnd: onScaleEnd,
      child: child,
    );
  }

  Offset rotateScale(Offset pt, double r, double th) {
    double x = pt.dx;
    double y = pt.dy;
    double px = r * cos(th) * x - r * sin(th) * y;
    double py = r * sin(th) * x + r * cos(th) * y;
    return Offset(px, py);
  }

  void onScaleStart(ScaleStartDetails details) {
    pointerCount = details.pointerCount;
    centerPt = details.localFocalPoint;

    if (details.pointerCount == 1) {
      finger1Pt = details.localFocalPoint;
      finger2Pt = details.localFocalPoint;
    } else if (details.pointerCount == 2) {
      finger1Pt = memoryPoint;
      Offset delta = centerPt - finger1Pt;
      finger2Pt = centerPt + delta;
    }
  }

  void onScaleUpdate(ScaleUpdateDetails details) {
    centerPt = details.localFocalPoint;

    if (details.pointerCount == 1) {
      finger1Pt = details.localFocalPoint;
      finger2Pt = details.localFocalPoint;
    } else if (details.pointerCount == 2) {
      Offset deltaXY = details.focalPointDelta - currTrans;

      double th = details.rotation - currRotation;
      currRotation = details.rotation;

      double r = details.scale / currScale;
      currScale = details.scale;

      double ssX = details.horizontalScale;
      double ssY = details.verticalScale;

      // print("$currScale $ssX, $ssY");
      currScaleX = ssX;
      currScaleY = ssY;
      currScaleX = currScale;
      currScaleY = currScale;

      finger1Pt = rotateScale(finger1Pt - centerPt, r, th) + centerPt + deltaXY;
      finger2Pt = rotateScale(finger2Pt - centerPt, r, th) + centerPt + deltaXY;

      Offset vector = finger1Pt - finger2Pt;
      double vx = vector.dx.abs();
      double vy = vector.dy.abs();
      if (vx > vy) currScaleY = (currScaleY - 1) * vy / vx + 1;
      if (vy > vx) currScaleX = (currScaleX - 1) * vx / vy + 1;
    }
    currTrans = details.focalPointDelta; //dont' move up before deltaXY compute
    sendUpdate();
  }

  Offset getDelta(double ssx, double ssy, Offset cxy, Offset pxy, Offset cpt) {
    double dx = (cxy.dx + pxy.dx - cpt.dx) * ssx + cpt.dx;
    double dy = (cxy.dy + pxy.dy - cpt.dy) * ssy + cpt.dy;
    Offset delta = Offset(dx, dy);
    return delta;
  }

  void onScaleEnd(ScaleEndDetails details) {
    print("Endx count changed $pointerCount");

    memoryPoint = finger1Pt;
    prevTrans =
        getDelta(currScaleX, currScaleY, currTrans, prevTrans, centerPt);
    prevScale *= currScale;
    prevScaleX *= currScaleX;
    prevScaleY *= currScaleY;
    prevRotation += currRotation;
    currScale = 1.0;
    currScaleX = 1.0;
    currScaleY = 1.0;

    currRotation = 0.0;
    currTrans = Offset(0.0, 0.0);
  }

  void sendUpdate() {
    Matrix4 m = Matrix4(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, dxx += 0.00001,
        dyy += 0.00001, 0, 1);

    var delta =
        getDelta(currScaleX, currScaleY, currTrans, prevTrans, centerPt);

    onUpdate(
        m,
        finger1Pt,
        finger2Pt,
        centerPt,
        pointerCount,
        delta,
        prevScale * currScale,
        prevScaleX * currScaleX,
        prevScaleY * currScaleY);
    // print("$currScale   =5==");
  }
}
