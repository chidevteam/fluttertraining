import 'package:flutter/material.dart';
// import 'dart:math' as math;
// import 'constellations.dart';
import 'dart:ui';
import 'dart:typed_data';

class ConstellationsPainter extends CustomPainter {
  final int frameCount;
  final List<Path> paths;
  final List<List<double>> pts;
  final List<List<Offset>> offsetPts;
  final List<Float32List> flist32;

  ConstellationsPainter(
      {required this.frameCount,
      required this.paths,
      required this.pts,
      required this.offsetPts,
      required this.flist32}) {}

  @override
  bool shouldRepaint(ConstellationsPainter oldDelegate) => true;

  @override
  void paint(Canvas canvas, Size size) {
    double hue = frameCount.toDouble();
    hue = (hue % 360);
    Paint paint0 = Paint()
      ..color = HSLColor.fromAHSL(1, hue, .8, .8).toColor()
      ..style = PaintingStyle.stroke
      ..strokeWidth = 1.0;

    // Paint paint1 = Paint()
    //   ..color = Colors.amber
    //   ..style = PaintingStyle.stroke
    //   ..strokeWidth = 1.0;

    // addPointsToPath(paths, pts, offsetPts, flist32, frameCount);

    // paintPath(canvas, size, paint0); // worst case 3 sec
    paintPathFromPTS(canvas, size, paint0);
    // paintFloat32(canvas, size, paint0);
    // paintLines(canvas, size, paint0); //13.196 sec
    // paintPolygons(canvas, size, paint0);

    // double speed = 3.1;
    // Offset startingPoint = Offset((frameCount * speed) % 400, 0);
    // Offset endingPoint = Offset((frameCount * speed) % 400, 800);
    // canvas.drawLine(startingPoint, endingPoint, paint1);
  }

  void paintPath(Canvas canvas, Size size, Paint paint0) {
    // int c = 0;
    for (Path path in paths) {
      // c++;
      // Path path2 = Path();
      // double hue = (frameCount % 360).toDouble();
      // double y = 50.0 * math.sin(hue * math.pi / 180.0);
      // path.addPath(path2, Offset(0, y));
      // path.extendWithPath(path2, Offset(0, y));
      canvas.drawPath(path, paint0);
      // if (c == 3) break;
    }
  }

  void paintPathFromPTS(Canvas canvas, Size size, Paint paint0) {
    for (int i = 0; i < 8; i++) {
      Path path = Path();

      List<double> p = pts[i];
      if (p.length > 2) path.moveTo(p[0], p[1]);

      for (int j = 2; j < p.length; j += 2) {
        double x = p[j];
        double y = p[j + 1];
        if (p[j] < p[j - 2])
          path.moveTo(x, y);
        else
          path.lineTo(x, y);
      }
      if (p.length > 2 * 2380) pts[i].removeRange(0, 60);

      canvas.drawPath(path, paint0);
    }
  }

  void paintLines(Canvas canvas, Size size, Paint paint0) {
    for (int i = 0; i < 8; i++) {
      List<Offset> offset = offsetPts[i];
      for (int j = 0; j < offset.length - 1; j += 1) {
        Offset p1 = offset[j];
        Offset p2 = offset[j + 1];
        // print("${p1.dx} ${p1.dy} ${p2.dx} ${p2.dy} ");
        canvas.drawLine(p1, p2, paint0);
      }
    }
  }

  void paintPolygons(Canvas canvas, Size size, Paint paint0) {
    for (int i = 0; i < 8; i++) {
      List<Offset> offset = offsetPts[i];
      canvas.drawPoints(PointMode.polygon, offset, paint0);
    }
  }

  void paintFloat32(Canvas canvas, Size size, Paint paint0) {
    for (int i = 0; i < 8; i++) {
      // Float32List float32list = Float32List.fromList(pts[i]);
      canvas.drawRawPoints(PointMode.polygon, flist32[i], paint0);
    }
  }

  void paintOffset(Canvas canvas, Size size, Paint paint0) {
    for (Path path in paths) {
      canvas.drawPath(path, paint0);
    }
  }
}


// @override
//   void paint(Canvas canvas, Size size) {
//     count += 1.5;
//     Paint paint0 = Paint()
//       ..color = Colors.white
//       ..style = PaintingStyle.stroke
//       ..strokeWidth = 1.0;

//     for (Path path in paths) {
//       Matrix4 mat = Matrix4.skewY(0.3);
//       double x = 50 * math.cos(1 * hue * math.pi / 180.0);
//       double y = 50 * math.sin(1 * hue * math.pi / 180.0);
//       mat.translate(x, y);

//       Path p = path.transform(mat.storage);

//       // Path path2 = Path();
//       // path2.addPath(path, Offset(,);
//       canvas.drawPath(p, paint0);
//     }

//     Offset startingPoint = Offset(hue, 0);
//     Offset endingPoint = Offset(hue, 400);
//     canvas.drawLine(startingPoint, endingPoint, paint0);
