// import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'custom.dart';
import 'dart:math' as math;
import 'dart:async';
import 'dart:typed_data';

class Constellations extends StatelessWidget {
  const Constellations({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    print("building Constellations=====================================XXXX");
    return Scaffold(
      appBar: AppBar(
        title: Text("Constellations"),
        centerTitle: false,
      ),
      body: LayoutBuilder(builder: (context, constrains) {
        return PainterBody(
            width: constrains.maxWidth, height: constrains.maxHeight);
      }),
    );
  }
}

class PainterBody extends StatefulWidget {
  final double width;
  final double height;

  PainterBody({Key? key, required this.width, required this.height})
      : super(key: key);

  @override
  _PainterBodyState createState() => _PainterBodyState();
}

class _PainterBodyState extends State<PainterBody>
    with SingleTickerProviderStateMixin {
  List<List<double>> pts = [
    [],
    [],
    [],
    [],
    [],
    [],
    [],
    [],
  ];
  late AnimationController? _controller;
  int frameCount = 0;
  late List<Path> paths = [];

  late List<Float32List> flist32 = [];

  List<List<Offset>> offsetPts = [[], [], [], [], [], [], [], []];

  double count = 0;
  double t = 0;
  int pointCount = 0;
  Timer? timer;
  late int lastTime;

  bool animCont = true;

  @override
  void initState() {
    super.initState();
    print("=================animCont $animCont");
    lastTime = DateTime.now().millisecondsSinceEpoch;

    for (int i = 0; i < 8; i++) {
      paths.add(Path());
      flist32.add(Float32List(2390 * 2));
    }

    if (animCont) {
      _controller =
          AnimationController(vsync: this, duration: Duration(days: 365));
      _controller?.addListener(repeatFunc);

      _controller?.forward();
      print("==========================================anim cont");
    } else {
      const oneSec = Duration(microseconds: 16666);
      timer = Timer.periodic(oneSec, (Timer t) => repeatFunc());
      print("========================================== TIMER");
    }
  }

  repeatFunc() {
    frameCount++;
    // print(frameCount);

    int fps = 60;

    addPointsToPath(paths, pts, offsetPts, flist32, frameCount);

    if (frameCount % (60 ~/ fps) == 0) setState(() {});
    if ((frameCount % 60) == 0) {
      int currentTime = DateTime.now().millisecondsSinceEpoch;
      int diff = currentTime - lastTime;
      lastTime = currentTime;
      int numpts = (pts[0].length ~/ 2);
      print("+++++ ${frameCount / 60}, $diff  $numpts  ${numpts * 8}");
      // print(">>>>> NUMPOINTS = $frameCount ${}");
    }
  }

  @override
  void dispose() {
    if (animCont)
      _controller?.dispose();
    else
      timer?.cancel();
    super.dispose();
  }

  void touchDetected(event) {
    int numpts = (pts[0].length ~/ 2);
    print("touchDetected numpts -------------  = ${numpts}");
  }

  @override
  Widget build(BuildContext context) {
    // print("building _PainterBodyState===============================YYYY");
    return GestureDetector(
      onPanUpdate: touchDetected,
      child: Container(
        width: widget.width,
        height: widget.height,
        child: CustomPaint(
          painter: ConstellationsPainter(
              frameCount: frameCount,
              paths: paths,
              pts: pts,
              offsetPts: offsetPts,
              flist32: flist32),
        ),
      ),
    );
  }
}

void addPointsToPath(List<Path> paths, List<List<double>> pts,
    List<List<Offset>> offsetPts, List<Float32List> flist32, int frameCount) {
  // if (frameCount < 110)
  {
    int pts_per_frame = 30;
    // print("-------------------------- $frameCount");
    for (int j = 0; j < pts_per_frame; j++) {
      double t = (frameCount * pts_per_frame + j).toDouble();
      addPointsToPathCore(
          paths, pts, offsetPts, flist32, t, frameCount, pts_per_frame);
    }
  }
}

void addPointsToPathCore(
    List<Path> paths,
    List<List<double>> pts,
    List<List<Offset>> offsetPts,
    List<Float32List> flist32,
    double t,
    int frameCount,
    int pts_per_frame) {
  double f = .003;
  double y0 = 70;
  double x = 4.5 * t / (pts_per_frame.toDouble()); //50;
  // double y = ((t * .2) % 40);
  for (int k = 0; k < 8; k++) {
    double y = 25 * math.cos(2 * math.pi * f * t) +
        30 * math.cos(2 * math.pi * 2 * f * t + 1.3 * math.pi / k);

    if (y > 0) y = 0;
    // y = y + 30;

    double X = x;
    double Y = y0 / 2 + k * y0 + y;

    if (pts[0].length == 0)
      paths[k].moveTo(X, Y);
    else
      paths[k].lineTo(X, Y);

    pts[k].add(X % 400);
    pts[k].add(Y);

    // offsetPts[k].add(Offset(X, Y));
    // int index = pts[k].length;
    // flist32[k][index - 2] = X;
    // flist32[k][index - 1] = Y;
  }
}
