import 'package:flutter/material.dart';

class ScrollDemoBoxes extends StatelessWidget {
  final _transformationController = TransformationController();
  final ValueNotifier<TapDownDetails> notifier =
      ValueNotifier(TapDownDetails());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey,
      appBar: AppBar(
        title: Text('Transform Demo'),
      ),
      body: doubleTap(_transformationController, context),
    );
  }

  Widget doubleTap(_transformationController, context) {
    return Container(
        margin: EdgeInsets.all(10),
        width: double.infinity,
        color: Colors.blue,
        height: 400,
        child: InteractiveViewer(
          transformationController: _transformationController,
          constrained: false,
          child: GestureDetector(
            onTapDown: (TapDownDetails details) => onTapDown(context, details),
            onDoubleTap: _handleDoubleTap,
            child: Container(
              width: 500,
              height: 500,
              color: Colors.yellow,
              child: Column(children: [
                SizedBox(height: 200),
                Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
                  SizedBox(width: 200),
                  Container(color: Colors.red, width: 20, height: 20),
                  SizedBox(width: 20),
                  Container(color: Colors.blue, width: 20, height: 20)
                ])
              ]),
            ),
          ),
        ));
  }

  void onTapDown(BuildContext context, TapDownDetails details) {
    notifier.value = details;
    print('globalPosition : ${notifier.value.globalPosition}');
    print('localPosition : ${notifier.value.globalPosition}');
  }

  void _handleDoubleTap() {
    print("Double tappin!!! and zoomin");

    print('globalPosition : ${notifier.value.globalPosition}');
    print('localPosition : ${notifier.value.globalPosition}');

    final position = notifier.value.localPosition;

    _transformationController.value = Matrix4.identity()
      ..translate(-position.dx * 2, -position.dy * 2)
      ..scale(1.0);
  }
}
