import 'package:flutter/material.dart';

class ScrollDemo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey,
      appBar: AppBar(
        title: Text('Transform Demo'),
      ),
      body: scroll2(),
    );
  }
}

Widget scroll1() {
  return SingleChildScrollView(
    scrollDirection: Axis.horizontal,
    child: Container(
      width: 800,
      height: 400,
      color: Colors.yellow,
      child: FittedBox(
        fit: BoxFit.contain,
        child: Icon(
          Icons.favorite,
          color: Colors.green,
        ),
      ),
    ),
  );
}

Widget scroll2() {
  return Container(
    margin: EdgeInsets.all(30),
    width: double.infinity,
    height: 400,
    child: InteractiveViewer(
      constrained: false,
      alignPanAxis: true,
      child: Container(
        width: 800,
        height: 800,
        color: Colors.yellow,
        child: FittedBox(
          fit: BoxFit.contain,
          child: Icon(
            Icons.favorite,
            color: Colors.green,
          ),
        ),
      ),
    ),
  );
}
