import 'package:flutter/material.dart';
// import 'package:universal/chart_performance/REF/transform_demo2.dart';

class ScrollDemo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey,
      appBar: AppBar(
        title: Text('Transform Demo'),
      ),
      body: scroll3(),
    );
  }
}

Widget scroll1() {
  return SingleChildScrollView(
    scrollDirection: Axis.horizontal,
    child: Container(
      width: 800,
      height: 400,
      color: Colors.yellow,
      child: FittedBox(
        fit: BoxFit.contain,
        child: Icon(
          Icons.favorite,
          color: Colors.green,
        ),
      ),
    ),
  );
}

Widget scroll2() {
  return Container(
    // margin: EdgeInsets.all(0),
    width: 300,
    height: 300,
    color: Colors.red,
    child: InteractiveViewer(
      // minScale: .5,
      constrained: true, //
      // alignPanAxis: true,
      child: Container(
        width: 300,
        height: 400,
        color: Colors.yellow,
        child: FittedBox(
          fit: BoxFit.contain,
          child: Icon(
            Icons.favorite,
            color: Colors.green,
          ),
        ),
      ),
    ),
  );
}

Widget scroll3() {
  return Container(
    // margin: EdgeInsets.fromLTRB(20, 0),
    width: 400,
    height: 700,
    color: Colors.red,
    child: InteractiveViewer(
      constrained: true,
      child: Container(
        width: 200,
        height: 200,
        color: Colors.yellow,
        child: Stack(
          children: [
            Positioned(
              top: 10,
              left: 10,
              child: Container(color: Colors.red, width: 40, height: 40),
            ),
            Positioned(
              top: 10,
              right: 10,
              child: Container(color: Colors.blue, width: 40, height: 40),
            ),
            Positioned(
              bottom: 10,
              left: 10,
              child: Container(color: Colors.green, width: 40, height: 40),
            ),
            Positioned(
              bottom: 10,
              right: 10,
              child: Container(color: Colors.purple, width: 40, height: 40),
            ),
          ],
        ),
      ),
    ),
  );
}
