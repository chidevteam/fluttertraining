import 'package:flutter/material.dart';
import 'discrete_graphs/discrete_graph.dart';
import 'live_graphs/live_graph.dart';
import 'demo_matrix_transform/transform_demo.dart';
import 'interactive_viewer/interactive_viewer.dart';
import 'interactive_viewer/interactive_viewer_boxes.dart';
import 'SCICHARTS/scichart.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Art',
      theme: ThemeData.dark(),
      debugShowCheckedModeBanner: false,
      home: Wrapper(),
    );
  }
}

class Wrapper extends StatelessWidget {
  const Wrapper({Key? key}) : super(key: key);

  Widget _project({
    required String title,
    required String subtitle,
    required Widget page,
    required BuildContext context,
  }) {
    return ListTile(
      title: Text(title),
      subtitle: Text(subtitle),
      onTap: () {
        Navigator.of(context)
            .push(MaterialPageRoute(builder: (context) => page));
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Flutter Art"),
        centerTitle: false,
        elevation: 2.0,
      ),
      body: ListView(
        children: [
          _project(
            title: "Live Graphs",
            subtitle: "For ECG real time graphs",
            page: Constellations(),
            context: context,
          ),
          _project(
            title: "Discrete Graphs",
            subtitle: "For different Discrete BP graphs, etc.",
            page: Forest(),
            context: context,
          ),
          _project(
            title: "TransformDemo",
            subtitle: "Heart Demo",
            page: TransformDemo(),
            context: context,
          ),
          _project(
            title: "Scroll Demo",
            subtitle: "Scroll dmmos",
            page: ScrollDemo(),
            context: context,
          ),
          _project(
            title: "Scroll Demo - Boxes",
            subtitle: "Scroll",
            page: ScrollDemoBoxes(),
            context: context,
          ),
          _project(
            title: "Scichart Demo",
            subtitle: "Scichart",
            page: ScichartDemo(),
            context: context,
          ),
        ],
      ),
    );
  }
}
