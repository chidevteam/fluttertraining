// Copyright 2019 The Flutter team. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'package:flutter/material.dart';
// import 'package:gallery/themes/gallery_theme_data.dart';
import 'transformations_demo_board.dart';
import 'transformations_demo_color_picker.dart';

const backgroundColor = Color(0xFF272727);

// The panel for editing a board point.
@immutable
// ignore: must_be_immutable
class EditBoardPoint extends StatelessWidget {
  EditBoardPoint({
    Key? key,
    this.boardPoint,
    this.onColorSelection,
  })  : assert(boardPoint != null),
        super(key: key);

  late BoardPoint? boardPoint;
  late ValueChanged<Color>? onColorSelection;

  @override
  Widget build(BuildContext context) {
    final boardPointColors = <Color>{
      Colors.white,
      Colors.red,
      Colors.green,
      Colors.blue,
      backgroundColor,
    };

    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Text(
          '${boardPoint!.q}, ${boardPoint!.r}',
          textAlign: TextAlign.right,
          style: const TextStyle(fontWeight: FontWeight.bold),
        ),
        ColorPicker(
          colors: boardPointColors,
          selectedColor: boardPoint!.color,
          onColorSelection: onColorSelection!,
        ),
      ],
    );
  }
}
