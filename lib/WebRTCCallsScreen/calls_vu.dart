// import 'package:universal/WebRTCCallsScreen/models.dart';

import 'package:flutter/services.dart';

import 'calls_vm.dart';
import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'models.dart';
import 'widget_size_wrapper.dart';

class WebrtcApp extends StatelessWidget {
  WebrtcApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    printWidthHeight(context, "WebrtcApp");

    print(">>>>>>>>>>>>>>>>>>>>>> Making the view again <<<<<<<<<<<");

    return Scaffold(
      body: SafeArea(child: UsersWidget()),
    );
  }
}

class UsersWidget extends ViewModelBuilderWidget<CallViewModel> {
  UsersWidget({Key? key}) : super(key: key);

  @override
  Widget builder(BuildContext context, CallViewModel viewModel, Widget? child) {
    return OrientationBuilder(builder: (context, orientation) {
      printWidthHeight(context, "UsersWidget");

      List<Widget> list = [];
      for (int i = viewModel.numUsers - 1; i >= 0; i--) {
        Positioned p = UserView(i, viewModel, context);
        list.add(p);
      }

      Stack st = Stack(
        children: list,
      );

      viewModel.setOrientation(orientation == Orientation.portrait);

      WidgetSizeOffsetWrapper wrapperSt = WidgetSizeOffsetWrapper(
          onSizeChange: (Size size) {
            print('Size: ${size.width}, ${size.height}');
            viewModel.setWidthHeight(size.width, size.height);
          },
          child: st);

      Column column = Column(
        children: [
          Expanded(
            flex: 9,
            child: wrapperSt,
          ),
          Expanded(
            flex: 1,
            child: Container(
              width: double.infinity,
              height: double.infinity,
              color: Colors.yellow,
              child: MenuBar(viewModel),
            ),
          ),
        ],
      );

      return column;
    });
  }

  @override
  CallViewModel viewModelBuilder(BuildContext context) {
    return CallViewModel();
  }
}

Positioned UserView(int i, CallViewModel viewModel, BuildContext context) {
  double px = viewModel.users[i].metadata!.left;
  double py = viewModel.users[i].metadata!.top;
  Positioned p = Positioned(
    child: CellDesign(i, viewModel.users[i]),
    top: py,
    left: px,
    width: viewModel.users[i].metadata!.width,
    height: viewModel.users[i].metadata!.height,
  );
  // list.add(p);
  return p;
}

Widget CellDesign(int i, CallUiUser? user) {
  List<Color> colors = [
    Colors.red,
    Colors.cyan,
    Colors.pinkAccent,
    Colors.green,
    Colors.amber,
  ];

  return Container(
    // width: 50,
    // height: 50,
    color: colors[i % 5],
    child: Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        const SizedBox(
          height: 0,
        ),
        Center(
          child: Text(
            '${user!.metadata!.user_id}',
            style: const TextStyle(
              color: Colors.black,
            ),
          ),
        ),
        const Divider(
          color: Colors.white,
          height: 10,
          indent: 0,
          endIndent: 0,
        ),
      ],
    ),
  );
}

Widget callUILandscape(CallViewModel viewModel, BuildContext context) {
  return SizedBox(
    width: double.infinity,
    height: double.infinity,
    child: Column(
      children: [
        Expanded(
          flex: 9,
          child: Container(color: Colors.green),
        ),
        Expanded(
          flex: 1,
          child: Container(
            width: double.infinity,
            height: double.infinity,
            color: Colors.yellow,
            child: MenuBar(viewModel),
          ),
        ),
      ],
    ),
  );
}

Widget MenuBar(CallViewModel viewModel) {
  return SizedBox(
    width: double.infinity,
    height: double.infinity,
    child: Row(
      children: [
        Expanded(
          flex: 1,
          child: ElevatedButton(
            onPressed: viewModel.addUser,
            child: const Text('Add User'),
            // style: ButtonStyle(backgroundCo),
          ),
        ),
        Expanded(
          flex: 1,
          child: ElevatedButton(
            onPressed: viewModel.removeUser,
            child: const Text('Remove User'),
            // style: ButtonStyle(backgroundCo),
          ),
        ),
        Expanded(
          flex: 1,
          child: ElevatedButton(
            onPressed: viewModel.randomPosition,
            child: const Text('Random Position'),
            // style: ButtonStyle(backgroundCo),
          ),
        ),
        Expanded(
          flex: 1,
          child: ElevatedButton(
            onPressed: viewModel.startNativeCall,
            child: const Text('Start Call'),
            // style: ButtonStyle(backgroundCo),
          ),
        ),
      ],
    ),
  );
}

void printWidthHeight(BuildContext context, String widgetName) {
  double width = MediaQuery.of(context).size.width;
  double height = MediaQuery.of(context).size.height;
  print("$widgetName, $width, $height");
}


