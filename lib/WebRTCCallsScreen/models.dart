import "dart:ui";

class UserMeta {
  int? user_id;
  String? user_name;
  String? user_image;
  bool? is_recorder;
  double top = 0;
  double left = 0;
  late double width;
  late double height;
  late int index;

  UserMeta({this.user_id, this.user_name, this.user_image, this.is_recorder}) {
    print("User Meta data");
    index = user_id!;
    // double v = (30 + 20 * id).toDouble();
    // position = Offset(v, v);
    // index = int.parse(this.user_id!);
  }

  // static UserMeta fromMap(Map<String, dynamic> resp) {
  //   return UserMeta(
  //     resp['user_id'] as int,
  //     resp['user_name'] as String,
  //     resp['user_image'] as String,
  //     resp['is_recorder'] as bool,
  //   );
  // }

  String toString() {
    String st = "User : " + user_id!.toString();
    st += "   Postion:(${left},${top})";
    return st;
  }
}

class ParticipantDeviceState {
  bool? audioActive;
  bool? videoActive;
  bool? screenActive;

  ParticipantDeviceState(
      {this.audioActive, this.videoActive, this.screenActive});
}

class CallUiUser {
  UserMeta? metadata;
  ParticipantDeviceState? deviceState;
  bool isLocal = false;

  CallUiUser(this.metadata, this.deviceState, this.isLocal);

  String toString() {
    return metadata.toString();
  }
}
