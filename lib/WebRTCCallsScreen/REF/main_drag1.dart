import 'package:flutter/material.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: App(),
      ),
    );
  }
}

class App extends StatefulWidget {
  @override
  AppState createState() => AppState();
}

class AppState extends State<App> {
  late Offset position;
  late Offset positionStart;
  @override
  void initState() {
    super.initState();
    position = Offset(0, 0);
    positionStart = Offset(100, 100);
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onPanStart: (details) => _onPanStart(context, details),
      onPanUpdate: (details) => _onPanUpdate(context, details, position),
      // onPanEnd: (details) => _onPanEnd(context, details),
      // onPanCancel: () => _onPanCancel(context),
      child: SafeArea(
        child: Stack(
          children: <Widget>[
            Positioned(
              top: position.dy + positionStart.dy,
              left: position.dx + positionStart.dx,
              child: Container(
                color: Colors.red,
                width: 100,
                height: 100,
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _onPanStart(BuildContext context, DragStartDetails details) {
    print(details.localPosition);
    positionStart = details.localPosition;
  }

  void _onPanUpdate(
      BuildContext context, DragUpdateDetails details, Offset offset) {
    setState(() {
      position += details.delta;
    });
  }

  void _onPanEnd(BuildContext context, DragEndDetails details) {
    print(details.velocity);
  }

  void _onPanCancel(BuildContext context) {
    print("Pan canceled !!");
  }
}
