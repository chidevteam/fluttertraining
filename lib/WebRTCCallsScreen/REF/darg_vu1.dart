import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("Drag app"),
        ),
        body: HomePage1(),
      ),
    );
  }
}

class HomePage1 extends StatelessWidget {
  Widget build(BuildContext context) {
    print("tewst");
    return Scaffold(
      appBar: AppBar(
        title: Text('Woolha.com Flutter Tutorial'),
      ),
      body: Center(
        child: GestureDetector(
          onTap: () {
            print('onTap');
            Feedback.forTap(context);
          },
          onLongPress: () {
            print('onLongPress');
            Feedback.forLongPress(context);
          },
          child: ElevatedButton(
            onPressed: () {
              print("button pressed");
            },
            child: Text('Click'),
          ),
        ),
      ),
    );
  }
}

void main() {
  runApp(MaterialApp(
      title: 'Webrtc',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: HomePage1()));
}
