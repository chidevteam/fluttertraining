import 'package:flutter/material.dart';

class SecondScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("Drag app"),
        ),
        body: HomePage(),
      ),
    );
  }
}

class HomePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _HomePageState();
  }
}

class _HomePageState extends State<HomePage> {
  double width = 100.0, height = 100.0;
  Offset? position;

  @override
  void initState() {
    super.initState();
    position = Offset(0.0, height - 20);
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Positioned(
          left: position!.dx,
          top: position!.dy,
          //top: position.dy - height + 20,
          child: Draggable(
            child: Container(
              width: width,
              height: height,
              color: Colors.blue,
              child: Center(
                child: Text(
                  "Drag",
                  style: Theme.of(context).textTheme.bodyText1,
                ),
              ),
            ),
            feedback: Container(
              child: Center(
                child: Text(
                  "Drag",
                  style: Theme.of(context).textTheme.bodyText1,
                ),
              ),
              color: Colors.red[800],
              width: width,
              height: height,
            ),
            onDraggableCanceled: (Velocity velocity, Offset offset) {
              setState(() => position = offset);
            },
          ),
        ),
      ],
    );
  }
}

void main() {
  runApp(MaterialApp(
      title: 'Webrtc',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: SecondScreen()));
}
