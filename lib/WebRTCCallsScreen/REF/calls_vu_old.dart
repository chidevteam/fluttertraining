// import 'calls_vm.dart';
// import 'package:flutter/material.dart';
// import 'package:stacked/stacked.dart';

// class WebrtcApp extends ViewModelBuilderWidget<CallViewModel> {
//   const WebrtcApp({Key? key}) : super(key: key);

//   @override
//   Widget builder(BuildContext context, CallViewModel viewModel, Widget? child) {
//     return Scaffold(
//       body: SafeArea(
//         child: callUIScreen(viewModel, context),
//       ),
//     );
//   }

//   @override
//   CallViewModel viewModelBuilder(BuildContext context) {
//     return CallViewModel();
//   }

//   Widget callUIScreen(CallViewModel viewModel, BuildContext context) {
//     return SizedBox(
//       width: double.infinity,
//       height: double.infinity,
//       child: Column(
//         children: [
//           Expanded(
//             flex: 9,
//             child: participantView(viewModel, context),
//           ),
//           Expanded(
//             flex: 1,
//             child: Container(
//               width: double.infinity,
//               height: double.infinity,
//               color: Color(Colors.black87.value),
//               child: bottomMenuBar(viewModel),
//             ),
//           ),
//         ],
//       ),
//     );
//   }

//   Widget participantView(CallViewModel viewModel, BuildContext context) {
//     return Container(
//         width: double.infinity,
//         height: double.infinity,
//         color: Color(Colors.black45.value),
//         child: participants(viewModel, context));
//   }

//   participants(CallViewModel viewModel, BuildContext context) {
//     final children = <Widget>[];
//     var length = viewModel.remoteUsers.length;
//     var index = 0;

//     viewModel.remoteUsers.values.forEach((element) {
//       children.add(
//         Positioned(
//           width: MediaQuery.of(context).size.width,
//           height: length > 1
//               ? MediaQuery.of(context).size.height / length
//               : MediaQuery.of(context).size.height,
//           top: (MediaQuery.of(context).size.height / length) * index,
//           child: Container(
//             color: Colors.red,
//             child: Column(
//               mainAxisAlignment: MainAxisAlignment.spaceBetween,
//               children: [
//                 const SizedBox(
//                   height: 0,
//                 ),
//                 Center(
//                   child: Text(
//                     '${element.metadata.user_name}',
//                     style: const TextStyle(
//                       color: Colors.black,
//                     ),
//                   ),
//                 ),
//                 const Divider(
//                   color: Colors.white,
//                   height: 10,
//                   indent: 0,
//                   endIndent: 0,
//                 ),
//               ],
//             ),
//           ),
//         ),
//       );

//       index++;
//     });

//     children.add(
//       Positioned(
//         width: length != 0 ? 80 : MediaQuery.of(context).size.width,
//         height: length != 0 ? 140 : MediaQuery.of(context).size.height,
//         left: length != 0 ? MediaQuery.of(context).size.width - 100 : 0,
//         top: length != 0 ? 20 : 0,
//         child: Container(
//           color: Colors.green,
//           child: Center(
//             child: Text(
//               '${viewModel.localUser.metadata.user_name}',
//               style: const TextStyle(
//                 color: Colors.black,
//               ),
//             ),
//           ),
//         ),
//       ),
//     );

//     return Stack(
//       children: children,
//     );
//   }

//   Widget bottomMenuBar(CallViewModel viewModel) {
//     return Row(
//       children: [
//         Padding(
//           padding: const EdgeInsets.only(left: 10),
//           child: Text(
//             viewModel.textDuration,
//             style: const TextStyle(color: Colors.white),
//           ),
//         ),
//         Padding(
//           padding: const EdgeInsets.only(left: 10),
//           child: IconButton(
//             color: Colors.white,
//             onPressed: () {},
//             icon: const Icon(Icons.more_horiz),
//           ),
//         ),
//         const VerticalDivider(
//           color: Colors.white,
//           width: 10,
//           indent: 0,
//           endIndent: 0,
//         ),
//         Padding(
//           padding: const EdgeInsets.only(left: 10),
//           child: IconButton(
//             color: Colors.white,
//             onPressed: () {
//               viewModel.updateCamState();
//             },
//             icon: viewModel.videoState
//                 ? const Icon(Icons.videocam)
//                 : const Icon(Icons.videocam_off),
//           ),
//         ),
//         Padding(
//           padding: const EdgeInsets.only(left: 10),
//           child: IconButton(
//             color: Colors.white,
//             onPressed: () {
//               viewModel.updateMicState();
//             },
//             icon: viewModel.micState
//                 ? const Icon(Icons.mic)
//                 : const Icon(Icons.mic_off),
//           ),
//         ),
//         Padding(
//           padding: const EdgeInsets.only(left: 10),
//           child: Container(
//             decoration: BoxDecoration(
//                 color: Colors.red, borderRadius: BorderRadius.circular(5)),
//             // color: Colors.red,
//             child: IconButton(
//               color: Colors.white,
//               onPressed: () {
//                 debugPrint('end call click');
//                 viewModel.stopTimer();
//               },
//               icon: const Icon(Icons.call_end),
//             ),
//           ),
//         ),
//       ],
//     );
//   }
// }
