import 'package:flutter/material.dart';
import "calls_vu.dart";

void main() {
  runApp(MaterialApp(
      title: 'Webrtc',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: WebrtcApp()));
}
