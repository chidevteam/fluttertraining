import 'package:flutter/services.dart';

import 'models.dart';
import 'package:stacked/stacked.dart';
import 'dart:ui';

class CallViewModel extends BaseViewModel {

  static const platform = MethodChannel('com.chi.calls/host.base');

  Offset positionLocal = Offset(10, 10);
  List<CallUiUser> users = [];
  int numUsers = 0;
  double viewWidth = 0;
  double viewHeight = 0;
  bool isViewPortrait = false;
  bool isModeFloating = true;

  CallViewModel() {
    addUser();
    addUser();
    // addUser();
    // addUser();
    // addUser();
    // addUser();
    // addUser();
    print("View Model created ...");
  }

  void addUser() {
    CallUiUser? callUser = null;
    int uid = numUsers;

    if (users.length == 0) {
      callUser = CallUiUser(UserMeta(user_id: uid), null, true);
    } else {
      callUser = CallUiUser(UserMeta(user_id: uid), null, false);
    }
    users.add(callUser);
    numUsers++;
    // print(callUser);
    // this.Print();
    setWidthHeightUsers();
    notifyListeners();
  }

  void removeUser() {
    numUsers--;
    users.removeAt(numUsers);

    print("Removing user");
    setWidthHeightUsers();
    // this.Print();
    notifyListeners();
  }

  void randomPosition() {
    print("Random Position");
  }

  void floatingToggle() {
    print("floating toggle");
  }

  void Print() {
    print("------------- List of Users -----------------");
    print("Users = $numUsers");
    for (int i = 0; i < users.length; i++) {
      print(users[i]);
      // st += ${users[us]} + "\n";
    }
    // return st;
  }

  void setOrientation(bool isPortrait) {
    isViewPortrait = isPortrait;
  }

  void setWidthHeight(double width, double height) {
    viewWidth = width;
    viewHeight = height;

    // print("($width, $height) ,,");
    setWidthHeightUsers();
    notifyListeners();
  }

  void setWidthHeightUsers() {
    if (isViewPortrait)
      portraitLayout();
    else
      landscapeLayout();

    // this.Print();
  }

  void portraitLayout() {
    print("portraitLayout orientation");
    List<Map> rowsColsArr = [
      {"users": 0, "rows": 1, "cols": 1, "last_row": 1, "floatWin": false},
      {"users": 1, "rows": 1, "cols": 1, "last_row": 0, "floatWin": false},
      {"users": 2, "rows": 2, "cols": 1, "last_row": 0, "floatWin": false},
      {"users": 3, "rows": 2, "cols": 2, "last_row": 1, "floatWin": false},
      {"users": 4, "rows": 2, "cols": 2, "last_row": 2, "floatWin": false},
      {"users": 5, "rows": 3, "cols": 2, "last_row": 1, "floatWin": false},
      {"users": 6, "rows": 3, "cols": 2, "last_row": 2, "floatWin": false},
      {"users": 7, "rows": 4, "cols": 2, "last_row": 1, "floatWin": false},
      {"users": 8, "rows": 4, "cols": 2, "last_row": 2, "floatWin": false},
    ];

    Map rowCols = rowsColsArr[numUsers];
    int numRows = rowCols["rows"];
    int numCols = rowCols["cols"];
    double dh = viewHeight / numRows;
    double dw = viewWidth / numCols;

    for (int i = 0; i < users.length; i++) {
      users[i].metadata!.left = (i % numCols) * dw;
      users[i].metadata!.top = (i ~/ numCols).toDouble() * dh;
      users[i].metadata!.width = dw;
      users[i].metadata!.height = dh;
      if ((rowCols["last_row"] == 1) && (i == users.length - 1)) {
        users[i].metadata!.width = 2 * dw;
      }
    }
  }

  void landscapeLayout() {
    print("landscapeLayout orientation");
    List<Map> rowsColsArr = [
      {"users": 0, "rows": 1, "cols": 1, "last_col": 1, "floatWin": false},
      {"users": 1, "rows": 1, "cols": 1, "last_col": 0, "floatWin": false},
      {"users": 2, "rows": 1, "cols": 2, "last_col": 0, "floatWin": false},
      {"users": 3, "rows": 2, "cols": 2, "last_col": 1, "floatWin": false},
      {"users": 4, "rows": 2, "cols": 2, "last_col": 2, "floatWin": false},
      {"users": 5, "rows": 2, "cols": 3, "last_col": 1, "floatWin": false},
      {"users": 6, "rows": 2, "cols": 3, "last_col": 2, "floatWin": false},
      {"users": 7, "rows": 2, "cols": 4, "last_col": 1, "floatWin": false},
      {"users": 8, "rows": 2, "cols": 4, "last_col": 2, "floatWin": false},
    ];

    Map rowCols = rowsColsArr[numUsers];
    int numRows = rowCols["rows"];
    int numCols = rowCols["cols"];
    double dh = viewHeight / numRows;
    double dw = viewWidth / numCols;

    for (int i = 0; i < users.length; i++) {
      users[i].metadata!.left = (i ~/ numRows).toDouble() * dw;
      users[i].metadata!.top = (i % numRows) * dh;

      users[i].metadata!.width = dw;
      users[i].metadata!.height = dh;
      if ((rowCols["last_col"] == 1) && (i == users.length - 1)) {
        users[i].metadata!.height = 2 * dh;
      }
    }
  }

  Future<void> startNativeCall() async {

    try {
      final result = await platform.invokeMethod('setupNativeCall', {
        'server': "https://webrtc.cognitivehealthintl.com",
        'room_id': "room253",
        'my_user_id': "201",
        'my_name': "John Wick",
      });

      if (result['status'] == 'Ok') {
        print('OK response');
      } else {
        print('Unknown response sendMessageToAndroid');
        print(result);
      }
    } on PlatformException catch (e) {
      print('Failed to sendMessageToAndroid $e');
    }
  }
}
