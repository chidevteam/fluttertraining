import 'modified_data_for_graphs.dart';
// import "package:collection/collection.dart";
import 'dart:convert';
import 'dart:io';

main() {
  // print(chartsData);

  // Find the indices of systolic and diastolic
  List data = chartsData['vital_graph_data'];
  Map<String, int> map = {};
  for (int i = 0; i < data.length; i++) {
    String vitalName = data[i]['vitals'][0]['name'];
    // print(vitalName);
    map[vitalName] = i;
  }
  print(map);

  // Print indices of Systolic and Distolic
  // if (map.containsKey("Systolic") && map.containsKey("Diastolic")) {
  //   print(map["Systolic"]);
  //   print(map["Diastolic"]);
  // }

  // add all other than systolic and diastolic
  List data2 = [];
  for (int i = 0; i < data.length; i++) {
    if (i == map["Systolic"]) continue;
    if (i == map["Diastolic"]) continue;
    print(i);
    data2.add(data[i]);
  }

  // Add systolic and diastolic
  if (map["Systolic"] != null && map["Diastolic"] != null) {
    Map diastolic = data[map["Diastolic"]!];
    Map systolic = data[map["Systolic"]!];
    diastolic["vitals"].add(systolic["vitals"][0]);
    data2.add(diastolic);
  }

  writeJsonToFile({"vital_graph_data": data2}, "test.dart");
}

void writeJsonToFile(data, fileName) {
  JsonEncoder encoder = new JsonEncoder.withIndent('  ');
  String prettyPrintData = encoder.convert(data);
  // final fileName = 'data.dart';
  File(fileName).writeAsStringSync(
      'Map <String, dynamic> response = ' + prettyPrintData + ';');
}
