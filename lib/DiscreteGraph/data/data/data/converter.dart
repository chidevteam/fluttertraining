import 'dart:io';
import 'response.dart';
import 'dart:convert';

void main() {
  Map<String, dynamic> graphData = response['data'];
  Map<String, dynamic> data = {'vital_graph_data': []};
  // print(graphData.keys);
  // exit(0);

  for (var key in graphData.keys) {
    Map<String, dynamic> ranges = graphData[key]['ranges'];
    Map<String, dynamic> stats = graphData[key]['stats'];
    Map<String, dynamic> trendPoints = graphData[key]['trend_points'];
    List<Map<String, dynamic>> values = graphData[key]['values'];

    data['vital_graph_data']!.add({
      'title': graphData[key]['vital_name'],
      'time_start': values[0]['date_added'],
      'time_end': values[values.length - 1]['date_added'],
      'vitals': [
        {
          'name': ranges['name'],
          'ranges': [
            ranges['warn_low_end_value'],
            ranges['warn_low_value'],
            ranges['normal_low_value'],
            ranges['normal_high_value'],
            ranges['warn_high_value'],
            ranges['warn_high_end_value']
          ],
          'trend_line': graphData[key].containsKey('trend_points') &&
                  graphData[key]['trend_points'].isNotEmpty
              ? {
                  't1': trendPoints[0]['start_point']['date'],
                  'v1': trendPoints[0]['start_point']['value'],
                  't2': trendPoints[1]['end_point']['date'],
                  'v2': trendPoints[1]['end_point']['value'],
                }
              : null,
          'stats': {
            'min': stats['min'],
            'max': stats['max'],
            'mean': stats['mean']
          },
          'data': graphValues(values),
        },
      ],
    });
  }
  // writeToFile(data);
}

void writeToFile(data) async {
  JsonEncoder encoder = new JsonEncoder.withIndent('  ');
  String prettyPrintData = encoder.convert(data);
  final filename = 'data.json';
  var file = await File(filename).writeAsString(prettyPrintData);
}

List graphValues(List values) {
  var data = [];
  for (var i = 0; i < values.length; i++) {
    data.add([
      values[i]['date_added'],
      values[i]['value'],
      values[i]['min_value'],
      values[i]['max_value']
    ]);
  }
  return data;
}
