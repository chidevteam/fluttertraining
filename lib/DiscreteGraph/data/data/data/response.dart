Map<String, dynamic> response = {
  "data": {
    "Pulse": {
      "ranges": {
        "measurement_name_to_display": "Pulse",
        "normal_low_value": 61.0,
        "normal_high_value": 90.0,
        "warn_low_value": 51.0,
        "warn_low_end_value": 60.0,
        "warn_high_value": 91.0,
        "warn_high_end_value": 110.0,
        "alert_low_value": 0.0,
        "alert_low_end_value": 50.0,
        "alert_high_value": 111.0,
        "alert_high_end_value": 999.0,
        "name": "Pulse"
      },
      "stats": {
        "min": 34.0,
        "max": 90.0,
        "lower_percentage": 0.0,
        "greater_percentage": 0.0,
        "mean": 74.96
      },
      "trend_points": [
        {
          "start_point": {
            "date": 1625961600000,
            "value": 76.82206494125148,
            "Pulse": 76.82206494125148
          },
          "name": "Pulse"
        },
        {
          "end_point": {
            "date": 1628553600000,
            "value": 73.14792235630375,
            "Pulse": 73.14792235630375
          },
          "name": "Pulse"
        }
      ],
      "values": [
        {
          "reading_id": -1,
          "name": "Pulse",
          "measurement_name": "Pulse",
          "measurement_name_to_display": "Pulse",
          "measurement_unit": "bpm",
          "measurement_value": 77.0,
          "service_name": "Blood pressure monitor",
          "service_name_to_display": "Blood pressure monitor",
          "status": "Normal",
          "status_to_display": "Normal",
          "value": 77.0,
          "min_value": 77.0,
          "max_value": 77.0,
          "date_added": 1625961600,
          "Pulse": 77.0
        },
        {
          "reading_id": -1,
          "name": "Pulse",
          "measurement_name": "Pulse",
          "measurement_name_to_display": "Pulse",
          "measurement_unit": "bpm",
          "measurement_value": 79.0,
          "service_name": "Blood pressure monitor",
          "service_name_to_display": "Blood pressure monitor",
          "status": "Normal",
          "status_to_display": "Normal",
          "value": 79.0,
          "min_value": 79.0,
          "max_value": 79.0,
          "date_added": 1626048000,
          "Pulse": 79.0
        },
        {
          "reading_id": -1,
          "name": "Pulse",
          "measurement_name": "Pulse",
          "measurement_name_to_display": "Pulse",
          "measurement_unit": "bpm",
          "measurement_value": 76.0,
          "service_name": "Blood pressure monitor",
          "service_name_to_display": "Blood pressure monitor",
          "status": "Normal",
          "status_to_display": "Normal",
          "value": 76.0,
          "min_value": 76.0,
          "max_value": 76.0,
          "date_added": 1626134400,
          "Pulse": 76.0
        },
        {
          "reading_id": -1,
          "name": "Pulse",
          "measurement_name": "Pulse",
          "measurement_name_to_display": "Pulse",
          "measurement_unit": "bpm",
          "measurement_value": 75.0,
          "service_name": "Blood pressure monitor",
          "service_name_to_display": "Blood pressure monitor",
          "status": "Normal",
          "status_to_display": "Normal",
          "value": 75.0,
          "min_value": 75.0,
          "max_value": 75.0,
          "date_added": 1626220800,
          "Pulse": 75.0
        },
        {
          "reading_id": -1,
          "name": "Pulse",
          "measurement_name": "Pulse",
          "measurement_name_to_display": "Pulse",
          "measurement_unit": "bpm",
          "measurement_value": 74.18,
          "service_name": "Blood pressure monitor",
          "service_name_to_display": "Blood pressure monitor",
          "status": "Normal",
          "status_to_display": "Normal",
          "value": 74.18,
          "min_value": 56.0,
          "max_value": 90.0,
          "date_added": 1627948800,
          "Pulse": 74.18
        },
        {
          "reading_id": -1,
          "name": "Pulse",
          "measurement_name": "Pulse",
          "measurement_name_to_display": "Pulse",
          "measurement_unit": "bpm",
          "measurement_value": 73.23,
          "service_name": "Blood pressure monitor",
          "service_name_to_display": "Blood pressure monitor",
          "status": "Normal",
          "status_to_display": "Normal",
          "value": 73.23,
          "min_value": 47.0,
          "max_value": 75.0,
          "date_added": 1628035200,
          "Pulse": 73.23
        },
        {
          "reading_id": -1,
          "name": "Pulse",
          "measurement_name": "Pulse",
          "measurement_name_to_display": "Pulse",
          "measurement_unit": "bpm",
          "measurement_value": 70.63,
          "service_name": "Blood pressure monitor",
          "service_name_to_display": "Blood pressure monitor",
          "status": "Normal",
          "status_to_display": "Normal",
          "value": 70.63,
          "min_value": 34.0,
          "max_value": 86.0,
          "date_added": 1628121600,
          "Pulse": 70.63
        },
        {
          "reading_id": -1,
          "name": "Pulse",
          "measurement_name": "Pulse",
          "measurement_name_to_display": "Pulse",
          "measurement_unit": "bpm",
          "measurement_value": 75.0,
          "service_name": "Blood pressure monitor",
          "service_name_to_display": "Blood pressure monitor",
          "status": "Normal",
          "status_to_display": "Normal",
          "value": 75.0,
          "min_value": 75.0,
          "max_value": 75.0,
          "date_added": 1628467200,
          "Pulse": 75.0
        },
        {
          "reading_id": -1,
          "name": "Pulse",
          "measurement_name": "Pulse",
          "measurement_name_to_display": "Pulse",
          "measurement_unit": "bpm",
          "measurement_value": 74.58,
          "service_name": "Blood pressure monitor",
          "service_name_to_display": "Blood pressure monitor",
          "status": "Normal",
          "status_to_display": "Normal",
          "value": 74.58,
          "min_value": 67.0,
          "max_value": 79.0,
          "date_added": 1628553600,
          "Pulse": 74.58
        }
      ],
      "measurement_name": "Pulse",
      "measurement_name_to_display": "Pulse",
      "measurement_unit": "bpm",
      "vital_name": "Blood pressure monitor"
    },
    "QTC": {
      "ranges": {
        "measurement_name_to_display": "QTC",
        "normal_low_value": 2.0,
        "normal_high_value": 431.0,
        "warn_low_value": 1.0,
        "warn_low_end_value": -999999,
        "warn_high_value": 441.0,
        "warn_high_end_value": 999999,
        "alert_low_value": 0.0,
        "alert_low_end_value": -999999,
        "alert_high_value": 451.0,
        "alert_high_end_value": 999999,
        "name": "QTC"
      },
      "stats": {
        "min": 2.0,
        "max": 2.0,
        "lower_percentage": 0.0,
        "greater_percentage": 0.0,
        "mean": 2.0
      },
      "trend_points": [],
      "values": [
        {
          "reading_id": -1,
          "name": "QTC",
          "measurement_name": "QTC",
          "measurement_name_to_display": "QTC",
          "measurement_unit": "ms",
          "measurement_value": 2.0,
          "service_name": "Blood pressure monitor",
          "service_name_to_display": "Blood pressure monitor",
          "status": "Normal",
          "status_to_display": "Normal",
          "value": 2.0,
          "min_value": 2.0,
          "max_value": 2.0,
          "date_added": 1628553600,
          "QTC": 2.0
        }
      ],
      "measurement_name": "QTC",
      "measurement_name_to_display": "QTC",
      "measurement_unit": "ms",
      "vital_name": "Blood pressure monitor"
    },
    "QRS": {
      "ranges": {
        "measurement_name_to_display": "QRS",
        "normal_low_value": 79.0,
        "normal_high_value": 111.0,
        "warn_low_value": 69.0,
        "warn_low_end_value": -999999,
        "warn_high_value": 121.0,
        "warn_high_end_value": 999999,
        "alert_low_value": 59.0,
        "alert_low_end_value": -999999,
        "alert_high_value": 150.0,
        "alert_high_end_value": 999999,
        "name": "QRS"
      },
      "stats": {
        "min": 75.0,
        "max": 75.0,
        "lower_percentage": 100.0,
        "greater_percentage": 0.0,
        "mean": 75.0
      },
      "trend_points": [],
      "values": [
        {
          "reading_id": -1,
          "name": "QRS",
          "measurement_name": "QRS",
          "measurement_name_to_display": "QRS",
          "measurement_unit": "ms",
          "measurement_value": 75.0,
          "service_name": "Blood pressure monitor",
          "service_name_to_display": "Blood pressure monitor",
          "status": "Normal",
          "status_to_display": "Normal",
          "value": 75.0,
          "min_value": 75.0,
          "max_value": 75.0,
          "date_added": 1628553600,
          "QRS": 75.0
        }
      ],
      "measurement_name": "QRS",
      "measurement_name_to_display": "QRS",
      "measurement_unit": "ms",
      "vital_name": "Blood pressure monitor"
    },
    "HR": {
      "ranges": {
        "measurement_name_to_display": "HR",
        "normal_low_value": 65.0,
        "normal_high_value": 90.0,
        "warn_low_value": 60.0,
        "warn_low_end_value": -999999,
        "warn_high_value": 95.0,
        "warn_high_end_value": 999999,
        "alert_low_value": 59.0,
        "alert_low_end_value": -999999,
        "alert_high_value": 101.0,
        "alert_high_end_value": 999999,
        "name": "HR"
      },
      "stats": {
        "min": 47.0,
        "max": 316.0,
        "lower_percentage": 0.0,
        "greater_percentage": 0.0,
        "mean": 79.41
      },
      "trend_points": [
        {
          "start_point": {
            "date": 1628035200000,
            "value": 75.63249999999607,
            "HR": 75.63249999999607
          },
          "name": "HR"
        },
        {
          "end_point": {
            "date": 1628553600000,
            "value": 83.1924999999901,
            "HR": 83.1924999999901
          },
          "name": "HR"
        }
      ],
      "values": [
        {
          "reading_id": -1,
          "name": "HR",
          "measurement_name": "HR",
          "measurement_name_to_display": "HR",
          "measurement_unit": "bpm",
          "measurement_value": 72.98,
          "service_name": "Blood pressure monitor",
          "service_name_to_display": "Blood pressure monitor",
          "status": "Normal",
          "status_to_display": "Normal",
          "value": 72.98,
          "min_value": 47.0,
          "max_value": 85.0,
          "date_added": 1628035200,
          "HR": 72.98
        },
        {
          "reading_id": -1,
          "name": "HR",
          "measurement_name": "HR",
          "measurement_name_to_display": "HR",
          "measurement_unit": "bpm",
          "measurement_value": 79.94,
          "service_name": "Blood pressure monitor",
          "service_name_to_display": "Blood pressure monitor",
          "status": "Normal",
          "status_to_display": "Normal",
          "value": 79.94,
          "min_value": 79.0,
          "max_value": 80.0,
          "date_added": 1628121600,
          "HR": 79.94
        },
        {
          "reading_id": -1,
          "name": "HR",
          "measurement_name": "HR",
          "measurement_name_to_display": "HR",
          "measurement_unit": "bpm",
          "measurement_value": 82.61,
          "service_name": "Blood pressure monitor",
          "service_name_to_display": "Blood pressure monitor",
          "status": "Normal",
          "status_to_display": "Normal",
          "value": 82.61,
          "min_value": 79.0,
          "max_value": 283.0,
          "date_added": 1628467200,
          "HR": 82.61
        },
        {
          "reading_id": -1,
          "name": "HR",
          "measurement_name": "HR",
          "measurement_name_to_display": "HR",
          "measurement_unit": "bpm",
          "measurement_value": 82.12,
          "service_name": "Blood pressure monitor",
          "service_name_to_display": "Blood pressure monitor",
          "status": "Normal",
          "status_to_display": "Normal",
          "value": 82.12,
          "min_value": 79.0,
          "max_value": 316.0,
          "date_added": 1628553600,
          "HR": 82.12
        }
      ],
      "measurement_name": "HR",
      "measurement_name_to_display": "HR",
      "measurement_unit": "bpm",
      "vital_name": "Blood pressure monitor"
    },
    "Diastolic": {
      "ranges": {
        "measurement_name_to_display": "Diastolic",
        "normal_low_value": 70.0,
        "normal_high_value": 100.0,
        "warn_low_value": 46.0,
        "warn_low_end_value": 69.0,
        "warn_high_value": 101.0,
        "warn_high_end_value": 110.0,
        "alert_low_value": 0.0,
        "alert_low_end_value": 45.0,
        "alert_high_value": 110.0,
        "alert_high_end_value": 999.0,
        "name": "Diastolic"
      },
      "stats": {
        "min": 61.0,
        "max": 105.0,
        "lower_percentage": 12.5,
        "greater_percentage": 12.5,
        "mean": 80.05
      },
      "trend_points": [
        {
          "start_point": {
            "date": 1625961600000,
            "value": 81.19248933901827,
            "Diastolic": 81.19248933901827
          },
          "name": "Diastolic"
        },
        {
          "end_point": {
            "date": 1628553600000,
            "value": 77.75752132196158,
            "Diastolic": 77.75752132196158
          },
          "name": "Diastolic"
        }
      ],
      "values": [
        {
          "reading_id": -1,
          "name": "Diastolic",
          "measurement_name": "Diastolic",
          "measurement_name_to_display": "Diastolic",
          "measurement_unit": "mmHg",
          "measurement_value": 74.0,
          "service_name": "Blood pressure monitor",
          "service_name_to_display": "Blood pressure monitor",
          "status": "Normal",
          "status_to_display": "Normal",
          "value": 74.0,
          "min_value": 74.0,
          "max_value": 74.0,
          "date_added": 1625961600,
          "Diastolic": 74.0
        },
        {
          "reading_id": -1,
          "name": "Diastolic",
          "measurement_name": "Diastolic",
          "measurement_name_to_display": "Diastolic",
          "measurement_unit": "mmHg",
          "measurement_value": 73.0,
          "service_name": "Blood pressure monitor",
          "service_name_to_display": "Blood pressure monitor",
          "status": "Normal",
          "status_to_display": "Normal",
          "value": 73.0,
          "min_value": 73.0,
          "max_value": 73.0,
          "date_added": 1626048000,
          "Diastolic": 73.0
        },
        {
          "reading_id": -1,
          "name": "Diastolic",
          "measurement_name": "Diastolic",
          "measurement_name_to_display": "Diastolic",
          "measurement_unit": "mmHg",
          "measurement_value": 75.0,
          "service_name": "Blood pressure monitor",
          "service_name_to_display": "Blood pressure monitor",
          "status": "Normal",
          "status_to_display": "Normal",
          "value": 75.0,
          "min_value": 75.0,
          "max_value": 75.0,
          "date_added": 1626134400,
          "Diastolic": 75.0
        },
        {
          "reading_id": -1,
          "name": "Diastolic",
          "measurement_name": "Diastolic",
          "measurement_name_to_display": "Diastolic",
          "measurement_unit": "mmHg",
          "measurement_value": 76.0,
          "service_name": "Blood pressure monitor",
          "service_name_to_display": "Blood pressure monitor",
          "status": "Normal",
          "status_to_display": "Normal",
          "value": 76.0,
          "min_value": 76.0,
          "max_value": 76.0,
          "date_added": 1626220800,
          "Diastolic": 76.0
        },
        {
          "reading_id": -1,
          "name": "Diastolic",
          "measurement_name": "Diastolic",
          "measurement_name_to_display": "Diastolic",
          "measurement_unit": "mmHg",
          "measurement_value": 105.0,
          "service_name": "Blood pressure monitor",
          "service_name_to_display": "Blood pressure monitor",
          "status": "Normal",
          "status_to_display": "Normal",
          "value": 105.0,
          "min_value": 105.0,
          "max_value": 105.0,
          "date_added": 1626307200,
          "Diastolic": 105.0
        },
        {
          "reading_id": -1,
          "name": "Diastolic",
          "measurement_name": "Diastolic",
          "measurement_name_to_display": "Diastolic",
          "measurement_unit": "mmHg",
          "measurement_value": 88.0,
          "service_name": "Blood pressure monitor",
          "service_name_to_display": "Blood pressure monitor",
          "status": "Normal",
          "status_to_display": "Normal",
          "value": 88.0,
          "min_value": 88.0,
          "max_value": 88.0,
          "date_added": 1627516800,
          "Diastolic": 88.0
        },
        {
          "reading_id": -1,
          "name": "Diastolic",
          "measurement_name": "Diastolic",
          "measurement_name_to_display": "Diastolic",
          "measurement_unit": "mmHg",
          "measurement_value": 80.0,
          "service_name": "Blood pressure monitor",
          "service_name_to_display": "Blood pressure monitor",
          "status": "Normal",
          "status_to_display": "Normal",
          "value": 80.0,
          "min_value": 80.0,
          "max_value": 80.0,
          "date_added": 1627862400,
          "Diastolic": 80.0
        },
        {
          "reading_id": -1,
          "name": "Diastolic",
          "measurement_name": "Diastolic",
          "measurement_name_to_display": "Diastolic",
          "measurement_unit": "mmHg",
          "measurement_value": 69.38,
          "service_name": "Blood pressure monitor",
          "service_name_to_display": "Blood pressure monitor",
          "status": "Normal",
          "status_to_display": "Normal",
          "value": 69.38,
          "min_value": 61.0,
          "max_value": 79.0,
          "date_added": 1628553600,
          "Diastolic": 69.38
        }
      ],
      "measurement_name": "Diastolic",
      "measurement_name_to_display": "Diastolic",
      "measurement_unit": "mmHg",
      "vital_name": "Blood pressure monitor"
    },
    "Systolic": {
      "ranges": {
        "measurement_name_to_display": "Systolic",
        "normal_low_value": 90.0,
        "normal_high_value": 120.0,
        "warn_low_value": 71.0,
        "warn_low_end_value": 89.0,
        "warn_high_value": 121.0,
        "warn_high_end_value": 149.0,
        "alert_low_value": 0.0,
        "alert_low_end_value": 70.0,
        "alert_high_value": 150.0,
        "alert_high_end_value": 999.0,
        "name": "Systolic"
      },
      "stats": {
        "min": 104.0,
        "max": 155.0,
        "lower_percentage": 0.0,
        "greater_percentage": 75.0,
        "mean": 129.36
      },
      "trend_points": [
        {
          "start_point": {
            "date": 1625961600000,
            "value": 136.8695948827244,
            "Systolic": 136.8695948827244
          },
          "name": "Systolic"
        },
        {
          "end_point": {
            "date": 1628553600000,
            "value": 114.34081023454019,
            "Systolic": 114.34081023454019
          },
          "name": "Systolic"
        }
      ],
      "values": [
        {
          "reading_id": -1,
          "name": "Systolic",
          "measurement_name": "Systolic",
          "measurement_name_to_display": "Systolic",
          "measurement_unit": "mmHg",
          "measurement_value": 127.0,
          "service_name": "Blood pressure monitor",
          "service_name_to_display": "Blood pressure monitor",
          "status": "Normal",
          "status_to_display": "Normal",
          "value": 127.0,
          "min_value": 127.0,
          "max_value": 127.0,
          "date_added": 1625961600,
          "Systolic": 127.0
        },
        {
          "reading_id": -1,
          "name": "Systolic",
          "measurement_name": "Systolic",
          "measurement_name_to_display": "Systolic",
          "measurement_unit": "mmHg",
          "measurement_value": 129.0,
          "service_name": "Blood pressure monitor",
          "service_name_to_display": "Blood pressure monitor",
          "status": "Normal",
          "status_to_display": "Normal",
          "value": 129.0,
          "min_value": 129.0,
          "max_value": 129.0,
          "date_added": 1626048000,
          "Systolic": 129.0
        },
        {
          "reading_id": -1,
          "name": "Systolic",
          "measurement_name": "Systolic",
          "measurement_name_to_display": "Systolic",
          "measurement_unit": "mmHg",
          "measurement_value": 131.0,
          "service_name": "Blood pressure monitor",
          "service_name_to_display": "Blood pressure monitor",
          "status": "Normal",
          "status_to_display": "Normal",
          "value": 131.0,
          "min_value": 131.0,
          "max_value": 131.0,
          "date_added": 1626134400,
          "Systolic": 131.0
        },
        {
          "reading_id": -1,
          "name": "Systolic",
          "measurement_name": "Systolic",
          "measurement_name_to_display": "Systolic",
          "measurement_unit": "mmHg",
          "measurement_value": 135.0,
          "service_name": "Blood pressure monitor",
          "service_name_to_display": "Blood pressure monitor",
          "status": "Normal",
          "status_to_display": "Normal",
          "value": 135.0,
          "min_value": 135.0,
          "max_value": 135.0,
          "date_added": 1626220800,
          "Systolic": 135.0
        },
        {
          "reading_id": -1,
          "name": "Systolic",
          "measurement_name": "Systolic",
          "measurement_name_to_display": "Systolic",
          "measurement_unit": "mmHg",
          "measurement_value": 155.0,
          "service_name": "Blood pressure monitor",
          "service_name_to_display": "Blood pressure monitor",
          "status": "Normal",
          "status_to_display": "Normal",
          "value": 155.0,
          "min_value": 155.0,
          "max_value": 155.0,
          "date_added": 1626307200,
          "Systolic": 155.0
        },
        {
          "reading_id": -1,
          "name": "Systolic",
          "measurement_name": "Systolic",
          "measurement_name_to_display": "Systolic",
          "measurement_unit": "mmHg",
          "measurement_value": 129.0,
          "service_name": "Blood pressure monitor",
          "service_name_to_display": "Blood pressure monitor",
          "status": "Normal",
          "status_to_display": "Normal",
          "value": 129.0,
          "min_value": 129.0,
          "max_value": 129.0,
          "date_added": 1627516800,
          "Systolic": 129.0
        },
        {
          "reading_id": -1,
          "name": "Systolic",
          "measurement_name": "Systolic",
          "measurement_name_to_display": "Systolic",
          "measurement_unit": "mmHg",
          "measurement_value": 120.0,
          "service_name": "Blood pressure monitor",
          "service_name_to_display": "Blood pressure monitor",
          "status": "Normal",
          "status_to_display": "Normal",
          "value": 120.0,
          "min_value": 120.0,
          "max_value": 120.0,
          "date_added": 1627862400,
          "Systolic": 120.0
        },
        {
          "reading_id": -1,
          "name": "Systolic",
          "measurement_name": "Systolic",
          "measurement_name_to_display": "Systolic",
          "measurement_unit": "mmHg",
          "measurement_value": 108.88,
          "service_name": "Blood pressure monitor",
          "service_name_to_display": "Blood pressure monitor",
          "status": "Normal",
          "status_to_display": "Normal",
          "value": 108.88,
          "min_value": 104.0,
          "max_value": 118.0,
          "date_added": 1628553600,
          "Systolic": 108.88
        }
      ],
      "measurement_name": "Systolic",
      "measurement_name_to_display": "Systolic",
      "measurement_unit": "mmHg",
      "vital_name": "Blood pressure monitor"
    }
  },
  "Status": "Ok",
  "Time": 1642045245.169,
  "Duration": 0.246
};
