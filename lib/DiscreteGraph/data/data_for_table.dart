var erroneousData = {
  'vital_graph_data': [
    {
      'title': ' Pulse & Saturation',
      'time_start': 1637648835,
      'time_end': 1611556790,
      'vitals': [
        {
          'name': 'Pulse',
          'ranges': [64.0, 61.0, 65.0, 90.0, 95.0, 100.0],
          'trend_line': null,
          'stats': {'min': 54.0, 'max': 114.0, 'mean': 76.33},
          'data': [
            [1637648835, 85.0, null, null],
            [1622204535, 114.0, null, null],
            [1622204493, 54.0, null, null],
            [1611559140, 70.0, null, null],
            [1611557645, 69.0, null, null],
            [1611556790, 71.0, null, null]
          ]
        }
      ]
    },
    {
      'title': 'Pulse & Saturation',
      'time_start': 1622204535,
      'time_end': 1611556790,
      'vitals': [
        {
          'name': 'Pulse',
          'ranges': [64.0, 61.0, 65.0, 90.0, 95.0, 100.0],
          'trend_line': null,
          'stats': {'min': 54.0, 'max': 114.0, 'mean': 76.33},
          'data': [
            [1637648835, 80.0, null, null],
            [1622204535, 114.0, null, null],
            [1622204493, 54.0, null, null],
            [1611559140, 70.0, null, null],
            [1611557645, 69.0, null, null],
            [1611556790, 71.0, null, null]
          ]
        }
      ]
    },
    {
      'title': 'Pulse & Saturation',
      'time_start': 1622204493,
      'time_end': 1611556790,
      'vitals': [
        {
          'name': 'Pulse',
          'ranges': [64.0, 61.0, 65.0, 90.0, 95.0, 100.0],
          'trend_line': null,
          'stats': {'min': 54.0, 'max': 114.0, 'mean': 7}
        }
      ]
    }
  ]
};
