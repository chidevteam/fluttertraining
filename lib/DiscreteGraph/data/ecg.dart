Map<String, dynamic> ecgData = {
  "data": {
    "HR": {
      "ranges": {
        "measurement_name_to_display": "HR",
        "normal_low_value": 65,
        "normal_high_value": 90,
        "warn_low_value": 60,
        "warn_low_end_value": -999999,
        "warn_high_value": 95,
        "warn_high_end_value": 999999,
        "alert_low_value": 59,
        "alert_low_end_value": -999999,
        "alert_high_value": 101,
        "alert_high_end_value": 999999,
        "name": "HR"
      },
      "stats": {
        "min": 28,
        "max": 28,
        "lower_percentage": 100,
        "greater_percentage": 0,
        "mean": 28
      },
      "trend_points": [],
      "values": [
        {
          "reading_id": -1,
          "name": "HR",
          "measurement_name": "HR",
          "measurement_name_to_display": "HR",
          "measurement_unit": "bpm",
          "measurement_value": 28,
          "service_name": "ECG",
          "service_name_to_display": "ECG",
          "status": "Normal",
          "status_to_display": "Normal",
          "value": 28,
          "min_value": 28,
          "max_value": 28,
          "date_added": 1613347200,
          "HR": 28
        }
      ],
      "measurement_name": "HR",
      "measurement_name_to_display": "HR",
      "measurement_unit": "bpm",
      "vital_name": "ECG"
    }
  },
  "Status": "Ok",
  "Time": 1637919665.612,
  "Duration": 0.098
};
