Map<String, dynamic> prSpO2Data = {
  "data": {
    "Pulse": {
      "ranges": {
        "measurement_name_to_display": "Pulse",
        "normal_low_value": 61,
        "normal_high_value": 90,
        "warn_low_value": 51,
        "warn_low_end_value": 60,
        "warn_high_value": 91,
        "warn_high_end_value": 110,
        "alert_low_value": 0,
        "alert_low_end_value": 50,
        "alert_high_value": 111,
        "alert_high_end_value": 999,
        "name": "Pulse"
      },
      "stats": {
        "min": 76,
        "max": 89,
        "lower_percentage": 0,
        "greater_percentage": 0,
        "mean": 85.08
      },
      "trend_points": [
        {
          "start_point": {
            "date": 1610928000000,
            "value": 80.40131578946966,
            "Pulse": 80.40131578946966
          },
          "name": "Pulse"
        },
        {
          "end_point": {
            "date": 1611792000000,
            "value": 90.43421052631675,
            "Pulse": 90.43421052631675
          },
          "name": "Pulse"
        }
      ],
      "values": [
        {
          "reading_id": -1,
          "name": "Pulse",
          "measurement_name": "Pulse",
          "measurement_name_to_display": "Pulse",
          "measurement_unit": "bpm",
          "measurement_value": 78.25,
          "service_name": "Pulse & Saturation",
          "service_name_to_display": "Pulse & Saturation",
          "status": "Normal",
          "status_to_display": "Normal",
          "value": 78.25,
          "min_value": 76,
          "max_value": 82,
          "date_added": 1610928000,
          "Pulse": 78.25
        },
        {
          "reading_id": -1,
          "name": "Pulse",
          "measurement_name": "Pulse",
          "measurement_name_to_display": "Pulse",
          "measurement_unit": "bpm",
          "measurement_value": 88,
          "service_name": "Pulse & Saturation",
          "service_name_to_display": "Pulse & Saturation",
          "status": "Normal",
          "status_to_display": "Normal",
          "value": 88,
          "min_value": 88,
          "max_value": 88,
          "date_added": 1611273600,
          "Pulse": 88
        },
        {
          "reading_id": -1,
          "name": "Pulse",
          "measurement_name": "Pulse",
          "measurement_name_to_display": "Pulse",
          "measurement_unit": "bpm",
          "measurement_value": 89,
          "service_name": "Pulse & Saturation",
          "service_name_to_display": "Pulse & Saturation",
          "status": "Normal",
          "status_to_display": "Normal",
          "value": 89,
          "min_value": 89,
          "max_value": 89,
          "date_added": 1611792000,
          "Pulse": 89
        }
      ],
      "measurement_name": "Pulse",
      "measurement_name_to_display": "Pulse",
      "measurement_unit": "bpm",
      "vital_name": "Pulse & Saturation"
    },
    "SpO2": {
      "ranges": {
        "measurement_name_to_display": "SpO2",
        "normal_low_value": 90,
        "normal_high_value": 100,
        "warn_low_value": 71,
        "warn_low_end_value": 89,
        "warn_high_value": 101,
        "warn_high_end_value": 1000,
        "alert_low_value": 0,
        "alert_low_end_value": 70,
        "alert_high_value": 1001,
        "alert_high_end_value": 10000,
        "name": "SpO2"
      },
      "stats": {
        "min": 96,
        "max": 97,
        "lower_percentage": 0,
        "greater_percentage": 0,
        "mean": 96.5
      },
      "trend_points": [
        {
          "start_point": {
            "date": 1610928000000,
            "value": 96.22368421052613,
            "SpO2": 96.22368421052613
          },
          "name": "SpO2"
        },
        {
          "end_point": {
            "date": 1611792000000,
            "value": 96.81578947368473,
            "SpO2": 96.81578947368473
          },
          "name": "SpO2"
        }
      ],
      "values": [
        {
          "reading_id": -1,
          "name": "SpO2",
          "measurement_name": "SpO2",
          "measurement_name_to_display": "SpO2",
          "measurement_unit": "%",
          "measurement_value": 96.5,
          "service_name": "Pulse & Saturation",
          "service_name_to_display": "Pulse & Saturation",
          "status": "Normal",
          "status_to_display": "Normal",
          "value": 96.5,
          "min_value": 96,
          "max_value": 97,
          "date_added": 1610928000,
          "SpO2": 96.5
        },
        {
          "reading_id": -1,
          "name": "SpO2",
          "measurement_name": "SpO2",
          "measurement_name_to_display": "SpO2",
          "measurement_unit": "%",
          "measurement_value": 96,
          "service_name": "Pulse & Saturation",
          "service_name_to_display": "Pulse & Saturation",
          "status": "Normal",
          "status_to_display": "Normal",
          "value": 96,
          "min_value": 96,
          "max_value": 96,
          "date_added": 1611273600,
          "SpO2": 96
        },
        {
          "reading_id": -1,
          "name": "SpO2",
          "measurement_name": "SpO2",
          "measurement_name_to_display": "SpO2",
          "measurement_unit": "%",
          "measurement_value": 97,
          "service_name": "Pulse & Saturation",
          "service_name_to_display": "Pulse & Saturation",
          "status": "Normal",
          "status_to_display": "Normal",
          "value": 97,
          "min_value": 97,
          "max_value": 97,
          "date_added": 1611792000,
          "SpO2": 97
        }
      ],
      "measurement_name": "SpO2",
      "measurement_name_to_display": "SpO2",
      "measurement_unit": "%",
      "vital_name": "Pulse & Saturation"
    }
  }
};
