Map<String, dynamic> temperatureData = {
  "data": {
    "TEMP": {
      "ranges": {
        "measurement_name_to_display": "Temperature",
        "normal_low_value": 35.5,
        "normal_high_value": 38,
        "warn_low_value": 35,
        "warn_low_end_value": 35.4,
        "warn_high_value": 39,
        "warn_high_end_value": 40.9,
        "alert_low_value": 34,
        "alert_low_end_value": 34.9,
        "alert_high_value": 41,
        "alert_high_end_value": 999,
        "name": "TEMP"
      },
      "stats": {
        "min": 37,
        "max": 1000,
        "lower_percentage": 0,
        "greater_percentage": 66.67,
        "mean": 64.72
      },
      "trend_points": [
        {
          "start_point": {
            "date": 1612310400000,
            "value": 109.76670542638749,
            "TEMP": 109.76670542638749
          },
          "name": "TEMP"
        },
        {
          "end_point": {
            "date": 1613433600000,
            "value": 26.11472868216515,
            "TEMP": 26.11472868216515
          },
          "name": "TEMP"
        }
      ],
      "values": [
        {
          "reading_id": -1,
          "name": "TEMP",
          "measurement_name": "Temperature",
          "measurement_name_to_display": "Temperature",
          "measurement_unit": "C",
          "measurement_value": 117.57,
          "service_name": "Temperature",
          "service_name_to_display": "Temperature",
          "status": "Normal",
          "status_to_display": "Normal",
          "value": 117.57,
          "min_value": 37,
          "max_value": 1000,
          "date_added": 1612310400,
          "TEMP": 117.57
        },
        {
          "reading_id": -1,
          "name": "TEMP",
          "measurement_name": "Temperature",
          "measurement_name_to_display": "Temperature",
          "measurement_unit": "C",
          "measurement_value": 38,
          "service_name": "Temperature",
          "service_name_to_display": "Temperature",
          "status": "Normal",
          "status_to_display": "Normal",
          "value": 38,
          "min_value": 38,
          "max_value": 38,
          "date_added": 1613001600,
          "TEMP": 38
        },
        {
          "reading_id": -1,
          "name": "TEMP",
          "measurement_name": "Temperature",
          "measurement_name_to_display": "Temperature",
          "measurement_unit": "C",
          "measurement_value": 38.6,
          "service_name": "Temperature",
          "service_name_to_display": "Temperature",
          "status": "Normal",
          "status_to_display": "Normal",
          "value": 38.6,
          "min_value": 38.6,
          "max_value": 38.6,
          "date_added": 1613433600,
          "TEMP": 38.6
        }
      ],
      "measurement_name": "Temperature",
      "measurement_name_to_display": "Temperature",
      "measurement_unit": "C",
      "vital_name": "Temperature"
    }
  }
};
