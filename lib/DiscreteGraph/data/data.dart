Map dataBP = {
  "vital_graph_data": [
    {
      "title": "Blood Pressure",
      "time_start": 1611792000,
      "time_end": 1613606400,
      "vitals": [
        {
          "name": "Systolic",
          "ranges": [89, 71, 90, 120, 121, 149],
          "trend_line": {
            "t1": 1611792000,
            "v1": 157.62916030534598,
            "t2": 1613606400,
            "v2": 148.8897137404556
          },
          "stats": {"min": 70, "max": 200, "mean": 153.56},
          "data": [
            [1611792000, 119, 119, 119],
            [1612137600, 162.5, 140, 170],
            [1612224000, 170.71, 170, 175],
            [1612310400, 188.5, 177, 200],
            [1612396800, 173.5, 171, 176],
            [1612828800, 170.5, 170, 171],
            [1613001600, 89.33, 70, 120],
            [1613433600, 135, 135, 135],
            [1613606400, 173, 171, 175]
          ]
        },
        {
          "name": "Diastolic",
          "ranges": [69, 46, 70, 100, 101, 110],
          "trend_line": {
            "t1": 1611792000,
            "v1": 110.68262813521142,
            "t2": 1613606400,
            "v2": 151.77255179934582
          },
          "stats": {"min": 70, "max": 627, "mean": 129.81},
          "data": [
            [1611792000, 72, 72, 72],
            [1612137600, 112.5, 90, 120],
            [1612224000, 120, 120, 120],
            [1612310400, 138.5, 127, 150],
            [1612396800, 123.5, 121, 126],
            [1612828800, 120.5, 120, 121],
            [1613001600, 172.33, 70, 227],
            [1613433600, 86, 86, 86],
            [1613606400, 123, 121, 125]
          ]
        }
      ]
    }
  ]
};
