import 'dart:io';
import 'bp.dart';
import 'dart:convert';

void main() {
  var rangesSystolic = bpData['data']['Systolic']['ranges'];
  var rangesDiastolic = bpData['data']['Diastolic']['ranges'];
  var statsSystolic = bpData['data']['Systolic']['stats'];
  var statsDiastolic = bpData['data']['Diastolic']['stats'];
  var trendPointsSystolic = bpData['data']['Systolic']['trend_points'];
  var trendPointsDiastolic = bpData['data']['Diastolic']['trend_points'];
  var valuesSystolic = bpData['data']['Systolic']['values'];
  var valuesDiastolic = bpData['data']['Diastolic']['values'];

  final data = {};
  data['vital_graph_data'] = [
    {
      'title': bpData['data']['Systolic']['vital_name'],
      'time_start': valuesSystolic[0]['date_added'],
      'time_end': valuesSystolic[valuesSystolic.length - 1]['date_added'],
      'vitals': [
        {
          'name': rangesSystolic['name'],
          'ranges': [
            rangesSystolic['warn_low_end_value'],
            rangesSystolic['warn_low_value'],
            rangesSystolic['normal_low_value'],
            rangesSystolic['normal_high_value'],
            rangesSystolic['warn_high_value'],
            rangesSystolic['warn_high_end_value']
          ],
          'trend_line': {
            't1': trendPointsSystolic[0]['start_point']['date'],
            'v1': trendPointsSystolic[0]['start_point']['value'],
            't2': trendPointsSystolic[1]['end_point']['date'],
            'v2': trendPointsSystolic[1]['end_point']['value'],
          },
          'stats': {
            'min': statsSystolic['min'],
            'max': statsSystolic['max'],
            'mean': statsSystolic['mean']
          },
          'data': bpValues(valuesSystolic),
        },
        {
          'name': rangesDiastolic['name'],
          'ranges': [
            rangesDiastolic['warn_low_end_value'],
            rangesDiastolic['warn_low_value'],
            rangesDiastolic['normal_low_value'],
            rangesDiastolic['normal_high_value'],
            rangesDiastolic['warn_high_value'],
            rangesDiastolic['warn_high_end_value']
          ],
          'trend_line': {
            't1': trendPointsDiastolic[0]['start_point']['date'],
            'v1': trendPointsDiastolic[0]['start_point']['value'],
            't2': trendPointsDiastolic[1]['end_point']['date'],
            'v2': trendPointsDiastolic[1]['end_point']['value'],
          },
          'stats': {
            'min': statsDiastolic['min'],
            'max': statsDiastolic['max'],
            'mean': statsDiastolic['mean']
          },
          'data': bpValues(valuesDiastolic),
        }
      ],
    }
  ];
  print(json.encode(data));
  writeToFile(data);
}

void writeToFile(data) async {
  JsonEncoder encoder = new JsonEncoder.withIndent('  ');
  String prettyPrintData = encoder.convert(data);
  final filename = 'data.json';
  await File(filename).writeAsString(prettyPrintData);
}

List bpValues(List values) {
  var data = [];
  for (var i = 0; i < values.length; i++) {
    data.add([
      values[i]['date_added'],
      values[i]['value'],
      values[i]['min_value'],
      values[i]['max_value']
    ]);
  }
  return data;
}
