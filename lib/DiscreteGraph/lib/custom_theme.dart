import 'package:flutter/material.dart';

class CustomTheme {
  customLightTheme() {
    return ThemeData(
      //Defines a single color as well a color swatch with ten shades of the color.
      primarySwatch: Colors.blue,
      primaryColor: Colors.blue,
      colorScheme: ColorScheme.fromSwatch(primarySwatch: Colors.blue)
          .copyWith(secondary: Colors.blueAccent),
      backgroundColor: Colors.white,
      scaffoldBackgroundColor: Colors.white,
      dividerColor: Colors.blue,
      brightness: Brightness.light,
      textTheme: Typography.blackCupertino,
    );
  }

  customDarkTheme() {
    return ThemeData(
      primarySwatch: Colors.grey,
      colorScheme: ColorScheme.fromSwatch(primarySwatch: Colors.grey)
          .copyWith(secondary: Colors.grey.shade900),
      scaffoldBackgroundColor: Colors.grey.shade700,
      backgroundColor: Color(0xFF141414),
      dividerColor: Colors.grey.shade800,
      brightness: Brightness.light,
      textTheme: Typography.whiteCupertino,
    );
  }

  List graphStrokes() {
    List colors = [
      Colors.red,
      Colors.blue,
      Colors.green,
      Colors.purple,
      Colors.yellow,
      Color(0xFF141414),
    ];

    return colors;
  }
}
