import "CHITime.dart";

class GridDesignTime {
  double tmin;
  double tmax;
  final bool scale_to_edge;

  late double gridDelta;
  late double gridTmin;
  late double gridTmax;
  late double fmt1;
  late double fmt2;
  late int dateFormat = 1;
  late int numLinesDate = 2;

  final double s = CHITime.s;
  final double m = CHITime.m;
  final double h = CHITime.h;
  final double D = CHITime.D;
  final double M = CHITime.M;
  final double Y = CHITime.Y;

  late String deltaString;
  late String gridDeltaString;

  GridDesignTime(this.tmin, this.tmax, this.scale_to_edge) {
    gridDesignTime();
  }

  void gridDesignTime() {
    double delta = tmax - tmin;
    dateFormat = 1;
    if (delta <= 128 * Y) gridDelta = 15 * Y;
    if (delta <= 64 * Y) gridDelta = 10 * Y;
    if (delta <= 32 * Y) gridDelta = 5 * Y;
    if (delta <= 16 * Y) gridDelta = 2 * Y;
    if (delta <= 8 * Y) gridDelta = 1 * Y;
    if (delta <= 4 * Y) gridDelta = 6 * M;
    if (delta <= 2 * Y) gridDelta = 3 * M;
    if (delta <= 16 * M) gridDelta = 2 * M;
    if (delta <= 8 * M) gridDelta = 1 * M;
    if (delta <= 4 * M) gridDelta = 14 * D;
    if (delta <= 2 * M) gridDelta = 7 * D;
    if (delta <= 1 * M) gridDelta = 4 * D;
    if (delta <= 16 * D) gridDelta = 2 * D;
    if (delta <= 8 * D) gridDelta = 1 * D;
    if (delta <= 4 * D) gridDelta = 12 * h;
    if (delta <= 2 * D) gridDelta = 8 * h;
    if (delta <= 1 * D) gridDelta = 3 * h;
    if (delta <= 16 * h) gridDelta = 2 * h;
    if (delta <= 8 * h) gridDelta = 1 * h;
    if (delta <= 4 * h) gridDelta = 30 * m;
    if (delta <= 3 * h) gridDelta = 20 * m;
    if (delta <= 90 * m) gridDelta = 10 * m;
    if (delta <= 45 * m) gridDelta = 5 * m;
    if (delta <= 20 * m) gridDelta = 2 * m;
    if (delta <= 10 * m) gridDelta = 1 * m;

    double tOffset = 0 * h;

    if (scale_to_edge == true) {
      gridTmin =
          ((tmin + tOffset) / gridDelta).ceilToDouble() * gridDelta - tOffset;
      gridTmax =
          ((tmax + tOffset) / gridDelta).floorToDouble() * gridDelta - tOffset;
    } else {
      gridTmin =
          ((tmin + tOffset) / gridDelta).floorToDouble() * gridDelta - tOffset;
      gridTmax =
          ((tmax + tOffset) / gridDelta).ceilToDouble() * gridDelta - tOffset;
      tmin = gridTmin;
      tmax = gridTmax;
    }

    deltaString = convertToHourMinSec(delta);
    gridDeltaString = convertToHourMinSec(gridDelta);

    updateFormatAndDecJanOffset();
  }

  void updateFormatAndDecJanOffset() {
    //some additional delta is added to accommodatea 365.25 etc.
    // otherwise we get december 31 also.
    if (gridDelta > 11 * Y) {
      // gridTmin += .75 * D;
      // gridTmax += .75 * D;
      dateFormat = 1;
      numLinesDate = 1;
    } else if (gridDelta > 1 * Y) {
      // gridTmin += .75 * D;
      // gridTmax += .75 * D;
      dateFormat = 2;
      numLinesDate = 1;
    } else if (gridDelta > 15 * D) {
      dateFormat = 3;
      numLinesDate = 2;
    } else if (gridDelta > 1 * D) {
      dateFormat = 4;
      numLinesDate = 2;
    } else if (gridDelta > 2 * h) {
      dateFormat = 5;
      numLinesDate = 2;
    } else if (gridDelta > 1 * h) {
      dateFormat = 5;
      numLinesDate = 2;
    } else {
      dateFormat = 5;
      numLinesDate = 2;
    }

    // print("----------------------------- $dateFormat");
  }

// for Debuggging: Just a convenience method to print the delta and gridDelta
  String convertToHourMinSec(double total_time) {
    // double s = CHITime.s;
    double m = CHITime.m;
    double h = CHITime.h;
    double D = CHITime.D;
    double M = CHITime.M;
    double Y = CHITime.Y;

    int years = total_time ~/ Y;
    total_time = total_time - years * Y;

    int months = total_time ~/ M;
    total_time = total_time - months * M;

    int days = total_time ~/ D;
    total_time = total_time - days * D;

    int hours = total_time ~/ h;
    total_time = total_time - hours * h;

    int minutes = total_time ~/ m;
    int seconds = (total_time - minutes * m).toInt();

    String YY = format2(years);
    String MM = format2(months);
    String DD = format2(days);
    String hh = format2(hours);
    String mm = format2(minutes);
    String ss = format2(seconds);

    String msg = '';
    if (YY != '00') msg += YY + ' years ';
    if (MM != '00') msg += MM + ' months ';
    if (DD != '00') msg += DD + ' days ';
    if (hh != '00') msg += hh + ' hours ';
    if (mm != '00') msg += mm + ' minutes ';
    if (ss != '00') msg += ss + ' seconds ';

    // if (MM + '/' + DD + ' ---' + hh + '/' + mm + '/' + ss;

    ///$months/$days $hours:$minutes:$secs';

    return msg;
  }
}
