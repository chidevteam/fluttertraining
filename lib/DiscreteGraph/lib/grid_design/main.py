import numpy as np
from chi.modules.reporting.libnew.widgets.CHIVitalChart.dart.grid_design_time import gridDesignTime2
from chi.modules.reporting.libnew.core.utils.CHITime import convertToHourMinSec, CHITime, date_utils

#python3 -m chi.modules.reporting.libnew.widgets.CHIVitalChart.dart.main

def main():
    s = CHITime.s
    m = CHITime.m
    h = CHITime.h
    D = CHITime.D
    M = CHITime.M
    Y = CHITime.Y
    tmin = 50 * Y-24*h #+ 6 * M + 15 * D + 16 * h + 3 * m
    duration = 64 * Y #+ 2 * M + 5 * D
    tmax = tmin + duration 
    d1 = date_utils.date_from_timestamp(tmin)
    d2 = date_utils.date_from_timestamp(tmax)
    print(d1, d2)

    scale_to_edge = True
    gridDelta, gridTmin, gridTmax, tmin, tmax, fmt1, fmt2  = gridDesignTime2(tmin, tmax, scale_to_edge)

    delta = tmax - tmin
    print("============================================")
    print("Start Time --- ", convertToHourMinSec(delta))
    print("Grid Delta --- ", convertToHourMinSec(gridDelta))
 
    
    for t in np.arange(gridTmin, gridTmax+1e-5, gridDelta):
        f0 = date_utils.date_from_timestamp(t)
        f1 = date_utils.date_from_timestamp(t, fmt1)#'%d %b') #'%d-%m-%Y %H:%M'
        f2 = date_utils.date_from_timestamp(t, fmt2)#'%Y') #'%d-%m-%Y %H:%M'
        print(f0, '---', f1, f2)

main()