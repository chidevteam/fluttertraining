from math import ceil, floor
import numpy as np
from chi.modules.reporting.libnew.core.utils.CHITime import CHITime

def gridDesignTime2(tmin, tmax, scale_to_edge):

    delta = tmax - tmin
    s = CHITime.s
    m = CHITime.m
    h = CHITime.h
    D = CHITime.D
    M = CHITime.M
    Y = CHITime.Y
    
    if delta <= 128*Y : fmt1 = "%d %b"   ; fmt2 = "%Y"       ; gridDelta =  15*Y
    if delta <=  64*Y : fmt1 = "%d %b"   ; fmt2 = "%Y"       ; gridDelta =   5*Y
    if delta <=  32*Y : fmt1 = "%d %b"   ; fmt2 = "%Y"       ; gridDelta =   5*Y
    if delta <=  16*Y : fmt1 = "%d %b"   ; fmt2 = "%Y"       ; gridDelta =   2*Y
    if delta <=   8*Y : fmt1 = "%d %b"   ; fmt2 = "%Y"       ; gridDelta =   1*Y # <- broke pattern for year
    if delta <=   4*Y : fmt1 = "%d %b"   ; fmt2 = "%Y"       ; gridDelta =   6*M
    if delta <=   2*Y : fmt1 = "%d %b"   ; fmt2 = "%Y"       ; gridDelta =   3*M
    if delta <=  16*M : fmt1 = "%d %b"   ; fmt2 = "%Y"       ; gridDelta =   2*M
    if delta <=   8*M : fmt1 = "%d %b"   ; fmt2 = "%Y"       ; gridDelta =   1*M # <- broke pattern for month
    if delta <=   4*M : fmt1 = "%A"      ; fmt2 = "%d %b %Y" ; gridDelta =  14*D
    if delta <=   2*M : fmt1 = "%A"      ; fmt2 = "%d %b %Y" ; gridDelta =   7*D # <- week
    if delta <=   1*M : fmt1 = "%d %b"   ; fmt2 = "%Y"       ; gridDelta =   4*D
    if delta <=  16*D : fmt1 = "%d %b"   ; fmt2 = "%Y"       ; gridDelta =   2*D
    if delta <=   8*D : fmt1 = "%d %b"   ; fmt2 = "%Y"       ; gridDelta =   1*D    
    if delta <=   4*D : fmt1 = "%I:%M %p"; fmt2 = "%d %b, %y"; gridDelta =  12*h
    if delta <=   2*D : fmt1 = "%I:%M %p"; fmt2 = "%d %b, %y"; gridDelta =   8*h
    if delta <=   1*D : fmt1 = "%I:%M %p"; fmt2 = "%d %b, %y"; gridDelta =   3*h   #[5,8] 
    if delta <=  16*h : fmt1 = "%I:%M %p"; fmt2 = "%d %b, %y"; gridDelta =   2*h   #[4,8] 
    if delta <=   8*h : fmt1 = "%I:%M %p"; fmt2 = "%d %b, %y"; gridDelta =   1*h   #[4,8] 
    if delta <=   4*h : fmt1 = "%I:%M %p"; fmt2 = "%d %b, %y"; gridDelta =  30*m   #[6,8] 
    if delta <=   3*h : fmt1 = "%I:%M %p"; fmt2 = "%d %b, %y"; gridDelta =  20*m   #[4,9] 
    if delta <=  90*m : fmt1 = "%I:%M %p"; fmt2 = "%d %b, %y"; gridDelta =  10*m   #[4,9] 
    if delta <=  45*m : fmt1 = "%I:%M %p"; fmt2 = "%d %b, %y"; gridDelta =   5*m   #[4,9] 
    if delta <=  20*m : fmt1 = "%I:%M %p"; fmt2 = "%d %b, %y"; gridDelta =   2*m   #[5,10]
    if delta <=  10*m : fmt1 = "%I:%M %p"; fmt2 = "%d %b, %y"; gridDelta =   1*m   #[  10] 

    if scale_to_edge == True:
        gridTmin = ceil(tmin/gridDelta)*gridDelta 
        gridTmax = floor(tmax/gridDelta)*gridDelta
    else:
        gridTmin = floor(tmin/gridDelta)*gridDelta   
        gridTmax = ceil(tmax/gridDelta)*gridDelta    
        tmin = gridTmin
        tmax = gridTmax

    return gridDelta, gridTmin, gridTmax, tmin, tmax, fmt1, fmt2


# class Vector {
#   int x;
#   int y;

#   static final String X = "x";
#   static final String Y = "y";
