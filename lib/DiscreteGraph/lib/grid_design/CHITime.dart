class CHITime {
  static final double s = 1.0;
  static final double m = 60 * s;
  static final double h = 60 * m;
  static final double D = 24 * h;
  static final double M = 30.4375 * D;
  static final double Y = 365.25 * D;
}

List<String> date_from_timestamp(double timestamp1, {int format = 0}) {
  // int format = 1;
  final DateTime date = DateTime.fromMillisecondsSinceEpoch(
      timestamp1.toInt() * 1000,
      isUtc: true);

  List<String> months = [
    'Jan',
    'Feb',
    'Mar',
    'Apr',
    'May',
    'Jun',
    'Jul',
    'Aug',
    'Sep',
    'Oct',
    'Nov',
    'Dec'
  ];

  String line1 = '';
  String line2 = '';
  String sDay = date.day.toString().padLeft(2, '0');
  String sMonth = months[date.month - 1];

  String ampm = "AM";
  int hour = date.hour;
  if (hour == 0) hour = 12;
  if (hour > 12) {
    hour -= 12;
    ampm = 'PM';
  }
  String sHour = hour.toString(); //.padLeft(2, '0');
  String sMinte = date.minute.toString().padLeft(2, '0');
  String sSecond = date.second.toString().padLeft(2, '0');

  line1 = '${sDay}/${sMonth}/${date.year}';
  line2 = '${sHour}:${sMinte}:${sSecond}';

  String time = '${sHour}:${sMinte}$ampm';
  String ymd = '${sDay}-${sMonth}-${date.year}';
  switch (format) {
    case 0: /* 3:00AM 20-Dec-2021 */
      line1 = time;
      line2 = ymd;
      break;
    case 1: /* 2021 */
      line1 = '****';
      line2 = '${date.year}';
      break;
    case 2: /* Jan 2021 */
      line1 = '****';
      line2 = '${sMonth} ${date.year}';
      break;
    case 3: /* 20 Jan    2021 */
      line1 = '${sMonth} ${sDay}';
      line2 = '${date.year}';
      break;
    case 4: /* 3:00AM 20-Dec-2021 */
      line1 = time;
      line2 = ymd;
      break;
    case 5: /* 3:00AM 20-Dec-2021 */
      line1 = time;
      line2 = ymd;
      break;
  }

  return [line1, line2];
  // print("Timestamp to DateTime : $date1");
  // print('just thw time: ${date1.hour}:${date1.minute}:${date1.second}');
}

// List<String> DateFormat() {}

// String f2(double v) {
//   // return "{:.3f}".format(v)
//   return (v).toStringAsFixed(2);
// }

String format2(int i) {
  String s = i.toString().padLeft(2, '0');
  return s;
}
