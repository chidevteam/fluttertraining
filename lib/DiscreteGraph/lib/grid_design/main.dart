import "CHITime.dart";
import "grid_design_time.dart";

void main() {
  // double s = CHITime.s;
  double m = CHITime.m;
  double h = CHITime.h;
  double D = CHITime.D;
  double M = CHITime.M;
  double Y = CHITime.Y;

  double tmin = 50 * Y + 11 * h + 33 * m;
  // double duration = 1 * Y; // #+ 2 * M + 5 * D
  // double tmax = tmin + duration;

  List<double> durArray = [
    20 * Y + 2 * M,
    5 * Y + 3 * M,
    2 * Y + 3 * M,
    1 * Y + 3 * M,
    8 * M + 2 * D,
    6 * M + 2 * D,
    2 * M + 2 * D,
    1 * M + 2 * D,
    20 * D,
    10 * D,
    5 * D,
    2 * D,
    1 * D,
    15 * h,
    5 * h,
    2 * h,
    1 * h,
    30 * m,
  ];

  for (var d in durArray) processTime(tmin, tmin + d);
}

void processTime(double tmin, double tmax) {
  print("==============================================");

  var t1 = date_from_timestamp(tmin);
  var t2 = date_from_timestamp(tmax);
  print("${t1[0]} ${t1[1]}, ${t2[0]} ${t2[1]}");

  bool scale_to_edge = true;
  GridDesignTime gridDesign = GridDesignTime(tmin, tmax, scale_to_edge);

  print(
      'Total Duration --- ${gridDesign.deltaString}      gridDelta = ${gridDesign.gridDeltaString}');
  double gridTmin = gridDesign.gridTmin;
  double gridTmax = gridDesign.gridTmax;
  double gridDeltaY = gridDesign.gridDelta;

  for (double t = gridTmin; t <= gridTmax + 1e-5; t += gridDeltaY) {
    var f0 = date_from_timestamp(t, format: gridDesign.dateFormat);
    print("$f0 ---${gridDesign.dateFormat} -     ${f0[0]}  --- ${f0[1]}");
  }
}


// main()