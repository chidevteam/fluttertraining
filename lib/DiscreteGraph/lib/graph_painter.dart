import 'dart:math';

import 'package:flutter/material.dart';
import 'graph_utils.dart';
import 'grid_design/CHITime.dart';
import 'grid_design/grid_design_float.dart';
import 'grid_design/grid_design_time.dart';

///Find min and max of systolic distolic

class GraphPainter extends CustomPainter {
  List color;
  var context;
  final Offset f1Pt;

  /// First touch point.
  final Offset f2Pt;

  /// Second touch point.
  final Offset cPt;
  final Map data;
  late Map ddata;

  late Offset dxy;
  late double sx;

  /// Horizontal scale x-axis.
  late double sy;

  /// Vertical scale y-axis.
  late bool longPressMode;

  /// Colors for trend lines and main graphs

  late double width = -1;
  late double height = -1;

  double leftMargin = 40;
  double topMargin = 10;
  double rightMargin = 10;
  double bottomMargin = 30;

  /// Initialize gradient and intecepts.
  late double mx = 1.0, my = 1.0, cx, cy;

  late double yMinPx, yMaxPx, yDeltaPx;
  late double xMinPx, xMaxPx, xDeltaPx;

  /// These three seem redundant.
  late Offset revFocal;
  late double newXmin, newXmax, newYmin, newYmax;
  late bool timeAxis;

  GraphPainter(this.data, this.f1Pt, this.f2Pt, this.cPt, this.dxy, this.sx,
      this.sy, this.longPressMode, this.context, this.color) {
    /// Whether the x-axis should show time or timestamps
    this.timeAxis = true;
    this.ddata = this.data;
  }

  @override
  bool shouldRepaint(GraphPainter oldDelegate) => true;

  @override
  void paint(Canvas canvas, Size size) {
    // if (this.nF == 1) return;

    if (size.width != width || size.height != height) {
      width = size.width;
      height = size.height;
    }
    initVars(size.width, size.height);

    // var backGroundColor1 = Theme.of(context).backgroundColor;
    // var backGroundColor2 = Theme.of(context).backgroundColor;
    var backGroundColor1 = Color(0xFF141414);
    var backGroundColor2 = Color(0xFF1B1B1B);

    Paint paintOuter = makePaint(backGroundColor1, style: PaintingStyle.fill);
    Paint paintInner = makePaint(backGroundColor2, style: PaintingStyle.fill);
    Paint paintBorder =
        makePaint(Color(0xFF555555), style: PaintingStyle.stroke);

    Rect outerRect = Rect.fromLTRB(0, 0, size.width, size.height);
    Rect innerRect = Rect.fromLTRB(leftMargin, topMargin,
        size.width - rightMargin, size.height - bottomMargin);

    canvas.drawRect(outerRect, paintOuter);
    canvas.drawRect(innerRect, paintInner);
    drawGrid(canvas, size);
    canvas.drawRect(innerRect, paintBorder);
    canvas.clipRect(innerRect);

    for (int i = 0; i < ddata["vitals"].length; i++) {
      paintPathFromPTS(ddata["vitals"][i]["data"], canvas, size);
    }
    // drawDebugCircles(canvas, f1Pt, f2Pt, cPt);
  }

  void initVars(double w, double h) {
    Map<String, double> minMax = {
      "ymin": double.infinity,
      "ymax": -double.infinity,
      "xmin": double.infinity,
      "xmax": -double.infinity,
    };

    for (int i = 0; i < ddata["vitals"].length; i++) {
      computeMinMax(ddata["vitals"][i], minMax);
    }

    double? ymin = minMax["ymin"]!;
    double? ymax = minMax["ymax"]!;
    double? xmin = minMax["xmin"]!;
    double? xmax = minMax["xmax"]!;
    // List<num> range = minMax["ranges"]!;

    ymin = ymin - .10 * (ymax - ymin);
    ymax = ymax + .10 * (ymax - ymin);
    computeVars(w, h, xmin, xmax, ymin, ymax);
  }

  /// Computing the minimum and maximum of the data points
  void computeMinMax(Map vital, Map<String, double> minMax) {
    double? ymin = minMax["ymin"]!;
    double? ymax = minMax["ymax"]!;
    double? xmin = minMax["xmin"]!;
    double? xmax = minMax["xmax"]!;

    for (int i = 0; i < vital["data"].length; i++) {
      List vData = vital["data"][i];
      num vt = vData[0];
      if (vt < xmin!) xmin = vt.toDouble();
      if (vt > xmax!) xmax = vt.toDouble();
      if (vData[1] < ymin)
        ymin = vData[2] != null ? vData[2].toDouble() : vData[1];
      if (vData[1] > ymax)
        ymax = vData[3] != null ? vData[3].toDouble() : vData[1];
    }
    // return;
    minMax["ymin"] = ymin!;
    minMax["ymax"] = ymax!;
    minMax["xmin"] = xmin!;
    minMax["xmax"] = xmax!;
  }

  List<double> solveLinear(
      double x1, double x2, double y1, double y2, d, scale) {
    double mx = (y2 - y1) / (x2 - x1);
    double cx = y1 - mx * x1;
    mx = mx * scale;
    cx = cx * scale + d;
    double xmin_new = (y1 - cx) / mx;
    double xmax_new = (y2 - cx) / mx;
    return [mx, cx, xmin_new, xmax_new];
  }

  void computeVars(
    double w,
    double h,
    double xmin,
    double xmax,
    double ymin,
    double ymax,
  ) {
    List<double> mxcx =
        solveLinear(xmin, xmax, leftMargin, w - rightMargin, dxy.dx, sx);
    mx = mxcx[0];
    cx = mxcx[1];
    double desiredNumLines = 4;
    List<double> resultsX = gridDesignFloat(mxcx[2], mxcx[3], desiredNumLines);
    xDeltaPx = resultsX[0] * mx;
    xMinPx = resultsX[1] * mx + cx;
    xMaxPx = resultsX[2] * mx + cx;

    List<double> mycy =
        solveLinear(ymin, ymax, h - bottomMargin, topMargin, dxy.dy, sy);
    my = mycy[0];
    cy = mycy[1];

    List<double> resultsY = gridDesignFloat(mycy[2], mycy[3], desiredNumLines);
    yDeltaPx = resultsY[0] * my;
    yMinPx = resultsY[1] * my + cy;
    yMaxPx = resultsY[2] * my + cy;
  }

  void paintPathFromPTS(List<List> vData, Canvas canvas, Size size) {
    Paint paint0 = makePaint(Colors.green, strokeWidth: 1.0);

    Path path = Path();
    double distance = 1e10;
    double bX = 0, bY = 0;
    double by = 0;
    double X = mx * vData[0][0] + cx;
    double Y = my * vData[0][1] + cy;
    path.moveTo(X, Y);

    for (int i = 1; i < vData.length; i++) {
      print("--  ${vData[i][0]}   ${vData[i][1]}");

      X = mx * vData[i][0] + cx;
      Y = my * vData[i][1] + cy;
      double d = (X - f1Pt.dx).abs(); //
      if (d < distance) {
        distance = d;
        bX = X;
        bY = Y;
        // bx = x[i];
        by = vData[i][1].toDouble();
      }
      drawRanges(canvas, size, bX, bY, by);
      path.lineTo(mx * vData[i][0] + cx, my * vData[i][1] + cy);
    }
    var color = Colors.red;

    for (var i = 0; i < ddata['vitals'].length; i++) {
      color = i % 2 == 0 ? Colors.purple : Colors.blue;
      if (ddata['vitals'][i].containsKey("trend_line")) {
        if (ddata['vitals'][i]['trend_line'] != null) {
          drawTrendLine(canvas, size, ddata['vitals'][i], color);
        }
      }
    }

    canvas.drawPath(path, paint0);
    drawCrossHairs(canvas, size, bX, bY, by);
  }

  void drawTrendLine(Canvas canvas, Size size, vitalData, color) {
    double x1 = vitalData["trend_line"]['t1'].toDouble();
    double y1 = vitalData["trend_line"]['v1'].toDouble();
    double x2 = vitalData["trend_line"]['t2'].toDouble();
    double y2 = vitalData["trend_line"]['v2'].toDouble();

    double X1 = mx * x1 + cx;
    double Y1 = my * y1 + cy;
    double X2 = mx * x2 + cx;
    double Y2 = my * y2 + cy;
    canvas.drawLine(
        Offset(X1, Y1), Offset(X2, Y2), makePaint(color, strokeWidth: 1));
  }

  void drawCrossHairs(
      Canvas canvas, Size size, double bX, double bY, double by) {
    if (this.longPressMode == true) {
      Path path = Path();
      Paint paint = makePaint(Colors.red, strokeWidth: 1.0);

      path.moveTo(bX, topMargin);
      path.lineTo(bX, size.height - bottomMargin);
      canvas.drawPath(path, paint);

      path.moveTo(leftMargin, bY);
      path.lineTo(size.width - rightMargin, bY);
      canvas.drawPath(path, paint);

      textPaint(context, canvas, size, by.toStringAsFixed(2), bX, bY);
      // textPaint(canvas, size, yn.toStringAsFixed(2), 15, y - 6);
    }
  }

  void drawGrid(Canvas canvas, Size size) {
    Path path = Path();
    Paint paint = makePaint(Colors.grey.shade800);

    for (double x = xMinPx; x <= xMaxPx + 1e-3; x += xDeltaPx) {
      path.moveTo(x, topMargin);
      path.lineTo(x, size.height - bottomMargin);
      double xn = (x - cx) / mx;
      if (this.timeAxis == true) {
        GridDesignTime gridDesign = GridDesignTime(xMinPx, xMaxPx + 1e-3, true);
        var f0 = date_from_timestamp(xn, format: gridDesign.dateFormat);
        var fmt = '${f0[1]}\n\t\t\t\t${f0[0]}';
        textPaint(
            context, canvas, size, fmt, x - 10, size.height - bottomMargin + 5);
      } else {
        textPaint(context, canvas, size, xn.toStringAsFixed(2), x - 10,
            size.height - bottomMargin + 5);
      }
    }

    for (double y = yMaxPx; y <= yMinPx + 1e-3; y -= yDeltaPx) {
      path.moveTo(leftMargin, y);
      path.lineTo(size.width - rightMargin, y);
      double yn = (y - cy) / my;
      textPaint(context, canvas, size, yn.toStringAsFixed(2), 5, y - 6);
    }
    canvas.drawPath(path, paint);
  }

  drawRanges(Canvas canvas, Size size, double bX, double bY, double by) {
    for (int j = 0; j < this.ddata['vitals'].length; j++) {
      List data = this.ddata['vitals'][j]['data'];
      Path path = Path();
      Paint paint = makePaint(Colors.pink, strokeWidth: 1);

      for (int i = 0; i < data.length; i++) {
        ;
        double x1 = mx * data[i][0].toDouble() + cx;
        double y1 =
            my * (data[i][2] != null ? data[i][2].toDouble() : data[i][1]) + cy;

        double xD = mx * data[i][0].toDouble() + cx;
        double yD = my * data[i][1].toDouble() + cy;

        double x2 = mx * data[i][0].toDouble() + cx;
        double y2 =
            my * (data[i][3] != null ? data[i][3].toDouble() : data[i][1]) + cy;

        canvas.drawCircle(Offset(xD, yD), 2.0, paint);

        path.moveTo(x1, y1);
        path.lineTo(x2, y2);

        double leftXLowerBound = mx * data[i][0].toDouble() + cx - 5;
        double rightXLowerBound = mx * data[i][0].toDouble() + cx + 5;

        path.moveTo(leftXLowerBound, y1);
        path.lineTo(rightXLowerBound, y1);

        double leftXUpperBound = mx * data[i][0].toDouble() + cx - 5;
        double rightXUpperBound = mx * data[i][0].toDouble() + cx + 5;

        path.moveTo(leftXUpperBound, y2);
        path.lineTo(rightXUpperBound, y2);
      }

      canvas.drawPath(path, paint);
    }
  }

  // String _addTenthDecimalZero(onesDecimal) {
  //   var zeroethPlace = onesDecimal.toString().length == 1 ? '0' : '';
  //   return '${zeroethPlace}${onesDecimal}';
  // }
}

//  void drawGrid(Canvas canvas, Size size) {
//     Path path = Path();
//     Paint paint = makePaint(Colors.grey.shade800);

//     for (double x = xMinPx; x <= xMaxPx + 1e-3; x += xDeltaPx) {
//       path.moveTo(x, topMargin);
//       path.lineTo(x, size.height - bottomMargin);
//       double xn = (x - cx) / mx;
//       var dateTime = DateTime.fromMillisecondsSinceEpoch(xn.toInt() * 1000);
//       var d =
//           '${_addTenthDecimalZero(dateTime.day)}-${_addTenthDecimalZero(dateTime.month)}-${_addTenthDecimalZero(dateTime.year)}\n\t\t\t\t${_addTenthDecimalZero(dateTime.hour)}:${_addTenthDecimalZero(dateTime.minute)}';
//       textPaint(
//           context, canvas, size, d, x - 10, size.height - bottomMargin + 5);
//     }

//     for (double y = yMaxPx; y <= yMinPx + 1e-3; y -= yDeltaPx) {
//       path.moveTo(leftMargin, y);
//       path.lineTo(size.width - rightMargin, y);
//       double yn = (y - cy) / my;
//       textPaint(context, canvas, size, yn.toStringAsFixed(2), 5, y - 6);
//     }
//     canvas.drawPath(path, paint);
//   }

//   String _addTenthDecimalZero(onesDecimal) {
//     var zeroethPlace = onesDecimal.toString().length == 1 ? '0' : '';
//     return '${zeroethPlace}${onesDecimal}';
//   }
