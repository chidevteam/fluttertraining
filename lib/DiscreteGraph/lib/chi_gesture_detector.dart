/*

The important variables along with their initial values are:
  
  Offset finger1Pt = Offset.zero;
  Offset finger2Pt = Offset.zero;
  Offset centerPt = Offset.zero;

  // The scales based on current touch processs (from touch down to touch up)
  double currScale = 1.0;
  double currScaleX = 1.0;
  double currScaleY = 1.0;
  double currRotation = 0.0;
  Offset currTrans = Offset(0.0, 0.0);

  // The net scales/translations upto the last touch up.
  double prevScale = 1.0;
  double prevScaleX = 1.0;
  double prevScaleY = 1.0;
  double prevRotation = 0.0;
  Offset prevTrans = Offset(0.0, 0.0);

  Offset memoryPoint = Offset.infinite;

  late int touchStartTime = 0;
  late int touchEndTime = 30000;

  THE FINGER TOUCH POINTS: Used to identify direction of scaling.
  ---------------------------------------------------------------
    The primary touch point is finger1Pt. GestureDetector gives this as the
    "details.localFocalPoint" if we use one finger. As soon as the second finger 
    touches, the middle point of finger touches received in 
    "details.localFocalPoint".

    I use "memoryPoint" to remember the one finger touch point. When second 
    finger touches, the memoryPoint is used along with the localFocalPoint to 
    calculate the position of the second finger. Initial estimate is obained by
    adding f1pt-cpt to cpt. This is not 100% accurate (which can be seen by 
    drawing a triangle between the points f1Pt, f2Pt, and cPt - which sometimes
    is not a straight line). The delta movement as well as the rotation is used
    to compute the current touch points - though they are not explicitly given
    by the official GestureDetector.

 */

import 'package:flutter/material.dart';
import 'dart:math';

// ignore: must_be_immutable
class CHIGestureDetector extends StatelessWidget {
  final Widget child;

  final void Function(int valueNotif, Offset f1Pt, Offset f2Pt, Offset cPt,
      Offset dxy, double sx, double sy, bool longPressMode) onUpdate;

  int valueNotif = 0;
  int quickTapCount = 0; //Used for double tap determination
  bool longPressMode = false; //Long press state, just the previous

  // The touch points : Used to detect the direction of scaling (X/Y)
  Offset finger1Pt = Offset.zero;
  Offset finger2Pt = Offset.zero;
  Offset centerPt = Offset.zero;

  // the scales based on current touch processs (from touch down to touch up)
  double currScale = 1.0;
  double currScaleX = 1.0;
  double currScaleY = 1.0;
  double currRotation = 0.0;
  Offset currTrans = Offset(0.0, 0.0);

  // The net scales upto the last touch up.
  double prevScale = 1.0;
  double prevScaleX = 1.0;
  double prevScaleY = 1.0;
  double prevRotation = 0.0;
  Offset prevTrans = Offset(0.0, 0.0);

  Offset memoryPoint = Offset.infinite;

  late int touchStartTime = 0;
  late int touchEndTime = 30000;

  CHIGestureDetector({Key? key, required this.onUpdate, required this.child}) {
    initVars();
  }

  void initCurrVars() {
    finger1Pt = Offset.zero;
    finger2Pt = Offset.zero;
    centerPt = Offset.zero;
    currScale = 1.0;
    currScaleX = 1.0;
    currScaleY = 1.0;
    currRotation = 0.0;
    currTrans = Offset(0.0, 0.0);
    longPressMode = false;
  }

  void initVars() {
    initCurrVars();
    prevScale = 1.0;
    prevScaleX = 1.0;
    prevScaleY = 1.0;
    prevRotation = 0.0;
    prevTrans = Offset(0.0, 0.0);
    memoryPoint = Offset.infinite;
    sendUpdate();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onScaleStart: onScaleStart,
      onScaleUpdate: onScaleUpdate,
      onScaleEnd: onScaleEnd,
      child: child,
    );
  }

  Offset rotateScale(Offset pt, double r, double th) {
    double px = r * cos(th) * pt.dx - r * sin(th) * pt.dy;
    double py = r * sin(th) * pt.dx + r * cos(th) * pt.dy;
    return Offset(px, py);
  }

  void onScaleStart(ScaleStartDetails details) {
    print("scale start");
    centerPt = details.localFocalPoint;

    if (details.pointerCount == 1) {
      finger1Pt = details.localFocalPoint;
      finger2Pt = details.localFocalPoint;

      doubleTouchStartDetect(details.pointerCount);
    } else if (details.pointerCount == 2) {
      // Use the memoryPoint and centerPt to compute reflected f2Pt
      finger1Pt = memoryPoint;
      Offset delta = centerPt - finger1Pt;
      finger2Pt = centerPt + delta;
    }
  }

  void onScaleUpdate(ScaleUpdateDetails details) {
    print("scale update");
    centerPt = details.localFocalPoint;

    longPressDetect(details.pointerCount, details.focalPointDelta);
    if (details.pointerCount == 1) {
      finger1Pt = details.localFocalPoint;
      finger2Pt = details.localFocalPoint;
    } else if (details.pointerCount == 2) {
      Offset deltaXY = details.focalPointDelta - currTrans;
      double th = details.rotation - currRotation;
      currRotation = details.rotation;

      double r = details.scale / currScale;
      currScale = details.scale;

      // Use the original f1Pt and cPt to compute rotated/scaled points
      // and translate the current postion via deltaXY
      finger1Pt = rotateScale(finger1Pt - centerPt, r, th) + centerPt + deltaXY;
      finger2Pt = rotateScale(finger2Pt - centerPt, r, th) + centerPt + deltaXY;

      // Use f1Pt and f2Pt to compute scaleX and scaleY
      // One of the scale will be retained at original value, the other
      // will be scaled down by the ratio of sides touch point triangle.
      Offset vector = finger1Pt - finger2Pt;
      double vx = vector.dx.abs();
      double vy = vector.dy.abs();
      if (vx > vy) {
        currScaleX = currScale;
        currScaleY = (currScale - 1) * vy / vx + 1;
      }
      if (vy > vx) {
        currScaleX = (currScale - 1) * vx / vy + 1;
        currScaleY = currScale;
      }
    }
    currTrans = details.focalPointDelta;
    sendUpdate();
  }

  void onScaleEnd(ScaleEndDetails details) {
    doubleTouchEndDetect(details.pointerCount);
    memoryPoint = finger1Pt;
    prevTrans =
        getDelta(currScaleX, currScaleY, currTrans, prevTrans, centerPt);
    prevScale *= currScale;
    prevScaleX *= currScaleX;
    prevScaleY *= currScaleY;
    prevRotation += currRotation;
    initCurrVars();
    sendUpdate();
  }

  Offset getDelta(double ssx, double ssy, Offset cxy, Offset pxy, Offset cpt) {
    double dx = (cxy.dx + pxy.dx - cpt.dx) * ssx + cpt.dx;
    double dy = (cxy.dy + pxy.dy - cpt.dy) * ssy + cpt.dy;
    Offset delta = Offset(dx, dy);
    return delta;
  }

  void sendUpdate() {
    valueNotif += 1;

    var delta =
        getDelta(currScaleX, currScaleY, currTrans, prevTrans, centerPt);

    // The follow code segment can be used to fix the lot fo callback
    // which gesture detector unnecessarily provides
    // TO DO:
    // if ((currScaleX - 1.0).abs() < 0.001 &&
    //     (currScaleY - 1.0).abs() < 0.001 &&
    //     currTrans.distance < 1.0) {
    // } else {
    //   print("$longPressMode");
    // }

    if (longPressMode == true) {
      onUpdate(valueNotif, finger1Pt, finger2Pt, centerPt, prevTrans,
          prevScaleX, prevScaleY, longPressMode);
      currTrans = Offset.zero;
    } else {
      onUpdate(valueNotif, finger1Pt, finger2Pt, centerPt, delta,
          prevScaleX * currScaleX, prevScaleY * currScaleY, longPressMode);
    }
  }

  void longPressDetect(int pointerCount, Offset delta) {
    int currTime = DateTime.now().millisecondsSinceEpoch;
    int deltaTime = currTime - touchStartTime;

    if (pointerCount == 1 &&
        delta.distance < 2 &&
        deltaTime > 200 &&
        longPressMode == false) {
      longPressMode = true;
      print("Long Press detected");
    }
  }

  void doubleTouchStartDetect(int pointerCount) {
    int currTime = DateTime.now().millisecondsSinceEpoch;
    int deltaTime = currTime - touchStartTime;
    if (deltaTime < 300)
      quickTapCount += 1;
    else
      quickTapCount = 0;
    touchStartTime = currTime;
  }

  void doubleTouchEndDetect(int pointerCount) {
    if (pointerCount < 1) {
      touchEndTime = DateTime.now().millisecondsSinceEpoch;
      int tapDiff = touchEndTime - touchStartTime;

      if (tapDiff < 130)
        quickTapCount += 1;
      else
        quickTapCount = 0;

      if (quickTapCount >= 3) {
        initVars();
        quickTapCount = 0;
      }
    } else {
      quickTapCount = 0;
    }
  }
}
