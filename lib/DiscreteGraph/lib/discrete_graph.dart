import 'package:flutter/material.dart';
import 'custom_theme.dart';
import 'graph_painter.dart';
import 'chi_gesture_detector.dart';

// ignore: must_be_immutable
class DiscreteGraph extends StatelessWidget with CustomTheme {
  late ValueNotifier<int> notifier = ValueNotifier(0);

  Offset m_f1Pt = Offset.infinite;
  Offset m_f2Pt = Offset.infinite;
  Offset m_cPt = Offset.infinite;
  Offset m_dxy = Offset(0, 0);
  double m_scale = 1.0;
  double m_sx = 1.0;
  double m_sy = 1.0;
  bool m_longPressMode = false;
  double width;
  double height;
  // final Map<String, List<double>> data;
  Map data;

  DiscreteGraph(
      {Key? key,
      required this.data,
      this.width = double.infinity,
      this.height = double.infinity}) {}

  initVars() {
    // notifier = ValueNotifier(0);
    m_f1Pt = Offset.infinite;
    m_f2Pt = Offset.infinite;
    m_cPt = Offset.infinite;
    m_dxy = Offset(0, 0);
    m_scale = 1.0;
    m_sx = 1.0;
    m_sy = 1.0;
    m_longPressMode = false;
  }

  @override
  Widget build(BuildContext context) {
    initVars();

    return Padding(
      // padding: const EdgeInsets.fromLTRB(5.0, 10, 5.0, 30),
      padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
      child: CHIGestureDetector(
        onUpdate: (int m, Offset f1Pt, Offset f2Pt, Offset cPt, Offset dxy,
            double sx, double sy, longPressMode) {
          m_f1Pt = f1Pt;
          m_f2Pt = f2Pt;
          m_cPt = cPt;
          m_dxy = dxy;
          m_sx = sx;
          m_sy = sy;
          m_longPressMode = longPressMode;
          notifier.value = m;
        },
        child: getValueListenableBuilder(context, notifier),
      ),
    );
  }

  String toTwoDecimal(double v) {
    return v.toStringAsFixed(2);
  }

  Widget getValueListenableBuilder(ctx, notifier) {
    var color = graphStrokes();
    print("Yes, this is being called when notified");
    return ValueListenableBuilder<int>(
      builder: (BuildContext context, int value, Widget? complexWgt) {
        return ClipRect(
          child: Container(
            width: width,
            height: height,
            color: Colors.grey.shade100,
            child: Container(
              child: CustomPaint(
                painter: GraphPainter(data, m_f1Pt, m_f2Pt, m_cPt, m_dxy, m_sx,
                    m_sy, m_longPressMode, context, color),
              ),
            ),
          ),
        );
      },
      valueListenable: notifier,
    );
  }
}
