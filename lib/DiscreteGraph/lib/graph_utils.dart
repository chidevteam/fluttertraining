import 'package:flutter/material.dart';

Paint makePaint(Color color,
    {PaintingStyle style = PaintingStyle.stroke, double strokeWidth = 1.0}) {
  Paint paint = Paint()
    ..color = color
    ..style = style
    ..strokeWidth = strokeWidth;
  return paint;
}

void drawDebugCircles(Canvas canvas, Offset f1Pt, Offset f2Pt, Offset cPt) {
  Paint paint1 = makePaint(Colors.white, strokeWidth: 5.0);
  Paint paint2 = makePaint(Colors.yellow, strokeWidth: 5.0);
  Paint paintc = makePaint(Colors.green, strokeWidth: 5.0);
  // Paint paintcc = makePaint(Colors.yellow, strokeWidth: 5.0);

  if (f1Pt.distance > 0) canvas.drawCircle(f2Pt, 50, paint2);
  if (cPt.distance > 0) canvas.drawCircle(cPt, 50, paintc);
  if (f2Pt.distance > 0) canvas.drawCircle(f1Pt, 50, paint1);
  // canvas.drawCircle(ccPt, 80, paintcc);
}

double minimum(List<double> x) {
  return x.reduce((curr, next) => curr < next ? curr : next);
}

double maximum(List<double> x) {
  return x.reduce((curr, next) => curr > next ? curr : next);
}

//.toStringAsFixed(1)
void textPaint(
    context, Canvas canvas, Size size, String str, double x, double y) {
  // var textColor = Theme.of(context).textTheme.bodyText1!.color;

  final textStyle = TextStyle(
    color: Colors.grey.shade400,
    fontSize: 10,
    //overflow: TextOverflow.ellipsis);
  );
  final textSpan = TextSpan(
    text: str,
    style: textStyle,
  );
  final textPainter = TextPainter(
    text: textSpan,
    textDirection: TextDirection.ltr,
  );
  textPainter.layout(
    minWidth: 0,
    maxWidth: size.width,
  );
  final offset = Offset(x, y);
  textPainter.paint(canvas, offset);
}
