import 'dart:math' as math;

import 'package:flutter/material.dart';


void main() => runApp(CrossTest());

class CrossTest extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const SamplePage(),
    );
  }
}

const kCanvasSize = 300.0;

class SamplePage extends StatefulWidget {
  const SamplePage({Key? key}) : super(key: key);

  @override
  _SamplePageState createState() => _SamplePageState();
}

class _SamplePageState extends State<SamplePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          decoration: BoxDecoration(
            color: Colors.grey,
              border: Border.all(color: Colors.greenAccent, width: 2)),
          width: kCanvasSize,
          height: kCanvasSize,
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Container(
              color: Colors.blueAccent,
              child: CustomPaint(
                painter: DrawLine(),
                foregroundPainter: DrawPath(),
                //child: Container(),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class CrossDrawPaint extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    Paint crossBrush = Paint()
      ..color = Colors.red
      ..style = PaintingStyle.stroke
      ..strokeWidth = 12;
    canvas.drawLine(const Offset(50, 50),
        Offset(size.width - 50, size.height - 50), crossBrush);
    canvas.drawLine(
        Offset(size.width - 50, 50), Offset(50, size.height - 50), crossBrush);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}

class DrawLine extends CustomPainter{
  @override
  void paint(Canvas canvas, Size size) {
    var center = size/2;
    var paint = Paint()..color = Colors.white..strokeWidth = 5.0;

    canvas.drawLine(Offset(0, center.height), Offset(size.width, center.height), paint);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
  
}

class DrawPath extends CustomPainter{
  @override
  void paint(Canvas canvas, Size size){
    // var paint = Paint()
    //   ..color = Colors.yellow
    //   ..strokeWidth = 20.0;
    
    // var path = Path()
    //   ..moveTo(0, 0)
    //   ..lineTo(100, 100)
    //   ..lineTo(0, 100)
    //   ..lineTo(0, 0);
    
    //canvas.drawPath(path, paint);

    var center = size/2;
    double initalX = 0;
    double initalX2 = 2;
    double initialY = center.height;
    double initialY2 = center.height-5;
    var paint = Paint()..color = Colors.white..strokeWidth = 4.0;
    int g_size = 15;
    int g_size2 = 30;
    Path path = Path();
    //up
    for(int i = 0; i<g_size;i++){
      canvas.drawLine(Offset(initalX, initialY - math.sin(initialY * 0.045 )), Offset(initalX2 , initialY2 - math.sin(initialY2 * 0.045 )), paint); 
      initalX+= 2;
      initalX2+= 2;
      initialY= (initialY- 5) - (math.sin(initialY * 0.045 ));
      initialY2= (initialY2 -5) - (math.sin(initialY2 * 0.045 )); 
    }
    // path.quadraticBezierTo(initalX, initialY, initalX2, initialY2);
    // canvas.drawPath(path, paint);
    
    //down
    for(int i = 0; i<g_size2;i++){
      canvas.drawLine(Offset(initalX, initialY + math.sin(initialY * 0.045 )), Offset(initalX2 , initialY2 + math.sin(initialY2 * 0.045 )), paint); 
      initalX+= 2;
      initalX2+= 2;
      initialY= (initialY + 5) + (math.sin(initialY * 0.045 ));
      initialY2= (initialY2 + 5) + (math.sin(initialY2 * 0.045 ));
    }

    //up
    for(int i = 0; i<g_size2;i++){
      canvas.drawLine(Offset(initalX, initialY - math.sin(initialY * 0.045 )), Offset(initalX2 , initialY2 - math.sin(initialY2 * 0.045 )), paint); 
      initalX+= 2;
      initalX2+= 2;
      initialY= (initialY- 5) - (math.sin(initialY * 0.045 ));
      initialY2= (initialY2 -5) - (math.sin(initialY2 * 0.045 ));
    }
    for(int i = 0; i<g_size2;i++){
      canvas.drawLine(Offset(initalX, initialY + math.sin(initialY * 0.045 )), Offset(initalX2 , initialY2 + math.sin(initialY2 * 0.045 )), paint); 
      initalX+= 2;
      initalX2+= 2;
      initialY= (initialY+ 5) + (math.sin(initialY * 0.045 ));
      initialY2= (initialY2 +5) + (math.sin(initialY2 * 0.045 ));
    }
    
  }
  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
  
}










void paint(Canvas canvas, Size size) { 
    
    final paint = Paint(); 
    paint.color = Colors.red; 
    canvas.drawLine(new Offset(0, 0), new Offset(size.width, size.height), paint);
    canvas.drawLine(new Offset(size.width, 0), new Offset(0, size.height), paint); 
}