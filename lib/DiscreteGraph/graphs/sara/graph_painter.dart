import 'package:flutter/material.dart';
import 'dart:math' as math;

class GraphPainter extends CustomPainter {
  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) => true;

  @override
  void paint(Canvas canvas, Size size) {
    // _bisectScreen(canvas, size);
    _sineContainer(canvas, size);
  }

  _sineContainer(Canvas canvas, Size size) {
    Paint paint = makePaint(Colors.red, strokeWidth: 2.0);

    double xInset0 = 150;
    double xInset1 = 50;
    double yInset0 = 150;
    double yInset1 = 200;

    double x0 = xInset0;
    double y0 = yInset0;
    double x1 = size.width - xInset1;
    double y1 = size.height - yInset1;

    /// left / right/ top / bottom
    canvas.drawLine(Offset(x0, y0), Offset(x1, y0), paint);
    canvas.drawLine(Offset(x1, y0), Offset(x1, y1), paint);
    canvas.drawLine(Offset(x1, y1), Offset(x0, y1), paint);
    canvas.drawLine(Offset(x0, y1), Offset(x0, y0), paint);

    _sinusoid(canvas, Offset(x0, y0),
        Size(size.width - xInset0 - xInset1, size.height - yInset0 - yInset1));
  }

  _sinusoid(Canvas canvas, Offset x0y0, Size size) {
    Paint g = makePaint(Colors.black, strokeWidth: 2.0);
    Path path = Path();
    double offsetY = x0y0.dy + size.height / 2;
    double amplitude = size.height / 2;
    path.moveTo(x0y0.dx, offsetY);

    for (double t = 0; t < 1.01; t += 0.01) {
      double sine = offsetY - amplitude * math.sin(2 * math.pi * 3 * t);
      path.lineTo(x0y0.dx + t * size.width, sine);
      path.moveTo(x0y0.dx + t * size.width, sine);
    }

    canvas.drawPath(path, g);
  }

  Paint makePaint(Color color,
      {PaintingStyle style = PaintingStyle.stroke, double strokeWidth = 1.0}) {
    Paint paint = Paint()
      ..color = color
      ..style = style
      ..strokeWidth = strokeWidth;
    return paint;
  }
}



  // _bisectScreen(Canvas canvas, Size size) {
  //   var y = size.height / 2;
  //   Paint l = makePaint(Colors.black, strokeWidth: 2.0);
  //   canvas.drawLine(Offset(0, y), Offset(size.width, y), l);
  // }

  // _drawCross(Canvas canvas, Size size) {
  //   Paint paint = makePaint(Colors.white, strokeWidth: 3.0);

  //   /// cross
  //   canvas.drawLine(
  //       const Offset(10, 10), Offset(size.width - 10, size.height - 10), paint);
  //   canvas.drawLine(
  //       Offset(size.width - 10, 10), Offset(10, size.height - 10), paint);

  //   Paint paint2 = makePaint(Colors.grey, strokeWidth: 5.0);

  //   /// vertical borders (10px)
  //   canvas.drawLine(const Offset(10, 10), Offset(10, size.height), paint2);
  //   canvas.drawLine(Offset(size.width - 10, 10),
  //       Offset(size.width - 10, size.height), paint2);

  //   /// horizontal borders (10px)

  //   canvas.drawLine(const Offset(10, 10), Offset(size.width - 10, 10), paint2);
  //   canvas.drawLine(Offset(10, size.height - 10),
  //       Offset(size.width - 10, size.height - 10), paint2);
  // }
