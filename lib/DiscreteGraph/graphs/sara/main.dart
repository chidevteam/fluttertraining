import 'package:flutter/material.dart';

import 'graph_painter.dart';

class Graph extends StatelessWidget {
  const Graph({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      // padding: EdgeInsets.fromLTRB(50, 50, 50, 200),
      color: Colors.blue,
      child: CustomPaint(
        painter: GraphPainter(),
      ),
    );
  }
}

void main() {
  runApp(const Graph());
}
