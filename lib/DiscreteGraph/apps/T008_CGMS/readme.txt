
9:12am Copied the standard CustomPainter code, which can be used as starting
       point for any customDraw/canvas application

9:20am Planning
       1- Read the json file and put in appropriate format - 15min-45min
       2- How to draw filled polygon - 10-30min
       3- How to draw vertical label - 10-20min
       4- Draw grid - 15-30min
       5- label horizontal/vertical grid - 10-20min
       6- Put legend/title - 15min
       7- landscape/portrait
       8- on which screen it will appear
       9- make the two polygon via points from data
       10- Understand what is copied (Text rotation)
9:26 Planning complete       

9:33 gupshup, estimated that one full day is a good time to complete
     - will try to finish it in half day or earlier
     
9:34 Task2: Search starts
9:42 we know how to fill a polygon     
9:45 we know how rotate text
9:57 Printed the relevant data
10:00 Going to prepare data for polygons
10:05 we have the series data for time and d0, d1, ...
10:16 Explained things to Daniyal
10:16 Going to plot the data
10:35 made the scale function,
      and finally able to plot data
10:42 some gupshup
10:45 going to draw a filled polygon
10:54 Both filled polygons working
11:10 xgrid working, gup shup included
11:15 yGrid working
      gup shup on webrtc
11:25 Resuming, xlabels      
