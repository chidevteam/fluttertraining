import 'package:flutter/material.dart';
import 'dart:math' as math;

class AGPGraphPainter extends CustomPainter {
  List<Map<String, dynamic>> data;
  List<double> timeData = [];
  List<double> d0 = [];
  List<double> d1 = [];
  List<double> d2 = [];
  List<double> d3 = [];
  List<double> d4 = [];
  late double mx;
  late double cx;
  late double my;
  late double cy;

  AGPGraphPainter(this.data) {
    // print(data);
    for (Map<String, dynamic> mData in data) {
      timeData.add(mData["reading_time"]);
      d0.add(mData["five_percent"]);
      d1.add(mData["twenty_five_percent"]);
      d2.add(mData["fifty_percent"]);
      d3.add(mData["seventy_five_percent"]);
      d4.add(mData["ninety_five_percent"]);
    }
    print(timeData[0]);
    print(d0[0]);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) => true;

  Map<String, dynamic> computeScale(
      double x1, double x2, double y1, double y2) {
    mx = (y2 - y1) / (x2 - x1);
    cx = y1 - mx * x1;

    return {"m": mx, "c": cx};
    //y-y1 = (y2-y1)/(x2-x1)*(x-x1)
    //y = m*(x-x1)+y1
    //y = m*x-m*x1+y1
    //y = m*x+y1-m*x1
    //y = mx + c
  }

  @override
  void paint(Canvas canvas, Size size) {
    Paint paint = makePaint(Colors.white, strokeWidth: 2.0);

    double xInset0 = 150;
    double xInset1 = 50;
    double yInset0 = 150;
    double yInset1 = 200;

    double x0 = xInset0;
    double y0 = yInset0;
    double x1 = size.width - xInset1;
    double y1 = size.height - yInset1;

    Map<String, dynamic> mxcx = computeScale(0.0, 86400.0, x0, x1);
    Map<String, dynamic> mycy = computeScale(0.0, 200.0, y1, y0);

    Paint p1 = Paint()
      ..color = Colors.green
      ..style = PaintingStyle.fill
      ..strokeWidth = 2;

    drawPolygonCurve(canvas, mxcx, mycy, [d0, d4], p1);

    Paint p2 = Paint()
      ..color = Colors.orange
      ..style = PaintingStyle.fill
      ..strokeWidth = 2;

    drawPolygonCurve(canvas, mxcx, mycy, [d1, d3], p2);

    drawMiddleCurve(canvas, mxcx, mycy);

    drawXGrid(canvas, mxcx, y0, y1);
    drawYGrid(canvas, mycy, x0, x1);

    canvas.drawRect(Rect.fromLTRB(x0, y0, x1, y1), paint);
    // Path path = Path();
    // path.moveTo(x0, y0);
    // path.lineTo(x1, y0);

    // path.lineTo(x0, y1);
    // // path.lineTo(x0, y0);

    // p.moveTo()

    // canvas.drawPath(path, paint1);
    //   ..strokeWidth = strokeWidth;
    // _sinusoid(canvas, Offset(x0, y0),
    //     Size(size.width - xInset0 - xInset1, size.height - yInset0 - yInset1));
    drawText(canvas, "This is a graph", 100, 200, 3.1415 / 2 * 3);
  }

  void drawXGrid(Canvas canvas, Map mxcx, double y0, double y1) {
    Paint paint = makePaint(Colors.black, strokeWidth: 1.0);

    Path p = Path();
    double mx = mxcx["m"];
    double cx = mxcx["c"];
    double delta = 86400.0 / 8;
    List labels = [
      "12am",
      "3am",
      "6am",
      "9am",
      "12pm",
      "3pm",
      "6pm",
      "9pm",
      "12am",
    ];

    int k = 0;
    for (double x = 0; x < 86400.0 + delta; x += delta) {
      double px = mx * x + cx;
      // double py = my * d2[i] + cy;
      p.moveTo(px, y0);
      p.lineTo(px, y1);
      drawText(canvas, labels[k++], px - 25, y1 + 15, 0);
    }
    canvas.drawPath(p, paint);
  }

  void drawYGrid(Canvas canvas, Map mycy, double x0, double x1) {
    Paint paint = makePaint(Colors.black, strokeWidth: 1.0);

    Path p = Path();
    double mx = mycy["m"];
    double cx = mycy["c"];
    double delta = 200.0 / 3;

    for (double x = delta; x < 200.0; x += delta) {
      double py = mx * x + cx;
      // double py = my * d2[i] + cy;
      p.moveTo(x0, py);
      p.lineTo(x1, py);
    }
    canvas.drawPath(p, paint);
  }

  void drawMiddleCurve(Canvas canvas, Map mxcx, Map mycy) {
    Paint paint1 = Paint()
      ..color = Colors.red
      ..style = PaintingStyle.stroke
      ..strokeWidth = 2;

    Path p = Path();
    double mx = mxcx["m"];
    double cx = mxcx["c"];
    double my = mycy["m"];
    double cy = mycy["c"];
    for (int i = 0; i < timeData.length; i++) {
      double px = mx * timeData[i] + cx;
      double py = my * d2[i] + cy;
      if (i == 0)
        p.moveTo(px, py);
      else
        p.lineTo(px, py);
    }
    canvas.drawPath(p, paint1);
  }

  void drawPolygonCurve(Canvas canvas, Map mxcx, Map mycy,
      List<List<double>> listData2, Paint paint1) {
    List<double> y1 = listData2[0];
    List<double> y2 = listData2[1];

    Path p = Path();
    double mx = mxcx["m"];
    double cx = mxcx["c"];
    double my = mycy["m"];
    double cy = mycy["c"];
    for (int i = 0; i < timeData.length; i++) {
      double px = mx * timeData[i] + cx;
      double py = my * y1[i] + cy;
      if (i == 0)
        p.moveTo(px, py);
      else
        p.lineTo(px, py);
    }

    for (int i = timeData.length - 1; i >= 0; i--) {
      double px = mx * timeData[i] + cx;
      double py = my * y2[i] + cy;
      p.lineTo(px, py);
    }

    canvas.drawPath(p, paint1);
  }

  _sinusoid(Canvas canvas, Offset x0y0, Size size) {
    Paint paint = makePaint(Colors.black, strokeWidth: 2.0);
    Path path = Path();
    double offsetY = x0y0.dy + size.height / 2;
    double amplitude = size.height / 2;
    path.moveTo(x0y0.dx, offsetY);

    for (double t = 0; t < 1.01; t += 0.01) {
      double sine = offsetY - amplitude * math.sin(2 * math.pi * 3 * t);
      path.lineTo(x0y0.dx + t * size.width, sine);
      // path.moveTo(x0y0.dx + t * size.width, sine);
    }

    canvas.drawPath(path, paint);

    // drawPolygon(canvas, size);
  }

  Paint makePaint(Color color,
      {PaintingStyle style = PaintingStyle.stroke, double strokeWidth = 1.0}) {
    Paint paint = Paint()
      ..color = color
      ..style = style
      ..strokeWidth = strokeWidth;
    return paint;
  }

  // void drawPolygon(Canvas canvas, Size size) {
  //   Path leftEyePath = Path();
  //   leftEyePath.moveTo(leftEye[0].getX(), leftEye[0].getY()); //starting point
  //   for (int i = 1; i < leftEye.length; i++) {
  //     leftEyePath.lineTo(leftEye[i].getX(), leftEye[i].getY());
  //   }
  //   canvas.drawPath(leftEyePath, painter);
  // }

  void drawText(Canvas context, String name, double x, double y,
      double angleRotationInRadians) {
    context.save();
    context.translate(x, y);
    context.rotate(angleRotationInRadians);
    TextSpan span = new TextSpan(
        style: new TextStyle(
            color: Colors.red[800], fontSize: 24.0, fontFamily: 'Roboto'),
        text: name);
    TextPainter tp = new TextPainter(
        text: span,
        textAlign: TextAlign.left,
        textDirection: TextDirection.ltr);
    tp.layout();
    tp.paint(context, new Offset(0.0, 0.0));
    context.restore();
  }
}
