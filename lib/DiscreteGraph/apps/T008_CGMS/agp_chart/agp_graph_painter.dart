import 'dart:math';
import 'dart:math' as math;
import 'package:flutter/material.dart';

class AGPGraphPainter extends CustomPainter {
  List<Map<String, dynamic>> data;
  BuildContext context;
  List<double> timeData = [];
  List<double> d0 = [];
  List<double> d1 = [];
  List<double> d2 = [];
  List<double> d3 = [];
  List<double> d4 = [];
  late double mx;
  late double cx;
  late double my;
  late double cy;
  late double yMinPx, yMaxPx, yDeltaPx;
  late double xMinPx, xMaxPx, xDeltaPx;

  AGPGraphPainter(this.data, this.context) {
    _initVars();
  }

  _initVars() {
    for (Map<String, dynamic> mData in data) {
      timeData.add((mData["reading_time"]).toDouble());
      d0.add(mData["five_percent"]);
      d1.add(mData["twenty_five_percent"]);
      d2.add(mData["fifty_percent"]);
      d3.add(mData["seventy_five_percent"]);
      d4.add(mData["ninety_five_percent"]);
    }
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) => true;

  _computeMinMax(List<Map<String, dynamic>> data) {
    Map<String, double> minMax = {
      "ymin": double.infinity,
      "ymax": -double.infinity,
      "xmin": double.infinity,
      "xmax": -double.infinity,
    };
    List<double> xData = [];
    List<double> yData = [];

    for (int i = 0; i < data.length; i++) {
      for (String key in data[i].keys) {
        if (key == 'reading_time') {
          xData.add(data[i][key].toDouble());
        } else {
          yData.add(data[i][key].toDouble());
        }
      }
    }
    yData.sort((a, b) => a.compareTo(b));
    xData.sort((a, b) => a.compareTo(b));

    minMax["ymin"] = yData[0];
    minMax["ymax"] = yData[yData.length - 1];
    minMax["xmin"] = xData[0];
    minMax["xmax"] = xData[xData.length - 1];

    print(yData);
    print(xData);

    double? ymin = minMax["ymin"]!;
    double? ymax = minMax["ymax"]!;
    ymin = ymin - .10 * (ymax - ymin);
    ymax = ymax + .10 * (ymax - ymin);

    return minMax;
  }

  // void computeVars(
  //   double w,
  //   double h,
  //   double xmin,
  //   double xmax,
  //   double ymin,
  //   double ymax,
  // ) {
  //   List<double> mxcx =
  //       solveLinear(xmin, xmax, leftMargin, w - rightMargin, dxy.dx, sx);
  //   mx = mxcx[0];
  //   cx = mxcx[1];
  //   double desiredNumLines = 4;
  //   List<double> resultsX = gridDesignFloat(mxcx[2], mxcx[3], desiredNumLines);
  //   xDeltaPx = resultsX[0] * mx;
  //   xMinPx = resultsX[1] * mx + cx;
  //   xMaxPx = resultsX[2] * mx + cx;

  //   List<double> mycy =
  //       solveLinear(ymin, ymax, h - bottomMargin, topMargin, dxy.dy, sy);
  //   my = mycy[0];
  //   cy = mycy[1];

  //   List<double> resultsY = gridDesignFloat(mycy[2], mycy[3], desiredNumLines);
  //   yDeltaPx = resultsY[0] * my;
  //   yMinPx = resultsY[1] * my + cy;
  //   yMaxPx = resultsY[2] * my + cy;
  // }

  Map<String, dynamic> computeScale(
      double x1, double x2, double y1, double y2) {
    mx = (y2 - y1) / (x2 - x1);
    cx = y1 - mx * x1;

    return {"m": mx, "c": cx};

    /// Other forms of an equation of a straight line.
    //y-y1 = (y2-y1)/(x2-x1)*(x-x1)
    //y = m*(x-x1)+y1
    //y = m*x-m*x1+y1
    //y = m*x+y1-m*x1
    //y = mx + c
  }

  @override
  void paint(Canvas canvas, Size size) {
    Map<String, dynamic> mValues = _computeMinMax(data);

    double? ymin = mValues["ymin"];
    double? ymax = mValues["ymax"];
    double? xmin = mValues["xmin"];
    double? xmax = mValues["xmax"];

    Paint paint = makePaint(Colors.black, strokeWidth: 2.0);

    double xInset0 = 15;
    double xInset1 = 15;
    double yInset0 = 40;
    double yInset1 = 40;

    double x0 = xInset0;
    double y0 = yInset0;
    double x1 = size.width - xInset1;
    double y1 = size.height - yInset1;

    Map<String, dynamic> mxcx = computeScale(xmin!, xmax!, x0, x1);
    Map<String, dynamic> mycy = computeScale(ymin!, ymax!, y1, y0);

    Paint p1 = Paint()
      ..color = Colors.indigo.shade100
      ..style = PaintingStyle.fill
      ..strokeWidth = 2;
    Rect drawingArea = Rect.fromLTRB(x0, y0, x1, y1);

    drawXGrid(canvas, mxcx, y0, y1);
    drawYGrid(canvas, mycy, x0, x1, 250);

    canvas.drawRect(drawingArea, paint);

    canvas.clipRect(drawingArea);

    drawPolygonCurve(canvas, mxcx, mycy, [d0, d4], p1);

    Paint p2 = Paint()
      ..color = Colors.indigo.shade300
      ..style = PaintingStyle.fill
      ..strokeWidth = 2;

    drawPolygonCurve(canvas, mxcx, mycy, [d1, d3], p2);

    drawMiddleCurve(canvas, mxcx, mycy);
    canvas.clipRect(Rect.fromLTRB(0, 0, size.width, size.height));

    // drawText(canvas, "This is a graph", 100, 200, 3.1415 / 2 * 3);
  }

  void drawXGrid(Canvas canvas, Map mxcx, double y0, double y1) {
    Paint paint = makePaint(Colors.black, strokeWidth: 1.0);
    Path p = Path();
    double mx = mxcx["m"];
    double cx = mxcx["c"];
    double delta = 86400.0 / 8;
    List labels = [
      "12am",
      "3am",
      "6am",
      "9am",
      "12pm",
      "3pm",
      "6pm",
      "9pm",
      "12am",
    ];

    int k = 0;
    for (double x = 0; x < 86400.0 + delta; x += delta) {
      double px = mx * x + cx;
      p.moveTo(px, y0);
      p.lineTo(px, y1);
      drawText(canvas, labels[k++], px - 15, y1 + 15, 0);
    }
    canvas.drawPath(p, paint);
  }

  void drawYGrid(Canvas canvas, Map mycy, double x0, double x1, double yDelta) {
    Paint paint = makePaint(Colors.black, strokeWidth: 0.5);

    Path p = Path();
    double mx = mycy["m"];
    double cx = mycy["c"];
    double delta = yDelta / 4;

    for (double x = delta; x < 200.0; x += delta) {
      double py = mx * x + cx;
      p.moveTo(x0, py);
      p.lineTo(x1, py);
    }
    canvas.drawPath(p, paint);
  }

  void drawMiddleCurve(Canvas canvas, Map mxcx, Map mycy) {
    Paint paint1 = Paint()
      ..color = Colors.black
      ..style = PaintingStyle.stroke
      ..strokeWidth = 1;

    Path p = Path();
    double mx = mxcx["m"];
    double cx = mxcx["c"];
    double my = mycy["m"];
    double cy = mycy["c"];
    for (int i = 0; i < timeData.length; i++) {
      double px = mx * timeData[i] + cx;
      double py = my * d2[i] + cy;
      if (i == 0)
        p.moveTo(px, py);
      else
        p.lineTo(px, py);
    }
    canvas.drawPath(p, paint1);
  }

  void drawPolygonCurve(Canvas canvas, Map mxcx, Map mycy,
      List<List<double>> listData2, Paint paint1) {
    List<double> y1 = listData2[0];
    List<double> y2 = listData2[1];

    Path p = Path();
    double mx = mxcx["m"];
    double cx = mxcx["c"];
    double my = mycy["m"];
    double cy = mycy["c"];
    for (int i = 0; i < timeData.length; i++) {
      double px = mx * timeData[i] + cx;
      double py = my * y1[i] + cy;
      if (i == 0)
        p.moveTo(px, py);
      else
        p.lineTo(px, py);
    }

    for (int i = timeData.length - 1; i >= 0; i--) {
      double px = mx * timeData[i] + cx;
      double py = my * y2[i] + cy;
      p.lineTo(px, py);
    }

    canvas.drawPath(p, paint1);
  }

  List<double> _gridDesignFloat(double vmin, double vmax, double n) {
    double dx = vmax - vmin;
    double d = dx / n;
    double lg = log(d) / log(10);
    double flog = lg.floorToDouble();
    double v = pow(10.0, flog).toDouble();
    double sc = d / v;

    //sc is in range 1.xx - 9.999

    double scv = 0;
    if (sc < 1.5) {
      scv = v * 1;
    } else if (sc < 2.25) {
      scv = v * 2.0;
    } else if (sc < 3.75) {
      scv = v * 2.5;
    } else if (sc < 7.5) {
      scv = v * 5.0;
    } else {
      scv = v * 10.0;
    }

    double vvmin = (vmin / scv).ceil() * scv;
    double vvmax = (vmax / scv).floor() * scv;
    return [scv, vvmin, vvmax];
  }

  _sinusoid(Canvas canvas, Offset x0y0, Size size) {
    Paint paint = makePaint(Colors.black, strokeWidth: 2.0);
    Path path = Path();
    double offsetY = x0y0.dy + size.height / 2;
    double amplitude = size.height / 2;
    path.moveTo(x0y0.dx, offsetY);

    for (double t = 0; t < 1.01; t += 0.01) {
      double sine = offsetY - amplitude * math.sin(2 * math.pi * 3 * t);
      path.lineTo(x0y0.dx + t * size.width, sine);
      // path.moveTo(x0y0.dx + t * size.width, sine);
    }

    canvas.drawPath(path, paint);

    // drawPolygon(canvas, size);
  }

  Paint makePaint(Color color,
      {PaintingStyle style = PaintingStyle.stroke, double strokeWidth = 1.0}) {
    Paint paint = Paint()
      ..color = color
      ..style = style
      ..strokeWidth = strokeWidth;
    return paint;
  }

  // void drawPolygon(Canvas canvas, Size size) {
  //   Path leftEyePath = Path();
  //   leftEyePath.moveTo(leftEye[0].getX(), leftEye[0].getY()); //starting point
  //   for (int i = 1; i < leftEye.length; i++) {
  //     leftEyePath.lineTo(leftEye[i].getX(), leftEye[i].getY());
  //   }
  //   canvas.drawPath(leftEyePath, painter);
  // }

  void drawText(Canvas context, String name, double x, double y,
      double angleRotationInRadians) {
    context.save();
    context.translate(x, y);
    context.rotate(angleRotationInRadians);
    TextSpan span = new TextSpan(
        style: new TextStyle(
            color: Colors.black, fontSize: 12.0, fontFamily: 'Roboto'),
        text: name);
    TextPainter tp = new TextPainter(
        text: span,
        textAlign: TextAlign.left,
        textDirection: TextDirection.ltr);
    tp.layout();
    tp.paint(context, new Offset(0.0, 0.0));
    context.restore();
  }
}
