import 'model.dart';
import 'response.dart';

class APIResp<T> {
  bool isSuccess;
  T? data;

  APIResp(this.isSuccess, this.data);
}

class SystemApiService {
  static Future<APIResp> getPatients(req) async {
    PatientList items = PatientList.fromMap(response["data"]);
    return APIResp<PatientList>(
        true, PatientList(items.data, items.totalRecords));
  }
}

int loginFunc(a, b, c) {
  return 3;
}
