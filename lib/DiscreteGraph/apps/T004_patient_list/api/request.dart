Map<String, dynamic> request = {
  "columns": [
    "user_id",
    "employee_no",
    "image",
    "full_name",
    "user.primary_group.group_name",
    "gender",
    "mobile_phone",
    "date_added",
    "date_updated"
  ],
  "limit": 100,
  "offset": 0,
  "where": {
    "children": [
      {"column": "employee_id", "op": "ne", "search": "15"}
    ],
    "group": "and"
  }
};
