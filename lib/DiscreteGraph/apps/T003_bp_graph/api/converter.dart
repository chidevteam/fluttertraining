import 'dart:io';
import 'response.dart';
import 'dart:convert';

void main() {
  var graphData = response['data'];
  var data = {'vital_graph_data': []};
  for (var key in graphData.keys) {
    var ranges = graphData[key]['ranges'];
    var stats = graphData[key]['stats'];
    var trendPoints = graphData[key]['trend_points'];
    var values = graphData[key]['values'];

    data['vital_graph_data']!.add({
      'title': graphData[key]['vital_name'],
      'time_start': values[0]['date_added'],
      'time_end': values[values.length - 1]['date_added'],
      'vitals': [
        {
          'name': ranges['name'],
          'ranges': [
            ranges['warn_low_end_value'],
            ranges['warn_low_value'],
            ranges['normal_low_value'],
            ranges['normal_high_value'],
            ranges['warn_high_value'],
            ranges['warn_high_end_value']
          ],
          'trend_line': graphData[key].containsKey('trend_points') &&
                  graphData[key]['trend_points'].isNotEmpty
              ? {
                  't1': trendPoints[0]['start_point']['date'],
                  'v1': trendPoints[0]['start_point']['value'],
                  't2': trendPoints[1]['end_point']['date'],
                  'v2': trendPoints[1]['end_point']['value'],
                }
              : null,
          'stats': {
            'min': stats['min'],
            'max': stats['max'],
            'mean': stats['mean']
          },
          'data': graphValues(values),
        },
      ],
    });
  }
  writeToFile(data);
}

void writeToFile(data) async {
  JsonEncoder encoder = new JsonEncoder.withIndent('  ');
  String prettyPrintData = encoder.convert(data);
  final filename = 'data.json';
  var file = await File(filename).writeAsString(prettyPrintData);
}

List graphValues(List values) {
  var data = [];
  for (var i = 0; i < values.length; i++) {
    data.add([
      values[i]['date_added'],
      values[i]['value'],
      values[i]['min_value'],
      values[i]['max_value']
    ]);
  }
  return data;
}
