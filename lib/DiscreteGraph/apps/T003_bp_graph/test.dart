// import 'dart:io';

// import 'dart:io';

import '../../data/bp.dart';

void main() {
  Map<String, dynamic> data = bpData["data"];
  print(data.keys);

  Map graphData = {};
  for (String key in data.keys) {
    Map<String, dynamic> m = {};
    graphData[key] = m;

    processOrderable(data[key], graphData[key]);
    print(graphData[key]);
  }
}

void processOrderable(Map<String, dynamic> mp, Map gData) {
  gData["vital_name"] = mp["vital_name"];
  gData["measurement_name_to_display"] = mp["measurement_name_to_display"];
  gData["measurement_unit"] = mp["measurement_unit"];
  gData["ranges"] = mp["ranges"];
  gData["stats"] = mp["stats"];
  gData["trend_points"] = mp["trend_points"];

  // exit(0);

  gData["values"] = {'x': [], 'y': [], 'ymin': [], 'ymax': []};
  for (Map value in mp["values"]) {
    gData["values"]["x"].add(value['date_added']);
    gData["values"]["y"].add(value['value']);
    gData["values"]["ymin"].add(value['min_value']);
    gData["values"]["ymax"].add(value['max_value']);
  }
}
