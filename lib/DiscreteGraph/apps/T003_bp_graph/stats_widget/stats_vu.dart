import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
// import 'chart_stat_values.dart';

class ChartStatDrop extends StatefulWidget {
  @override
  _ChartStatDrop createState() => _ChartStatDrop(
      name, Color, lowervalue, greatervalue, minvalue, maxvalue, meanvalue);

  late String name;
  final Color;
  late double lowervalue;
  late double greatervalue;
  late double minvalue;
  late double maxvalue;
  late double meanvalue;

  ChartStatDrop(this.name, this.Color, this.lowervalue, this.greatervalue,
      this.minvalue, this.maxvalue, this.meanvalue);
}

class _ChartStatDrop extends State<ChartStatDrop> {
  bool isTapped = true;
  bool isExpanded = false;

  late String name;
  final Color;
  late double lowervalue;
  late double greatervalue;
  late double minvalue;
  late double maxvalue;
  late double meanvalue;

  _ChartStatDrop(this.name, this.Color, this.lowervalue, this.greatervalue,
      this.minvalue, this.maxvalue, this.meanvalue);

  @override
  Widget build(BuildContext context) {
    // ChartStatDrop call = ChartStatDrop(
    //     name, lowervalue, greatervalue, minvalue, maxvalue, meanvalue);

    // ChartStatValue value = ChartStatValue(call.name, 'Lower:', 0.01,
    //     'Greater:', 0.01, 'Min:', 74.00, 'Max:', 77.00, 'Mean:', 75.50);

    return Scaffold(
      body: Center(
        child: Column(
          children: [
            SizedBox(
              height: 50,
            ),
            InkWell(
              highlightColor: Colors.transparent,
              splashColor: Colors.transparent,
              onTap: () {
                setState(() {
                  isTapped = !isTapped;
                });
              },
              onHighlightChanged: (value) {
                setState(() {
                  isExpanded = value;
                });
              },
              child: AnimatedContainer(
                duration: Duration(milliseconds: 1000),
                curve: Curves.fastLinearToSlowEaseIn,
                height: isTapped
                    ? isExpanded
                        ? 105
                        : 110
                    : isExpanded
                        ? 105
                        : 110,
                // width: isExpanded ? 385 : 390,
                decoration: BoxDecoration(
                  color: Colors.white,
                  // borderRadius: BorderRadius.all(Radius.circular(20)),
                  // boxShadow: [
                  //   BoxShadow(
                  //     color: Colors.black.withOpacity(0.5),
                  //     blurRadius: 20,
                  //     offset: Offset(0, 20),
                  //   ),
                  // ],
                ),
                padding: EdgeInsets.all(20),
                child: isTapped
                    ? Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.elliptical(15, 15),
                                  topRight: Radius.elliptical(15, 15)),
                              border: Border.all(color: Colors.black, width: 2),
                              color: Colors.black,
                            ),
                            // color: Colors.red,
                            child: Padding(
                              padding: const EdgeInsets.only(left: 5),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Expanded(
                                    flex: 2,
                                    child: Text(
                                      '$name',
                                      style: TextStyle(
                                          color: Color,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                  Expanded(
                                    flex: 3,
                                    child: Text(
                                      'Lower: $lowervalue %',
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                  Expanded(
                                      flex: 3,
                                      child: Text('Greater: $greatervalue %',
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontWeight: FontWeight.bold))),
                                  Expanded(
                                    flex: 1,
                                    child: Icon(
                                      isTapped
                                          ? Icons.keyboard_arrow_down
                                          : Icons.keyboard_arrow_up,
                                      color: Colors.white,
                                      size: 27,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      )
                    : Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.elliptical(15, 15),
                                  topRight: Radius.elliptical(15, 15)),
                              border: Border.all(color: Colors.black, width: 2),
                              color: Colors.black,
                            ),
                            //color: Colors.red,
                            child: Padding(
                              padding: const EdgeInsets.only(left: 5),
                              child: Expanded(
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Expanded(
                                      flex: 2,
                                      child: Text(
                                        '$name',
                                        style: TextStyle(
                                            color: Color,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                    Expanded(
                                      flex: 3,
                                      child: Text(
                                        'Lower: $lowervalue %',
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                    Expanded(
                                        flex: 3,
                                        child: Text('Greater: $greatervalue %',
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontWeight: FontWeight.bold))),
                                    Expanded(
                                      flex: 1,
                                      child: Icon(
                                        isTapped
                                            ? Icons.keyboard_arrow_down
                                            : Icons.keyboard_arrow_up,
                                        color: Colors.white,
                                        size: 27,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                          Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.only(
                                bottomLeft: Radius.elliptical(15, 15),
                                bottomRight: Radius.elliptical(15, 15),
                              ),
                              border: Border.all(color: Colors.black, width: 2),
                              color: Colors.white,
                            ),
                            child: Expanded(
                              child: Row(
                                //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Expanded(
                                    flex: 3,
                                    child: Text(
                                      isTapped ? '' : 'Min: $minvalue',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                  Expanded(
                                      flex: 3,
                                      child: Text(
                                        'Max: $maxvalue',
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                        ),
                                      )),
                                  Expanded(
                                      flex: 4,
                                      child: Text(
                                        'Mean: $meanvalue',
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold),
                                      )),
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
