import 'dart:io';

import 'package:flutter/material.dart';
import 'package:universal/DiscreteGraph/apps/T003_bp_graph/api/modified_data_for_graphs.dart';
import 'package:universal/DiscreteGraph/data/data.dart';
import '../../lib/discrete_graph.dart';
import 'stats_widget/stats_vu.dart';

void main() {
  runApp(BPGraph());
}

// ignore: must_be_immutable
class BPGraph extends StatelessWidget {
  late Map m_data1;
  late Map m_data2;
  late Map m_data3;
  List graphData = [];

  Map createBaseDict() {
    Map responseData = chartsData;
    return responseData;
  }

  initVars() {
    /// needs to be removed later
    // graphData = [];
    m_data3 = createBaseDict();
    graphData = chartsData['vital_graph_data'];
  }

  @override
  Widget build(BuildContext context) {
    initVars();
    return MaterialApp(
      title: 'Flutter Art',
      theme: ThemeData.light(),
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        backgroundColor: Color.fromARGB(255, 80, 80, 80),
        appBar: AppBar(
          title: Text('Scicharts Zoom/Pan'),
          backgroundColor: Colors.green.shade600,
        ),
        body: makeGraphs(graphData),
      ),
    );
  }

  Widget makeGraphs(List graphData) {
    List<Widget> widgetList = [];
    for (var d in graphData) {
      widgetList.add(Expanded(
          child: Center(
              child: Column(
        children: [
          Expanded(
            child: DiscreteGraph(data: d),
          ),
          // Expanded(
          //   child: ChartStatDrop(
          //     "Widget",
          //     Colors.red,
          //     70,
          //     100,
          //     69,
          //     101,
          //     80,
          //   ),
          // ),
        ],
      ))));
    }
    return Column(children: widgetList);
  }
}
