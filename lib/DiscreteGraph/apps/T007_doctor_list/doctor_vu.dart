import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'doctor_vm.dart';

class DoctorListScreen extends ViewModelBuilderWidget<DoctorListViewModel> {
  DoctorListScreen({Key? key}) : super(key: key) {}

  @override
  DoctorListViewModel viewModelBuilder(BuildContext context) {
    var vm = DoctorListViewModel();

    vm.refreshData(context);
    return vm;
  }

  @override
  Widget builder(
      BuildContext context, DoctorListViewModel viewModel, Widget? child) {
    return Scaffold(
        appBar: AppBar(),
        body: RefreshIndicator(
            onRefresh: () async {
              await viewModel.onRefreshCalled(context);
            }, //viewModel.onRefreshCalled(context),
            child: Column(children: [
              TextField(
                onChanged: (text) {
                  viewModel.refreshData(context, searchText: text);
                  print('First text field: $text');
                },
              ),
              Expanded(child: DoctorListView(viewModel))
            ])));
  }

  Widget DoctorListView(DoctorListViewModel viewModel) {
    return ListView.builder(
      itemCount: viewModel.items!.length,
      itemBuilder: (BuildContext context, int index) {
        // return Container(color: Colors.red);
        return Column(children: [
          Text(viewModel.items![index].full_name.toString()),
          Text(viewModel.items![index].gender.toString())
        ]);
        // dateHeader(context, viewModel.items![index]);
      },
    );
  }
}
