Map<String, dynamic> request = {
  "app_type": "Patient",
  "episode_id": 228,
  "columns": [
    "user_id",
    "employee_no",
    "image",
    "full_name",
    "user.primary_group.group_name",
    "gender",
    "mobile_phone",
    "date_added",
    "date_updated"
  ],
  "limit": 10,
  "offset": 0,
  "app_version": "3.2.10-000",
  "os_name": "Android"
};
