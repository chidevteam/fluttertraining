import 'package:flutter/material.dart';
import 'patient_vu.dart';

void main() async {
  // loginFunc();
  // await loginFunc('charms-qa', "salmancd", "Salman@1");
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'CHI Charts',
      theme: ThemeData.light(),
      debugShowCheckedModeBanner: false,
      home: PatientListScreen(),
      // home: Wrapper(),
    );
  }
}
