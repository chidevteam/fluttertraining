import 'package:stacked/stacked.dart';
import 'package:flutter/material.dart';

// import 'package:universal/Tutorials/DART/T004_Network/lib/models/patient.dart';
// import 'package:universal/Tutorials/DART/T004_Network/lib/network/system_api_service.dart';
import 'api/model.dart';
import 'api/system_api_service.dart';

class RecentResultDetailViewModel extends BaseViewModel {
  List<Patient>? items = [];
  int limit = 30;
  int offset = 0;
  int totalRecord = 0;

  RecentResultDetailViewModel() {
    login();
  }

  login() async {
    await loginFunc('charms-qa', "kashifd", "Kashif123@");
    await refreshData();
  }

  onRefreshCalled(BuildContext context) async {
    print("Refresh called .........&&&&&&&&&");
    items?.clear();
    limit = 30;
    offset = 0;
    totalRecord = 0;
    refreshData();
  }

  refreshData({bool isPaginated = false}) async {
    print(".....................in api fuc");

    if (!isPaginated) {
      setBusy(true);
    }

    final resp = await SystemApiService.getPatients({
      "columns": [
        "user_id",
        "employee_no",
        "image",
        "full_name",
        "user.primary_group.group_name",
        "gender",
        "mobile_phone",
        "date_added",
        "date_updated"
      ],
      "limit": 100,
      "offset": 0,
      "where": {
        "children": [
          {"column": "employee_id", "op": "ne", "search": "15"}
        ],
        "group": "and"
      }
    });
    // print("$resp");
    // for (int i = 0; i < resp.data!.data!.length; i += 1)
    //   print('${resp.data!.data![i].userName}');

    // final resp = await SystemApiServiceNew.getHistoryVitals(req);

    if (resp.isSuccess) {
      if (!isPaginated) {
        setBusy(false);
      }
      print(".....................success");
      items!.addAll(resp.data!.data!);
      totalRecord = resp.data!.totalRecords!;
      print('totalRecords-----> $totalRecord');
      notifyListeners();
    } else {
      print(".....................fail");
      if (!isPaginated) {
        setBusy(false);
      }
      // print("")
      // showAlert(context,
      //     title: 'Data Fetched Failed',
      //     message: resp.errorMessage ?? 'Error occurred while getting data.');
    }
  }

  void getMoreRecords(int index, BuildContext context) async {
    print('------------------------------More Data , $index');
    if (index >= limit) {
      offset += 30;
      limit += 30;
      print('limit-----> $limit ------ offset------> $offset ');
      await refreshData(isPaginated: true);
    }
  }
}
