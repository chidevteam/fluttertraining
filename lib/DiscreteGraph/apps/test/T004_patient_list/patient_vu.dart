import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'patient_vm.dart';

class PatientListScreen
    extends ViewModelBuilderWidget<RecentResultDetailViewModel> {
  PatientListScreen({Key? key}) : super(key: key) {}

  @override
  RecentResultDetailViewModel viewModelBuilder(BuildContext context) {
    var vm = RecentResultDetailViewModel();

    return vm;
  }

  @override
  Widget builder(BuildContext context, RecentResultDetailViewModel viewModel,
      Widget? child) {
    return Scaffold(
        appBar: AppBar(),
        body: RefreshIndicator(
          onRefresh: () async {
            await viewModel.onRefreshCalled(context);
          }, //viewModel.onRefreshCalled(context),
          child: ListView.builder(
            itemCount: viewModel.items!.length,
            itemBuilder: (BuildContext context, int index) {
              // return Container(color: Colors.red);
              return Column(children: [
                Text(viewModel.items![index].firstName.toString()),
                Text(viewModel.items![index].hospitalNo.toString())
              ]);
              // dateHeader(context, viewModel.items![index]);
            },
          ),
        ));
  }
}
