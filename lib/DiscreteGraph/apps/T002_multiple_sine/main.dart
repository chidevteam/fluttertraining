import 'package:flutter/material.dart';
import '../../lib/discrete_graph.dart';
import 'dart:math' as math;
// import 'dart:async';
// import 'dart:html';

void main() {
  runApp(MultipleSinGraphs());
}

// ignore: must_be_immutable
class MultipleSinGraphs extends StatelessWidget {
  late Map<String, List<double>> m_data1;
  late Map<String, List<double>> m_data2;
  late Map<String, List<double>> m_data3;
  double tx = 0;

  initVars() {
    tx += 0.01;
    m_data1 = {"x": [], "y": []};
    m_data2 = {"x": [], "y": []};
    m_data3 = {"x": [], "y": []};
    for (double t = 1 + tx; t < 3 + tx; t += 0.001) {
      double x = t;
      double y = 3 +
          math.exp(-t) *
              (1 * math.sin(2 * math.pi * 1 * t) +
                  2 * math.sin(2 * math.pi * 3 * t));
      m_data3["x"]!.add(x);
      m_data3["y"]!.add(y);
    }

    for (double t = 1 + tx; t < 3 + tx; t += 0.001) {
      double x = t;
      double y = 3 +
          1 * math.sin(2 * math.pi * 1 * t) +
          .5 * math.sin(2 * math.pi * 3 * t + .4);
      m_data2["x"]!.add(x);
      m_data2["y"]!.add(y);
    }

    for (double t = 1 + tx; t < 3 + tx; t += 0.001) {
      double x = t;
      double y = 3 + 1 * math.sin(2 * math.pi * 1 * t);
      // 1 * math.sin(2 * math.pi * 3 * t);
      m_data1["x"]!.add(x);
      m_data1["y"]!.add(y);
    }
    // print("ues");
  }

  @override
  Widget build(BuildContext context) {
    // window.document.onContextMenu.listen((evt) => evt.preventDefault());
    initVars();
    // const oneSec = Duration(milliseconds: 100);
    // Timer.periodic(oneSec, (Timer t) => initVars());
    return MaterialApp(
      title: 'Flutter Art',
      theme: ThemeData.light(),
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        backgroundColor: Color.fromARGB(255, 80, 80, 80),
        appBar: AppBar(
          title: Text('Scicharts Zoom/Pan'),
          backgroundColor: Colors.green.shade600,
        ),
        body: Column(children: [
          Expanded(
              child: Center(
                  child: Row(
            children: [
              DiscreteGraph(width: 200, data: m_data1),
              DiscreteGraph(width: 200, data: m_data2),
            ],
          ))),
          Expanded(child: Center(child: DiscreteGraph(data: m_data3))),
          Expanded(child: Center(child: DiscreteGraph(data: m_data3))),
        ]),
      ),
    );
  }

  //  @override
  // Widget build(BuildContext context) {
  //   initVars();
  //   // const oneSec = Duration(milliseconds: 100);
  //   // Timer.periodic(oneSec, (Timer t) => initVars());
  //   return MaterialApp(
  //     title: 'Flutter Art',
  //     theme: ThemeData.light(),
  //     debugShowCheckedModeBanner: false,
  //     home: Scaffold(
  //       backgroundColor: Color.fromARGB(255, 80, 80, 80),
  //       appBar: AppBar(
  //         title: Text('Scicharts Zoom/Pan'),
  //         backgroundColor: Colors.green.shade600,
  //       ),
  //       body: Column(children: [
  //         Expanded(
  //             child: Center(
  //                 child: Row(
  //           children: [
  //             DiscreteGraph(width: 200, data: m_data1),
  //             DiscreteGraph(width: 200, data: m_data2),
  //           ],
  //         ))),
  //         // Divider(height: 1.0),
  //         Expanded(child: Center(child: DiscreteGraph(data: m_data3))),
  //       ]),
  //     ),
  //   );
  // }
}
