import 'package:stacked/stacked.dart';
import 'package:universal/Tutorials/DART/T004_Network/lib/network/system_api_service.dart';

import '../../../../Tutorials/DART/T004_Network/lib/models/requests.dart';

class HistoricalGraphsListViewModel extends BaseViewModel {
  List<int> selectedIndices = [];
  bool isSelected = false;
  int currentIndex = 0;
  List<String> historical = [];
  List<String> historicalGraphs = [];
  Map<String, dynamic> dashboardData = {};
  Map<String, dynamic>? graphData;
  bool isGraphSelected = false;
  onTap(int index) {
    currentIndex = index;
    if (selectedIndices.contains(index)) {
      selectedIndices.remove(index);
    } else {
      selectedIndices.add(index);
    }

    notifyListeners();
  }

  bool isVisible(int index) {
    return selectedIndices.contains(index);
  }

  Future<ApiPayload> makeRecentResultGraphRequest(
      String? episodeId, double? from, double? to, String? vitalName) async {
    final Map<String, dynamic> req = {
      "episode_id": episodeId,
      "from": from,
      "to": to,
      "vital_name": vitalName,
    };
    return req;
  }

  getData() async {
    var to = DateTime.now().millisecondsSinceEpoch;
    double from = to - (2629743 * 1000); // 30*864000
    setBusy(true);
    final requestPayload = await makeRecentResultGraphRequest(
      '171', 83519000, // from.toDouble(),
      1644519599, // 1644519599,
      'Blood pressure monitor',
    );
    var resp = await SystemApiService.getRecentResultGraphData(requestPayload);
    if (resp.isSuccess) {
      setBusy(false);
      // vmBusy = false;
      graphData = resp.data!.vvData;
      isGraphSelected = resp.data!.isSelected;
      print('graphData : $graphData');
      print('isGraphSelected : $isGraphSelected');
    } else {
      print('Problems');
      print('errorCode : ${resp.errorCode}');
      print('errorMessage : ${resp.errorMessage}');
      ;
    }

    dataPreProcessing();
    notifyListeners();
  }

  dataPreProcessing() {
    for (Map<String, dynamic> vData in graphData!['vital_graph_data']) {
      historical.add(vData['title']);
      for (Map<String, dynamic> vitals in vData['vitals']) {
        historicalGraphs.add(vitals['name']);
      }
      dashboardData[vData['title']] = historicalGraphs;
    }

    print('dashboardData :$dashboardData');
  }
}
