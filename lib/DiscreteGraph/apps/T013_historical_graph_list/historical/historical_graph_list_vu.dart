import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import '../widgets/graph_placeholder.dart';
import 'historical_graph_list_vm.dart';

class HistoricalGraphsList
    extends ViewModelBuilderWidget<HistoricalGraphsListViewModel> {
  HistoricalGraphsList({Key? key}) : super(key: key) {}

  @override
  HistoricalGraphsListViewModel viewModelBuilder(BuildContext context) {
    HistoricalGraphsListViewModel vm = HistoricalGraphsListViewModel();
    vm.getData();
    return vm;
  }

  @override
  Widget builder(BuildContext context, HistoricalGraphsListViewModel viewModel,
      Widget? child) {
    return Scaffold(
        body: SizedBox(
            height: 100000,
            width: 500,
            child: Column(
              children: [
                Expanded(flex: 1, child: _listH(viewModel)),
                Expanded(flex: 5, child: _listV(viewModel))
              ],
            )));
  }

  _listH(HistoricalGraphsListViewModel viewModel) {
    return ListView.builder(
      itemCount: viewModel.dashboardData.keys.length,
      scrollDirection: Axis.horizontal,
      itemBuilder: (context, index) {
        return InkWell(
          onTap: () {
            viewModel.onTap(index);
            viewModel.isSelected = !viewModel.isSelected;
          },
          child: SizedBox(
            width: MediaQuery.of(context).size.height * 0.2,
            height: 50,
            child: Card(
              // color:
              // viewModel.isSelected &&
              //         (viewModel.selectedIndices[index] == index)
              //     ? Colors.red
              //     : Colors.white,
              child: Text(viewModel.dashboardData.keys.toList()[index]),
            ),
          ),
        );
      },
    );
  }

  _listV(HistoricalGraphsListViewModel viewModel) {
    List<Widget> list = [];
    for (int i = 0; i < viewModel.dashboardData.values.length; i++) {
      list.add(
        Visibility(
          visible: viewModel.isVisible(i),
          child: GraphPlaceHolder(
            historical: viewModel.dashboardData.values.toList()[i].toString(),
          ),
        ),
      );
    }

    return SizedBox(
      height: 100000,
      child: SingleChildScrollView(child: Column(children: list)),
    );
  }
}
