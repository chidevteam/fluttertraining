import 'package:flutter/material.dart';

import 'historical/historical_graph_list_vu.dart';
import 'package:universal/Tutorials/DART/T004_Network/lib/network/system_api_service.dart';

void main() async {
  await loginFunc('charms-qa', "salmancd", "Salman@1");

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: Scaffold(
          appBar: AppBar(),
          body: HistoricalGraphsList(),
        ));
  }
}
