import 'package:flutter/material.dart';

class GraphPlaceHolder extends StatelessWidget {
  final String historical;
  const GraphPlaceHolder({Key? key, required this.historical})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: MediaQuery.of(context).size.height * 0.3,
      child: Card(
        child: Center(
          child: Text(historical),
        ),
      ),
    );
  }
}
