class ECGOneLead {
  List<double> ecg;
  int timestamp;
  String disease;
  double HR;
  double RR;
  // final int? totalRecords;

  ECGOneLead(this.ecg, this.timestamp, this.disease, this.HR, this.RR);

  static ECGOneLead fromMap(Map<String, dynamic> resp) {
    return ECGOneLead(
        resp["ecg"], resp["time"], resp["Disease"], resp["HR"], resp["RR"]);
  }

  String toString() {
    String st = "" + ecg.length.toString() + "\n";
    st += "$timestamp\n";
    st += "$disease\n";
    st += "$HR\n";
    st += "$RR\n";

    return st;
  }
}
