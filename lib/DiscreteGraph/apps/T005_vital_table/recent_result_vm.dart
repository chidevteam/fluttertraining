import 'package:stacked/stacked.dart';
import 'package:flutter/material.dart';

import 'api/request.dart';
// import 'model/recent_result_detail_model.dart';
// import 'api/system_api_service.dart';
import 'package:universal/Tutorials/DART/T004_Network/lib/models/recent_result_detail.dart';
import 'package:universal/Tutorials/DART/T004_Network/lib/network/system_api_service.dart';

class RecentResultDetailViewModel extends BaseViewModel {
  List<OrderableData>? items = [];
  int limit = 30;
  int offset = 0;
  int totalRecord = 0;

  onRefreshCalled(BuildContext context) {
    items?.clear();
    limit = 30;
    offset = 0;
    totalRecord = 0;
    refreshData(context);
  }

  refreshData(context, {bool isPaginated = false}) async {
    print(".....................in api fuc");

    if (!isPaginated) {
      setBusy(true);
    }

    // final req = makeResultRequest(); //Todo

    final resp = await SystemApiService.getVitalHistoryDataTabular(request);

    if (resp.isSuccess) {
      if (!isPaginated) {
        setBusy(false);
      }
      print(".....................success");
      items!.addAll(resp.data!.data!);
      totalRecord = resp.data!.totalRecords!;
      print('totalRecords-----> $totalRecord');
      notifyListeners();
    } else {
      print(".....................fail");
      if (!isPaginated) {
        setBusy(false);
      }
      // showAlert(context,
      //     title: 'Data Fetched Failed',
      //     message: resp.errorMessage ?? 'Error occurred while getting data.');
    }
  }

  void getMoreRecords(int index, BuildContext context) async {
    print('------------------------------More Data , $index');
    if (index >= limit) {
      offset += 30;
      limit += 30;
      print('limit-----> $limit ------ offset------> $offset ');
      await refreshData(context, isPaginated: true);
    }
  }
}

// class RecentResultDetailViewModel extends BaseViewModel {
//   List<OrderableData>? vitals = [];

//   getData() {
//     vitals!.add(OrderableData(1637648835, [
//       ResultableData('Systolic', 102, 'Normal'),
//       ResultableData('Diastolic', 102, 'Normal'),
//     ]));
//     vitals!.add(OrderableData(1637648835, [
//       ResultableData('Systolic', 102, 'Normal'),
//       ResultableData('Diastolic', 102, 'Normal'),
//     ]));
//     vitals!.add(OrderableData(1637648835, [
//       ResultableData('Systolic', 102, 'Normal'),
//       ResultableData('Diastolic', 102, 'Normal'),
//     ]));
//   }
// }