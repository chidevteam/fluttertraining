import "package:collection/collection.dart";
import 'dart:convert';
import 'dart:io';

// import 'input.dart';

void writeToFile(data, fileName) {
  JsonEncoder encoder = new JsonEncoder.withIndent('  ');
  String prettyPrintData = encoder.convert(data);
  // final fileName = 'data.dart';
  File(fileName).writeAsStringSync(
      'Map <String, dynamic> response = ' + prettyPrintData + ';');
}

void testSort() {
  List<Map<String, dynamic>> list = [
    {
      "date_time": 1628575591,
      "values": [
        {"name": "Pulse", "value": 79.0, "unit": "bpm", "status": "Normal"}
      ]
    },
    {
      "date_time": 1628575453,
      "values": [
        {"name": "Pulse", "value": 75.0, "unit": "bpm", "status": "Normal"}
      ]
    },
  ];

  list.sort((a, b) => a['date_time']!.compareTo(b['date_time']));
  print(list);
}

// main() {
//   Map<String, dynamic> recentResultData = dConverter(jsonData);
//   writeToFile(recentResultData, "../response.dart");
// }
