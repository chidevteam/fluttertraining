Map<String, dynamic> response = {
  "data": {
    "Pulse": {
      "values": [
        {
          "reading_id": 36958,
          "measurement_name": "Pulse",
          "measurement_unit": "bpm",
          "status": "Normal",
          "value": 79.0,
          "date_added": 1628575455,
        },
        {
          "reading_id": 36958,
          "measurement_name": "Pulse",
          "measurement_unit": "bpm",
          "status": "Normal",
          "value": 79.0,
          "date_added": 1628565455,
        },
        {
          "reading_id": 36958,
          "measurement_name": "Pulse",
          "measurement_unit": "bpm",
          "status": "Normal",
          "value": 79.0,
          "date_added": 1628465455,
        },
      ],
    },
    "Systolic": {
      "values": [
        {
          "reading_id": 36958,
          "measurement_name": "Systolic",
          "measurement_unit": "bpm",
          "status": "Normal",
          "value": 79.0,
          "date_added": 2628575455,
        },
        {
          "reading_id": 36958,
          "measurement_name": "Systolic",
          "measurement_unit": "bpm",
          "status": "Normal",
          "value": 79.0,
          "date_added": 2628565455,
        },
      ],
    },
    "Diastolic": {
      "values": [
        {
          "reading_id": 36958,
          "measurement_name": "Diastolic",
          "measurement_unit": "bpm",
          "status": "Normal",
          "value": 79.0,
          "date_added": 2628575455,
        },
        {
          "reading_id": 36958,
          "measurement_name": "Diastolic",
          "measurement_unit": "bpm",
          "status": "Warning High",
          "value": 179.0,
          "date_added": 2628565455,
        },
      ],
    },
  },
  "Status": "Ok",
  "Time": 1641632293.091,
  "Duration": 2.098
};
