import 'model.dart';
import 'response.dart';

class APIResp<T> {
  //OrderableList
  bool isSuccess;
  T? data;
  APIResp(this.isSuccess, this.data);

  String toString() {
    String st = "";
    st += "==== API Resp ====\n";
    st += " Success: $isSuccess \n";
    st += " Data: $data \n";
    return st;
  }
}

class SystemApiService {
  static Future<APIResp<OrderableList>> getVitalHistoryDataTabular(req) async {
    OrderableList orderableList = OrderableList.fromMap(response['data']);
    return APIResp<OrderableList>(true, orderableList);
  }
}

int loginFunc(a, b, c) {
  return 3;
}
