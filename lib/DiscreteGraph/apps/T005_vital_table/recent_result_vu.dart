import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
// import 'model/recent_result_detail_model.dart';
import 'package:universal/Tutorials/DART/T004_Network/lib/models/recent_result_detail.dart';
import 'recent_result_vm.dart';

class RecentResultDetailScreen
    extends ViewModelBuilderWidget<RecentResultDetailViewModel> {
  const RecentResultDetailScreen({Key? key}) : super(key: key);

  @override
  RecentResultDetailViewModel viewModelBuilder(BuildContext context) {
    var vm = RecentResultDetailViewModel();
    vm.refreshData(context);
    return vm;
  }

  @override
  Widget builder(BuildContext context, RecentResultDetailViewModel viewModel,
      Widget? child) {
    return Scaffold(
        appBar: AppBar(),
        body: ListView.builder(
          itemCount: viewModel.items!.length,
          itemBuilder: (BuildContext context, int index) {
            // return Container(color: Colors.red);
            return dateHeader(viewModel.items![index]);
          },
        ));
  }

  Column dateHeader(OrderableData vitals) {
    return Column(
      children: [
        Row(
          children: [
            const Expanded(
                child: Padding(
              padding: EdgeInsets.only(left: 10.0, right: 10.0),
              child: Divider(
                color: Color(0xFF6d6d6d),
              ),
            )),

            ///  TODO: Timestamp to date conversion later.
            Text(vitals.time.toString(),
                style: TextStyle(
                  color: Colors.grey,
                  fontWeight: FontWeight.bold,
                )),
            const Expanded(
                child: Padding(
              padding: EdgeInsets.only(left: 10.0, right: 10.0),
              child: Divider(
                color: Colors.grey,
              ),
            )),
          ],
        ),
        _listTile(vitals.values!)
      ],
    );
  }

  Column _listTile(List<ResultableData> values) {
    List<Widget> data = [];
    // return;
    for (int valueIdx = 0; valueIdx < values.length; valueIdx++) {
      data.add(Container(
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Row(
            children: [
              Expanded(
                  child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(values[valueIdx].name!,
                      style: TextStyle(
                        color: Color(0xFF387b96),
                        fontWeight: FontWeight.bold,
                      )),
                  ClipRRect(
                    borderRadius: BorderRadius.circular(20),
                    child: Container(
                      color: Colors.green,
                      padding: const EdgeInsets.fromLTRB(25, 5, 25, 5),
                      child: Text(values[valueIdx].status!,
                          style: TextStyle(color: Colors.white, fontSize: 12)),
                    ),
                  )
                ],
              )),
              Expanded(
                  child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Text('bpm',
                      style: TextStyle(
                          // fontWeight: FontWeight.bold,
                          color: Colors.black87,
                          fontSize: 16)),
                  Text(values[valueIdx].value.toString(),
                      style: TextStyle(
                          color: Colors.green,
                          fontWeight: FontWeight.bold,
                          fontSize: 18))
                ],
              ))
            ],
          ),
        ),
      ));
    }
    return Column(
      children: data,
    );
  }
}
