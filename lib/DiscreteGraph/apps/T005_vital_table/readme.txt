
- Date not correct in view

- And API returning success = false.0


Master Plan
- Make view friendly data structure -- done
- make Hard coded View -- done
- Make Model 
- Use Hard coded data to populate view --done 
- Convert API DS to Friendly DS -- done
- look  up logical pixels

11:43am: Made the data structure complete.


Background ::
==================================

A sample Network Layer is implemented which is almost equivalent to
actual Network layer in our Flutter apps.

A Login function is also working.

Process to make a complete screen:
===================================

Stage 1: Specifications
-----------------------
- Screen solves a problem : Reports data in a certain format
- Wireframing, Artist designs screen
- Data communication Prototol/json is defined (request, response)
  > A concrete sample request and response is defined
- Specs available in JIRA
  > View, Request, Response, required conditions, 
  > test process  
    (programmer can modify req/response to test different cases)


Stage 2: Backend Development
----------------------------
- Make new end point, .....
  .....

Stage 3: Angular Development
----------------------------

State 4: Flutter development
-------------------------------
Plan to make the following directory structure

    NETWORK
    --- LIB
        --- login.dart

    SCREEN
    --- main.dart
    --- screen_vu.dart
    --- screen_vm.dart
    --- MODEL
        --- model.dart
    --- API
        --- login.dart
        --- system_api_service.dart
        --- response.dart
        --- request.dart       
        --- main.dart
        --- DATA_CONVERTER
            --- dconverter.dart 
            --- undesired_response.dart
            --- main.dart

S1: Define the model(s): screen/model/model.dart
    
S2 [S1]: Implement dummy APIservice: screen/
    (can implement delayed response Future, etc)
    response = await SystemApiService.getDesiredData(request);

S3: Implement a Crude View in dummmy_api_service
    with hard coded data

S4 [S3, S2]: Use Dummy APIService and populate in CrudeView

S5 [S2]: Use the Code/models to give actual implementation of API

S6: The _vm can now just change the import line to use actual 
    systemAPIService


S7: Landscape portrait support

S8: Test by Developer

S9: Test by Peer

S10: Handover to friendly QA

S11: Handover to Hard QA

==============================================================================

- Write the API folder first, test on hardcoded response available in response.dart

- Make a copy of API folder
  test patient list

- get response.dart, request.dart
==============================================================================
9:00: 9:05  : Cleaned up code.

- value2, status2, in model
- if systolic is in there, then distolic is also in there.