Map<String, dynamic> response = {
  "data": {
    "data": [
      {
        "reading_id": 37737,
        "observation_id": 30939,
        "user_id": 27,
        "patient_id": 12,
        "episode_id": 8,
        "episode_no": "CHI-EP-0008",
        "name": "Junaid Bashir",
        "first_name": "Junaid",
        "first_name_enc": null,
        "last_name": "Bashir",
        "last_name_enc": null,
        "image": "",
        "hospital_no": "CHI-HN-0012",
        "is_live": false,
        "service_title": "Battery",
        "service_category": "Alert",
        "service": "Battery",
        "measurement_id": 127,
        "measurement_key": "Battery",
        "measurement_name": "Battery",
        "measurement_value": 25.0,
        "measurement_unit": "%",
        "numeric_value": 25.0,
        "text_value": null,
        "has_data_value": false,
        "status": "Warning",
        "opr_status": "Unhandled",
        "observation_time": null,
        "acquisition_time": 1641803572,
        "capture_time": 1641803572,
        "arrival_time": 1641803572,
        "reading_time": 1641803572,
        "device_id": null,
        "device_model": null
      },
      {
        "reading_id": 37736,
        "observation_id": 30938,
        "user_id": 22,
        "patient_id": 8,
        "episode_id": 4,
        "episode_no": "CHI-EP-0004",
        "name": "Muhammad Kashif Mehmood",
        "first_name": "Muhammad Kashif",
        "first_name_enc": null,
        "last_name": "Mehmood",
        "last_name_enc": null,
        "image": "https://dev05.cognitivehealthintl.com/files/58/MTkwMQ.jpeg",
        "hospital_no": "CHI-HN-0008",
        "is_live": false,
        "service_title": "Symptom Check",
        "service_category": "Symptom Check",
        "service": "Symptom Check",
        "measurement_id": 105,
        "measurement_key": "symptom_check",
        "measurement_name": "Symptom Check",
        "measurement_value": "itchy throat",
        "measurement_unit": "",
        "numeric_value": null,
        "text_value": "itchy throat",
        "has_data_value": false,
        "status": "Warning",
        "opr_status": "Unhandled",
        "observation_time": null,
        "acquisition_time": 1641469711,
        "capture_time": 1641469711,
        "arrival_time": 1641469711,
        "reading_time": 1641469711,
        "device_id": null,
        "device_model": null
      },
      {
        "reading_id": 37724,
        "observation_id": 30927,
        "user_id": 22,
        "patient_id": 8,
        "episode_id": 4,
        "episode_no": "CHI-EP-0004",
        "name": "Muhammad Kashif Mehmood",
        "first_name": "Muhammad Kashif",
        "first_name_enc": null,
        "last_name": "Mehmood",
        "last_name_enc": null,
        "image": "https://dev05.cognitivehealthintl.com/files/58/MTkwMQ.jpeg",
        "hospital_no": "CHI-HN-0008",
        "is_live": false,
        "service_title": "Battery",
        "service_category": "Alert",
        "service": "Battery",
        "measurement_id": 127,
        "measurement_key": "Battery",
        "measurement_name": "Battery",
        "measurement_value": 1.0,
        "measurement_unit": "%",
        "numeric_value": 1.0,
        "text_value": null,
        "has_data_value": false,
        "status": "Critical",
        "opr_status": "Unhandled",
        "observation_time": null,
        "acquisition_time": 1641361514,
        "capture_time": 1641361515,
        "arrival_time": 1641361514,
        "reading_time": 1641361515,
        "device_id": null,
        "device_model": null
      },
      {
        "reading_id": 37723,
        "observation_id": 30926,
        "user_id": 24,
        "patient_id": 9,
        "episode_id": 5,
        "episode_no": "CHI-EP-0005",
        "name": "Muhammad Daniyal Altaf",
        "first_name": "Muhammad Daniyal",
        "first_name_enc": null,
        "last_name": "Altaf",
        "last_name_enc": null,
        "image": "https://dev05.cognitivehealthintl.com/files/58/MjEyOQ.png",
        "hospital_no": "CHI-HN-0009",
        "is_live": false,
        "service_title": "Battery",
        "service_category": "Alert",
        "service": "Battery",
        "measurement_id": 127,
        "measurement_key": "Battery",
        "measurement_name": "Battery",
        "measurement_value": 1.0,
        "measurement_unit": "%",
        "numeric_value": 1.0,
        "text_value": null,
        "has_data_value": false,
        "status": "Critical",
        "opr_status": "Unhandled",
        "observation_time": null,
        "acquisition_time": 1641361074,
        "capture_time": 1641361074,
        "arrival_time": 1641361074,
        "reading_time": 1641361074,
        "device_id": null,
        "device_model": null
      },
      {
        "reading_id": 37688,
        "observation_id": 30901,
        "user_id": 706,
        "patient_id": 238,
        "episode_id": 238,
        "episode_no": "CHI-EP-0238",
        "name": "Daud Patient",
        "first_name": "Daud",
        "first_name_enc": null,
        "last_name": "Patient",
        "last_name_enc": null,
        "image": null,
        "hospital_no": "CHI-HN-0238",
        "is_live": false,
        "service_title": "Vital",
        "service_category": "Homecare",
        "service": "Temperature",
        "measurement_id": 25,
        "measurement_key": "TEMP",
        "measurement_name": "Temperature",
        "measurement_value": 38.8,
        "measurement_unit": "C",
        "numeric_value": 38.8,
        "text_value": null,
        "has_data_value": false,
        "status": "Warning High",
        "opr_status": "Unhandled",
        "observation_time": null,
        "acquisition_time": 1641278827,
        "capture_time": 1641278814,
        "arrival_time": 1641278827,
        "reading_time": 1641278827,
        "device_id": null,
        "device_model": null
      },
      {
        "reading_id": 37687,
        "observation_id": 30900,
        "user_id": 706,
        "patient_id": 238,
        "episode_id": 238,
        "episode_no": "CHI-EP-0238",
        "name": "Daud Patient",
        "first_name": "Daud",
        "first_name_enc": null,
        "last_name": "Patient",
        "last_name_enc": null,
        "image": null,
        "hospital_no": "CHI-HN-0238",
        "is_live": false,
        "service_title": "Vital",
        "service_category": "Homecare",
        "service": "Temperature",
        "measurement_id": 25,
        "measurement_key": "TEMP",
        "measurement_name": "Temperature",
        "measurement_value": 37.7,
        "measurement_unit": "C",
        "numeric_value": 37.7,
        "text_value": null,
        "has_data_value": false,
        "status": "Normal",
        "opr_status": "Un Remark",
        "observation_time": null,
        "acquisition_time": 1641278810,
        "capture_time": 1641278799,
        "arrival_time": 1641278810,
        "reading_time": 1641278810,
        "device_id": null,
        "device_model": null
      },
      {
        "reading_id": 37686,
        "observation_id": 30899,
        "user_id": 706,
        "patient_id": 238,
        "episode_id": 238,
        "episode_no": "CHI-EP-0238",
        "name": "Daud Patient",
        "first_name": "Daud",
        "first_name_enc": null,
        "last_name": "Patient",
        "last_name_enc": null,
        "image": null,
        "hospital_no": "CHI-HN-0238",
        "is_live": false,
        "service_title": "Vital",
        "service_category": "Homecare",
        "service": "Temperature",
        "measurement_id": 25,
        "measurement_key": "TEMP",
        "measurement_name": "Temperature",
        "measurement_value": 36.6,
        "measurement_unit": "C",
        "numeric_value": 36.6,
        "text_value": null,
        "has_data_value": false,
        "status": "Normal",
        "opr_status": "Un Remark",
        "observation_time": null,
        "acquisition_time": 1641278796,
        "capture_time": 1641278786,
        "arrival_time": 1641278796,
        "reading_time": 1641278796,
        "device_id": null,
        "device_model": null
      },
      {
        "reading_id": 37684,
        "observation_id": 30898,
        "user_id": 706,
        "patient_id": 238,
        "episode_id": 238,
        "episode_no": "CHI-EP-0238",
        "name": "Daud Patient",
        "first_name": "Daud",
        "first_name_enc": null,
        "last_name": "Patient",
        "last_name_enc": null,
        "image": null,
        "hospital_no": "CHI-HN-0238",
        "is_live": false,
        "service_title": "Vital",
        "service_category": "Homecare",
        "service": "Pulse & Saturation",
        "measurement_id": 12,
        "measurement_key": "Pulse",
        "measurement_name": "Pulse",
        "measurement_value": 100.0,
        "measurement_unit": "bpm",
        "numeric_value": 100.0,
        "text_value": null,
        "has_data_value": false,
        "status": "Warning High",
        "opr_status": "Unhandled",
        "observation_time": null,
        "acquisition_time": 1641278783,
        "capture_time": 1641278775,
        "arrival_time": 1641278783,
        "reading_time": 1641278783,
        "device_id": null,
        "device_model": null
      },
      {
        "reading_id": 37685,
        "observation_id": 30898,
        "user_id": 706,
        "patient_id": 238,
        "episode_id": 238,
        "episode_no": "CHI-EP-0238",
        "name": "Daud Patient",
        "first_name": "Daud",
        "first_name_enc": null,
        "last_name": "Patient",
        "last_name_enc": null,
        "image": null,
        "hospital_no": "CHI-HN-0238",
        "is_live": false,
        "service_title": "Vital",
        "service_category": "Homecare",
        "service": "Pulse & Saturation",
        "measurement_id": 13,
        "measurement_key": "SpO2",
        "measurement_name": "SpO2",
        "measurement_value": 105.0,
        "measurement_unit": "%",
        "numeric_value": 105.0,
        "text_value": null,
        "has_data_value": false,
        "status": "Critical High",
        "opr_status": "Unhandled",
        "observation_time": null,
        "acquisition_time": 1641278783,
        "capture_time": 1641278775,
        "arrival_time": 1641278783,
        "reading_time": 1641278783,
        "device_id": null,
        "device_model": null
      },
      {
        "reading_id": 37683,
        "observation_id": 30897,
        "user_id": 706,
        "patient_id": 238,
        "episode_id": 238,
        "episode_no": "CHI-EP-0238",
        "name": "Daud Patient",
        "first_name": "Daud",
        "first_name_enc": null,
        "last_name": "Patient",
        "last_name_enc": null,
        "image": null,
        "hospital_no": "CHI-HN-0238",
        "is_live": false,
        "service_title": "Vital",
        "service_category": "Homecare",
        "service": "Pulse & Saturation",
        "measurement_id": 13,
        "measurement_key": "SpO2",
        "measurement_name": "SpO2",
        "measurement_value": 95.0,
        "measurement_unit": "%",
        "numeric_value": 95.0,
        "text_value": null,
        "has_data_value": false,
        "status": "Normal",
        "opr_status": "Un Remark",
        "observation_time": null,
        "acquisition_time": 1641278770,
        "capture_time": 1641278762,
        "arrival_time": 1641278770,
        "reading_time": 1641278770,
        "device_id": null,
        "device_model": null
      }
    ],
    "total_records": 21544
  },
  "Status": "Ok",
  "Time": 1641811608.057,
  "Duration": 1.005
};
