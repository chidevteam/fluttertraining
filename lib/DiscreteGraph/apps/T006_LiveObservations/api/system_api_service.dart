import 'model.dart';
import 'response.dart';

class APIResp<T> {
  //OrderableList
  bool isSuccess;
  T? data;
  APIResp(this.isSuccess, this.data);
}

class SystemApiService {
  static Future<APIResp> getLiveObservations(req) async {
    LiveObservationList orderableList =
        LiveObservationList.fromMap(response['data']);
    return APIResp<LiveObservationList>(true, orderableList);
  }
}

int loginFunc(a, b, c) {
  return 3;
}
