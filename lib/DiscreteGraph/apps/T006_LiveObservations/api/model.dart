// import 'package:flutter/material.dart';

class LiveObservationList {
  final List<LiveObservation> data;
  final int totalRecords;

  LiveObservationList(
    this.data,
    this.totalRecords,
  );

  static LiveObservationList fromMap(Map<String, dynamic> resp) {
    return LiveObservationList(
      List<LiveObservation>.from(
          resp['data'].map((item) => LiveObservation.fromMap(item))),
      resp['total_records'] as int,
    );
  }
}

class LiveObservation {
  final int? readingId;
  final int? observationId;
  final int? userId;
  final int? patientId;
  final int? episodeId;
  final String? episodeNo;
  final String? name;
  final String? image;
  final String? hospitalNo;
  final bool? isLive;
  final String? serviceTitle;
  final String? serviceCategory;
  final String? service;
  final int? measurementId;
  final String? measurementName;
  final String? measurementKey;
  final String? measurementUnit;
  final double? numericValue;
  final String? textValue;
  final String? dataValue;
  final String? status;
  final String? oprStatus;
  final int? observationTime;
  final int? acquisitionTime;
  final int? captureTime;
  final int? arrivalTime;
  final int? readingTime;
  final int? deviceId;
  final String? deviceModel;
  final String? firstNameEnc;
  final String? lastNameEnc;
  final String? mobilePhoneEnc;
  final int? subjectId;
  final List<String>? urls;

  LiveObservation(
    this.readingId,
    this.observationId,
    this.userId,
    this.patientId,
    this.episodeId,
    this.episodeNo,
    this.name,
    this.image,
    this.hospitalNo,
    this.isLive,
    this.serviceTitle,
    this.serviceCategory,
    this.service,
    this.measurementId,
    this.measurementName,
    this.measurementKey,
    this.measurementUnit,
    this.numericValue,
    this.textValue,
    this.dataValue,
    this.status,
    this.oprStatus,
    this.observationTime,
    this.acquisitionTime,
    this.captureTime,
    this.arrivalTime,
    this.readingTime,
    this.deviceId,
    this.deviceModel,
    this.firstNameEnc,
    this.lastNameEnc,
    this.mobilePhoneEnc,
    this.subjectId,
    this.urls,
  );

  // Color getHighlightColor() {
  //   if (oprStatus == 'Normal' || oprStatus == 'Un Remark') {
  //     return const Color.fromRGBO(1, 176, 80, 1);
  //   } else if (oprStatus == 'Unhandled') {
  //     return const Color.fromRGBO(192, 0, 0, 1);
  //   } else if (oprStatus == 'Escalated') {
  //     return const Color.fromRGBO(196, 90, 16, 1);
  //   } else if (oprStatus == 'Responded') {
  //     return const Color.fromRGBO(255, 191, 0, 1);
  //   } else if (oprStatus == 'Closed') {
  //     return const Color.fromRGBO(1, 176, 239, 1);
  //   } else if (oprStatus == 'Being Handled') {
  //     return const Color.fromRGBO(82, 128, 53, 1);
  //   } else if (oprStatus == 'Invalid') {
  //     return const Color.fromRGBO(98, 97, 102, 1);
  //   }
  //   return Colors.black;
  // }

  String getImage() {
    if (serviceTitle == 'Vital') {
      if (service == 'Body Composition Analysis') {
        return 'https://s3.us-east-2.amazonaws.com/pubshare.chi.com/orderables/weight-scale-01.png';
      } else if (service == 'ECG') {
        return 'https://s3.us-east-2.amazonaws.com/pubshare.chi.com/orderables/ECG+-+8+Channel.png';
      } else if (service == 'Temperature') {
        return 'https://s3.us-east-2.amazonaws.com/pubshare.chi.com/orderables/Temperature.png';
      } else if (service == 'Hemoglobin') {
        return 'https://s3.us-east-2.amazonaws.com/pubshare.chi.com/orderables/Fasting+Blood+Glucose.pnghttps://s3.us-east-2.amazonaws.com/pubshare.chi.com/orderables/ECG+-+8+Channel.png';
      } else if (service == 'Tablet') {
        return 'https://s3.us-east-2.amazonaws.com/pubshare.chi.com/orderables/Medicine.png';
      } else if (service == 'Urine') {
        return 'https://s3.us-east-2.amazonaws.com/pubshare.chi.com/orderables/Urine.png';
      } else if (service == 'Blood Pressure') {
        return 'https://s3.us-east-2.amazonaws.com/pubshare.chi.com/orderables/blood_pressure-1.png';
      } else if (service == 'Blood Glucose') {
        return 'https://s3.us-east-2.amazonaws.com/pubshare.chi.com/orderables/Fasting+Blood+Glucose.png';
      } else if (service == 'Blood Test') {
        return 'https://s3.us-east-2.amazonaws.com/pubshare.chi.com/orderables/ECG+-+8+Channel.png';
      } else if (service == 'Pulmonary Function') {
        return 'https://s3.us-east-2.amazonaws.com/pubshare.chi.com/orderables/Pulmonary+Function.png';
      } else if (service == 'Pulse & Saturation') {
        return 'https://s3.us-east-2.amazonaws.com/pubshare.chi.com/orderables/Pulse+%26+Saturation.png';
      } else if (service == 'Uric Acid Test') {
        return 'https://s3.us-east-2.amazonaws.com/pubshare.chi.com/orderables/Fasting+Blood+Glucose.png';
      } else if (service == 'Total Cholesterol') {
        return 'https://s3.us-east-2.amazonaws.com/pubshare.chi.com/orderables/Fasting+Blood+Glucose.png';
      } else if (service == 'Virtual Monitoring') {
        return 'https://s3.us-east-2.amazonaws.com/pubshare.chi.com/orderables/ECG.png';
      } else if (service == 'Fetal Monitor System') {
        return 'https://s3.us-east-2.amazonaws.com/pubshare.chi.com/orderables/ECG+-+8+Channel.png';
      } else if (service == 'Symptom Check') {
        return 'https://s3.us-east-2.amazonaws.com/pubshare.chi.com/orderables/ECG+-+8+Channel.png';
      } else if (service == 'Stethoscope') {
        return 'https://s3.us-east-2.amazonaws.com/pubshare.chi.com/orderables/ECG+-+8+Channel.png';
      }
    } else if (serviceTitle == 'Medicine') {
      return 'https://s3.us-east-2.amazonaws.com/pubshare.chi.com/orderables/Medicine.png';
    } else if (serviceTitle == 'Battery') {
      return 'https://s3.us-east-2.amazonaws.com/pubshare.chi.com/orderables/Battery.png';
    } else if (serviceTitle == 'Message') {
      return 'https://s3.us-east-2.amazonaws.com/pubshare.chi.com/orderables/message.png';
    }
    return '';
  }

  // Color getReadingColor() {
  //   if (status == 'Normal') {
  //     return const Color.fromRGBO(1, 176, 80, 1);
  //   } else if (status == 'Critical') {
  //     return const Color.fromRGBO(190, 0, 0, 1);
  //   } else if (status == 'Warning') {
  //     return const Color.fromRGBO(186, 110, 2, 1);
  //   } else if (status == 'Warning High') {
  //     return const Color.fromRGBO(186, 110, 2, 1.5);
  //   } else if (status == 'Warning Low') {
  //     return const Color.fromRGBO(186, 110, 2, 0.7);
  //   } else if (status == 'Critical High') {
  //     return const Color.fromRGBO(192, 0, 0, 1.5);
  //   } else if (status == 'Critical Low') {
  //     return const Color.fromRGBO(192, 0, 0, 0.7);
  //   } else if (status == 'Unknown') {
  //     return Colors.grey;
  //   }
  //   return Colors.black;
  // }

  static LiveObservation fromMap(Map<String, dynamic> resp) {
    List<String>? urls;
    if (resp['urls'] != null) {
      urls = List<String>.from(resp['urls'].map((item) => item));
    }
    return LiveObservation(
      resp['reading_id'] as int?,
      resp['observation_id'] as int?,
      resp['user_id'] as int?,
      resp['patient_id'] as int?,
      resp['episode_id'] as int?,
      resp['episode_no'] as String?,
      resp['name'] as String?,
      resp['image'] as String?,
      resp['hospital_no'] as String?,
      resp['is_live'] as bool?,
      resp['service_title'] as String?,
      resp['service_category'] as String?,
      resp['service'] as String?,
      resp['measurement_id'] as int?,
      resp['measurement_name'] as String?,
      resp['measurement_key'] as String?,
      resp['measurement_unit'] as String?,
      resp['numeric_value'] as double?,
      resp['text_value'] as String?,
      resp['data_value'] as String?,
      resp['status'] as String?,
      resp['opr_status'] as String?,
      resp['observation_time'] as int?,
      resp['acquisition_time'] as int?,
      resp['capture_time'] as int?,
      resp['arrival_time'] as int?,
      resp['reading_time'] as int?,
      resp['device_id'] as int?,
      resp['device_model'] as String?,
      resp['first_name_enc'] as String?,
      resp['last_name_enc'] as String?,
      resp['mobile_phone_enc'] as String?,
      resp['subject_id'] as int?,
      resp['urls'] == null ? null : urls,
    );
  }
}
