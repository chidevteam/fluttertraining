/*
import 'dart:async';
import 'dart:io';

import 'package:audioplayers/audioplayers.dart';
import 'package:charms_doctor_app_v4x/components/alerts.dart';
import 'package:charms_doctor_app_v4x/models/all.dart';
import 'package:charms_doctor_app_v4x/models/observation.dart';
import 'package:charms_doctor_app_v4x/models/profile.dart';
import 'package:charms_doctor_app_v4x/models/requests.dart';
import 'package:charms_doctor_app_v4x/network/system_api_service.dart';
import 'package:flutter/cupertino.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:record_mp3/record_mp3.dart';
import 'package:stacked/stacked.dart';
import 'package:http/http.dart' as http;
import 'package:http_parser/http_parser.dart';
import 'package:video_player/video_player.dart';

import '../../../preferences.dart';
import 'observation_filter_dialog.dart';

class ObservationsViewModel extends BaseViewModel {
  Profile? userProfile;
  int limit = 30;
  int offset = 0;
  int filterCount = 0;
  int totalRecord = 0;
  String escalationMessage = '';
  List<LiveObservation> items = [];
  List<String> handleStatus = [];
  List<String> readingStatus = [];
  bool isCopiedToPatient = false;
  bool isAudioRecording = false;
  bool isAudioPlayer = false;
  bool isAudioPlaying = false;
  bool isAudioLoading = false;
  String audioURL = '';

  String mFilePath = '';
  String sTargetTime = '';
  int iTimer = 0;
  var iMaxRecordTime = 90;
  int iDuration = 0;
  late VideoPlayerController audioPlayerController;
  late int audioDurationCount = 0;
  String audioPlayingCount = '';
  AudioPlayer audioPlayer = AudioPlayer();

  final escalationFormKey = GlobalKey<FormState>();
  final ScrollController scrollController = ScrollController();
  late StreamController<String> controller;
  late var subscription;

  Map<String, bool> escalationStateList = {
    'Closed': false,
    'Escalated': false,
    'Responded': false,
    'Unhandled': false,
    'Being Handled': false,
  };

  Map<String, bool> statusList = {
    'Critical': false,
    'Critical High': false,
    'Critical Low': false,
    'Normal': false,
    'Ultra Critical': false,
    'Ultra Critical High': false,
    'Ultra Critical Low': false,
    'Warning': false,
    'Warning High': false,
    'Warning Low': false,
  };


  ObservationsViewModel(this.controller);


  getController(BuildContext context) {
    subscription = controller.stream.listen((String data) {
      print('IN NotificationReceived +++++++++++++++__________________+++++++++++++++++');
      print(data);
      if (data == 'Observation') {
        Future.delayed(const Duration(seconds: 1), () {
          onRefreshCalled(context);
        });
      }
    });
  }


  void clearAll(StateSetter setState) {
    setState(() {
      filterCount = 0;

      escalationStateList.forEach((key, value) {
        escalationStateList[key] = false;
      });

      statusList.forEach((key, value) {
        statusList[key] = false;
      });
    });
  }

  void onCancel(context) {
    Navigator.of(context).pop();
  }

  onDismiss(BuildContext context) {
    isCopiedToPatient = false;
    isAudioRecording = false;
    isAudioPlayer = false;
    isAudioPlaying = false;
    isAudioLoading = false;
    audioURL = '';
    Navigator.of(context).pop();
  }

  onFilterClicked(context) {
    observationFilterDialog(context, this);
  }

  onCopiedToPatientClicked() {
    isCopiedToPatient = !isCopiedToPatient;
  }

  escalationMessageSave(String? value) {
    escalationMessage = value!.trim();
  }

  String? escalationMessageValidation(value) {
    if (value == null || value.isEmpty) {
      return 'Message is required';
    }
    return null;
  }

  onMessageEscalated(
      BuildContext context, String action, LiveObservation item) async {
    if (isAudioRecording) {
      return;
    }

    escalationFormKey.currentState!.save();

    if (escalationFormKey.currentState == null) {
      return;
    }

    if (!escalationFormKey.currentState!.validate()) {
      return;
    }

    setBusy(true);
    showProgress(context, title: 'sending, please wait...');


    String audio = audioURL.split('?token').first;
    print('--------------final audio------------> $audio');

    final req = await makeEscalationRequest(
        readingId: item.readingId!,
        action: action,
        messageText: escalationMessage,
        messageAudio: audio,
        marked: isCopiedToPatient);

    final resp = await SystemApiService.markObservationHandled(req);

    if (resp.isSuccess) {
      setBusy(false);
      hideProgress(context);
      Navigator.pop(context);
      isCopiedToPatient = false;
      isAudioRecording = false;
      isAudioPlayer = false;
      isAudioPlaying = false;
      audioURL = '';
      onRefreshCalled(context);
    } else {
      setBusy(false);
      hideProgress(context);

      showAlert(context,
          title: 'Error',
          message: resp.errorMessage ?? 'Error occurred while uploading data.');
    }
  }

  onEscalationAudio(BuildContext context, StateSetter setState) async {
    if (isAudioRecording) {
      return;
    }

    if (isAudioPlayer) {
      showConfirmationAlert(context, onConfirmClick: () {
        isAudioPlayer = false;
        onStartRecording(context, setState);
        Navigator.pop(context);
        setState(() {});
      }, onCancelClick: () {
        Navigator.pop(context);
      },
          message:
              'This action may lost your data, are you sure you want to continue?',
          title: 'Data Lost',
          confirmTitle: 'YES',
          cancelTitle: 'NO');
    } else {
      onStartRecording(context, setState);
    }
  }

  onStartRecording(BuildContext context, StateSetter setState) async {
    isAudioRecording = true;
    bool hasPermission = await checkPermission();
    if (hasPermission) {
      mFilePath = await getFilePath();
      RecordMp3.instance.start(mFilePath, (type) {});
      onStartRecordingTime(setState);
    } else {
      isAudioRecording = false;
      showErrorAlert(context,
          title: 'Permission denied',
          message: 'please grant permission and try again', onConfirmClick: () {
        Navigator.pop(context);
        Navigator.pop(context);
      });
    }
  }

  onStartRecordingTime(StateSetter setState) {
    sTargetTime = secondsToMinutes(iTimer);

    if (iTimer >= iMaxRecordTime || !isAudioRecording) {
      return;
    } else {
      Future.delayed(const Duration(milliseconds: 990), () {
        onStartRecordingTime(setState);
      });
    }

    iTimer += 1;
    setState(() {});
  }

  stopRecording(BuildContext context, bool isCanceled, StateSetter setState) {
    RecordMp3.instance.stop();
    iDuration = iTimer;
    iTimer = 0;
    isAudioRecording = false;
    if (isCanceled) return;
    Future.delayed(const Duration(milliseconds: 500), () {
      onUploadFile(context, setState);
    });
  }

  String secondsToMinutes(int sec) {
    int minutes = sec % 3600 ~/ 60;
    int seconds = sec % 60;
    var time =
        "${minutes < 10 ? '0$minutes' : '$minutes'}:${seconds < 10 ? '0$seconds' : '$seconds'}";
    return time;
  }

  onUploadFile(BuildContext context, StateSetter setState) async {
    showProgress(context, title: 'uploading, please wait...');
    final req = await http.MultipartFile.fromPath('UserFile', mFilePath,
        filename: 'test_test.mp3', contentType: MediaType("audio", "mp3"));
    final resp = await SystemApiService.upload(req);
    if (resp.isSuccess) {
      hideProgress(context);
      isAudioPlayer = true;
      audioURL = resp.data!.fileUrl!;
      setState(() {});
    } else {
      hideProgress(context);
      setBusy(false);
      isAudioPlayer = false;
      showAlert(context,
          title: 'File Upload',
          message: resp.errorMessage ?? 'Error occurred while uploading File.');
    }
  }

  startAudioPlayer(StateSetter setState) async {

    if (audioPlayer.state == PlayerState.STOPPED) {
      await audioPlayer.setUrl(audioURL);
      await audioPlayer.setReleaseMode(ReleaseMode.STOP);
      isAudioLoading = true;
      setState((){});
    }
    if (audioPlayer.state == PlayerState.COMPLETED) {
      await audioPlayer.play(audioURL);
    }

    audioPlayer.onAudioPositionChanged.listen((Duration p) {
      if(isAudioLoading){
        isAudioLoading = false;
        setState((){});
      }

      audioDurationCount = p.inSeconds + 1;
      audioPlayingCount =
          "${p.inMinutes < 10 ? '0${p.inMinutes}' : '${p.inMinutes}'}:${p.inSeconds < 10 ? '0${p.inSeconds}' : '${p.inSeconds}'}";

      setState(() {});
    });

    audioPlayer.onPlayerCompletion.listen((event) {
      isAudioPlaying = false;
      audioPlayingCount = '00:00';
      audioDurationCount = 0;
      isAudioLoading = false;
      setState(() {});
    });

    if (isAudioPlaying) {
      isAudioPlaying = false;
      await audioPlayer.pause();
      setState(() {});
    } else {
      isAudioPlaying = true;
      await audioPlayer.resume();
      setState(() {});
    }
  }

  onConfirm(context) {
    handleStatus = [];
    readingStatus = [];

    escalationStateList.forEach((key, value) {
      if (escalationStateList[key] == true) {
        handleStatus.add(key);
      }
    });

    statusList.forEach((key, value) {
      if (statusList[key] == true) {
        readingStatus.add(key);
      }
    });

    onRefreshCalled(context);
    Navigator.of(context).pop();
  }

  onCallsClicked(context) {
    Navigator.pushNamed(context, '/calls');
  }

  onGetProfile(BuildContext context) async {
    userProfile = await Preferences.getUserProfile() as Profile;
  }

  onRefreshCalled(BuildContext context) {
    limit = 30;
    offset = 0;
    totalRecord = 0;
    items.clear();
    onGetObservationData(context);
  }

  onGetObservationData(context, {bool isPaginated = false}) async {
    if (!isPaginated) {
      setBusy(true);
    }
    final req = await makeObservationRequest(
        handleStatus.isEmpty ? null : handleStatus,
        30,
        offset,
        readingStatus.isEmpty ? null : readingStatus);
    ;
    final resp = await SystemApiService.getLiveObservations(req);

    if (resp.isSuccess) {
      if (!isPaginated) {
        setBusy(false);
      }
      items.addAll(resp.data!.data);
      //items.clear();

      totalRecord = resp.data!.totalRecords;
      print('totalRecords-----> $totalRecord');

      print('------count observa----------------- ${items.length}');
      notifyListeners();
    } else {
      if (!isPaginated) {
        setBusy(false);
      }
      showAlert(context,
          title: 'Data Fetched Failed',
          message: resp.errorMessage ?? 'Error occurred while getting data.');
    }
  }

  void getMoreRecords(int index, BuildContext context) async {
    print('----Observations--------------------------More Data , $index');
    if (index >= limit) {
      offset += 30;
      limit += 30;
      print('limit-----> $limit ------ offset------> $offset ');
      await onGetObservationData(context, isPaginated: true);
    }
  }

  onItemClick(BuildContext context, LiveObservation item) {
    if (item.service == 'Ultrasound' ||
        item.service == 'Odontoscope' ||
        item.service == 'Otoscope') {
      print(item.service);
      Navigator.pushNamed(context, '/urls', arguments: item.urls);
      notifyListeners();
    } else {
      Navigator.pushNamed(context, '/dashboard', arguments: item.patientId);
    }
  }

  Future<String> getFilePath() async {
    Directory storageDirectory = await getTemporaryDirectory();
    String sdPath = storageDirectory.path + "/record";
    var d = Directory(sdPath);
    if(d.existsSync()) {
      d.deleteSync(recursive: true);
    }
    if (!d.existsSync()) {
      d.createSync(recursive: true);
    }
    return sdPath + "/test_kl_test.mp3";
  }

  Future<bool> checkPermission() async {
    if (!await Permission.microphone.isGranted) {
      PermissionStatus status = await Permission.microphone.request();
      if (status != PermissionStatus.granted &&
          status != PermissionStatus.permanentlyDenied) {
        return false;
      }
    }
    return true;
  }
}
*/