/*
import 'dart:async';

import 'package:charms_doctor_app_v4x/components/images.dart';
import 'package:charms_doctor_app_v4x/components/loading_pages.dart';
import 'package:charms_doctor_app_v4x/components/padded_text.dart';
import 'package:charms_doctor_app_v4x/models/observation.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:stacked/stacked.dart';

import '../../../preferences.dart';
import 'observations_escalation_dialog.dart';
import 'observations_vm.dart';

class ObservationsScreen extends ViewModelBuilderWidget<ObservationsViewModel> {
  late StreamController<String> controller;
  ObservationsScreen(this.controller, {Key? key}) : super(key: key);

  @override
  bool get reactive => true;

  @override
  bool get createNewModelOnInsert => false;

  @override
  bool get disposeViewModel => true;

  @override
  bool get initialiseSpecialViewModelsOnce =>
      true;

  @override
  Widget builder(
      BuildContext context, ObservationsViewModel viewModel, Widget? child) {
    return PlatformScaffold(
      appBar: PlatformAppBar(
        backgroundColor: Colors.white,
        leading: Padding(
          padding: const EdgeInsets.all(5.0),
          child: Image.asset(
            'assets/chi_logo.png',
          ),
        ),
        trailingActions: [
          Stack(
            children: [
              Visibility(
                visible: viewModel.userProfile == null ? false : true,
                child: InkWell(
                  onTap: () => viewModel.onFilterClicked(context),
                  child: const Padding(
                    padding: EdgeInsets.fromLTRB(10, 15, 10, 10),
                    child: Icon(Icons.filter_list_alt,
                        size: 25, color: Color.fromRGBO(0x38, 0x7b, 0x96, 1.0)),
                  ),
                ),
              ),
              Positioned(
                left: 30,
                child: Visibility(
                  visible: viewModel.filterCount != 0,
                  child: Container(
                      width: 13,
                      height: 13,
                      child: Center(
                        child: Text(
                          viewModel.filterCount > 100
                              ? "99+"
                              : viewModel.filterCount.toString(),
                          style: const TextStyle(
                              color: Colors.white, fontSize: 10),
                        ),
                      ),
                      decoration: BoxDecoration(
                        color: Colors.red[800],
                        borderRadius: BorderRadius.circular(100),
                      )),
                ),
              )
            ],
          ),
          Visibility(
            visible: viewModel.userProfile == null ? false : viewModel.userProfile!.permissionsV2!.internalCalling!,
            child: InkWell(
              onTap: () => viewModel.onCallsClicked(context),
              child: const Padding(
                padding: EdgeInsets.symmetric(horizontal: 10.0),
                child: Icon(Icons.call,
                    size: 25, color: Color.fromRGBO(0x38, 0x7b, 0x96, 1.0)),
              ),
            ),
          ),
        ],
        title: const Text(
          'Observation',
          style: TextStyle(color: Color.fromRGBO(0x38, 0x7b, 0x96, 1.0)),
        ),
        material:  (_, __) => MaterialAppBarData(
          systemOverlayStyle: const SystemUiOverlayStyle(
            statusBarColor: Color.fromRGBO(0x38, 0x7b, 0x96, 1.0), // For both Android + iOS
            statusBarIconBrightness: Brightness.light, // For Android (dark icons)
            statusBarBrightness: Brightness.light, // For iOS (dark icons)
          ),
        ),
        cupertino: (_, __) => CupertinoNavigationBarData(
          title: const SizedBox(
            width: double.infinity,
            child: Text(
              'Observation',
              style: TextStyle(
                  color: Color.fromRGBO(0x38, 0x7b, 0x96, 1.0),
                  fontSize: 20),
            ),
          ),
        ),
      ),
      body: viewModel.isBusy
              ? const GettingRecords()
              : viewModel.items.isEmpty
                  ?
                  NoRecord('No Observation Record', onRefresh: () async{
                    await viewModel.onRefreshCalled(context);
                  },)
                  : RefreshIndicator(
                      onRefresh: () async {
                        await viewModel.onRefreshCalled(context);
                      },
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(6, 8, 6, 0),
                        child: ListView.builder(
                          // controller: viewModel.scrollController,
                          itemBuilder: (context, index) {
                            viewModel.getMoreRecords(index + 1, context);
                            return ((index + 1 == viewModel.items.length) && !(index + 1 == viewModel.totalRecord))
                                ? const Center(
                                    child: CircularProgressIndicator(),
                                  )
                                : observationCell(
                                    viewModel, viewModel.items[index], context);
                          },
                          itemCount: viewModel.items.length,
                        ),
                      ),
                    ),
    );
  }

  Widget observationCell(ObservationsViewModel viewModel, LiveObservation item,
      BuildContext context) {
    return Column(
      children: [
        InkWell(
          onTap: () {
            viewModel.onItemClick(context, item);
          },
          onLongPress: () {
            observationEscalationDialog(context, viewModel, item);
          },
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                width: 8,
                height: 74,
                decoration: BoxDecoration(
                  color: item.getHighlightColor(),
                  borderRadius: BorderRadius.circular(2),
                ),
              ),
              const SizedBox(
                width: 8,
              ),
              Expanded(
                flex: 2,
                child: CHINetworkImage(
                  item.getImage(),
                  height: 48,
                  width: 40,
                  fit: BoxFit.contain,
                ),
              ),
              const SizedBox(
                width: 10,
              ),
              Expanded(flex: 7, child: observationsNamePatientDate(item)),
              const Spacer(),
              Expanded(flex: 3, child: observationsReading(item)),
              Expanded(flex: 1, child: observationsForwardIcon()),
            ],
          ),
        ),
        observationsDivider()
      ],
    );
  }

  Widget observationsDivider() {
    return const Padding(
      padding: EdgeInsets.symmetric(horizontal: 12),
      child: Divider(
        color: Colors.grey,
      ),
    );
  }

  Widget observationsForwardIcon() {
    return const Icon(Icons.arrow_forward_ios,
        color: Color.fromRGBO(56, 123, 150, 1));
  }

  Widget observationsReading(LiveObservation item) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        PaddedText(
          text: item.measurementUnit ?? '',
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
          style: const TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.normal,
              color: Color.fromRGBO(56, 123, 150, 1)),
          padding: const EdgeInsets.only(bottom: 4),
        ),
        PaddedText(
          text: item.numericValue.toString().contains("null")
              ? ""
              : item.numericValue.toString(),
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
          style: TextStyle(
              color: item.getReadingColor(),
              fontSize: 20,
              fontWeight: FontWeight.bold),
          padding: const EdgeInsets.only(bottom: 8),
        ),
        Text(item.measurementName.toString(),
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style: const TextStyle(
                color: Color.fromRGBO(56, 123, 150, 1), fontSize: 12)),
      ],
    );
  }

  Widget observationsNamePatientDate(LiveObservation item) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        PaddedText(
            text: item.service.toString(),
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style: const TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.bold,
                color: Color.fromRGBO(56, 123, 150, 1)),
            padding: const EdgeInsets.only(bottom: 8)),
        PaddedText(
          text: item.name.toString(),
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
          style: const TextStyle(
            color: Color.fromRGBO(56, 123, 150, 0.8),
            fontSize: 16,
          ),
          padding: const EdgeInsets.only(bottom: 8),
        ),
        Text(Preferences.timeStampToDateAndTime(item.acquisitionTime!),
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style: const TextStyle(color: Colors.grey)),
      ],
    );
  }

  @override
  ObservationsViewModel viewModelBuilder(BuildContext context) {
    final viewModel = ObservationsViewModel(controller);

    viewModel.onGetProfile(context);
    viewModel.getController(context);

    viewModel.onGetObservationData(context);
    return viewModel;
  }
}

// RefreshIndicator(
// onRefresh: () async {
// await viewModel.onRefreshCalled(context);
// },
// child: Stack(
// children: [ListView(), const NoRecord(
// 'No Observations Record'
// ),],
// )
// )
*/