import 'package:flutter/material.dart';
import 'T001_single_sine/main.dart';
import 'T002_multiple_sine/main.dart';
import 'T003_bp_graph/main.dart';
import 'T004_patient_list/patient_vu.dart';
import 'T005_vital_table/recent_result_vu.dart';
import 'T007_doctor_list/doctor_vu.dart';

import 'package:universal/Tutorials/DART/T004_Network/lib/network/system_api_service.dart';

// import 'T008_CGMS/agp_chart/agp_graph_vu.dart';
//
void main() async {
  // loginFunc();
  await loginFunc('charms-qa', "salmancd", "Salman@1");
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'CHI Charts',
      theme: ThemeData.light(),
      debugShowCheckedModeBanner: false,
      home: Wrapper(),
    );
  }
}

class Wrapper extends StatelessWidget {
  const Wrapper({Key? key}) : super(key: key);

  Widget _project({
    required String title,
    required String subtitle,
    required Widget page,
    required BuildContext context,
  }) {
    return ListTile(
      title: Text(title),
      subtitle: Text(subtitle),
      onTap: () {
        Navigator.of(context)
            .push(MaterialPageRoute(builder: (context) => page));
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Flutter Art"),
        centerTitle: false,
        elevation: 2.0,
      ),
      body: ListView(
        children: [
          _project(
            title: "Single Sine Graph",
            subtitle: "Single Sine graphs",
            page: SingleSinGraph(),
            context: context,
          ),
          _project(
            title: "Multiple Sin Demo",
            subtitle: "Multiple Sine graphs",
            page: MultipleSinGraphs(),
            context: context,
          ),
          _project(
            title: "Blood Pressure Graph",
            subtitle: "Blood Pressure Graph",
            page: BPGraph(),
            context: context,
          ),
          _project(
            title: "Patient List",
            subtitle: "Patient List",
            page: PatientListScreen(),
            context: context,
          ),
          _project(
            title: "Recent Results Detail",
            subtitle: "Recent Results Detail",
            page: RecentResultDetailScreen(),
            context: context,
          ),
          _project(
            title: "Doctor List",
            subtitle: "Doctor List",
            page: DoctorListScreen(),
            context: context,
          ),
          // _project(
          //   title: "Graphs",
          //   subtitle: "Graphs",
          //   page: CGMGraphsScreen(),
          //   context: context,
          // ),
        ],
      ),
    );
  }
}
