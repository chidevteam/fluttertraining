

- Step 1:  Convert bp.dart into sample10.json format
- Step 2: Use the new format to draw data
          (time stamps will overlap)
- Step 3: change time stamp to actual time format          

Support for line color.

Step:
- make multiple graphs dynamically if array of data

DONE:
- double tap -- implemented again -- using my own technique -- took 40 min
- long press to see values - took around 2.5 hours.
- remove matrix4 dependency - Done


NOTE:

- Have implemented long press without timer, which can fail on certain arch.
  > apparently fails on browser, 
  > the reason is that the phone starts giving scaleUpdate() even without 
    movement.
  > SHOULD BE CHECKED ON IPHONE ASAP.    


- Support for data as in our system.
  > multiple series
  > Trendline support
  > long press should work also.
  > error bars

- time axis in min, days, month, year, ...

- support for colors (background, foreground, series, etc)


Nice to have
============
- legend, legend move
- remove unnecessary callback since we have update() being called probably at 60ps
- animation
    animate the double tap initialization
    animate the data addition
- themes
- physics
- spline fit


=============================================================

void conversion() {
  timeStampToDate();
  dateToTimeStamp();
}
timeStampToDate() {
  //Timestamp to DateTime
  final timestamp1 = 1627510285; // timestamp in seconds
  final DateTime date1 = DateTime.fromMillisecondsSinceEpoch(timestamp1 * 1000);
  print("Timestamp to DateTime : $date1");
  print(
      'just thw time: ${date1.hour}:${date1.minute}:${date1.second}.${date1.millisecond}');
}
dateToTimeStamp() {
  final DateTime date2 = DateTime(2021, 8, 10); // August 10, 2021
  final timestamp2 = date2.millisecondsSinceEpoch / 1000;
  print("Datetime to timestamp : $timestamp2");
}
===========================================================

{vital_name: Blood Pressure, measurement_name_to_display: Diastolic, measurement_unit: mmHg, ranges: {measurement_name_to_display: Diastolic, normal_low_value: 70, normal_high_value: 100, warn_low_value: 46, warn_low_end_value: 69, warn_high_value: 101, warn_high_end_value: 110, alert_low_value: 0, alert_low_end_value: 45, alert_high_value: 111, alert_high_end_value: 999, name: Diastolic}, stats: {min: 70, max: 627, lower_percentage: 0, greater_percentage: 77.78, mean: 129.81}, trend_points: [{start_point: {date: 1611792000000, value: 110.68262813521142, Diastolic: 110.68262813521142}, name: Diastolic}, {end_point: {date: 1613606400000, value: 151.77255179934582, Diastolic: 151.77255179934582}, name: Diastolic}], values: {x: [1611792000, 1612137600, 1612224000, 1612310400, 1612396800, 1612828800, 1613001600, 1613433600, 1613606400], y: [72, 112.5, 120, 138.5, 123.5, 120.5, 272.33, 86, 123], ymin: [72, 90, 120, 127, 121, 120, 70, 86, 121], ymax: [72, 120, 120, 150, 126, 121, 627, 86, 125]}}
================================




Map createBaseDict() {
    Map dict = {
      "x": [],
      "y": [],
      "ymin": [],
      "ymax": [],
      "ranges": {
        "measurement_name_to_display": "Systolic",
        "normal_low_value": 3,
        "normal_high_value": 3.25,
        "warn_low_value": 2.75,
        "warn_low_end_value": 2.5,
        "warn_high_value": 3.25,
        "warn_high_end_value": 3.5,
        "alert_low_value": 2.5,
        "alert_low_end_value": 2.25,
        "alert_high_value": 3.5,
        "alert_high_end_value": 999,
        "name": "Systolic"
      },
      "stats": {
        "min": 1.75,
        "max": 4.25,
        "lower_percentage": 11.11,
        "greater_percentage": 77.78,
        "mean": 3.15
      },
      "trend_points": [
        {
          "start_point": {"date": 1, "value": 2.5, "Systolic": 2.5},
          "name": "Systolic"
        },
        {
          "end_point": {"date": 3, "value": 3.5, "Systolic": 3.5},
          "name": "Systolic"
        }
      ],
    };
    return {'x': [], 'y': []};
  }

  =======================

  