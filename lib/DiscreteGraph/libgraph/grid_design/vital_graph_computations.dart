Map<String, double> initializeVars(data) {
  Map<String, double> minMax = {
    "ymin": double.infinity,
    "ymax": -double.infinity,
    "xmin": double.infinity,
    "xmax": -double.infinity,
  };

  for (int i = 0; i < data["vitals"].length; i++) {
    if (data["vitals"][i]["data"].length == 0) {
      continue;
    }
  }
  minMax = computeMinMax(minMax, data);
  return minMax;
}

Map<String, double> computeMinMax(Map<String, double> minMax, data) {
  for (int i = 0; i < data!["vitals"].length; i++) {
    var vital = data["vitals"][i];
    double? ymin = minMax["ymin"]!;
    double? ymax = minMax["ymax"]!;
    double? xmin = minMax["xmin"]!;
    double? xmax = minMax["xmax"]!;
    List<num>? ranges = vital['ranges'];

    for (int j = 0; j < vital["data"].length; i++) {
      if (vital['data'][j].length < 1) {
        continue;
      }
      List vData = vital["data"][j];
      double vt = vData[0].toDouble();
      if (vt < xmin!) xmin = vt.toDouble();
      if (vt > xmax!) xmax = vt.toDouble();

      if (vData.length == 1) {
        ymin = ranges![2].toDouble();
        ymax = ranges[3].toDouble();
      } else if (vData.length == 2) {
        ymin = vData[1];
        ymax = vData[1];
      } else {
        if (vData[2] < ymin) ymin = vData[2].toDouble();
        if (vData[3] > ymax) ymax = vData[3].toDouble();
      }
    }
    if (ymin == ymax) {
      var average = (ranges![2] + ranges[3]) / 2;
      ymin = ranges[2].toDouble() - average;
      ymax = ranges[3].toDouble() + average;
    }
    minMax["ymin"] = ymin!;
    minMax["ymax"] = ymax!;
    minMax["xmin"] = xmin!;
    minMax["xmax"] = xmax!;
  }
  return minMax;
}
