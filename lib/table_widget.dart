import 'package:flutter/material.dart';
import 'Tutorials/T003_Navigation/dial_chart.dart';
import 'Tutorials/T003_Navigation/live_graph.dart';
import 'Tutorials/T006_oscilloscope/apps/T001_two_sinusoids/oscilloscope_widget.dart';
import 'Tutorials/T007_ecg/apps/T001_ecg/ecg_holder_app.dart';

import 'chart_performance/chart1/chart_holder1.dart';
import 'chart_performance/chart2/chart_holder2.dart';
import 'chart_performance/REF/main.dart';
// import 'chart_performance/REF/custom_painter_demo.dart';

class TableWidget extends StatefulWidget {
  const TableWidget({Key? key}) : super(key: key);

  @override
  _TableWidgetState createState() {
    return _TableWidgetState();
  }
}

class _TableWidgetState extends State<TableWidget> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Counter 6'), actions: [
        IconButton(
          icon: const Icon(Icons.list),
          onPressed: actionButtonPressed,
          tooltip: 'Saved Suggestions',
        ),
        IconButton(
          icon: const Icon(Icons.list),
          onPressed: actionButtonPressed,
          tooltip: 'Saved Suggestions',
        ),
      ]),
      body: _buildSuggestions(),
    );
  }

  void actionButtonPressed() {
    print("Action Button Pressed");
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => const LiveGraph()),
    );
  }

  void navigate(int i) {
    // List<Type> myList = [LiveGraph, LiveGraph, DialChart, OscilloscopeWidget]

    print("cell clicked new $i");
    // ignore: todo
    late Widget widget; //TODO: Initialize to unknown
    switch (i) {
      case 1:
        widget = LiveGraph();
        break;
      case 2:
        widget = DialChart();
        break;
      case 3:
        widget = OscilloscopeWidget();
        break;
      case 4:
        widget = ECGHolder();
        break;
      case 5:
        widget = ECGApp();
        break;
      case 6:
        widget = ChartHolder1App();
        break;
      case 7:
        widget = ChartHolder2App();
        break;
      case 8:
        widget = GestureApp();
        break;
      default:
        return;
    }

    print("Action Button Pressed $i");

    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => widget),
    );
  }

  Widget _buildSuggestions() {
    return ListView.builder(itemBuilder: /*1*/ (context, i) {
      if (i.isOdd)
        return Divider(); /*2*/
      else {
        int j = i ~/ 2 + 1;
        return _buildRow(j, "$j");
      }
    });
  }

  Widget _buildRow(int i, String str) {
    // var list<String> = List [];
    List<String> titles = [
      "Dummy",
      "LiveGraph4",
      "DialChart",
      "OscilloscopeWidget",
      "ECG",
      "ECGApp",
      "Chart1 App",
      "Chart2 App",
      "Gesture App",
    ];

    String title = "--";
    if (i < titles.length) {
      title = titles[i];

      return ListTile(
          title:
              Row(children: [Text("$i "), Icon(Icons.favorite), Text(title)]),
          trailing: Icon(
            Icons.favorite,
            color: Colors.red,
          ), // ... to here.
          onTap: () {
            // NEW lines from here...
            print("Cell Tapped $i");
            navigate(i);
          });
    } else {
      return ListTile(
          title: Row(children: [Text("$i "), Icon(Icons.favorite)]),
          trailing: Icon(
            Icons.favorite,
            color: Colors.red,
          ), // ... to here.
          onTap: () {
            // NEW lines from here...
            print("Cell Tapped $i");
            navigate(i);
          });
    }
  }
}
