Constraints in Flutter works a bit different than usual. 
Widgets themselves do not have constraints.

When you specify a width/height of a Container, you're not constraining Container.
You're constraining the child of the Container.

Container will then size itself based on the size of its child.

As such, parent widgets always have the last word on how their descendants should be sized.



11:40
https://api.flutter.dev/flutter/widgets/ValueListenableBuilder-class.html


AppBuilder.of(context).rebuild();
context.findAncestorStateOfType<AppBuilderState>()

ValueListenableBuilder<T> class. 


void rebuildAllChildren(BuildContext context) {
  void rebuild(Element el) {
    el.markNeedsBuild();
    el.visitChildren(rebuild);
  }
  (context as Element).visitChildren(rebuild);
}

https://www.youtube.com/watch?v=Q5C7sqVe2Vg&ab_channel=MetaBallStudios

12:00  