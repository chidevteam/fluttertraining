import 'package:flutter/material.dart';
import '../lib/dial_meter_holder.dart';
import '../lib/linear_meter_holder.dart';

void main() {
  runApp(MaterialApp(title: 'Material App', home: MyApp()));
}

class MyApp extends StatelessWidget {
  final List<double> ranges = [30, 40, 50, 55, 70, 80];

  @override
  Widget build(BuildContext context) {
    print('root build called1');
    Scaffold scaffold = Scaffold(
      appBar: AppBar(
        title: Text("Meter App2333"),
        backgroundColor: Colors.blue,
      ),
      body: Column(children: [
        Expanded(child: Center(child: getRow1())),
        Divider(height: 1.0),
        Expanded(child: Center(child: getRow2())),
      ]),
      backgroundColor: Colors.black,
    );
    return scaffold;
  }
}

getRow1() {
  final List<double> ranges1 = [30, 33, 40, 55, 70, 120];
  // final List<double> ranges2 = [30, 40, 50, 55, 70, 80];
  final String label = 'SpO2';
  num value = 25;
  final String unit = '%';

  return Row(children: [
    Expanded(
        child: Center(
            child: DialMeterHolder(
      ranges: ranges1,
      label: label,
      value: value,
      unit: unit,
    ))),
    // VerticalDivider(width: 1.0),
    // Expanded(
    //   child: Center(
    //     child: DialMeterHolder(
    //       ranges: ranges2,
    //       bgColor: Color(0xFFaaaaaa),
    //       label: label,
    //       value: value,
    //       unit: unit,
    //     ),
    //   ),
    // ),
  ]);
}

getRow2() {
  final List<double> ranges1 = [30, 37, 50, 55, 70, 120];
  final List<double> ranges2 = [30, 40, 50, 55, 70, 80];
  final String label = 'Pulse';
  num value = 70;
  final String unit = 'bpm';

  return Row(children: [
    Expanded(
        child: Center(
            child: LinearMeterHolder(
      ranges: ranges1,
      bgColor: Color(0xFFdddddd),
      label: label,
      value: value,
      unit: unit,
    ))),
    VerticalDivider(width: 1.0),
    Expanded(
        child: Center(
            child: LinearMeterHolder(
      ranges: ranges2,
      bgColor: Color(0xFF00FF00),
      label: label,
      value: value,
      unit: unit,
    ))),
  ]);
}
