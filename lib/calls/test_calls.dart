import 'package:flutter/services.dart';
// import 'package:flutter/widgets.dart';
import 'package:flutter/material.dart';

void main() {
  // WidgetsFlutterBinding.ensureInitialized();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  static const platform = MethodChannel('com.chi.calls/host.base');

  @override
  Widget build(BuildContext dcontext) {
    Scaffold createHome = Scaffold(
      appBar: createAppBar(),
      body: createBody(),
    );

    return MaterialApp(home: createHome);
  }

  createAppBar() {
    return AppBar(
      title: const Text('CHI Calls Test'),
    );
  }

  createBody() {
    Column column = Column(
      children: [
        ElevatedButton(
            onPressed: startCallsService, child: Text('Start Calls Service')),
        ElevatedButton(
            onPressed: stopCallsService, child: Text('Stop Calls Service')),
        ElevatedButton(
            onPressed: () => callUser('Audio'), child: Text('Call User Audio')),
        ElevatedButton(
            onPressed: () => callUser('Video'), child: Text('Call User Video')),
        ElevatedButton(
            onPressed: () => callCommandCenter('Audio'),
            child: Text('Call CommandCenter Audio')),
        ElevatedButton(
            onPressed: () => callCommandCenter('Video'),
            child: Text('Call CommandCenter Video')),
      ],
    );
    Center body = Center(
      child: column,
    );

    return body;
  }

  Future<void> startCallsService() async {
    String token =
        'lb3SaRmNR9nSNLxzHgkwWoYscXK1AegHV-CiU8rcFKn7rl5p_XgEYJfheCruFwjcINxTf-xUcITvcsNsKmC2MYpDdpAAjIBHDEvR0nn_aWfiNF4OXOHHwmB9DUXbXRnM15Tk-5HeklErMXHEa8ljjlrrX_ooAmV4fy1SCUQ6wSQ=';
    try {
      final result = await platform.invokeMethod('startCallsService', {
        'token': token,
        'siteCode': 'charms-qa',
      });

      if (result['status'] == 'Ok') {
        print('OK response');
      } else {
        print('Unknown response startCharmsSdk');
        print(result);
      }
    } on PlatformException catch (e) {
      print('Failed to start calls service $e');
    }
  }

  Future<void> stopCallsService() async {
    try {
      final result = await platform.invokeMethod('stopCallsService');

      if (result['status'] == 'Ok') {
        print('OK response');
      } else {
        print('Unknown response startCharmsSdk');
        print(result);
      }
    } on PlatformException catch (e) {
      print('Failed to stop calls service $e');
    }
  }

  Future<void> callUser(String callType) async {
    try {
      final result = await platform.invokeMethod('callUser', {
        'callType': callType,
        'd_user_id': 188,
        'd_full_name': 'Junaid Doctor',
        'd_image':
            'https://www.google.com/imgres?imgurl=https%3A%2F%2Fimg.freepik.com%2Ffree-vector%2Fdoctor-character-background_1270-84.jpg%3Fsize%3D338%26ext%3Djpg&imgrefurl=https%3A%2F%2Fwww.freepik.com%2Ffree-photos-vectors%2Fdoctor&tbnid=EfsLy1npxc-HFM&vet=12ahUKEwiPp9bWnIv0AhVggc4BHZhpBGwQMygAegUIARDVAQ..i&docid=bFpHl9iJxt80mM&w=338&h=338&q=doctor%20image&client=safari&ved=2ahUKEwiPp9bWnIv0AhVggc4BHZhpBGwQMygAegUIARDVAQ',
        'p_user_id': 27,
        'p_full_name': 'Junaid Patient',
        'p_image':
            'https://www.google.com/url?sa=i&url=https%3A%2F%2Fwww.hopkinsmedicine.org%2Fcoronavirus%2Farticles%2Ficu-recovery.html&psig=AOvVaw1bCEpehEvmdaAKAQgF5dEf&ust=1636545378882000&source=images&cd=vfe&ved=0CAsQjRxqFwoTCMi29uSci_QCFQAAAAAdAAAAABAD',
      });

      if (result['status'] == 'Ok') {
        print('OK response');
      } else {
        print('Unknown response startCharmsSdk');
        print(result);
      }
    } on PlatformException catch (e) {
      print('Failed to start calls service $e');
    }
  }

  Future<void> callCommandCenter(String callType) async {
    try {
      final result = await platform.invokeMethod('callCommandCenter', {
        'callType': callType,
        'd_user_id': 188,
        'd_full_name': 'Junaid Doctor',
        'd_image':
            'https://www.google.com/imgres?imgurl=https%3A%2F%2Fimg.freepik.com%2Ffree-vector%2Fdoctor-character-background_1270-84.jpg%3Fsize%3D338%26ext%3Djpg&imgrefurl=https%3A%2F%2Fwww.freepik.com%2Ffree-photos-vectors%2Fdoctor&tbnid=EfsLy1npxc-HFM&vet=12ahUKEwiPp9bWnIv0AhVggc4BHZhpBGwQMygAegUIARDVAQ..i&docid=bFpHl9iJxt80mM&w=338&h=338&q=doctor%20image&client=safari&ved=2ahUKEwiPp9bWnIv0AhVggc4BHZhpBGwQMygAegUIARDVAQ',
      });

      if (result['status'] == 'Ok') {
        print('OK response');
      } else {
        print('Unknown response startCharmsSdk');
        print(result);
      }
    } on PlatformException catch (e) {
      print('Failed to start calls service $e');
    }
  }
}
