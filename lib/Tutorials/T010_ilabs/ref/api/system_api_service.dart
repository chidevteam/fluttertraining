import 'doctor_model.dart';
import 'response.dart';

class APIResp<T> {
  bool isSuccess;
  T? data;

  APIResp(this.isSuccess, this.data);
}

class SystemApiService {
  static Future<APIResp> getDoctors(req) async {
    DoctorList items = DoctorList.fromMap(response["data"]);
    return APIResp<DoctorList>(
        true, DoctorList(items.data, items.totalRecords));
  }
}

int loginFunc(a, b, c) {
  return 3;
}
