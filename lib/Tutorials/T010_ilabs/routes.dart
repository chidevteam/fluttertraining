import 'package:flutter/cupertino.dart';
import 'screens/invoicing_made/invoice_made_vu.dart';
import 'screens/payment_instructions/payment_instruction_vu.dart';
import 'screens/add_new_item/new_item_vu.dart';
import 'screens/home_page/home_vu.dart';
import 'screens/invoice/invoice_vu.dart';
import 'screens/login/login_vu.dart';
import 'screens/new_client_login/new_client_login_vu.dart';
import 'screens/new_item/new_item_vu.dart';
import 'screens/notes/notes_vu.dart';

Map<String, WidgetBuilder> appRoutes = {
  '/': (context) => const AuthScreen(),
  '/logIn': (context) => const LoginScreen(),
  '/invoiceScreen': (context) => InvoiceScreen(),
  '/myHomePage': (context) => const MyHomePage(),
  '/newItemScreen': (context) => const NewItemScreen(),
  '/newClentLoginScreen': (context) => const NewClientLoginScreen(),
  '/notesScreen': (context) => const NotesScreen(),
  '/paymentInstructionScreen': (context) => const PaymentInstructionScreen(),
  '/addNewItemScreen': (context) => const AddNewItemScreen(),
};
