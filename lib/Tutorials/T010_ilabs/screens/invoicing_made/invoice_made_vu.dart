import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'invoice_made_vm.dart';

class AuthScreen extends ViewModelBuilderWidget<InvoiceMadeViewModel> {
  const AuthScreen({Key? key}) : super(key: key);

  @override
  Widget builder(
      BuildContext context, InvoiceMadeViewModel viewModel, Widget? child) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.only(
            top: 80.0, left: 16.0, right: 16.0, bottom: 22.0),
        child: Expanded(
          flex: 4,
          child: Column(
            children: [
              invoiceMadeImage(),
              const SizedBox(
                height: 34.0,
              ),
              const Text(
                'Invoice Labs Invoicing Made Easy',
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 18.0,
                    fontWeight: FontWeight.w500),
              ),
              const SizedBox(
                height: 16.0,
              ),
              Text(
                'invoice Labs makes invoicing simple and effective. We allows you to create professional invoices with our invoice Generator and estimates With in seconds, so that you can focus on your work',
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: Colors.grey[500],
                    fontSize: 14.0,
                    fontWeight: FontWeight.w500),
              ),
              const Expanded(flex: 5, child: Text('')),
              Expanded(child: loginButton(context)),
              const SizedBox(
                height: 14.0,
              ),
              Expanded(child: createAccountButton()),
              const SizedBox(
                height: 14.0,
              ),
              Expanded(child: continueAsGuestButton()),
            ],
          ),
        ),
      ),
    );
  }

  Image invoiceMadeImage() {
    return Image.network(
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRvCMZE__w6ke-EXUXwLzvlDh7i_PRftAZpdHhDOePTaMjETXOwY7IeyN2SlNiZUscy-AA&usqp=CAU',
      width: 140,
      height: 140,
      fit: BoxFit.cover,
    );
  }

  Widget loginButton(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
            primary: Colors.orange[700],
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(12),
            )),
        onPressed: () {
          Navigator.pushNamed(context, '/logIn');
        },
        child: const Text(
          'Login',
          style: TextStyle(color: Colors.white, fontSize: 14),
        ),
      ),
    );
  }

  Widget createAccountButton() {
    return SizedBox(
      width: double.infinity,
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
            primary: Colors.orange[200],
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(12),
            )),
        onPressed: () {},
        child: Text(
          'Create Account',
          style: TextStyle(color: Colors.orange[800], fontSize: 14),
        ),
      ),
    );
  }

  Widget continueAsGuestButton() {
    return SizedBox(
      width: double.infinity,
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
            primary: Colors.grey[100],
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(12),
            )),
        onPressed: () {},
        child: const Text(
          'Continue as Guest',
          style: TextStyle(color: Colors.black, fontSize: 14),
        ),
      ),
    );
  }

  @override
  InvoiceMadeViewModel viewModelBuilder(BuildContext context) {
    return InvoiceMadeViewModel();
  }
}
