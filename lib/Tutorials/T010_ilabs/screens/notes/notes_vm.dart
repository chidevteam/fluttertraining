import 'package:stacked/stacked.dart';

class NotesViewModel extends BaseViewModel {
  bool isSignOnToggle = false;

  onSignToggle() {
    isSignOnToggle = !isSignOnToggle;
    notifyListeners();
  }
}
