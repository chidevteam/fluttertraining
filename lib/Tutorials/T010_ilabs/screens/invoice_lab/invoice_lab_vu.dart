import 'package:flutter/material.dart';

import 'package:stacked/stacked.dart';

import 'invioce_lab_vm.dart';

class InvoiceLabScreen extends ViewModelBuilderWidget<InvoiceLabViewModel> {
  const InvoiceLabScreen({Key? key}) : super(key: key);

  @override
  Widget builder(BuildContext context, viewModel, Widget? child) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(4.0),
        child: Stack(
          children: [
            Positioned(
                top: 0,
                left: 0,
                child: Container(
                  width: 40,
                  height: 100,
                  decoration: BoxDecoration(
                    color: Colors.grey[200],
                    borderRadius: const BorderRadius.horizontal(
                      right: Radius.elliptical(150, 250),
                      left: Radius.elliptical(5, 5),
                    ),
                  ),
                )),
            Positioned(
                top: 50,
                left: 80,
                child: Container(
                  width: 50,
                  height: 56,
                  decoration: BoxDecoration(
                    color: Colors.grey[200],
                    borderRadius: BorderRadius.circular(100),
                  ),
                )),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  'Welcome to invoicelabs',
                  style: TextStyle(
                      color: Colors.blueGrey[700],
                      fontSize: 18.0,
                      fontWeight: FontWeight.w800),
                ),
                const SizedBox(
                  height: 6.0,
                ),
                const Text(
                  'invoicing solution for small businesses',
                  style: TextStyle(
                      color: Colors.grey,
                      fontSize: 12.0,
                      fontWeight: FontWeight.w400),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(92.0, 16.0, 92.0, 16.0),
                  child: Card(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(12.0),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.only(top: 12.0, bottom: 6.0),
                        child: Column(
                          children: [
                            Image.network(
                              'https://play-lh.googleusercontent.com/_BmTvgHqlj1Ai8ZAPTc00x4jn19vfXUFlNYWScBvoPu7obzf6NcCtfPq0skGfRKtgts',
                              height: 100,
                              width: 100,
                              fit: BoxFit.cover,
                            ),
                            Padding(
                              padding: const EdgeInsets.only(bottom: 12.0),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: const [
                                  Text(
                                    'Invoice',
                                    style: TextStyle(
                                        color: Colors.orange,
                                        fontSize: 20.0,
                                        fontWeight: FontWeight.w700),
                                  ),
                                  Text('Lab',
                                      style: TextStyle(
                                          color: Colors.blue,
                                          fontSize: 20.0,
                                          fontWeight: FontWeight.w700)),
                                ],
                              ),
                            )
                          ],
                        ),
                      )),
                ),
              ],
            ),
            Positioned(
                bottom: 0.0,
                left: 40,
                child: Container(
                  width: 350,
                  height: 80,
                  decoration: BoxDecoration(
                    color: Colors.grey[200],
                    borderRadius: const BorderRadius.vertical(
                      top: Radius.elliptical(150, 60),
                      bottom: Radius.elliptical(0, 0),
                    ),
                  ),
                )),
          ],
        ),
      ),
    );
  }

  @override
  InvoiceLabViewModel viewModelBuilder(BuildContext context) {
    return InvoiceLabViewModel();
  }
}
