import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';

class AddNewItemViewModel extends BaseViewModel {
  final formKey = GlobalKey<FormState>();
  String? itemName = '';
  String? unitCode = '';
  bool isTaxableToggle = false;

  onTaxableToggleToggle() {
    isTaxableToggle = !isTaxableToggle;
    notifyListeners();
  }

  onItemName(String? value) async {
    itemName = value!.trim();

    notifyListeners();
  }

  String? onItemNameValidator(value) {
    if (value == null || value.isEmpty) {
      return 'item is required';
    }
    return null;
  }

  onItemUnitCode(String? value) async {
    unitCode = value!.trim();

    notifyListeners();
  }

  String? onItemUnitCodeValidator(value) {
    if (value == null || value.isEmpty) {
      return 'item is required';
    }
    return null;
  }

  onClickItem(BuildContext context) async {
    formKey.currentState!.save();
    if (formKey.currentState == null) {
      return;
    }

    if (!formKey.currentState!.validate()) {
      return;
    }
    print('$itemName');
    print('$unitCode');

    notifyListeners();
  }
}
