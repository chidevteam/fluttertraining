import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';

import 'new_item_vm.dart';

class AddNewItemScreen extends ViewModelBuilderWidget<AddNewItemViewModel> {
  const AddNewItemScreen({Key? key}) : super(key: key);

  @override
  Widget builder(BuildContext context, viewModel, Widget? child) {
    return Scaffold(
      appBar: AppBar(
          backgroundColor: Colors.white,
          title: const Text(
            'Add New Item',
            style: TextStyle(
                color: Colors.black,
                fontSize: 16.0,
                fontWeight: FontWeight.w400),
          ),
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context, false);
            },
            icon: Icon(
              Icons.arrow_back,
              color: Colors.orange[800],
            ),
          ),
          actions: [
            TextButton(
              onPressed: () {},
              child: Text(
                'Save',
                style: TextStyle(color: Colors.orange[800], fontSize: 16.0),
              ),
            ),
          ]),
      body: Padding(
        padding: const EdgeInsets.only(top: 24.0, left: 18.0, right: 18.0),
        child: SingleChildScrollView(
          child: Form(
            autovalidateMode: AutovalidateMode.onUserInteraction,
            key: viewModel.formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Item Name',
                  style: TextStyle(
                      color: Colors.grey[700],
                      fontSize: 13.0,
                      fontWeight: FontWeight.w400),
                ),
                const SizedBox(
                  height: 14.0,
                ),
                typeItemName(viewModel, context),
                const SizedBox(
                  height: 12.0,
                ),
                Text(
                  'Unit Cost',
                  style: TextStyle(
                      color: Colors.grey[700],
                      fontSize: 13.0,
                      fontWeight: FontWeight.w400),
                ),
                const SizedBox(
                  height: 12.0,
                ),
                Row(
                  children: [
                    Expanded(flex: 2, child: unitCode(viewModel, context)),
                    const SizedBox(width: 10.0),
                    SizedBox(
                      width: 26.0,
                      height: 26.0,
                      child: FloatingActionButton(
                          elevation: 0.0,
                          child: const Text('-'),
                          backgroundColor: Colors.orange[700],
                          onPressed: () {}),
                    ),
                    const SizedBox(width: 10.0),
                    Expanded(flex: 1, child: quantity(viewModel, context)),
                    const SizedBox(width: 10.0),
                    SizedBox(
                      width: 26.0,
                      height: 26.0,
                      child: FloatingActionButton(
                          elevation: 0.0,
                          child: const Text('+'),
                          backgroundColor: Colors.orange[700],
                          onPressed: () {}),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 12.0,
                ),
                taxable(viewModel),
                const SizedBox(
                  height: 12.0,
                ),
                Text(
                  'Discount',
                  style: TextStyle(
                      color: Colors.grey[700],
                      fontSize: 13.0,
                      fontWeight: FontWeight.w400),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget typeItemName(AddNewItemViewModel viewModel, context) {
    return TextFormField(
        keyboardType: TextInputType.text,
        decoration: InputDecoration(
          contentPadding:
              const EdgeInsets.symmetric(vertical: 6, horizontal: 10),
          fillColor: Colors.grey[200],
          filled: true,
          hintText: 'Keyboard',
          hintStyle: const TextStyle(
              fontWeight: FontWeight.normal, fontSize: 12, color: Colors.black),
          focusedBorder: const OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(12.0)),
              borderSide: BorderSide(
                color: Colors.orange,
              )),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.grey.shade400),
            borderRadius: const BorderRadius.all(
              Radius.circular(12.0),
            ),
          ),
        ),
        validator: viewModel.onItemNameValidator,
        onSaved: (String? value) {
          viewModel.onItemName(value);
        });
  }

  Widget unitCode(AddNewItemViewModel viewModel, context) {
    return TextFormField(
        keyboardType: TextInputType.text,
        decoration: InputDecoration(
          contentPadding:
              const EdgeInsets.symmetric(vertical: 0, horizontal: 10),
          fillColor: Colors.grey[200],
          filled: true,
          hintText: 'unit code',
          hintStyle: const TextStyle(
              fontWeight: FontWeight.normal, fontSize: 12, color: Colors.grey),
          focusedBorder: const OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(12.0)),
              borderSide: BorderSide(
                color: Colors.orange,
              )),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.grey.shade400),
            borderRadius: const BorderRadius.all(
              Radius.circular(12.0),
            ),
          ),
        ),
        validator: viewModel.onItemUnitCodeValidator,
        onSaved: (String? value) {
          viewModel.onItemUnitCode(value);
        });
  }

  Widget quantity(AddNewItemViewModel viewModel, context) {
    return TextFormField(
        keyboardType: TextInputType.text,
        decoration: InputDecoration(
          contentPadding:
              const EdgeInsets.symmetric(vertical: 0, horizontal: 2),
          fillColor: Colors.grey[200],
          filled: true,
          hintText: '',
          hintStyle: const TextStyle(
              fontWeight: FontWeight.normal, fontSize: 12, color: Colors.grey),
          focusedBorder: const OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(12.0)),
              borderSide: BorderSide(
                color: Colors.orange,
              )),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.grey.shade400),
            borderRadius: const BorderRadius.all(
              Radius.circular(12.0),
            ),
          ),
        ),
        validator: viewModel.onItemUnitCodeValidator,
        onSaved: (String? value) {
          viewModel.onItemUnitCode(value);
        });
  }

  Widget taxable(AddNewItemViewModel viewModel) {
    return Card(
        margin: EdgeInsets.zero,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8.0),
        ),
        elevation: 0.7,
        child: Padding(
          padding: const EdgeInsets.only(
            left: 10,
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                'Taxable',
                style: TextStyle(
                    fontSize: 13.0,
                    fontWeight: FontWeight.w500,
                    color: Colors.orange[700]),
              ),
              Transform.scale(
                scale: 0.8,
                child: Switch(
                  materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                  value: viewModel.isTaxableToggle,
                  activeColor: Colors.orange[700],
                  onChanged: (value) {
                    viewModel.onTaxableToggleToggle();
                  },
                ),
              )
            ],
          ),
        ));
  }

  Widget percentage(AddNewItemViewModel viewModel, context) {
    return TextFormField(
        keyboardType: TextInputType.text,
        decoration: InputDecoration(
          contentPadding:
              const EdgeInsets.symmetric(vertical: 0, horizontal: 2),
          fillColor: Colors.grey[200],
          filled: true,
          hintText: '',
          hintStyle: const TextStyle(
              fontWeight: FontWeight.normal, fontSize: 12, color: Colors.grey),
          focusedBorder: const OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(12.0)),
              borderSide: BorderSide(
                color: Colors.orange,
              )),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.grey.shade400),
            borderRadius: const BorderRadius.all(
              Radius.circular(12.0),
            ),
          ),
        ),
        validator: viewModel.onItemUnitCodeValidator,
        onSaved: (String? value) {
          viewModel.onItemUnitCode(value);
        });
  }

  @override
  AddNewItemViewModel viewModelBuilder(BuildContext context) {
    return AddNewItemViewModel();
  }
}
