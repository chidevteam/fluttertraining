import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';

class LoginViewModel extends BaseViewModel {
  final formKey = GlobalKey<FormState>();
  String? email = '';
  String? password = '';

  onEmail(String? value) async {
    email = value!.trim();

    notifyListeners();
  }

  String? onEmailValidator(value) {
    if (value == null || value.isEmpty) {
      return 'email is required';
    }
    return null;
  }

  onPassword(String? value) async {
    password = value!.trim();

    notifyListeners();
  }

  String? onPasswordValidator(value) {
    if (value == null || value.isEmpty) {
      return 'password is required';
    }
    return null;
  }

  onClickItem(BuildContext context) async {
    formKey.currentState!.save();
    if (formKey.currentState == null) {
      return;
    }

    if (!formKey.currentState!.validate()) {
      return;
    }
    print('$email');
    print('$password');

    notifyListeners();
  }
}
