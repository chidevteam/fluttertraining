import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';

class Client {
  String text1;
  String text2;
  String text3;
  String text4;

  Client(this.text1, this.text2, this.text3, this.text4);
}

class ClientListViewModel extends BaseViewModel {
  List<Client> items = [
    Client('Agha Khan International ', '2 Invoice', '\$ 0.0', 'Total'),
    Client('sarfarz ', '1 Invoice', '\$ 0.0', 'Total'),
  ];
  final formKey = GlobalKey<FormState>();
  String? itemName = '';
  String? unitCode = '';

  onItemName(String? value) async {
    itemName = value!.trim();

    notifyListeners();
  }

  String? onItemNameValidator(value) {
    if (value == null || value.isEmpty) {
      return 'item is required';
    }
    return null;
  }

  onItemUnitCode(String? value) async {
    unitCode = value!.trim();

    notifyListeners();
  }

  String? onItemUnitCodeValidator(value) {
    if (value == null || value.isEmpty) {
      return 'item is required';
    }
    return null;
  }

  onClickItem(BuildContext context) async {
    formKey.currentState!.save();
    if (formKey.currentState == null) {
      return;
    }

    if (!formKey.currentState!.validate()) {
      return;
    }
    print('$itemName');
    print('$unitCode');

    notifyListeners();
  }
}
