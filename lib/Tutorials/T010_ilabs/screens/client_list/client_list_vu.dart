import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'client_list_vm.dart';

class ClientListScreen extends ViewModelBuilderWidget<ClientListViewModel> {
  const ClientListScreen({Key? key}) : super(key: key);

  @override
  Widget builder(BuildContext context, viewModel, Widget? child) {
    return Scaffold(
      body: Column(
        children: [
          Expanded(
            child: ListView.builder(
                shrinkWrap: true,
                itemCount: viewModel.items.length,
                itemBuilder: (context, index) {
                  return clientList2(viewModel.items[index]);
                }),
          )
        ],
      ),
    );
  }

  Widget clientList2(
    Client item,
  ) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 4.0),
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8.0),
        ),
        elevation: 0.7,
        child: IntrinsicHeight(
          child: Padding(
            padding: const EdgeInsets.fromLTRB(12.0, 14.0, 12.0, 14.0),
            child: Row(
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      item.text1,
                      style: const TextStyle(
                        fontSize: 14.0,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    Text(
                      item.text2,
                      style: const TextStyle(
                        fontSize: 12.0,
                        fontWeight: FontWeight.w500,
                        color: Colors.grey,
                      ),
                    ),
                  ],
                ),
                const Spacer(),
                const VerticalDivider(
                  thickness: 0.8,
                  color: Colors.grey,
                ),
                const SizedBox(
                  width: 14.0,
                ),
                Column(
                  children: [
                    Text(
                      item.text3,
                      style: const TextStyle(
                        fontSize: 13.0,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    Text(
                      item.text4,
                      style: const TextStyle(
                          fontSize: 12.0,
                          fontWeight: FontWeight.w500,
                          color: Colors.grey),
                    )
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  viewModelBuilder(BuildContext context) {
    return ClientListViewModel();
  }
}
