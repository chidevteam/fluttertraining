import 'package:stacked/stacked.dart';

class Client {
  String text1;
  String text2;
  String text3;
  String text4;

  Client(this.text1, this.text2, this.text3, this.text4);
}

class HomeViewModel extends BaseViewModel {
  List<Client> items = [
    Client('Shifa International ', '2 Invoice', '\$ 0.0', 'Total'),
    Client('sarfarz ', '1 Invoice', '\$ 0.0', 'Total'),
  ];

  int index = 0;

  onItemTapped(int tappedIndex) {
    index = tappedIndex;
    notifyListeners();
  }
}
