import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'home_vm.dart';
import '../client_list/client_list_vu.dart';
import '../invoice_item_list/invoice_item_list_vu.dart';

class MyHomePage extends ViewModelBuilderWidget<HomeViewModel> {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  Widget builder(BuildContext context, viewModel, Widget? child) {
    List<Widget> widgetOptions = <Widget>[
      invoiceBottomNavigation(context),
      estimatesBottomNavigation(context),
      clientBottomNavigation(context, viewModel),
      itemBottomNavigation(context, viewModel),
    ];

    return DefaultTabController(
      length: 3,
      child: Scaffold(
        bottomNavigationBar: bottomNavigationBar(viewModel),
        body: Center(
          child: widgetOptions.elementAt(viewModel.index),
        ),
      ),
    );
  }

  Scaffold estimatesBottomNavigation(BuildContext context) {
    return Scaffold(
      appBar: appBarr("Estimates", true),
      drawer: drawer(context),
      body: const TabBarView(
        children: [
          Center(
              child: Text('This is for All tab!',
                  style: TextStyle(color: Colors.red, fontSize: 15))),
          Center(
              child: Text('This is for open tab!',
                  style: TextStyle(color: Colors.red, fontSize: 15))),
          Center(
              child: Text('This is for Closed tab',
                  style: TextStyle(color: Colors.red, fontSize: 15))),
        ],
      ),
      floatingActionButton: floatingButton(context, '/invoiceScreen'),
    );
  }

  Scaffold invoiceBottomNavigation(BuildContext context) {
    return Scaffold(
      appBar: appBarr("Invoice", true),
      drawer: drawer(context),
      body: TabBarView(
        children: [
          Column(
            children: [
              Expanded(
                flex: 3,
                child: ListView.builder(
                    scrollDirection: Axis.vertical,
                    itemCount: 2,
                    itemBuilder: (context, index) {
                      return viewList();
                    }),
              ),
            ],
          ),
          const Center(
              child: Text('This is for Outstanding tab!',
                  style: TextStyle(color: Colors.green, fontSize: 15))),
          const Center(
              child: Text('This is for Paid tab',
                  style: TextStyle(color: Colors.green, fontSize: 15))),
        ],
      ),
      floatingActionButton: floatingButton(context, '/invoiceScreen'),
    );
  }

  Scaffold itemBottomNavigation(BuildContext context, viewModel) {
    return Scaffold(
      appBar: appBarr("Items", false),
      drawer: drawer(context),
      body: const Center(
        child: InvoiceItemListScreen(),
      ),
      floatingActionButton: floatingButton(
        context,
        '/newItemScreen',
      ),
    );
  }

  Scaffold clientBottomNavigation(BuildContext context, viewModel) {
    return Scaffold(
      appBar: appBarr("Client", false),
      drawer: drawer(context),
      body: Padding(
        padding: const EdgeInsets.only(top: 8.0, left: 6.0, right: 6.0),
        child: ClientListScreen(),
      ),
      floatingActionButton: floatingButton(context, '/newClentLoginScreen'),
    );
  }

  static AppBar appBarr(String title, bool isTabBar) {
    return AppBar(
      title: Center(
        child: Text(
          title,
          style: const TextStyle(color: Colors.black),
        ),
      ),
      actions: [
        IconButton(
          icon: const Icon(
            Icons.signal_cellular_alt_outlined,
            color: Colors.black87,
          ),
          onPressed: () {},
        ),
      ],
      bottom: isTabBar ? tabBarItems(title) : null,
      leading: Builder(
        builder: (context) => IconButton(
          color: Colors.black,
          icon: const Icon(Icons.menu),
          onPressed: () {
            Scaffold.of(context).openDrawer();
          },
        ),
      ),
      backgroundColor: Colors.white,
    );
  }

  static Drawer drawer(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: [
          drawerHeader(),
          drawerCell(context, "Business ", Icons.note_add, ''),
          drawerCell(context, "Setting", Icons.settings, ''),
          drawerCell(context, "Choose Template", Icons.pending_actions, ''),
          drawerCell(context, "Region", Icons.local_airport, ''),
          drawerCell(context, "Upgrade Subscription", Icons.ac_unit, ''),
          drawerCell(context, "Switch Account", Icons.manage_accounts, ''),
          drawerCell(context, "Contact Us", Icons.contact_mail, ''),
          drawerCell(context, "Help", Icons.help, ''),
          drawerCell(context, "Termsof Use", Icons.theater_comedy_sharp, ''),
          drawerCell(context, "Privacy Plicy", Icons.privacy_tip_outlined, ''),
          drawerCell(context, "Log Out", Icons.logout, '/logIn'),
        ],
      ),
    );
  }

  static DrawerHeader drawerHeader() {
    return DrawerHeader(
      decoration: BoxDecoration(
        color: Colors.orange.shade700,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Container(
                height: 70,
                width: 80,
                child: Icon(
                  Icons.business_rounded,
                  color: Colors.orange.shade700,
                ),
                decoration: const BoxDecoration(
                    color: Colors.white, shape: BoxShape.circle),
              ),
              const Spacer(),
              Container(
                padding: EdgeInsets.zero,
                child: Row(
                  children: const [
                    Padding(
                      padding: EdgeInsets.all(3.0),
                      child: Icon(
                        Icons.edit,
                        color: Colors.orange,
                      ),
                    ),
                    Text(
                      "Edit",
                      style: TextStyle(color: Colors.orange, fontSize: 12),
                    )
                  ],
                ),
                height: 35,
                width: 60,
                decoration: const BoxDecoration(
                  color: Colors.white,
                  shape: BoxShape.rectangle,
                  borderRadius: BorderRadius.horizontal(
                    left: Radius.circular(50.0),
                  ),
                ),
              ),
            ],
          ),
          const Padding(
            padding: EdgeInsets.only(top: 10, bottom: 18),
            child: Text(
              'CHI ',
              style: TextStyle(color: Colors.white),
            ),
          ),
          Text('faridullah1228@gmail.com',
              style: TextStyle(color: Colors.amber.shade50, fontSize: 10)),
        ],
      ),
    );
  }

  BottomNavigationBar bottomNavigationBar(HomeViewModel viewModel) {
    return BottomNavigationBar(
      selectedFontSize: 12,
      onTap: viewModel.onItemTapped,
      selectedItemColor: Colors.orange,
      unselectedItemColor: Colors.grey,
      unselectedLabelStyle: const TextStyle(color: Colors.grey),
      showUnselectedLabels: true,
      currentIndex:
          viewModel.index, // this will be set when a new tab is tapped
      items: [
        bottomNavigationItem('Invoices', Icons.home),
        bottomNavigationItem('Estimates', Icons.calculate),
        bottomNavigationItem('Clients', Icons.people),
        bottomNavigationItem('Items', Icons.countertops)
      ],
    );
  }

  BottomNavigationBarItem bottomNavigationItem(String lb, IconData icon) {
    return BottomNavigationBarItem(
      icon: Icon(icon),
      label: lb,
    );
  }

  static TabBar tabBarItems(String lable) {
    return TabBar(
      unselectedLabelColor: Colors.grey.shade500,
      indicatorColor: Colors.orange,
      labelColor: Colors.orange.shade500,
      tabs: [
        const Tab(
          text: 'All',
        ),
        Tab(
          text: lable.contains("Invoice") ? 'Outstanding' : "Open",
        ),
        Tab(
          text: lable.contains("Invoice") ? 'Paid' : "Closed",
        )
      ],
    );
  }

  FloatingActionButton floatingButton(BuildContext context, String route) {
    return FloatingActionButton(
      onPressed: () {
        Navigator.pushNamed(context, route);
      },
      backgroundColor: Colors.orange,
      child: const Icon(Icons.add),
    );
  }

  static ListTile drawerCell(
      BuildContext context, String lable, IconData icon, String route) {
    return ListTile(
      title: Column(
        children: [
          Row(
            children: [
              Icon(
                icon,
                color: Colors.orange.shade400,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 20),
                child: Text(
                  lable,
                  style: const TextStyle(color: Colors.grey),
                ),
              ),
            ],
          ),
          const Divider(),
        ],
      ),
      onTap: () {
        Navigator.pop(context);
        Navigator.pushNamed(context, route);
      },
    );
  }

  static Widget viewList() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(10, 5, 10, 5),
      child: Card(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(10, 5, 10, 5),
              child: Row(
                children: [
                  Icon(
                    Icons.date_range_outlined,
                    color: Colors.grey.shade500,
                    size: 30,
                  ),
                  const Padding(
                    padding: EdgeInsets.only(left: 5),
                    child: Text(
                      '2022-01-24',
                      style: TextStyle(color: Colors.grey, fontSize: 12),
                    ),
                  ),
                  const Spacer(),
                  const Text(
                    'IN#001',
                    style: TextStyle(color: Colors.grey, fontSize: 12),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(10, 5, 10, 5),
              child: Row(
                children: const [
                  Text(
                    'No Client',
                    style: TextStyle(color: Colors.black87, fontSize: 15),
                  ),
                  Spacer(),
                  Text(
                    '\$ 17796',
                    style: TextStyle(
                      color: Colors.black87,
                      fontSize: 14,
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(10, 5, 10, 5),
              child: Row(
                children: const [
                  Text(
                    'Read',
                    style: TextStyle(color: Colors.green, fontSize: 14),
                  ),
                  Spacer(),
                  Text(
                    'Unpaid',
                    style: TextStyle(color: Colors.grey, fontSize: 14),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  @override
  HomeViewModel viewModelBuilder(BuildContext context) {
    return HomeViewModel();
  }
}
