import 'package:stacked/stacked.dart';

class InvoiceViewModel extends BaseViewModel {
  List<String> items = ['Delete', 'Open In', 'Share', 'Print', 'Duplicate'];

  int currentindex = 0;
  bool isSignOnToggle = false;
  bool isMarkPaidToggle = false;

  onCurrent(index) {
    currentindex = index;
    notifyListeners();
  }

  onSignToggle() {
    isSignOnToggle = !isSignOnToggle;
    notifyListeners();
  }

  onMarkPaidToggle() {
    isMarkPaidToggle = !isMarkPaidToggle;
    notifyListeners();
  }
}
