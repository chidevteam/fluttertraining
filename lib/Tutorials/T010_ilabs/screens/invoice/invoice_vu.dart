import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

import 'package:stacked/stacked.dart';

import 'invoice_vm.dart';

class InvoiceScreen extends ViewModelBuilderWidget<InvoiceViewModel> {
  InvoiceScreen({Key? key}) : super(key: key);
  final GlobalKey<PopupMenuButtonState<int>> _key = GlobalKey();

  @override
  Widget builder(BuildContext context, viewModel, Widget? child) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: const Text(
          'Invoice',
          style: TextStyle(
              color: Colors.black, fontSize: 16.0, fontWeight: FontWeight.w500),
        ),
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context, false);
          },
          icon: Icon(
            Icons.arrow_back,
            color: Colors.orange[800],
          ),
        ),
        actions: [
          popUpMenuButton(
            viewModel,
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(12.0, 14.0, 12.0, 0),
              child: bussinessInfo(),
            ),
            Padding(
              padding: const EdgeInsets.all(12.0),
              child: invoiceInfo(),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(12.0, 0.0, 12.0, 0),
              child: clientInfo(),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(12.0, 10.0, 12.0, 0.0),
              child: items(context),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(12.0, 10.0, 12.0, 0),
              child: paymentDetail(),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(12.0, 10.0, 12.0, 0),
              child: images(),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(12.0, 10.0, 12.0, 0),
              child: paymentInstruction(
                context,
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(12.0, 10.0, 12.0, 0),
              child: signedOn(viewModel),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(12.0, 10.0, 12.0, 22.0),
              child: markPaid(viewModel),
            ),
          ],
        ),
      ),
      floatingActionButton: sendbutton(),
      bottomNavigationBar: bottomNavigationBar(viewModel),
    );
  }

  Widget popUpMenuButton(InvoiceViewModel viewModel) {
    return PopupMenuButton(
        key: _key,
        icon: Icon(Icons.more_vert, color: Colors.orange[800]),
        itemBuilder: (_) => <PopupMenuItem<String>>[
              PopupMenuItem<String>(
                child: SizedBox(
                    width: 140,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          viewModel.items[0],
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Text(
                          viewModel.items[1],
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Text(
                          viewModel.items[2],
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Text(
                          viewModel.items[3],
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Text(
                          viewModel.items[4],
                        ),
                      ],
                    )),
              )
            ],
        onSelected: (_) {});
  }

  Widget sendbutton() {
    return SizedBox(
      width: 44.0,
      height: 44.0,
      child: FloatingActionButton(
          elevation: 0.0,
          child: const Icon(
            Icons.send,
            size: 18.0,
          ),
          backgroundColor: Colors.orange[700],
          onPressed: () {}),
    );
  }

  Widget bussinessInfo() {
    return Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8.0),
        ),
        elevation: 0.7,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(
                  left: 10.0, top: 8.0, bottom: 8.0, right: 8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Text(
                    'Business info',
                    style:
                        TextStyle(fontSize: 15.0, fontWeight: FontWeight.w500),
                  ),
                  Icon(
                    Icons.edit_outlined,
                    color: Colors.orange[800],
                    size: 22.0,
                  )
                ],
              ),
            ),
            const Divider(
              color: Colors.grey,
              indent: 10,
              endIndent: 10,
            ),
            Padding(
              padding: const EdgeInsets.only(left: 10.0, bottom: 10.0),
              child: Row(
                children: [
                  Image.network(
                    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSdr_BgkFdprR5I-QgTRz1yXQxAobF4SlH8zg&usqp=CAU",
                    width: 36.0,
                    height: 48.0,
                    fit: BoxFit.cover,
                  ),
                  const SizedBox(
                    width: 10.0,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text(
                        'On2SoI',
                        style: TextStyle(
                            fontSize: 13.0, fontWeight: FontWeight.w500),
                      ),
                      const SizedBox(
                        height: 8.0,
                      ),
                      Text(
                        'ahmadhassanch@hotmail.com',
                        style: TextStyle(
                            fontSize: 13.0,
                            fontWeight: FontWeight.w400,
                            color: Colors.grey[700]),
                      ),
                    ],
                  ),
                ],
              ),
            )
          ],
        ));
  }

  Widget invoiceInfo() {
    return Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8.0),
        ),
        elevation: 0.7,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(10.0, 8.0, 8.0, 8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Text(
                    'Invoice info',
                    style:
                        TextStyle(fontSize: 15.0, fontWeight: FontWeight.w500),
                  ),
                  Icon(
                    Icons.edit_outlined,
                    color: Colors.orange[800],
                    size: 22.0,
                  )
                ],
              ),
            ),
            const Divider(
              color: Colors.grey,
              indent: 10,
              endIndent: 10,
            ),
            Padding(
              padding: const EdgeInsets.only(left: 10.0, bottom: 12.0),
              child: Row(
                children: [
                  Icon(
                    Icons.add_chart_outlined,
                    size: 18.0,
                    color: Colors.blue[300],
                  ),
                  const SizedBox(
                    width: 8.0,
                  ),
                  Text(
                    'IN#004',
                    style: TextStyle(
                        fontSize: 13.0,
                        fontWeight: FontWeight.w400,
                        color: Colors.grey[600]),
                  ),
                ],
              ),
            ),
            Padding(
              padding:
                  const EdgeInsets.only(left: 10.0, right: 10.0, bottom: 8.0),
              child: Row(
                children: [
                  Icon(
                    Icons.calendar_today,
                    size: 18.0,
                    color: Colors.blue[400],
                  ),
                  const SizedBox(
                    width: 8.0,
                  ),
                  Text(
                    '2022-01-23',
                    style: TextStyle(
                        fontSize: 13.0,
                        fontWeight: FontWeight.w400,
                        color: Colors.grey[700]),
                  ),
                  const Spacer(),
                  Text(
                    'Due on Receipt',
                    style: TextStyle(
                        fontSize: 13.0,
                        fontWeight: FontWeight.w400,
                        color: Colors.grey[600]),
                  ),
                ],
              ),
            ),
          ],
        ));
  }

  Widget clientInfo() {
    return Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8.0),
        ),
        elevation: 0.7,
        child: Padding(
          padding: const EdgeInsets.fromLTRB(10.0, 12.0, 8.0, 12.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              const Text(
                'Client info',
                style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.w500),
              ),
              Icon(
                Icons.edit_outlined,
                color: Colors.orange[800],
                size: 22.0,
              )
            ],
          ),
        ));
  }

  Widget items(context) {
    return Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8.0),
        ),
        elevation: 0.7,
        child: Padding(
          padding: const EdgeInsets.fromLTRB(10.0, 10.0, 8.0, 6.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Text(
                'items',
                style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.w500),
              ),
              const Divider(
                color: Colors.grey,
              ),
              Padding(
                padding: const EdgeInsets.only(top: 4.0, bottom: 4.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Sub Total',
                      style: TextStyle(
                          fontSize: 13.0,
                          fontWeight: FontWeight.w500,
                          color: Colors.blue[600]),
                    ),
                    Text(
                      '\$ 0.0',
                      style: TextStyle(
                          fontSize: 13.0,
                          fontWeight: FontWeight.w500,
                          color: Colors.blue[600]),
                    ),
                  ],
                ),
              ),
              const Divider(
                color: Colors.grey,
              ),
              Padding(
                padding: const EdgeInsets.only(top: 4.0, bottom: 4.0),
                child: Align(
                    child: TextButton(
                  onPressed: () {
                    Navigator.pushNamed(context, '/addNewItemScreen');
                  },
                  child: Text(
                    '+Click to Add New items',
                    style: TextStyle(color: Colors.orange[800], fontSize: 13.0),
                  ),
                  style: TextButton.styleFrom(
                    minimumSize: Size.zero,
                    padding: EdgeInsets.zero,
                    tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                  ),
                )),
              )
            ],
          ),
        ));
  }

  Card paymentDetail() {
    return Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8.0),
        ),
        elevation: 0.7,
        child: Padding(
          padding: const EdgeInsets.fromLTRB(10.0, 8.0, 8.0, 8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Text(
                'Payment Details',
                style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.w500),
              ),
              const Divider(
                color: Colors.grey,
              ),
              Padding(
                padding: const EdgeInsets.only(top: 6.0, bottom: 6.0),
                child: Row(
                  children: const [
                    Text(
                      'Discount',
                      style: TextStyle(
                        fontSize: 13.0,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                    Spacer(),
                    Icon(
                      Icons.edit_outlined,
                      size: 13,
                      color: Colors.orange,
                    ),
                    SizedBox(
                      width: 2.0,
                    ),
                    Text(
                      '\$ 0.0',
                      style: TextStyle(
                        fontSize: 13.0,
                        fontWeight: FontWeight.w500,
                        color: Colors.grey,
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 6.0, bottom: 6.0),
                child: Row(
                  children: const [
                    Text(
                      'Tax',
                      style: TextStyle(
                        fontSize: 13.0,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                    Spacer(),
                    Icon(
                      Icons.edit_outlined,
                      size: 13,
                      color: Colors.orange,
                    ),
                    SizedBox(
                      width: 2.0,
                    ),
                    Text(
                      '\$ 0.0',
                      style: TextStyle(
                        fontSize: 13.0,
                        fontWeight: FontWeight.w500,
                        color: Colors.grey,
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 6.0, bottom: 6.0),
                child: Row(
                  children: const [
                    Text(
                      'Total',
                      style: TextStyle(
                        fontSize: 13.0,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                    Spacer(),
                    Text(
                      '\$ 0.0',
                      style: TextStyle(
                        fontSize: 13.0,
                        color: Colors.grey,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 6.0, bottom: 6.0),
                child: Row(
                  children: const [
                    Text(
                      'Client Payment',
                      style: TextStyle(
                        fontSize: 13.0,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                    Spacer(),
                    Icon(
                      Icons.edit_outlined,
                      size: 13,
                      color: Colors.orange,
                    ),
                    SizedBox(
                      width: 2.0,
                    ),
                    Text(
                      '\$ 0.0',
                      style: TextStyle(
                        fontSize: 13.0,
                        fontWeight: FontWeight.w500,
                        color: Colors.grey,
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 6.0, bottom: 6.0),
                child: Row(
                  children: const [
                    Text(
                      'Balance Due',
                      style: TextStyle(
                          fontSize: 13.0,
                          fontWeight: FontWeight.w500,
                          color: Colors.blue),
                    ),
                    Spacer(),
                    Text(
                      ' \$ 0.0',
                      style: TextStyle(
                        fontSize: 13.0,
                        fontWeight: FontWeight.w500,
                        color: Colors.blue,
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ));
  }

  Widget images() {
    return Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8.0),
        ),
        elevation: 0.7,
        child: Padding(
          padding: const EdgeInsets.fromLTRB(10.0, 14.0, 8.0, 14.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              const Text(
                'Images',
                style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.w500),
              ),
              TextButton(
                onPressed: () {},
                child: Text(
                  '+Add Image',
                  style: TextStyle(color: Colors.orange[800], fontSize: 13.0),
                ),
                style: TextButton.styleFrom(
                  minimumSize: Size.zero,
                  padding: EdgeInsets.zero,
                  tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                ),
              )
            ],
          ),
        ));
  }

  Card paymentInstruction(context) {
    return Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8.0),
        ),
        elevation: 0.7,
        child: Padding(
          padding: const EdgeInsets.fromLTRB(10.0, 8.0, 8.0, 8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Text(
                'Payment Instructions',
                style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.w500),
              ),
              const Divider(
                color: Colors.grey,
              ),
              Padding(
                padding: const EdgeInsets.only(top: 6.0, bottom: 6.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Text(
                      'Payment Method',
                      style: TextStyle(
                        fontSize: 13.0,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                    IconButton(
                      padding: EdgeInsets.zero,
                      constraints: const BoxConstraints(),
                      onPressed: () {
                        Navigator.pushNamed(
                            context, '/paymentInstructionScreen');
                      },
                      icon: const Icon(
                        Icons.edit_outlined,
                        size: 13,
                        color: Colors.orange,
                      ),
                    )
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 6.0, bottom: 6.0),
                child: Row(
                  children: [
                    const Text(
                      'Notes',
                      style: TextStyle(
                        fontSize: 13.0,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                    const Spacer(),
                    IconButton(
                      padding: EdgeInsets.zero,
                      constraints: const BoxConstraints(),
                      onPressed: () {
                        Navigator.pushNamed(context, '/notesScreen');
                      },
                      icon: const Icon(
                        Icons.edit_outlined,
                        size: 13,
                        color: Colors.orange,
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ));
  }

  Widget signedOn(InvoiceViewModel viewModel) {
    return Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8.0),
        ),
        elevation: 0.7,
        child: Padding(
          padding: const EdgeInsets.only(
            left: 10,
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              const Text(
                'Signed on',
                style: TextStyle(fontSize: 14.0, fontWeight: FontWeight.w500),
              ),
              Transform.scale(
                scale: 0.8,
                child: Switch(
                  value: viewModel.isSignOnToggle,
                  activeColor: Colors.orange[700],
                  onChanged: (value) {
                    viewModel.onSignToggle();
                  },
                ),
              )
            ],
          ),
        ));
  }

  Widget markPaid(InvoiceViewModel viewModel) {
    return Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8.0),
        ),
        elevation: 0.7,
        child: Padding(
          padding: const EdgeInsets.only(
            left: 10,
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              const Text(
                'Mark Paid',
                style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w500),
              ),
              Transform.scale(
                scale: 0.8,
                child: Switch(
                  value: viewModel.isMarkPaidToggle,
                  activeColor: Colors.orange[700],
                  onChanged: (value) {
                    viewModel.onMarkPaidToggle();
                  },
                ),
              )
            ],
          ),
        ));
  }

  Widget bottomNavigationBar(InvoiceViewModel viewModel) {
    return BottomNavigationBar(
        currentIndex: viewModel.currentindex,
        type: BottomNavigationBarType.fixed,
        selectedItemColor: Colors.orange,
        unselectedItemColor: Colors.grey,
        items: const [
          BottomNavigationBarItem(
            icon: Icon(
              Icons.edit_outlined,
              size: 18.0,
            ),
            label: 'Edit',
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.preview,
              size: 18.0,
            ),
            label: 'Preview',
          ),
          BottomNavigationBarItem(
              icon: Icon(
                Icons.history,
                size: 18.0,
              ),
              label: 'History',
              backgroundColor: Colors.grey),
        ],
        onTap: (index) => viewModel.onCurrent(index));
  }

  @override
  InvoiceViewModel viewModelBuilder(BuildContext context) {
    return InvoiceViewModel();
  }
}
