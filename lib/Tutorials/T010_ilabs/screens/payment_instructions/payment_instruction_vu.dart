import 'package:flutter/material.dart';
import 'payment_instruction_vm.dart';

import 'package:stacked/stacked.dart';

class PaymentInstructionScreen
    extends ViewModelBuilderWidget<PaymentInstructionViewModel> {
  const PaymentInstructionScreen({Key? key}) : super(key: key);

  @override
  Widget builder(BuildContext context, PaymentInstructionViewModel viewModel,
      Widget? child) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: const Text(
          'Payment instructions',
          style: TextStyle(
              color: Colors.black, fontSize: 16.0, fontWeight: FontWeight.w500),
        ),
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context, false);
          },
          icon: Icon(
            Icons.arrow_back,
            color: Colors.orange[800],
          ),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.only(top: 18.0, right: 14.0, left: 14.0),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Paypal Email',
                style: TextStyle(
                    fontSize: 12.0,
                    fontWeight: FontWeight.w500,
                    color: Colors.grey[400]),
              ),
              const SizedBox(
                height: 12.0,
              ),
              paypalEmail(viewModel, context),
              const SizedBox(
                height: 12.0,
              ),
              Text(
                'Make Checks Payable To',
                style: TextStyle(
                    fontSize: 12.0,
                    fontWeight: FontWeight.w500,
                    color: Colors.grey[400]),
              ),
              const SizedBox(
                height: 12.0,
              ),
              makeCheckPayable(viewModel, context),
              const SizedBox(
                height: 12.0,
              ),
              Text(
                'Bank Transfer',
                style: TextStyle(
                    fontSize: 12.0,
                    fontWeight: FontWeight.w500,
                    color: Colors.grey[400]),
              ),
              const SizedBox(
                height: 12.0,
              ),
              bankTransfer(viewModel, context),
              addNowButton(context)
            ],
          ),
        ),
      ),
    );
  }

  Widget bankTransfer(PaymentInstructionViewModel viewModel, context) {
    return TextFormField(
      minLines: 4,
      maxLines: null,
      keyboardType: TextInputType.text,
      decoration: InputDecoration(
        hintText: 'Enter bank account and other details here',
        hintStyle: TextStyle(
            fontWeight: FontWeight.w500, fontSize: 13, color: Colors.grey[500]),
        focusedBorder: const OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(8.0)),
            borderSide: BorderSide(
              color: Colors.orange,
            )),
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.grey.shade300),
          borderRadius: const BorderRadius.all(
            Radius.circular(8.0),
          ),
        ),
      ),
    );
  }

  Widget paypalEmail(PaymentInstructionViewModel viewModel, context) {
    return TextFormField(
        keyboardType: TextInputType.text,
        decoration: InputDecoration(
          contentPadding:
              const EdgeInsets.symmetric(vertical: 4.0, horizontal: 6.0),
          hintText: 'Type Paypal Email',
          hintStyle: const TextStyle(
              fontWeight: FontWeight.w500, fontSize: 12, color: Colors.grey),
          focusedBorder: const OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(8.0)),
              borderSide: BorderSide(
                color: Colors.orange,
              )),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.grey.shade300),
            borderRadius: const BorderRadius.all(
              Radius.circular(8.0),
            ),
          ),
        )
        // validator: viewModel.onOfficeValidator,
        // onSaved: (String? value) {
        // //   viewModel.onOffice(value);
        // }
        );
  }

  Widget makeCheckPayable(PaymentInstructionViewModel viewModel, context) {
    return TextFormField(
      keyboardType: TextInputType.text,
      decoration: InputDecoration(
        contentPadding:
            const EdgeInsets.symmetric(vertical: 4.0, horizontal: 6.0),
        hintText: 'Type you or your business name',
        hintStyle: const TextStyle(
            fontWeight: FontWeight.w500, fontSize: 12, color: Colors.grey),
        focusedBorder: const OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(8.0)),
            borderSide: BorderSide(
              color: Colors.orange,
            )),
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.grey.shade300),
          borderRadius: const BorderRadius.all(
            Radius.circular(8.0),
          ),
        ),
      ),
      // validator: viewModel.onFaxValidator,
      // onSaved: (String? value) {
      //   viewModel.onFax(value);
      // }
    );
  }

  Widget addNowButton(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 22.0),
      child: SizedBox(
        width: double.infinity,
        height: 44.0,
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(
              primary: Colors.orange[600],
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(8.0),
              )),
          onPressed: () {},
          child: const Text(
            'Save',
            style: TextStyle(
                color: Colors.white,
                fontSize: 14.0,
                fontWeight: FontWeight.normal),
          ),
        ),
      ),
    );
  }

  @override
  PaymentInstructionViewModel viewModelBuilder(BuildContext context) {
    return PaymentInstructionViewModel();
  }
}
