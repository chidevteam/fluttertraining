import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';

class NewClientViewModel extends BaseViewModel {
  final formKey = GlobalKey<FormState>();
  String? clientName = '';
  String? email = '';
  String? mobile = '';
  String? office = '';
  String? fax = '';
  String? officeAddress = "";

  onClientName(String? value) async {
    clientName = value!.trim();

    notifyListeners();
  }

  String? onClientNameValidator(value) {
    if (value == null || value.isEmpty) {
      return 'ClientName is required';
    }
    return null;
  }

  onEmail(String? value) async {
    email = value!.trim();

    notifyListeners();
  }

  String? onEmailValidator(value) {
    if (value == null || value.isEmpty) {
      return 'Email is required';
    }
    return null;
  }

  onMobile(String? value) async {
    mobile = value!.trim();

    notifyListeners();
  }

  String? onMobileValidator(value) {
    if (value == null || value.isEmpty) {
      return 'Mobile Number is required';
    }
    return null;
  }

  onOffice(String? value) async {
    office = value!.trim();

    notifyListeners();
  }

  String? onOfficeValidator(value) {
    if (value == null || value.isEmpty) {
      return 'Office Number is required';
    }
    return null;
  }

  onFax(String? value) async {
    fax = value!.trim();

    notifyListeners();
  }

  String? onFaxValidator(value) {
    if (value == null || value.isEmpty) {
      return 'Fax is required';
    }
    return null;
  }

  onOfficeAddress(String? value) async {
    officeAddress = value!.trim();

    notifyListeners();
  }

  String? onOfficeAddressValidator(value) {
    if (value == null || value.isEmpty) {
      return 'Office Address is required';
    }
    return null;
  }

  onClickItem(BuildContext context) async {
    formKey.currentState!.save();
    if (formKey.currentState == null) {
      return;
    }

    if (!formKey.currentState!.validate()) {
      return;
    }

    notifyListeners();
  }
}
