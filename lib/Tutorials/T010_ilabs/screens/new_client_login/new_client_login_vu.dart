import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';

import 'new_client_login_vm.dart';

class NewClientLoginScreen extends ViewModelBuilderWidget<NewClientViewModel> {
  const NewClientLoginScreen({Key? key}) : super(key: key);

  @override
  Widget builder(BuildContext context, viewModel, Widget? child) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: const Text(
          'Create New Client',
          style: TextStyle(
              color: Colors.black, fontSize: 16.0, fontWeight: FontWeight.w400),
        ),
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context, false);
          },
          icon: Icon(
            Icons.arrow_back,
            color: Colors.orange[800],
          ),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.only(top: 24.0, left: 18.0, right: 18.0),
        child: SingleChildScrollView(
          child: Form(
            autovalidateMode: AutovalidateMode.onUserInteraction,
            key: viewModel.formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Client Name',
                  style: TextStyle(
                      color: Colors.grey[500],
                      fontSize: 12.0,
                      fontWeight: FontWeight.w400),
                ),
                const SizedBox(
                  height: 14.0,
                ),
                clientName(viewModel, context),
                const SizedBox(
                  height: 12.0,
                ),
                Text(
                  'Email',
                  style: TextStyle(
                      color: Colors.grey[500],
                      fontSize: 12.0,
                      fontWeight: FontWeight.w400),
                ),
                const SizedBox(
                  height: 12.0,
                ),
                email(viewModel, context),
                const SizedBox(
                  height: 12.0,
                ),
                Text(
                  'Mobile Number',
                  style: TextStyle(
                      color: Colors.grey[500],
                      fontSize: 12.0,
                      fontWeight: FontWeight.w400),
                ),
                const SizedBox(
                  height: 12.0,
                ),
                mobileNumber(viewModel, context),
                const SizedBox(
                  height: 12.0,
                ),
                Text(
                  'Office Phone',
                  style: TextStyle(
                      color: Colors.grey[500],
                      fontSize: 12.0,
                      fontWeight: FontWeight.w400),
                ),
                const SizedBox(
                  height: 12.0,
                ),
                officePhone(viewModel, context),
                const SizedBox(
                  height: 12.0,
                ),
                Text(
                  'Fax',
                  style: TextStyle(
                      color: Colors.grey[500],
                      fontSize: 12.0,
                      fontWeight: FontWeight.w400),
                ),
                const SizedBox(
                  height: 12.0,
                ),
                faxNumber(viewModel, context),
                const SizedBox(
                  height: 12.0,
                ),
                Text(
                  'Office Address',
                  style: TextStyle(
                      color: Colors.grey[600],
                      fontSize: 12.0,
                      fontWeight: FontWeight.w400),
                ),
                const SizedBox(
                  height: 12.0,
                ),
                officeAddress(viewModel, context),
                itemSaveButton(viewModel, context),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget clientName(NewClientViewModel viewModel, context) {
    return TextFormField(
        keyboardType: TextInputType.text,
        decoration: InputDecoration(
          contentPadding:
              const EdgeInsets.symmetric(vertical: 6, horizontal: 10),
          hintText: ' ff',
          hintStyle: const TextStyle(
              fontWeight: FontWeight.normal, fontSize: 12, color: Colors.grey),
          suffixIcon: IconButton(
              icon: const Icon(
                Icons.person,
                color: Colors.orange,
              ),
              onPressed: () {}),
          focusedBorder: const OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(8.0)),
              borderSide: BorderSide(
                color: Colors.orange,
              )),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.grey.shade300),
            borderRadius: const BorderRadius.all(
              Radius.circular(8.0),
            ),
          ),
          errorBorder: OutlineInputBorder(
            borderSide: const BorderSide(color: Colors.red),
            borderRadius: BorderRadius.circular(8.0),
          ),
          focusedErrorBorder: OutlineInputBorder(
            borderSide: const BorderSide(color: Colors.red),
            borderRadius: BorderRadius.circular(8.0),
          ),
        ),
        validator: viewModel.onClientNameValidator,
        onSaved: (String? value) {
          viewModel.onClientName(value);
        });
  }

  Widget email(NewClientViewModel viewModel, context) {
    return TextFormField(
        keyboardType: TextInputType.text,
        decoration: InputDecoration(
          contentPadding:
              const EdgeInsets.symmetric(vertical: 6, horizontal: 10),
          hintText: 'Type Email',
          hintStyle: const TextStyle(
              fontWeight: FontWeight.normal, fontSize: 12, color: Colors.grey),
          focusedBorder: const OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(8.0)),
              borderSide: BorderSide(
                color: Colors.orange,
              )),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.grey.shade300),
            borderRadius: const BorderRadius.all(
              Radius.circular(8.0),
            ),
          ),
          errorBorder: OutlineInputBorder(
            borderSide: const BorderSide(color: Colors.red),
            borderRadius: BorderRadius.circular(8.0),
          ),
          focusedErrorBorder: OutlineInputBorder(
            borderSide: const BorderSide(color: Colors.red),
            borderRadius: BorderRadius.circular(8.0),
          ),
        ),
        validator: viewModel.onEmailValidator,
        onSaved: (String? value) {
          viewModel.onEmail(value);
        });
  }

  Widget mobileNumber(NewClientViewModel viewModel, context) {
    return TextFormField(
        keyboardType: TextInputType.text,
        decoration: InputDecoration(
          contentPadding:
              const EdgeInsets.symmetric(vertical: 6, horizontal: 10),
          hintText: 'Enter Mobile Number',
          hintStyle: const TextStyle(
              fontWeight: FontWeight.normal, fontSize: 12, color: Colors.grey),
          focusedBorder: const OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(8.0)),
              borderSide: BorderSide(
                color: Colors.orange,
              )),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.grey.shade300),
            borderRadius: const BorderRadius.all(
              Radius.circular(8.0),
            ),
          ),
          errorBorder: OutlineInputBorder(
            borderSide: const BorderSide(color: Colors.red),
            borderRadius: BorderRadius.circular(8.0),
          ),
          focusedErrorBorder: OutlineInputBorder(
            borderSide: const BorderSide(color: Colors.red),
            borderRadius: BorderRadius.circular(8.0),
          ),
        ),
        validator: viewModel.onMobileValidator,
        onSaved: (String? value) {
          viewModel.onMobile(value);
        });
  }

  Widget officePhone(NewClientViewModel viewModel, context) {
    return TextFormField(
        keyboardType: TextInputType.text,
        decoration: InputDecoration(
          contentPadding:
              const EdgeInsets.symmetric(vertical: 4.0, horizontal: 6.0),
          hintText: 'Enter Office Phone',
          hintStyle: const TextStyle(
              fontWeight: FontWeight.normal, fontSize: 12, color: Colors.grey),
          focusedBorder: const OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(8.0)),
              borderSide: BorderSide(
                color: Colors.orange,
              )),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.grey.shade300),
            borderRadius: const BorderRadius.all(
              Radius.circular(8.0),
            ),
          ),
          errorBorder: OutlineInputBorder(
            borderSide: const BorderSide(color: Colors.red),
            borderRadius: BorderRadius.circular(8.0),
          ),
          focusedErrorBorder: OutlineInputBorder(
            borderSide: const BorderSide(color: Colors.red),
            borderRadius: BorderRadius.circular(8.0),
          ),
        ),
        validator: viewModel.onOfficeValidator,
        onSaved: (String? value) {
          viewModel.onOffice(value);
        });
  }

  Widget faxNumber(NewClientViewModel viewModel, context) {
    return TextFormField(
        keyboardType: TextInputType.text,
        decoration: InputDecoration(
          contentPadding:
              const EdgeInsets.symmetric(vertical: 4.0, horizontal: 6.0),
          hintText: 'Enter Fax Number',
          hintStyle: const TextStyle(
              fontWeight: FontWeight.normal, fontSize: 12, color: Colors.grey),
          focusedBorder: const OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(8.0)),
              borderSide: BorderSide(
                color: Colors.orange,
              )),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.grey.shade300),
            borderRadius: const BorderRadius.all(
              Radius.circular(8.0),
            ),
          ),
          errorBorder: OutlineInputBorder(
            borderSide: const BorderSide(color: Colors.red),
            borderRadius: BorderRadius.circular(8.0),
          ),
          focusedErrorBorder: OutlineInputBorder(
            borderSide: const BorderSide(color: Colors.red),
            borderRadius: BorderRadius.circular(8.0),
          ),
        ),
        validator: viewModel.onFaxValidator,
        onSaved: (String? value) {
          viewModel.onFax(value);
        });
  }

  Widget officeAddress(NewClientViewModel viewModel, context) {
    return TextFormField(
        keyboardType: TextInputType.text,
        decoration: InputDecoration(
          contentPadding:
              const EdgeInsets.symmetric(vertical: 6, horizontal: 10),
          hintText: 'TypeAddress',
          hintStyle: const TextStyle(
              fontWeight: FontWeight.normal, fontSize: 12, color: Colors.grey),
          focusedBorder: const OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(8.0)),
              borderSide: BorderSide(
                color: Colors.orange,
              )),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.grey.shade300),
            borderRadius: const BorderRadius.all(
              Radius.circular(8.0),
            ),
          ),
          errorBorder: OutlineInputBorder(
            borderSide: const BorderSide(color: Colors.red),
            borderRadius: BorderRadius.circular(8.0),
          ),
          focusedErrorBorder: OutlineInputBorder(
            borderSide: const BorderSide(color: Colors.red),
            borderRadius: BorderRadius.circular(8.0),
          ),
        ),
        validator: viewModel.onOfficeValidator,
        onSaved: (String? value) {
          viewModel.onOffice(value);
        });
  }

  Widget itemSaveButton(NewClientViewModel viewModel, BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 16.0),
      child: SizedBox(
        width: double.infinity,
        height: 43.0,
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(
              primary: Colors.orange[600],
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(8.0),
              )),
          onPressed: () {
            viewModel.onClickItem(context);
          },
          child: const Text(
            'ADD NOW',
            style: TextStyle(
                color: Colors.white,
                fontSize: 13.0,
                fontWeight: FontWeight.normal),
          ),
        ),
      ),
    );
  }

  @override
  NewClientViewModel viewModelBuilder(BuildContext context) {
    return NewClientViewModel();
  }
}
