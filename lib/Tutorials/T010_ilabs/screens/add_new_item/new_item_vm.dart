import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import '../../services/service.dart';
import 'dart:convert';

class NewItemViewModel extends BaseViewModel {
  final formKey = GlobalKey<FormState>();
  String? itemName = '';
  String? unitCode = '';

  onItemName(String? value) async {
    itemName = value!.trim();

    notifyListeners();
  }

  String? onItemNameValidator(value) {
    if (value == null || value.isEmpty) {
      return 'item is required';
    }
    return null;
  }

  onItemUnitCode(String? value) async {
    unitCode = value!.trim();

    notifyListeners();
  }

  String? onItemUnitCodeValidator(value) {
    if (value == null || value.isEmpty) {
      return 'item is required';
    }
    return null;
  }

  onClickItem(BuildContext context) async {
    formKey.currentState!.save();
    if (formKey.currentState == null) {
      return;
    }

    if (!formKey.currentState!.validate()) {
      return;
    }
    print('$itemName');
    print('$unitCode');

    Request addItemReq = {
      "taxable": true,
      "description": itemName,
      "additional_detail": "This unit",
      "unit_cost": unitCode
    };
    var x = await login();
    var json = jsonDecode(x.body);
    String token = json["accessToken"];
    print(token);

    var item = await addItem(addItemReq, token);
    print(item);
    // notifyListeners();
  }
}
