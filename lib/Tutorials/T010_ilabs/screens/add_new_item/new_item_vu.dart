import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';

import 'new_item_vm.dart';

class NewItemScreen extends ViewModelBuilderWidget<NewItemViewModel> {
  const NewItemScreen({Key? key}) : super(key: key);

  @override
  Widget builder(BuildContext context, viewModel, Widget? child) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: const Text(
          'Add New Item',
          style: TextStyle(
              color: Colors.black, fontSize: 16.0, fontWeight: FontWeight.w400),
        ),
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context, false);
          },
          icon: Icon(
            Icons.arrow_back,
            color: Colors.orange[800],
          ),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.only(top: 24.0, left: 18.0, right: 18.0),
        child: SingleChildScrollView(
          child: Form(
            autovalidateMode: AutovalidateMode.onUserInteraction,
            key: viewModel.formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Item Name',
                  style: TextStyle(
                      color: Colors.grey[700],
                      fontSize: 13.0,
                      fontWeight: FontWeight.w400),
                ),
                const SizedBox(
                  height: 14.0,
                ),
                typeItemName(viewModel, context),
                const SizedBox(
                  height: 16.0,
                ),
                Text(
                  'Unit Cost',
                  style: TextStyle(
                      color: Colors.grey[700],
                      fontSize: 13.0,
                      fontWeight: FontWeight.w400),
                ),
                const SizedBox(
                  height: 14.0,
                ),
                unitCode(viewModel, context),
                itemSaveButton(viewModel, context),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget typeItemName(NewItemViewModel viewModel, context) {
    return TextFormField(
        keyboardType: TextInputType.text,
        decoration: InputDecoration(
          contentPadding:
              const EdgeInsets.symmetric(vertical: 6, horizontal: 10),
          fillColor: Colors.grey[200],
          filled: true,
          hintText: 'Type Item Name',
          hintStyle: const TextStyle(
              fontWeight: FontWeight.normal, fontSize: 12, color: Colors.grey),
          focusedBorder: const OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(12.0)),
              borderSide: BorderSide(
                color: Colors.orange,
              )),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.grey.shade400),
            borderRadius: const BorderRadius.all(
              Radius.circular(12.0),
            ),
          ),
          errorBorder: OutlineInputBorder(
            borderSide: const BorderSide(color: Colors.red),
            borderRadius: BorderRadius.circular(12.0),
          ),
          focusedErrorBorder: OutlineInputBorder(
            borderSide: const BorderSide(color: Colors.red),
            borderRadius: BorderRadius.circular(12.0),
          ),
        ),
        validator: viewModel.onItemNameValidator,
        onSaved: (String? value) {
          viewModel.onItemName(value);
        });
  }

  Widget unitCode(NewItemViewModel viewModel, context) {
    return TextFormField(
        keyboardType: TextInputType.text,
        decoration: InputDecoration(
          contentPadding:
              const EdgeInsets.symmetric(vertical: 6, horizontal: 10),
          fillColor: Colors.grey[200],
          filled: true,
          hintText: 'unit code',
          hintStyle: const TextStyle(
              fontWeight: FontWeight.normal, fontSize: 12, color: Colors.grey),
          focusedBorder: const OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(12.0)),
              borderSide: BorderSide(
                color: Colors.orange,
              )),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.grey.shade400),
            borderRadius: const BorderRadius.all(
              Radius.circular(12.0),
            ),
          ),
          errorBorder: OutlineInputBorder(
            borderSide: const BorderSide(color: Colors.red),
            borderRadius: BorderRadius.circular(12.0),
          ),
          focusedErrorBorder: OutlineInputBorder(
            borderSide: const BorderSide(color: Colors.red),
            borderRadius: BorderRadius.circular(12.0),
          ),
        ),
        validator: viewModel.onItemUnitCodeValidator,
        onSaved: (String? value) {
          viewModel.onItemUnitCode(value);
        });
  }

  Widget itemSaveButton(NewItemViewModel viewModel, BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 22.0, vertical: 16.0),
      child: SizedBox(
        width: double.infinity,
        height: 45.0,
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(
              primary: Colors.orange[600],
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(12),
              )),
          onPressed: () {
            viewModel.onClickItem(context);
          },
          child: const Text(
            'SAVE',
            style: TextStyle(
              color: Colors.white,
              fontSize: 14,
            ),
          ),
        ),
      ),
    );
  }

  @override
  viewModelBuilder(BuildContext context) {
    return NewItemViewModel();
  }
}
