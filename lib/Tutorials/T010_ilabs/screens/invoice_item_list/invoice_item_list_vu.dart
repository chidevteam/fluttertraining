import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'invoice_item_list_vm.dart';
import '../../models/invoice_item/invoice_item_model.dart';

class InvoiceItemListScreen
    extends ViewModelBuilderWidget<InvoiceItemListViewModel> {
  const InvoiceItemListScreen({Key? key}) : super(key: key);

  @override
  Widget builder(
      BuildContext context, InvoiceItemListViewModel viewModel, Widget? child) {
    return Scaffold(
      body: Column(
        children: [
          Expanded(
            child: ListView.builder(
                shrinkWrap: true,
                itemCount: viewModel.items.length,
                itemBuilder: (context, index) {
                  return invoiceItemCell(viewModel.items[index]);
                }),
          )
        ],
      ),
    );
  }

  Widget invoiceItemCell(
    InvoiceItem item,
  ) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 4.0),
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8.0),
        ),
        elevation: 0.7,
        child: IntrinsicHeight(
          child: Padding(
            padding: const EdgeInsets.fromLTRB(12.0, 14.0, 12.0, 14.0),
            child: Row(
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      item.description!,
                      style: const TextStyle(
                        fontSize: 14.0,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    Text(
                      item.unit_cost.toString(),
                      style: const TextStyle(
                        fontSize: 12.0,
                        fontWeight: FontWeight.w500,
                        color: Colors.grey,
                      ),
                    ),
                  ],
                ),
                const Spacer(),
                const VerticalDivider(
                  thickness: 0.8,
                  color: Colors.grey,
                ),
                const SizedBox(
                  width: 14.0,
                ),
                Column(
                  children: [
                    Text(
                      item.taxable.toString(),
                      style: const TextStyle(
                        fontSize: 13.0,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    Text(
                      "Hellow !!",
                      style: const TextStyle(
                          fontSize: 12.0,
                          fontWeight: FontWeight.w500,
                          color: Colors.grey),
                    )
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  viewModelBuilder(BuildContext context) {
    var vm = InvoiceItemListViewModel();

    vm.refreshData(context);
    return vm;
  }
}
