import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'dart:convert';
import '../../services/service.dart';
import '../../models/invoice_item/invoice_item_model.dart';

// class InvoiceItem {
//   String text1;
//   String text2;
//   String text3;
//   String text4;

//   InvoiceItem(this.text1, this.text2, this.text3, this.text4);
// }

class InvoiceItemListViewModel extends BaseViewModel {
  // List<InvoiceItem> items = [
  //   InvoiceItem('Shifa International ', '2 Invoice', '\$ 0.0', 'Total'),
  //   InvoiceItem('sarfarz ', '1 Invoice', '\$ 0.0', 'Total'),
  // ];
  List<InvoiceItem> items = [];

  onRefreshCalled(BuildContext context) {
    print("Refresh called .........&&&&&&&&&");
    items.clear();
    // limit = 30;
    // offset = 0;
    // totalRecord = 0;

    refreshData(context);
  }

  refreshData(context,
      {bool isPaginated = false, String searchText = ""}) async {
    print(".....................in api fuc");

    // if (!isPaginated) {
    //   setBusy(true);
    // }

    // print("$resp");
    // for (int i = 0; i < resp.data!.data!.length; i += 1)
    //   print('${resp.data!.data![i].userName}');

    // final resp = await SystemApiServiceNew.getHistoryVitals(req);
    var x = await login();
    var json = jsonDecode(x.body);
    String token = json["accessToken"];
    print(token);
    items = await getItems({}, token);

    notifyListeners();

    // if (resp.isSuccess) {
    //   if (!isPaginated) {
    //     setBusy(false);
    //   }
    //   print(".....................success");
    //   items!.clear();
    //   items!.addAll(resp.data!.data!);

    //   notifyListeners();
    // } else {
    //   print(".....................fail");
    //   if (!isPaginated) {
    //     setBusy(false);
    //   }
    // print("")
    // showAlert(context,
    //     title: 'Data Fetched Failed',
    //     message: resp.errorMessage ?? 'Error occurred while getting data.');
    // }
  }
}
