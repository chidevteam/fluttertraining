import 'dart:convert';
import 'package:http/http.dart' as http;
import 'dart:io';
import '../models/invoice_item/invoice_item_model.dart';

typedef Request = Map<String, dynamic>;

Future<http.Response> login() {
  Map<String, dynamic> req = {
    "email": "ahmadhassanch@hotmail.com",
    "password": "test1234"
  };
  return http.post(
    Uri.parse('https://invoicelabs.co/api/auth/login'),
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    },
    body: jsonEncode(req),
  );
}

Future<Map> addItem(Request req, String mAuthToken) async {
  final response = await http.post(
    Uri.parse('https://invoicelabs.co/api/items'),
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
      'Authorization': "Bearer " + mAuthToken
    },
    body: jsonEncode(req),
  );
  if (response.statusCode == 200) {
    var json = jsonDecode(response.body);
    return json;
  } else {
    throw Exception('Failed to load album');
  }
}

Future<List<InvoiceItem>> getItems(Request req, String mAuthToken) async {
  String url = 'https://invoicelabs.co/api/items/0/10/id/DESC';
  final response = await http.get(Uri.parse(url), headers: <String, String>{
    'Content-Type': 'application/json; charset=UTF-8',
    'Authorization': "Bearer " + mAuthToken
  });
  if (response.statusCode == 200) {
    final json = jsonDecode(response.body);
    InvoiceItemList iList = InvoiceItemList.fromJson(json);
    return iList.invoiceList;

    //   final resultList = json.map((e) => Photo.fromJson(e)).toList();
    //   String name = resultList[0].url.toString();
    //   print(name);
    //   return resultList;
  } else {
    throw Exception('Error Fetching Files');
  }
}
