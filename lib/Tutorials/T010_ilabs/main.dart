import 'package:flutter/material.dart';
import 'routes.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:universal/firebase_options.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

// void main() {
//   print("hello world");
// }

void main() async {
  print("Starting");
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  await addUser();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      routes: appRoutes,
      initialRoute: "/",
    );
  }
}

Future<void> addUser() {
  CollectionReference users = FirebaseFirestore.instance.collection('clients');
  // Call the user's CollectionReference to add a new user
  return users
      .add({
        'name': "Ali Khan", // John Doe
        'age': 28 // 42
      })
      .then((value) => print("User Added"))
      .catchError((error) => print("Failed to add user: $error"));
}
