import 'package:flutter/material.dart';

class DialChart extends StatelessWidget {
  const DialChart({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("DialChart Chart Title"),
      ),
      body: Center(
        child: ElevatedButton(
          onPressed: () {
            Navigator.pop(context);
          },
          child: const Text('DialChart Chart'),
        ),
      ),
    );
  }
}
