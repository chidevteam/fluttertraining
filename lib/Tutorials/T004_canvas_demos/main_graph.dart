import 'package:flutter/material.dart';
import 'dial_chart/dial_chart.dart';
import 'dial_chart/sunflower.dart';
// SunflowerPainter
import 'live_graph/live_graph.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext dcontext) {
    return MaterialApp(
        home: RandomWords(),
        theme: ThemeData(
          // Add the 5 lines from here...
          appBarTheme: const AppBarTheme(
            backgroundColor: Colors.green,
            foregroundColor: Colors.black,
          ),
        ));
  }
}

class RandomWords extends StatefulWidget {
  const RandomWords({Key? key}) : super(key: key);

  @override
  _RandomWordsState createState() {
    return _RandomWordsState();
  }
}

class _RandomWordsState extends State<RandomWords> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Counter 15'), actions: [
        IconButton(
          icon: const Icon(Icons.list),
          onPressed: actionButtonPressed,
          tooltip: 'Saved Suggestions',
        ),
        IconButton(
          icon: const Icon(Icons.list),
          onPressed: actionButtonPressed,
          tooltip: 'Saved Suggestions',
        ),
      ]),
      body: _buildSuggestions(),
    );
  }

  void actionButtonPressed() {
    print("Action Button Pressed");
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => const LiveGraph()),
    );
  }

  void navigate(int i) {
    print("Action Button Pressed");
    if (i == 1) {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => const LiveGraph()),
      );
    }
    if (i == 2) {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => const DialChart()),
      );
    }
    if (i == 3) {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => Sunflower()),
      );
    }
  }

  Widget _buildSuggestions() {
    return ListView.builder(itemBuilder: /*1*/ (context, i) {
      if (i.isOdd)
        return Divider(); /*2*/
      else {
        int j = i ~/ 2 + 1;
        return _buildRow(j, "$j");
      }
    });
  }

  Widget _buildRow(int i, String str) {
    return ListTile(
        title: Row(children: [Text(str), Icon(Icons.favorite), Text(str)]),
        trailing: Icon(
          // NEW from here...
          (i % 2) == 0 ? Icons.favorite : Icons.favorite_border,
          color: (i % 2) == 0 ? Colors.red : null,
          semanticLabel: (i % 2) == 0 ? 'Remove from saved' : 'Save',
        ), // ... to here.
        onTap: () {
          // NEW lines from here...
          print("Cell3 Tapped $i");
          navigate(i);
        });
  }
}
