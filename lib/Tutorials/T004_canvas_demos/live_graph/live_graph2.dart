import 'package:flutter/material.dart';

class LiveGraph extends StatelessWidget {
  // const LiveGraph({Key? key}) : super(key: key);
  const LiveGraph();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Live Graph3 Title"),
        backgroundColor: Colors.purple,
      ),
      body: Center(
        child: createTestWidget(context),
      ),
    );
  }
}

createTestWidget(BuildContext context) {
  ElevatedButton button = ElevatedButton(
    onPressed: () {
      Navigator.pop(context);
    },
    child: const Text('Live Graph'),
  );
  return button;
}
