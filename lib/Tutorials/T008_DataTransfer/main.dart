// import 'package:flutter/cupertino.dart';

// UNDER CONSTRUCTION

import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

// ignore: must_be_immutable
class MyApp extends StatelessWidget {
  late List<double> graphBuffer = [];
  @override
  Widget build(BuildContext context) {
    return Graph(buffer: graphBuffer);
  }
}

class Graph extends StatelessWidget {
  const Graph({Key? key, required List<double> buffer}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.red,
      width: double.infinity,
      height: 300,
    );
  }
}
