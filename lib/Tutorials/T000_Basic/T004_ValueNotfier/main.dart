import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(home: TestPage()));
}

class TestPage extends StatefulWidget {
  @override
  _TestPageState createState() => _TestPageState();
}

class _TestPageState extends State<TestPage> {
  final _controller = TextEditingController();
  late ValueNotifier<String> _textNotifier;

  @override
  void initState() {
    super.initState();
    _textNotifier = ValueNotifier<String>('');
    _textNotifier.addListener(() {
      print("Listener called ..............");
      _controller.text = _textNotifier.value;
    });
  }

  @override
  void dispose() {
    _controller.dispose();
    _textNotifier.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          TextFormField(
            controller: _controller,
          ),
          ElevatedButton(
            onPressed: () {
              _textNotifier.value += "1";
            },
            child: const Text('Click Me'),
          )
        ],
      ),
    );
  }
}
