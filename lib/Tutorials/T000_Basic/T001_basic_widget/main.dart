import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext dcontext) {
    Scaffold createHome = Scaffold(
      appBar: createAppBar(),
      body: createText(),
    );

    return MaterialApp(home: createHome);
  }
}

createAppBar() {
  return AppBar(
    title: const Text('Welcome to Flutter2221'),
  );
}

createText() {
  Center center = const Center(
    child: Text('Hello World 33'),
  );

  return center;
}
