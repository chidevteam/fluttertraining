import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext dcontext) {
    Scaffold createHome = Scaffold(
      appBar: createAppBar(),
      body: myWidget(),
      // body: Column(
      //   children: [createText(), createText(), myWidget()],
      // )
    );

    return MaterialApp(home: createHome);
  }
}

createAppBar() {
  return AppBar(
    title: const Text('Welcome to Flutter2221'),
  );
}

createText() {
  Center center = const Center(
    child: Text('Hello World 33'),
  );

  return center;
}

Widget myWidget() {
  print("Widget created");
  return ListView.builder(
    itemBuilder: (BuildContext context, int index) {
      return ListTile(
        title: Text('Row $index'),
        onTap: () {
          // do something
        },
      );
    },
  );
}
