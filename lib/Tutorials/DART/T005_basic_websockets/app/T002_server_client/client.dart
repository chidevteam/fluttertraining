import 'dart:convert' show json;
import 'dart:async' show Timer;
import '../../lib/ipc_client.dart';

main() {
  IPCClient ipcClient = IPCClient(receiveFunc: receiveData);

  Timer.periodic(
      Duration(milliseconds: 100), (Timer t) => {sendData(ipcClient)});
}

sendData(client) {
  String data = 'client Sent ${DateTime.now().toString()}';
  bool success = client.sendData(data); // THE REAL LINE
  print("$success");
}

receiveData(data) {
  print(' -- ${Map<String, String>.from(json.decode(data))}');
}
