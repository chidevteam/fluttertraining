import 'dart:io';

void main() {
  WebsocketServer(port: 7000);
}

class WebsocketServer {
  final int port;
  int count = 0;
  List<WebsocketClient> clients = [];

  WebsocketServer({required this.port}) {
    Future<ServerSocket> server =
        ServerSocket.bind(InternetAddress.anyIPv4, 7000);

    server.then((ServerSocket ws) {
      ws.listen((client) {
        // handleConnection(client);
        WebsocketClient wsc = WebsocketClient(server: this, client: client);
        clients.add(wsc);
      });
    });
  }
}

class WebsocketClient {
  final WebsocketServer server;
  final Socket client;
  late String address;
  late int port;

  WebsocketClient({required this.server, required this.client}) {
    address = client.remoteAddress.address;
    port = client.remotePort;
    print('Received Connection from $address:${port}');

    client.listen(
      (data) => receiveDataFunc(data),
      onError: (error) => handleErrorFunc(error),
      onDone: () => handleDoneFunc(),
    );
  }

  handleDoneFunc() {
    print('Client left');
    client.close();
  }

  handleErrorFunc(error) {
    print(error);
    client.close();
  }

  receiveDataFunc(data) {
    final message = String.fromCharCodes(data);
    print("Client sent: $message");
    client.write('-------- Very funny.');
    // client.close();
  }
}
