import 'dart:io';
import 'dart:typed_data';
import 'dart:convert' show json;

void main() async {
  // connect to the socket server
  final socket = await Socket.connect('localhost', 7000);
  print('Connected to: ${socket.remoteAddress.address}:${socket.remotePort}');

  // listen for responses from the server
  socket.listen(
    // handle data from the server
    (Uint8List data) {
      final serverResponse = String.fromCharCodes(data);
      print('Server: $serverResponse');
    },

    // handle errors
    onError: (error) {
      print(error);
      socket.destroy();
    },

    // handle server ending connection
    onDone: () {
      print('Server left.');
      socket.destroy();
    },
  );

  // send some messages to the server
  await sendMessage(socket, 'Knock, knock.');
  await sendMessage(socket, 'Banana');
  await sendMessage(socket, 'Orange');
  await sendMessage(socket, "Orange you glad I didn't say banana again?");
}

sendMessage(Socket socket, String message) async {
  print('Client---: $message');

  var dataJson = json.encode({
    'data': message,
  });

  socket.write(dataJson);
  // await Future.delayed(Duration(milliseconds: 1));
}
