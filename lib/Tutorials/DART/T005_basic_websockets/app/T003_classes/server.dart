import 'dart:io' show HttpServer, HttpRequest, WebSocket, WebSocketTransformer;
// import 'dart:convert' show json;
// import 'dart:async' show Timer;

void main() {
  WebsocketServer(port: 7000);
}

class WebsocketServer {
  final int port;
  int count = 0;
  String host = 'localhost'; //'10.8.0.135';
  List<Connection> connections = [];

  WebsocketServer({required this.port}) {
    HttpServer.bind(host, port).then((HttpServer server) {
      print('[+]WebSocket listening at -- ws://$host:${port}/');
      server.listen((HttpRequest request) {
        WebSocketTransformer.upgrade(request).then(
            (ws) => createSocket(ws, request),
            onError: (err) => handleUpgradeError(err));
      }, onError: (err) => handleRequestError(err));
    }, onError: (err) => handleBindError(err));
  }

  handleUpgradeError(err) {
    print('[!]UpgradeError -- ${err.toString()}');
  }

  handleRequestError(err) {
    print('[!]RequestError -- ${err.toString()}');
  }

  handleBindError(err) {
    print('[!]BindError -- ${err.toString()}');
  }

  createSocket(WebSocket ws, request) {
    count++;
    print('${request.connectionInfo?.remoteAddress}');
    print("here $count");

    Connection(ws: ws);
  }
}

class Connection {
  final WebSocket ws;
  Connection({required this.ws}) {
    ws.listen(
      receiveData,
      onDone: closeConnectionFunc,
      onError: errorFunc,
      cancelOnError: true,
    );
  }

  receiveData(data) {
    // print(
    //     '${request.connectionInfo?.remoteAddress} -- ${Map<String, String>.from(json.decode(data))}');
    print(data);

    // if (ws.readyState == WebSocket.open)
    //   ws.add(json.encode({
    //     'data': 'server mmm!!!', //${DateTime.now().toString()}
    //   }));
  }

  closeConnectionFunc() {
    print('Disconnecting');
  }

  errorFunc(err) {
    print('[!]Error -- ${err.toString()}');
    print('Disconnecting');
  }
}
