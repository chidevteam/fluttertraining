import 'dart:async' show Timer;
import '../../lib/ipc_client.dart';

main() {
  IPCClient ipcClient = IPCClient(receiveFunc: receiveData);

  Timer.periodic(
      Duration(milliseconds: 1000), (Timer t) => {sendData(ipcClient)});
}

sendData(client) {
  // String data = 'client Sent ${DateTime.now().toString()}';
  String data = "This is a car";
  bool success = client.sendData(data); // THE REAL LINE
  if (success == false) print("Failure:  $success");
}

receiveData(data) {
  // print(data);
  // print(' -- ${Map<String, String>.from(json.decode(data))}');
}
