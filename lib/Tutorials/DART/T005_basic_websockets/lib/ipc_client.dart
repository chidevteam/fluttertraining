import 'dart:io' show WebSocket;
import 'dart:convert' show json;

class IPCClient {
  final int port = 7000;
  final dynamic receiveFunc;
  WebSocket? websocket = null;

  IPCClient({this.receiveFunc}) {
    WebSocket.connect('ws://localhost:${port}').then((WebSocket ws) {
      // WebSocket.connect('ws://10.8.0.135:${port}').then((WebSocket ws) {
      websocket = ws;

      if (ws.readyState == WebSocket.open) {
        //   Timer.periodic(Duration(milliseconds: 100), (Timer t) => {timerFunc(ws)});

        ws.listen(
          receiveFunc,
          onDone: () => print('[+]Done :)'),
          onError: (err) => print('[!]Error -- ${err.toString()}'),
          cancelOnError: true,
        );
      } else
        print('[!]Connection Denied'); // in case, server is not running
    }, onError: (err) => print('[!]Error -- ${err.toString()}'));
  }

  bool sendData(data) {
    if (websocket != null) {
      if (websocket!.readyState == WebSocket.open) {
        var dataJson = json.encode({
          'data': data,
        });
        websocket!.add(dataJson);
      }
      return true;
    } else
      return false;
  }
}
