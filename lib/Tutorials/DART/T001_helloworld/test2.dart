import 'dart:core';

main() {
  Test t = Test();
  t.testfunc();
}

class Test {
  int samplesPerTimer = 14;
  int sampleConsumed = 0;
  List<int> dataBuf = [];

  Test() {
    for (int i = 0; i < 10; i++) {
      dataBuf.add(i);
    }
  }

  testfunc() {
    for (int i = 0; i < samplesPerTimer; i++) {
      print("&&&& $i");
      if (i >= dataBuf.length) break;
      sampleConsumed++;
      print('comsuming $i ${dataBuf[i]}  ==  $dataBuf');
      print('===========================  $dataBuf');
    }

    dataBuf.removeRange(0, sampleConsumed);
    print('$sampleConsumed ===========================  $dataBuf');
  }
}
