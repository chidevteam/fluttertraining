/*
A setter does not allow setting the value

*/

class MyListClass {
  final List<int> _values = [1, 2, 3];

  List<int> get values => _values;

  set append(int value) {
    if (value >= 0) {
      _values.add(value);
    }
  }

  String toString() => _values.toString();
}

main() {
  MyListClass lc = MyListClass();
  lc.append = 66;
  lc.append = -66;
  lc.append = 166;
  print(lc);
}
