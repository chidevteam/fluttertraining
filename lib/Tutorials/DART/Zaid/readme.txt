
person!.run(), we are sure person is not null

person?.run(), won't crash even if person is null

cascade operator returns the same object on which it was called
person.run().hide()