class Name {
  String? firstName;
  String? lastName;

  Name(this.firstName, this.lastName);

  String toString() => "$firstName $lastName";
}

class Person {
  Name? name;
  int? age;

  Person(this.name, this.age);

  void run() {
    print("I am running");
  }

  void talk() {
    print("I am talking");
  }

  String toString() => "name: $name, age: $age";
}

main() {
  Person? zaid = null;
  // Person(Name("Zaid", "Hassan"), 34);
  print(zaid);
  makeRun(zaid);
  // zaid?.run();
  // ..talk();
}

void makeRun(Person? person) {
  person?.run();
}
