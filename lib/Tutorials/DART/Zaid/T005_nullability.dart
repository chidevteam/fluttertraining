const oneSecond = Duration(seconds: 4);
// // ···
// Future<void> printWithDelay(String message) async {
//   await Future.delayed(oneSecond);
//   print(message);
// }

Future<void> printWithDelay(String message) {
  return Future.delayed(oneSecond).then((_) {
    print(message);
  });
}

main1() async {
  void v = await printWithDelay("Test");
  print("abc");
}

main() {
  void v = printWithDelay("Test");
  print("abc");
}
