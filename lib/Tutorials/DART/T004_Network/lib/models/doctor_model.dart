class DoctorList {
  final List<Doctor>? data;
  final int? totalRecords;

  DoctorList(
    this.data,
    this.totalRecords,
  );

  static DoctorList fromMap(Map<String, dynamic> resp) {
    List<Doctor>? data;
    if (resp['data'] != null)
      data =
          List<Doctor>.from(resp['data'].map((item) => Doctor.fromMap(item)));

    return DoctorList(
      resp['data'] == null ? null : data,
      resp['total_records'] as int?,
    );
  }
}

class Doctor {
  final String? full_name;
  final String? image;
  final String? gender;

  Doctor(this.full_name, this.image, this.gender);

  static Doctor fromMap(Map<String, dynamic> resp) {
    return Doctor(resp['full_name'] as String?, resp['image'] as String?,
        resp['gender'] as String?);
  }

  String toString() {
    String st = "";
    st += "Doctor: " + full_name!;
    return st;
  }
}
