class CMSessionList {
  final List<CMSession>? data;
  final int? totalRecords;

  CMSessionList(
    this.data,
    this.totalRecords,
  );

  static CMSessionList fromMap(Map<String, dynamic> resp) {
    List<CMSession>? data;
    if (resp['data'] != null)
      data = List<CMSession>.from(
          resp['data'].map((item) => CMSession.fromMap(item)));

    return CMSessionList(
      resp['data'] == null ? null : data,
      resp['total_records'] as int?,
    );
  }
}

class CMSession {
  final int? observationId;
  final int? episodePatientUserId;
  final int? episodePatientId;
  final int? episodeId;
  final String? episodePatientHospitalNo;
  final String? episodeEpisodeNo;
  final String? episodePatientFullName;
  final String? episodePatientFirstName;
  final dynamic episodePatientFirstNameEnc;
  final String? episodePatientLastName;
  final dynamic episodePatientLastNameEnc;
  final String? serviceCategory;
  final String? service;
  final bool? isManual;
  final String? oprStatus;
  final dynamic observationTime;
  final int? dateAdded;
  final int? captureTime;
  final int? deviceId;
  final String? deviceDeviceTypeDeviceModel;
  final String? cmFile2;
  final int? episodePatientBirthDate;
  final double? sessionDuration;
  final String? age;
  final dynamic numericValue;
  final dynamic textValue;
  final dynamic readingId;
  final String? name;
  final int? viewingCount;

  CMSession(
    this.observationId,
    this.episodePatientUserId,
    this.episodePatientId,
    this.episodeId,
    this.episodePatientHospitalNo,
    this.episodeEpisodeNo,
    this.episodePatientFullName,
    this.episodePatientFirstName,
    this.episodePatientFirstNameEnc,
    this.episodePatientLastName,
    this.episodePatientLastNameEnc,
    this.serviceCategory,
    this.service,
    this.isManual,
    this.oprStatus,
    this.observationTime,
    this.dateAdded,
    this.captureTime,
    this.deviceId,
    this.deviceDeviceTypeDeviceModel,
    this.cmFile2,
    this.episodePatientBirthDate,
    this.sessionDuration,
    this.age,
    this.numericValue,
    this.textValue,
    this.readingId,
    this.name,
    this.viewingCount,
  );

  static CMSession fromMap(Map<String, dynamic> resp) {
    return CMSession(
      resp['observation_id'] as int?,
      resp['episode.patient.user_id'] as int?,
      resp['episode.patient_id'] as int?,
      resp['episode_id'] as int?,
      resp['episode.patient.hospital_no'] as String?,
      resp['episode.episode_no'] as String?,
      resp['episode.patient.full_name'] as String?,
      resp['episode.patient.first_name'] as String?,
      resp['episode.patient.first_name_enc'] as dynamic,
      resp['episode.patient.last_name'] as String?,
      resp['episode.patient.last_name_enc'] as dynamic,
      resp['service_category'] as String?,
      resp['service'] as String?,
      resp['is_manual'] as bool?,
      resp['opr_status'] as String?,
      resp['observation_time'] as dynamic,
      resp['date_added'] as int?,
      resp['capture_time'] as int?,
      resp['device_id'] as int?,
      resp['device.device_type.device_model'] as String?,
      resp['cm_file2'] as String?,
      resp['episode.patient.birth_date'] as int?,
      resp['session_duration'] as double?,
      resp['age'] as String?,
      resp['numeric_value'] as dynamic,
      resp['text_value'] as dynamic,
      resp['reading_id'] as dynamic,
      resp['name'] as String?,
      resp['viewing_count'] as int?,
    );
  }
}
