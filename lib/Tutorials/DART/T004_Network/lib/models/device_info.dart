class DeviceVersion {
  String? release;
  String? sdkInt;
}

class DeviceInfo {
  String? manufacturer;
  String? device;
  String? androidId;
  String? iosId;
  String? model;
  String? name;
  String? systemVersion;
  int? identifierForVendor;
  late DeviceVersion version;
}

class DeviceInfoPlugin {
  late DeviceInfo androidInfo;
  late DeviceInfo iosInfo;
}
