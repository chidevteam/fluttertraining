class ObservationList {
  final List<Observation>? data;
  final int? totalRecords;

  ObservationList(
    this.data,
    this.totalRecords,
  );

  static ObservationList fromMap(Map<String, dynamic> resp) {
    List<Observation>? data;
    if (resp['data'] != null)
      data = List<Observation>.from(
          resp['data'].map((item) => Observation.fromMap(item)));

    return ObservationList(
      resp['data'] == null ? null : data,
      resp['total_records'] as int?,
    );
  }
}

class Observation {
  final int? readingId;
  final int? observationId;
  final int? userId;
  final int? patientId;
  final int? episodeId;
  final String? episodeNo;
  final String? name;
  final String? firstName;
  final dynamic firstNameEnc;
  final String? lastName;
  final dynamic lastNameEnc;
  final String? image;
  final String? hospitalNo;
  final bool? isLive;
  final String? serviceTitle;
  final String? serviceCategory;
  final String? service;
  final int? measurementId;
  final String? measurementKey;
  final String? measurementName;
  final double? measurementValue;
  final String? measurementUnit;
  final double? numericValue;
  final dynamic textValue;
  final bool? hasDataValue;
  final String? status;
  final String? oprStatus;
  final dynamic observationTime;
  final int? acquisitionTime;
  final int? captureTime;
  final int? arrivalTime;
  final int? readingTime;
  final dynamic deviceId;
  final dynamic deviceModel;

  Observation(
    this.readingId,
    this.observationId,
    this.userId,
    this.patientId,
    this.episodeId,
    this.episodeNo,
    this.name,
    this.firstName,
    this.firstNameEnc,
    this.lastName,
    this.lastNameEnc,
    this.image,
    this.hospitalNo,
    this.isLive,
    this.serviceTitle,
    this.serviceCategory,
    this.service,
    this.measurementId,
    this.measurementKey,
    this.measurementName,
    this.measurementValue,
    this.measurementUnit,
    this.numericValue,
    this.textValue,
    this.hasDataValue,
    this.status,
    this.oprStatus,
    this.observationTime,
    this.acquisitionTime,
    this.captureTime,
    this.arrivalTime,
    this.readingTime,
    this.deviceId,
    this.deviceModel,
  );

  static Observation fromMap(Map<String, dynamic> resp) {
    return Observation(
      resp['reading_id'] as int?,
      resp['observation_id'] as int?,
      resp['user_id'] as int?,
      resp['patient_id'] as int?,
      resp['episode_id'] as int?,
      resp['episode_no'] as String?,
      resp['name'] as String?,
      resp['first_name'] as String?,
      resp['first_name_enc'] as dynamic,
      resp['last_name'] as String?,
      resp['last_name_enc'] as dynamic,
      resp['image'] as String?,
      resp['hospital_no'] as String?,
      resp['is_live'] as bool?,
      resp['service_title'] as String?,
      resp['service_category'] as String?,
      resp['service'] as String?,
      resp['measurement_id'] as int?,
      resp['measurement_key'] as String?,
      resp['measurement_name'] as String?,
      resp['measurement_value'] as double?,
      resp['measurement_unit'] as String?,
      resp['numeric_value'] as double?,
      resp['text_value'] as dynamic,
      resp['has_data_value'] as bool?,
      resp['status'] as String?,
      resp['opr_status'] as String?,
      resp['observation_time'] as dynamic,
      resp['acquisition_time'] as int?,
      resp['capture_time'] as int?,
      resp['arrival_time'] as int?,
      resp['reading_time'] as int?,
      resp['device_id'] as dynamic,
      resp['device_model'] as dynamic,
    );
  }
}
