import 'data_converter.dart';

class OrderableList {
  List<OrderableData>? data;
  int? totalRecords = null;

  OrderableList(this.data, this.totalRecords);

  static OrderableList fromMap(Map<String, dynamic> resp2) {
    List? resp = dconvert_full(resp2);
    List<OrderableData>? data = null;
    if (resp != null)
      data = List<OrderableData>.from(
          resp.map((item) => OrderableData.fromMap(item)));

    int? numRecords = (data == null) ? null : data.length;
    OrderableList oList = OrderableList(data, numRecords);
    return oList;
  }

  @override
  String toString() {
    String st = "";
    st += "==== OrderableList ====\n";
    st += " TotalRecords: $totalRecords \n";
    st += " Data: $data \n";
    return st;
  }
}

class OrderableData {
  final int? time;
  final List<ResultableData>? values;

  OrderableData(this.time, this.values);

  static OrderableData fromMap(Map<String, dynamic> resp) {
    List<ResultableData>? vals;
    if (resp['values'] != null) {
      vals = List<ResultableData>.from(
          resp['values'].map((item) => ResultableData.fromMap(item)));
    }
    return OrderableData(
        resp['date_time'] as int?, resp['values'] == null ? null : vals);
  }

  @override
  String toString() {
    String st = "";
    st += "==== OrderableData ====\n";
    st += " time: $time \n";
    st += " Data: $values \n";
    return st;
  }
}

class ResultableData {
  final String? name;
  final num? value;
  final num? value2;
  final String? unit;
  final String? status;

  ResultableData(this.name, this.value, this.unit, this.status, this.value2);

  static ResultableData fromMap(Map<String, dynamic> resp) {
    return ResultableData(
        resp['name'] as String?,
        resp['value'] as num?,
        resp['unit'] as String?,
        resp['status'] as String?,
        resp['value2'] as num?);
  }

  @override
  String toString() {
    String st = "";
    st += "\nDate :: ";
    st += " Name: $name -> ";
    st += " Value: $value -> ";
    if (value2 != null) st += " Value2: $value2 -> ";
    st += " Unit: $unit -> ";
    st += " Status: $status ";
    return st;
  }
}
