class VerificationList {
  final List<Verification>? data;
  final int? totalRecords;

  VerificationList(
    this.data,
    this.totalRecords,
  );

  static VerificationList fromMap(Map<String, dynamic> resp) {
    List<Verification>? data;
    if (resp['data'] != null)
      data = List<Verification>.from(
          resp['data'].map((item) => Verification.fromMap(item)));

    return VerificationList(
      resp['data'] == null ? null : data,
      resp['total_records'] as int?,
    );
  }
}

class Verification {
  final int? patientId;
  final String? patientName;
  final String? firstName;
  final dynamic firstNameEnc;
  final String? lastName;
  final dynamic lastNameEnc;
  final int? episodeId;
  final int? referenceId;
  final String? referenceName;
  final String? recordType;
  final String? frequency;
  final int? datetime;
  final String? icon;
  final bool? isAdvanceSchedule;
  final List<AdvanceSchedules>? advanceSchedules;
  final String? dosageForm;
  final String? strength;

  Verification(
    this.patientId,
    this.patientName,
    this.firstName,
    this.firstNameEnc,
    this.lastName,
    this.lastNameEnc,
    this.episodeId,
    this.referenceId,
    this.referenceName,
    this.recordType,
    this.frequency,
    this.datetime,
    this.icon,
    this.isAdvanceSchedule,
    this.advanceSchedules,
    this.dosageForm,
    this.strength,
  );

  static Verification fromMap(Map<String, dynamic> resp) {
    List<AdvanceSchedules>? advanceSchedules;
    if (resp['advance_schedules'] != null)
      advanceSchedules = List<AdvanceSchedules>.from(resp['advance_schedules']
          .map((item) => AdvanceSchedules.fromMap(item)));

    return Verification(
      resp['patient_id'] as int?,
      resp['patient_name'] as String?,
      resp['first_name'] as String?,
      resp['first_name_enc'] as dynamic,
      resp['last_name'] as String?,
      resp['last_name_enc'] as dynamic,
      resp['episode_id'] as int?,
      resp['reference_id'] as int?,
      resp['reference_name'] as String?,
      resp['record_type'] as String?,
      resp['frequency'] as String?,
      resp['datetime'] as int?,
      resp['icon'] as String?,
      resp['is_advance_schedule'] as bool?,
      resp['advance_schedules'] == null ? null : advanceSchedules,
      resp['dosage_form'] as String?,
      resp['strength'] as String?,
    );
  }
}

class AdvanceSchedules {
  final int? startDate;
  final String? repeatType;
  final String? repeatCycle;
  final int? repeatUnit;
  final String? frequency;
  final List<Days>? days;
  final String? dosageType;
  final int? dosage;
  final String? repeatCycleCustom;
  final int? repeatUnitCustom;
  final bool? isAdvance;
  final String? endType;
  final int? endValue;

  AdvanceSchedules(
    this.startDate,
    this.repeatType,
    this.repeatCycle,
    this.repeatUnit,
    this.frequency,
    this.days,
    this.dosageType,
    this.dosage,
    this.repeatCycleCustom,
    this.repeatUnitCustom,
    this.isAdvance,
    this.endType,
    this.endValue,
  );

  static AdvanceSchedules fromMap(Map<String, dynamic> resp) {
    List<Days>? days;
    if (resp['days'] != null)
      days = List<Days>.from(resp['days'].map((item) => Days.fromMap(item)));

    return AdvanceSchedules(
      resp['start_date'] as int?,
      resp['repeat_type'] as String?,
      resp['repeat_cycle'] as String?,
      resp['repeat_unit'] as int?,
      resp['frequency'] as String?,
      resp['days'] == null ? null : days,
      resp['dosage_type'] as String?,
      resp['dosage'] as int?,
      resp['repeat_cycle_custom'] as String?,
      resp['repeat_unit_custom'] as int?,
      resp['is_advance'] as bool?,
      resp['end_type'] as String?,
      resp['end_value'] as int?,
    );
  }
}

class Days {
  final String? type;
  final String? day;
  final List<Times>? times;

  Days(
    this.type,
    this.day,
    this.times,
  );

  static Days fromMap(Map<String, dynamic> resp) {
    List<Times>? times;
    if (resp['times'] != null)
      times =
          List<Times>.from(resp['times'].map((item) => Times.fromMap(item)));

    return Days(
      resp['type'] as String?,
      resp['day'] as String?,
      resp['times'] == null ? null : times,
    );
  }
}

class Times {
  final int? time;
  final int? dosage;
  final int? index;
  final int? hIndex;
  final int? mIndex;

  Times(
    this.time,
    this.dosage,
    this.index,
    this.hIndex,
    this.mIndex,
  );

  static Times fromMap(Map<String, dynamic> resp) {
    return Times(
      resp['time'] as int?,
      resp['dosage'] as int?,
      resp['index'] as int?,
      resp['h_index'] as int?,
      resp['m_index'] as int?,
    );
  }
}
