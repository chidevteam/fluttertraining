import 'dart:io';

// import 'package:device_info_plus/device_info_plus.dart';
// ignore: todo
import 'device_info.dart'; //TODO: uncomment above

typedef ApiPayload = Map<String, dynamic>;

Future<ApiPayload> makeLoginRequest(context, username, password) async {
  final DeviceInfoPlugin plugin = DeviceInfoPlugin();

  final Map<String, dynamic> req = {
    'username': username,
    'password': password,
    'pn_type': 'None',
    'pn_token': 'None',
    'app_version': '4.0.1',
    'app_type': 'Doctor',
    'lang': 'en',
  };

  // if (Platform.isAndroid)
  {
    //   final info = await plugin.androidInfo;

    //   String manufacturer = info.manufacturer?.toUpperCase() ?? 'Unknown';
    //   String device = info.device?.toUpperCase() ?? 'Unknown';

    //   req['device_id'] = info.androidId;
    //   req['device_name'] = '$manufacturer $device';
    //   req['device_type'] = 'Android';
    //   req['device_model'] = info.model;
    //   req['os_name'] = 'Android';
    //   req['os_version'] = '${info.version.release} API-${info.version.sdkInt}';
    // } else if (Platform.isIOS) {
    //   final info = await plugin.iosInfo;

    //   req['device_id'] = info.identifierForVendor.toString().replaceAll('-', '');
    //   req['device_name'] = info.name;
    //   req['device_type'] = 'iOS';
    //   req['device_model'] = info.model;
    //   req['os_name'] = 'iOS';
    //   req['os_version'] = info.systemVersion;
    // } else {
    //throw Exception('Platform not supported');
    // final info = await plugin.iosInfo;

    // req['device_id'] = info.identifierForVendor.toString().replaceAll('-', '');
    // req['device_name'] = info.name;
    // req['device_type'] = 'iOS';
    // req['device_model'] = info.model;
    // req['os_name'] = 'iOS';
    // req['os_version'] = info.systemVersion;

    req['device_id'] = "Ahmad_WEB";
    req['device_name'] = 'Kashif Limited';
    req['device_type'] = 'Android';
    req['device_model'] = "A131";
    req['os_name'] = 'Android';
    req['os_version'] = '13.2';
  }

  return req;
}

Future<ApiPayload> makeResetPasswordRequest(username, code, password) async {
  final Map<String, dynamic> req = {
    'username': username,
    'password': password,
    'code': code,
    'os_name': 'Android',
  };

  return req;
}

ApiPayload makeUnVerifiedOrdersRequest(limit, offset) {
  final Map<String, dynamic> req = {
    'limit': limit,
    'offset': offset,
  };

  return req;
}

Future<ApiPayload> makeForgotPasswordRequest(username) async {
  final Map<String, dynamic> req = {
    'username': username,
    'os_name': 'Android',
  };

  return req;
}
