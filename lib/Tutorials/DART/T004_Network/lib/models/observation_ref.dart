class LiveObservationList {
  final List<LiveObservation> data;
  final int totalRecords;

  LiveObservationList(
    this.data,
    this.totalRecords,
  );

  static LiveObservationList fromMap(Map<String, dynamic> resp) {
    return LiveObservationList(
      List<LiveObservation>.from(
          resp['data'].map((item) => LiveObservation.fromMap(item))),
      resp['total_records'] as int,
    );
  }
}

class LiveObservation {
  final int? readingId;
  final int? observationId;
  final int? userId;
  final int? patientId;
  final int? episodeId;
  final String? episodeNo;
  final String? name;
  final String? image;
  final String? hospitalNo;
  final bool? isLive;
  final String? serviceTitle;
  final String? serviceCategory;
  final String? service;
  final int? measurementId;
  final String? measurementName;
  final String? measurementKey;
  final String? measurementUnit;
  final double? numericValue;
  final String? textValue;
  final String? dataValue;
  final String? status;
  final String? oprStatus;
  final int? observationTime;
  final int? acquisitionTime;
  final int? captureTime;
  final int? arrivalTime;
  final int? readingTime;
  final int? deviceId;
  final String? deviceModel;
  final String? firstNameEnc;
  final String? lastNameEnc;
  final String? mobilePhoneEnc;
  final int? subjectId;
  // final List<String>? urls;

  LiveObservation(
    this.readingId,
    this.observationId,
    this.userId,
    this.patientId,
    this.episodeId,
    this.episodeNo,
    this.name,
    this.image,
    this.hospitalNo,
    this.isLive,
    this.serviceTitle,
    this.serviceCategory,
    this.service,
    this.measurementId,
    this.measurementName,
    this.measurementKey,
    this.measurementUnit,
    this.numericValue,
    this.textValue,
    this.dataValue,
    this.status,
    this.oprStatus,
    this.observationTime,
    this.acquisitionTime,
    this.captureTime,
    this.arrivalTime,
    this.readingTime,
    this.deviceId,
    this.deviceModel,
    this.firstNameEnc,
    this.lastNameEnc,
    this.mobilePhoneEnc,
    this.subjectId,
    // this.urls,
  );

  static LiveObservation fromMap(Map<String, dynamic> resp) {
    return LiveObservation(
      resp['reading_id'] as int?,
      resp['observation_id'] as int?,
      resp['user_id'] as int?,
      resp['patient_id'] as int?,
      resp['episode_id'] as int?,
      resp['episode_no'] as String?,
      resp['name'] as String?,
      resp['image'] as String?,
      resp['hospital_no'] as String?,
      resp['is_live'] as bool?,
      resp['service_title'] as String?,
      resp['service_category'] as String?,
      resp['service'] as String?,
      resp['measurement_id'] as int?,
      resp['measurement_name'] as String?,
      resp['measurement_key'] as String?,
      resp['measurement_unit'] as String?,
      resp['numeric_value'] as double?,
      resp['text_value'] as String?,
      resp['data_value'] as String?,
      resp['status'] as String?,
      resp['opr_status'] as String?,
      resp['observation_time'] as int?,
      resp['acquisition_time'] as int?,
      resp['capture_time'] as int?,
      resp['arrival_time'] as int?,
      resp['reading_time'] as int?,
      resp['device_id'] as int?,
      resp['device_model'] as String?,
      resp['first_name_enc'] as String?,
      resp['last_name_enc'] as String?,
      resp['mobile_phone_enc'] as String?,
      resp['subject_id'] as int?,
      // resp['urls'] as List<String>?,

      //resp['urls'].map((item) => item),
    );
  }
}
