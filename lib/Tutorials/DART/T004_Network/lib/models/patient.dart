class PatientList {
  final List<Patient>? data;
  final int? totalRecords;

  PatientList(
    this.data,
    this.totalRecords,
  );

  static PatientList fromMap(Map<String, dynamic> resp) {
    List<Patient>? data;
    if (resp['data'] != null)
      data =
          List<Patient>.from(resp['data'].map((item) => Patient.fromMap(item)));

    return PatientList(
      resp['data'] == null ? null : data,
      resp['total_records'] as int?,
    );
  }

  static PatientList fromMap1(Map<String, dynamic> resp) {
    List<Patient>? data;

    if (resp['data'] == null) {
      return PatientList(null, null);
    }

    List<Map<String, dynamic>> pList = resp['data'];

    //Option 1
    // data = List<Patient>.from(pList.map((item) => Patient.fromMap(item)));

    //Option 2
    // var iterableList = pList.map((item) => Patient.fromMap(item));
    // data = List<Patient>.from(iterableList);

    //Option 3 -- Standard loop
    data = [];
    for (Map<String, dynamic> item in pList) {
      data.add(Patient.fromMap(item));
    }

    // Option 4
    // data = List<Patient>.from(pList.map((item) {
    //   return Patient.fromMap(item);
    // }));

    return PatientList(
      resp['data'] = data,
      resp['total_records'] as int?,
    );
  }
}

class Patient {
  final int? patientId;
  final int? patientUserId;
  final String? userName;
  final String? firstName;
  final dynamic firstNameEnc;
  final String? lastName;
  final dynamic lastNameEnc;
  final String? name;
  final String? image;
  final String? gender;
  final String? mobilePhone;
  final String? hospitalNo;
  final int? age;
  final int? episodeId;
  final bool? liveSession;
  final bool? abnormal;

  Patient(
    this.patientId,
    this.patientUserId,
    this.userName,
    this.firstName,
    this.firstNameEnc,
    this.lastName,
    this.lastNameEnc,
    this.name,
    this.image,
    this.gender,
    this.mobilePhone,
    this.hospitalNo,
    this.age,
    this.episodeId,
    this.liveSession,
    this.abnormal,
  );

  static Patient fromMap(Map<String, dynamic> resp) {
    return Patient(
      resp['patient_id'] as int?,
      resp['patient_user_id'] as int?,
      resp['user_name'] as String?,
      resp['first_name'] as String?,
      resp['first_name_enc'] as dynamic,
      resp['last_name'] as String?,
      resp['last_name_enc'] as dynamic,
      resp['name'] as String?,
      resp['image'] as String?,
      resp['gender'] as String?,
      resp['mobile_phone'] as String?,
      resp['hospital_no'] as String?,
      resp['age'] as int?,
      resp['episode_id'] as int?,
      resp['live_session'] as bool?,
      resp['abnormal'] as bool?,
    );
  }
}
