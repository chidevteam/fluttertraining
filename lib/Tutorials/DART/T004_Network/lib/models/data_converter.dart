import "package:collection/collection.dart";
import 'dart:convert';
import 'dart:io';

List? dconvert_full(Map<String, dynamic> response2) {
  List resp = dConverter(response2);

  var x = groupBy(resp, (var obj) {
    Map<String, dynamic> m = obj as Map<String, dynamic>;
    return m['date_time'].toString();
  });

  List<Map<String, dynamic>> y = aggregate(x);

  return y;
}

void writeJsonToFile(data, fileName) {
  JsonEncoder encoder = new JsonEncoder.withIndent('  ');
  String prettyPrintData = encoder.convert(data);
  // final fileName = 'data.dart';
  File(fileName).writeAsStringSync(
      'Map <String, dynamic> response = ' + prettyPrintData + ';');
}

List<Map<String, dynamic>> aggregate(Map<String, dynamic> x) {
  List<Map<String, dynamic>> list = [];

  for (String key in x.keys) {
    List values = [];
    for (Map<String, dynamic> v in x[key]) {
      values.addAll(v["values"]);
    }

    values = processBloodPressure(values);
    Map<String, dynamic> m = {"date_time": int.parse(key), 'values': values};
    list.add(m);
    // print(values);
  }

  return list;
}

List dConverter(Map<String, dynamic> jsonDatax) {
  var apiData = jsonDatax;
  List recentResultData = [];

  var groupBY = {};
  apiData = jsonDecode(jsonEncode(apiData));

  for (var i in apiData.keys) {
    // print("key: ==  $i");
    var v = apiData[i]['values'];
    // v = v.cast<String, dynamic>();

    groupBY.addAll(groupBy(v, (var obj) {
      Map<String, dynamic> m = obj as Map<String, dynamic>;
      return m['date_added'];
    }));
    for (var j in groupBY.keys) {
      recentResultData.add({'date_time': j, 'values': getValues(groupBY[j])});
    }
  }

  return recentResultData;
}

List getValues(values) {
  var data = [];
  for (int i = 0; i < values.length; i++) {
    data.add({
      "name": values[i]['measurement_name'],
      "value": values[i]['value'],
      "unit": values[i]['measurement_unit'],
      "status": values[i]['status_to_display']
    });
  }
  return data;
}

List processBloodPressure(List values) {
  Map<String, dynamic> mp = {};
  for (Map vx in values) {
    mp[vx["name"]] = vx;
  }

  //TODO: Put appropriate status (systolic/diastolic)
  // systolic and diastolic are both present

  if (mp.containsKey('Systolic') || mp.containsKey('Diastolic')) {
    // print(mp['Systolic']['value']);
    Map<String, dynamic> m = {
      "name": "Blood Pressure",
      'value': mp['Systolic']['value'],
      'value2': mp['Diastolic']['value'],
      'unit': mp['Systolic']['unit'],
      'status': mp['Systolic']['status'],
      'status2': mp['Diastolic']['status'],
    };
    values = [m];
  } else {
    List vList = [];
    for (String key in mp.keys) {
      vList.add(mp[key]);
    }
    values = vList;
  }
  return values;
}
