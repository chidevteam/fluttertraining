class Profile {
  final int? employeeId;
  final int? userId;
  final int? deviceId;
  final String? firstName;
  final dynamic middleName;
  final String? lastName;
  final String? fullName;
  final String? gender;
  final String? mobilePhone;
  final String? image;
  final String? userName;
  final String? email;
  final String? group;
  final String? groupType;
  final String? groupCategory;
  final bool? SecureModeEnabled;
  final Map<String, PermissionsV2>? permissionsV2;

  Profile(
    this.employeeId,
    this.userId,
    this.deviceId,
    this.firstName,
    this.middleName,
    this.lastName,
    this.fullName,
    this.gender,
    this.mobilePhone,
    this.image,
    this.userName,
    this.email,
    this.group,
    this.groupType,
    this.groupCategory,
    this.SecureModeEnabled,
    this.permissionsV2,
  );

  static Profile fromMap(Map<String, dynamic> resp) {
    return Profile(
        resp['employee_id'] as int?,
        resp['user_id'] as int?,
        resp['device_id'] as int?,
        resp['first_name'] as String?,
        resp['middle_name'] as dynamic,
        resp['last_name'] as String?,
        resp['full_name'] as String?,
        resp['gender'] as String?,
        resp['mobile_phone'] as String?,
        resp['image'] as String?,
        resp['user_name'] as String?,
        resp['email'] as String?,
        resp['group'] as String?,
        resp['group_type'] as String?,
        resp['group_category'] as String?,
        resp['SecureModeEnabled'] as bool?,
        resp['permissionsV2']
        // Map<String, PermissionsV2>.from(
        //     resp['permissionsV2'].map((item) => PermissionsV2.fromMap(item))),
        );
  }
}

class PermissionsV2 {
  final bool? viewProfile;
  final bool? changePassword;
  final bool? viewTodo;
  final bool? help;
  final bool? reportIssueOrSuggestion;
  final bool? internalCalling;
  final bool? viewClinics;
  final bool? clinicManagement;
  final bool? viewMasterSettings;
  final bool? masterSettingsInClinic;
  final bool? viewRolesAndPermissions;
  final bool? rolesAndPermissionsManagement;
  final bool? viewEmployees;
  final bool? employeeManagement;
  final bool? viewApiLogs;
  final bool? viewMonitoring;
  final bool? viewSystemSettings;
  final bool? viewDevices;
  final bool? deviceManagement;
  final bool? viewCcSettingsOrRanges;
  final bool? ccServicesManagement;
  final bool? packageManagement;
  final bool? allowBypassSubscription;
  final bool? viewCcServices;
  final bool? viewAccounts;
  final bool? viewGuardians;
  final bool? viewHmsReports;
  final bool? employeeSchedule;
  final bool? doctorSettings;
  final bool? clinicEmployeeManagement;
  final bool? clinicServicesManagement;
  final bool? clinicAccountsManagement;
  final bool? writePrescriptionTemplates;
  final bool? viewHospitalReports;
  final bool? viewClinicReports;
  final bool? viewCashTransactionReport;
  final bool? viewClinicEmployees;
  final bool? viewClinicServices;
  final bool? unverifiedPrescription;
  final bool? markFavouriteTemplate;
  final bool? writePrescriptionOnDoctorBehalf;
  final bool? writePrescriptionTemplateOnDoctorBehalf;
  final bool? writeBackdatedPrescriptions;
  final bool? specialityAndDoctorSelection;
  final bool? ipdDashboard;
  final bool? tileDashboard;
  final bool? viewPatients;
  final bool? patientManagement;
  final bool? onboardPatient;
  final bool? attachPatientsDocuments;
  final bool? viewPhr;
  final bool? viewClinicPatients;
  final bool? clinicPatientManagement;
  final bool? addAppointment;
  final bool? manualAppointmentPayment;
  final bool? viewAppointments;
  final bool? viewAppointmentsForAllDoctorsOfClinic;
  final bool? viewAppointmentsForAllClinicsOfDoctor;
  final bool? prePostAppointmentNotes;
  final bool? changeAppointmentStatus;
  final bool? appointmentsCancellationReschedule;
  final bool? preConsultationOrHistorySession;
  final bool? writePrescription;
  final bool? generateSmsForOnlineConsultationLink;
  final bool? approvePrescription;
  final bool? followUpAppointment;
  final bool? callPatient;
  final bool? callCc;
  final bool? messageManagement;
  final bool? viewMessages;
  final bool? viewScanQueue;
  final bool? viewCallRecordings;
  final bool? viewLiveSessions;
  final bool? viewObservations;
  final bool? updateObservation;
  final bool? addAdhocObservation;
  final bool? generateProgressReport;
  final bool? viewProgressReport;
  final bool? viewMissedSchedules;
  final bool? viewNurseVisitNotes;
  final bool? sosCall;
  final bool? escalateObservation;
  final bool? closeObservation;
  final bool? validInvalidObservation;
  final bool? unreadMessages;
  final bool? viewHomeAdmissions;
  final bool? onlinePayment;
  final bool? createDraftInvoice;
  final bool? generateInvoice;
  final bool? viewInvoices;
  final bool? cancelInvoice;
  final bool? receivePayments;
  final bool? verificationOfBankPayment;
  final bool? refundPayment;
  final bool? ipdManagement;
  final bool? labOrders;
  final bool? labTestTemplate;
  final bool? labOrderTests;
  final bool? labPanels;
  final bool? labOrdCancel;
  final bool? labOrdCreate;
  final bool? labOrderTestList;
  final bool? labGetPanels;
  final bool? labIsAccession;
  final bool? labIsTechnician;
  final bool? labIsPathologist;
  final bool? radiologyTestTemplates;
  final bool? radiologyOrders;
  final bool? radiologyOrdersTests;
  final bool? radiologySignatures;
  final bool? radiologyCategoryEmployees;
  final bool? radAccountHeadNotRequired;
  final bool? radOrdCancel;
  final bool? radOrdCreate;
  final bool? radOrdTestList;
  final bool? radOrdTestComp;
  final bool? radOrdTestAddReport;
  final bool? radOrdTestTemplate;
  final bool? invDashboard;
  final bool? invAddInventory;
  final bool? invPhmcyManfac;
  final bool? invPhmcyUnits;
  final bool? invPhmcyFormulas;
  final bool? invPhmcyLabels;
  final bool? invPhmcySuppliers;
  final bool? invPhmcyFormInteract;
  final bool? invPhmcyStores;
  final bool? invPhmcyStoresEmp;
  final bool? invStoreNotRequired;
  final bool? invCalcSellingPrice;
  final bool? invSearchAvailableInventory;
  final bool? invGetLabels;
  final bool? invGetStores;
  final bool? invGetStoreEmployees;
  final bool? invSupplierInventories;
  final bool? invPhmcyPurchaseReq;
  final bool? invPhmcyPurchaseOrder;
  final bool? invPhmcyGrn;
  final bool? invPhmcyGrnApprove;
  final bool? invPhmcyPurchaseReturn;
  final bool? invPhmcyStockTransfer;
  final bool? invPhmcyPos;
  final bool? invPhmcyPurchaseReqApprove;
  final bool? invPhmcyConsumptions;
  final bool? invPhmcyStockReport;
  final bool? invPhmcySalesReport;
  final bool? invPhmcyNarcoticsReport;
  final bool? invPhmcySaleSumReport;
  final bool? invPhmcyPurchaseReport;
  final bool? invPhmcyStockAdjReport;
  final bool? invPhmcyLedgerReport;
  final bool? invPhmcyLowStockReport;
  final bool? invPhmcyInvoiceReport;
  final bool? invPhmcyItemsExpReport;
  final bool? invPhmcyItemConsumptionReport;

  PermissionsV2(
    this.viewProfile,
    this.changePassword,
    this.viewTodo,
    this.help,
    this.reportIssueOrSuggestion,
    this.internalCalling,
    this.viewClinics,
    this.clinicManagement,
    this.viewMasterSettings,
    this.masterSettingsInClinic,
    this.viewRolesAndPermissions,
    this.rolesAndPermissionsManagement,
    this.viewEmployees,
    this.employeeManagement,
    this.viewApiLogs,
    this.viewMonitoring,
    this.viewSystemSettings,
    this.viewDevices,
    this.deviceManagement,
    this.viewCcSettingsOrRanges,
    this.ccServicesManagement,
    this.packageManagement,
    this.allowBypassSubscription,
    this.viewCcServices,
    this.viewAccounts,
    this.viewGuardians,
    this.viewHmsReports,
    this.employeeSchedule,
    this.doctorSettings,
    this.clinicEmployeeManagement,
    this.clinicServicesManagement,
    this.clinicAccountsManagement,
    this.writePrescriptionTemplates,
    this.viewHospitalReports,
    this.viewClinicReports,
    this.viewCashTransactionReport,
    this.viewClinicEmployees,
    this.viewClinicServices,
    this.unverifiedPrescription,
    this.markFavouriteTemplate,
    this.writePrescriptionOnDoctorBehalf,
    this.writePrescriptionTemplateOnDoctorBehalf,
    this.writeBackdatedPrescriptions,
    this.specialityAndDoctorSelection,
    this.ipdDashboard,
    this.tileDashboard,
    this.viewPatients,
    this.patientManagement,
    this.onboardPatient,
    this.attachPatientsDocuments,
    this.viewPhr,
    this.viewClinicPatients,
    this.clinicPatientManagement,
    this.addAppointment,
    this.manualAppointmentPayment,
    this.viewAppointments,
    this.viewAppointmentsForAllDoctorsOfClinic,
    this.viewAppointmentsForAllClinicsOfDoctor,
    this.prePostAppointmentNotes,
    this.changeAppointmentStatus,
    this.appointmentsCancellationReschedule,
    this.preConsultationOrHistorySession,
    this.writePrescription,
    this.generateSmsForOnlineConsultationLink,
    this.approvePrescription,
    this.followUpAppointment,
    this.callPatient,
    this.callCc,
    this.messageManagement,
    this.viewMessages,
    this.viewScanQueue,
    this.viewCallRecordings,
    this.viewLiveSessions,
    this.viewObservations,
    this.updateObservation,
    this.addAdhocObservation,
    this.generateProgressReport,
    this.viewProgressReport,
    this.viewMissedSchedules,
    this.viewNurseVisitNotes,
    this.sosCall,
    this.escalateObservation,
    this.closeObservation,
    this.validInvalidObservation,
    this.unreadMessages,
    this.viewHomeAdmissions,
    this.onlinePayment,
    this.createDraftInvoice,
    this.generateInvoice,
    this.viewInvoices,
    this.cancelInvoice,
    this.receivePayments,
    this.verificationOfBankPayment,
    this.refundPayment,
    this.ipdManagement,
    this.labOrders,
    this.labTestTemplate,
    this.labOrderTests,
    this.labPanels,
    this.labOrdCancel,
    this.labOrdCreate,
    this.labOrderTestList,
    this.labGetPanels,
    this.labIsAccession,
    this.labIsTechnician,
    this.labIsPathologist,
    this.radiologyTestTemplates,
    this.radiologyOrders,
    this.radiologyOrdersTests,
    this.radiologySignatures,
    this.radiologyCategoryEmployees,
    this.radAccountHeadNotRequired,
    this.radOrdCancel,
    this.radOrdCreate,
    this.radOrdTestList,
    this.radOrdTestComp,
    this.radOrdTestAddReport,
    this.radOrdTestTemplate,
    this.invDashboard,
    this.invAddInventory,
    this.invPhmcyManfac,
    this.invPhmcyUnits,
    this.invPhmcyFormulas,
    this.invPhmcyLabels,
    this.invPhmcySuppliers,
    this.invPhmcyFormInteract,
    this.invPhmcyStores,
    this.invPhmcyStoresEmp,
    this.invStoreNotRequired,
    this.invCalcSellingPrice,
    this.invSearchAvailableInventory,
    this.invGetLabels,
    this.invGetStores,
    this.invGetStoreEmployees,
    this.invSupplierInventories,
    this.invPhmcyPurchaseReq,
    this.invPhmcyPurchaseOrder,
    this.invPhmcyGrn,
    this.invPhmcyGrnApprove,
    this.invPhmcyPurchaseReturn,
    this.invPhmcyStockTransfer,
    this.invPhmcyPos,
    this.invPhmcyPurchaseReqApprove,
    this.invPhmcyConsumptions,
    this.invPhmcyStockReport,
    this.invPhmcySalesReport,
    this.invPhmcyNarcoticsReport,
    this.invPhmcySaleSumReport,
    this.invPhmcyPurchaseReport,
    this.invPhmcyStockAdjReport,
    this.invPhmcyLedgerReport,
    this.invPhmcyLowStockReport,
    this.invPhmcyInvoiceReport,
    this.invPhmcyItemsExpReport,
    this.invPhmcyItemConsumptionReport,
  );

  static PermissionsV2 fromMap(Map<String, dynamic> resp) {
    return PermissionsV2(
      resp['view_profile'] as bool?,
      resp['change_password'] as bool?,
      resp['view_todo'] as bool?,
      resp['help'] as bool?,
      resp['report_issue_or_suggestion'] as bool?,
      resp['internal_calling'] as bool?,
      resp['view_clinics'] as bool?,
      resp['clinic_management'] as bool?,
      resp['view_master_settings'] as bool?,
      resp['master_settings_in_clinic'] as bool?,
      resp['view_roles_and_permissions'] as bool?,
      resp['roles_and_permissions_management'] as bool?,
      resp['view_employees'] as bool?,
      resp['employee_management'] as bool?,
      resp['view_api_logs'] as bool?,
      resp['view_monitoring'] as bool?,
      resp['view_system_settings'] as bool?,
      resp['view_devices'] as bool?,
      resp['device_management'] as bool?,
      resp['view_cc_settings_or_ranges'] as bool?,
      resp['cc_services_management'] as bool?,
      resp['package_management'] as bool?,
      resp['allow_bypass_subscription'] as bool?,
      resp['view_cc_services'] as bool?,
      resp['view_accounts'] as bool?,
      resp['view_guardians'] as bool?,
      resp['view_hms_reports'] as bool?,
      resp['employee_schedule'] as bool?,
      resp['doctor_settings'] as bool?,
      resp['clinic_employee_management'] as bool?,
      resp['clinic_services_management'] as bool?,
      resp['clinic_accounts_management'] as bool?,
      resp['write_prescription_templates'] as bool?,
      resp['view_hospital_reports'] as bool?,
      resp['view_clinic_reports'] as bool?,
      resp['view_cash_transaction_report'] as bool?,
      resp['view_clinic_employees'] as bool?,
      resp['view_clinic_services'] as bool?,
      resp['unverified_prescription'] as bool?,
      resp['mark_favourite_template'] as bool?,
      resp['write_prescription_on_doctor_behalf'] as bool?,
      resp['write_prescription_template_on_doctor_behalf'] as bool?,
      resp['write_backdated_prescriptions'] as bool?,
      resp['speciality_and_doctor_selection'] as bool?,
      resp['ipd_dashboard'] as bool?,
      resp['tile_dashboard'] as bool?,
      resp['view_patients'] as bool?,
      resp['patient_management'] as bool?,
      resp['onboard_patient'] as bool?,
      resp['attach_patients_documents'] as bool?,
      resp['view_phr'] as bool?,
      resp['view_clinic_patients'] as bool?,
      resp['clinic_patient_management'] as bool?,
      resp['add_appointment'] as bool?,
      resp['manual_appointment_payment'] as bool?,
      resp['view_appointments'] as bool?,
      resp['view_appointments_for_all_doctors_of_clinic'] as bool?,
      resp['view_appointments_for_all_clinics_of_doctor'] as bool?,
      resp['pre_post_appointment_notes'] as bool?,
      resp['change_appointment_status'] as bool?,
      resp['appointments_cancellation_reschedule'] as bool?,
      resp['pre_consultation_or_history_session'] as bool?,
      resp['write_prescription'] as bool?,
      resp['generate_sms_for_online_consultation_link'] as bool?,
      resp['approve_prescription'] as bool?,
      resp['follow_up_appointment'] as bool?,
      resp['call_patient'] as bool?,
      resp['call_cc'] as bool?,
      resp['message_management'] as bool?,
      resp['view_messages'] as bool?,
      resp['view_scan_queue'] as bool?,
      resp['view_call_recordings'] as bool?,
      resp['view_live_sessions'] as bool?,
      resp['view_observations'] as bool?,
      resp['update_observation'] as bool?,
      resp['add_adhoc_observation'] as bool?,
      resp['generate_progress_report'] as bool?,
      resp['view_progress_report'] as bool?,
      resp['view_missed_schedules'] as bool?,
      resp['view_nurse_visit_notes'] as bool?,
      resp['sos_call'] as bool?,
      resp['escalate_observation'] as bool?,
      resp['close_observation'] as bool?,
      resp['valid_invalid_observation'] as bool?,
      resp['unread_messages'] as bool?,
      resp['view_home_admissions'] as bool?,
      resp['online_payment'] as bool?,
      resp['create_draft_invoice'] as bool?,
      resp['generate_invoice'] as bool?,
      resp['view_invoices'] as bool?,
      resp['cancel_invoice'] as bool?,
      resp['receive_payments'] as bool?,
      resp['verification_of_bank_payment'] as bool?,
      resp['refund_payment'] as bool?,
      resp['ipd_management'] as bool?,
      resp['lab_orders'] as bool?,
      resp['lab_test_template'] as bool?,
      resp['lab_order_tests'] as bool?,
      resp['lab_panels'] as bool?,
      resp['lab_ord_cancel'] as bool?,
      resp['lab_ord_create'] as bool?,
      resp['lab_order_test_list'] as bool?,
      resp['lab_get_panels'] as bool?,
      resp['lab_is_accession'] as bool?,
      resp['lab_is_technician'] as bool?,
      resp['lab_is_pathologist'] as bool?,
      resp['radiology_test_templates'] as bool?,
      resp['radiology_orders'] as bool?,
      resp['radiology_orders_tests'] as bool?,
      resp['radiology_signatures'] as bool?,
      resp['radiology_category_employees'] as bool?,
      resp['rad_account_head_not_required'] as bool?,
      resp['rad_ord_cancel'] as bool?,
      resp['rad_ord_create'] as bool?,
      resp['rad_ord_test_list'] as bool?,
      resp['rad_ord_test_comp'] as bool?,
      resp['rad_ord_test_add_report'] as bool?,
      resp['rad_ord_test_template'] as bool?,
      resp['inv_dashboard'] as bool?,
      resp['inv_add_inventory'] as bool?,
      resp['inv_phmcy_manfac'] as bool?,
      resp['inv_phmcy_units'] as bool?,
      resp['inv_phmcy_formulas'] as bool?,
      resp['inv_phmcy_labels'] as bool?,
      resp['inv_phmcy_suppliers'] as bool?,
      resp['inv_phmcy_form_interact'] as bool?,
      resp['inv_phmcy_stores'] as bool?,
      resp['inv_phmcy_stores_emp'] as bool?,
      resp['inv_store_not_required'] as bool?,
      resp['inv_calc_selling_price'] as bool?,
      resp['inv_search_available_inventory'] as bool?,
      resp['inv_get_labels'] as bool?,
      resp['inv_get_stores'] as bool?,
      resp['inv_get_store_employees'] as bool?,
      resp['inv_supplier_inventories'] as bool?,
      resp['inv_phmcy_purchase_req'] as bool?,
      resp['inv_phmcy_purchase_order'] as bool?,
      resp['inv_phmcy_grn'] as bool?,
      resp['inv_phmcy_grn_approve'] as bool?,
      resp['inv_phmcy_purchase_return'] as bool?,
      resp['inv_phmcy_stock_transfer'] as bool?,
      resp['inv_phmcy_pos'] as bool?,
      resp['inv_phmcy_purchase_req_approve'] as bool?,
      resp['inv_phmcy_consumptions'] as bool?,
      resp['inv_phmcy_stock_report'] as bool?,
      resp['inv_phmcy_sales_report'] as bool?,
      resp['inv_phmcy_narcotics_report'] as bool?,
      resp['inv_phmcy_sale_sum_report'] as bool?,
      resp['inv_phmcy_purchase_report'] as bool?,
      resp['inv_phmcy_stock_adj_report'] as bool?,
      resp['inv_phmcy_ledger_report'] as bool?,
      resp['inv_phmcy_low_stock_report'] as bool?,
      resp['inv_phmcy_invoice_report'] as bool?,
      resp['inv_phmcy_items_exp_report'] as bool?,
      resp['inv_phmcy_item_consumption_report'] as bool?,
    );
  }
}
