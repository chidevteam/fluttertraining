// import 'package:universal/DiscreteGraph/apps/T004_patient_list/api/response.dart';
class VitalGraphData {
  Map<String, dynamic> vvData;
  bool isSelected = false;

  VitalGraphData(this.vvData);

  static fromJson(resp) {
    Map<String, dynamic> vData = _dataConverter(resp);
    return VitalGraphData(
      vData,
    );
  }

  @override
  String toString() {
    String st = '$vvData';
    return st;
  }
}

Map<String, dynamic> preDataConverter(Map<String, dynamic> graphData) {
  Map<String, dynamic> data = {'vital_graph_data': []};
  for (String key in graphData.keys) {
    Map<String, dynamic> ranges = graphData[key]['ranges'];
    Map<String, dynamic> stats = graphData[key]['stats'];
    List<dynamic> trendPoints = graphData[key]['trend_points'];
    List<dynamic> values = graphData[key]['values'];

    data['vital_graph_data']!.add({
      'title': graphData[key]['vital_name'],
      'time_start': values[0]['date_added'],
      'time_end': values[values.length - 1]['date_added'],
      'vitals': [
        {
          'name': ranges['name'],
          'ranges': [
            ranges['warn_low_end_value'],
            ranges['warn_low_value'],
            ranges['normal_low_value'],
            ranges['normal_high_value'],
            ranges['warn_high_value'],
            ranges['warn_high_end_value']
          ],
          'trend_line': graphData[key].containsKey('trend_points') &&
                  graphData[key]['trend_points'].isNotEmpty
              ? {
                  't1': trendPoints[0]['start_point']['date'],
                  'v1': trendPoints[0]['start_point']['value'],
                  't2': trendPoints[1]['end_point']['date'],
                  'v2': trendPoints[1]['end_point']['value'],
                }
              : null,
          'stats': {
            // 'min': ranges.values.contains(stats['min']) ? stats['min'] : '--',
            // 'max': ranges.values.contains(stats['max']) ? stats['max'] : '--',
            // 'mean':ranges.values.contains(stats['mean']) ? stats['mean'] : '--',
            'min': getInRangeValue(ranges, stats['min']),
            'max': getInRangeValue(ranges, stats['max']),
            'mean': getInRangeValue(ranges, stats['mean']),
            'lower_percentage': stats['lower_percentage'],
            'greater_percentage': stats['greater_percentage']
          },
          'data': graphValues(values, ranges),
        },
      ],
    });
  }
  return data;
}

_dataConverter(vvData) {
  // Find the indices of systolic and diastolic
  List data2 = [];
  Map<String, dynamic> apiData = preDataConverter(vvData);
  if (apiData.containsKey('vital_graph_data')) {
    List<dynamic> data = apiData['vital_graph_data'];
    Map<String, int> map = {};
    for (int i = 0; i < data.length; i++) {
      String vitalName = data[i]['vitals'][0]['name'];
      map[vitalName] = i;
    }

    // add all other than systolic and diastolic

    for (int i = 0; i < data.length; i++) {
      if (i == map["Systolic"]) continue;
      if (i == map["Diastolic"]) continue;
      data2.add(data[i]);
    }

    // Add systolic and diastolic
    if (map["Systolic"] != null && map["Diastolic"] != null) {
      Map<String, dynamic> diastolic = data[map["Diastolic"]!];
      Map<String, dynamic> systolic = data[map["Systolic"]!];
      diastolic["vitals"].add(systolic["vitals"][0]);
      data2.add(diastolic);
    }

    // writeToFile({"vital_graph_data": data2}, "test.dart");
  }
  return {"vital_graph_data": data2};
}

List graphValues(List values, Map<String, dynamic> ranges) {
  var data = [];
  for (var i = 0; i < values.length; i++) {
    data.add([
      values[i]['date_added'],
      getInRangeValue(ranges, values[i]['value']),
      getInRangeValue(ranges, values[i]['min_value']),
      getInRangeValue(ranges, values[i]['max_value'])
    ]);
  }
  return data;
}

getInRangeValue(Map<String, dynamic> ranges, double value) {
  List<double> r = [];
  for (var key in ranges.keys) {
    if (ranges[key].runtimeType == String) {
      continue;
    } else {
      r.add(ranges[key].toDouble());
    }
  }
  r.getRange(0, r.length - 1);

  if (value > maximum(r)) {
    return maximum(r);
  } else if (value < minimum(r)) {
    return minimum(r);
  } else {
    return value;
  }
}

double minimum(List<double> x) {
  return x.reduce((curr, next) => curr < next ? curr : next);
}

double maximum(List<double> x) {
  return x.reduce((curr, next) => curr > next ? curr : next);
}
