import '../models/observation.dart';
import '../models/verification.dart';
import '../models/profile.dart';
import '../models/patient.dart';
import '../models/doctor_model.dart';
import '../models/recent_result_detail.dart';
import '../models/cm_session_list.dart';

import '../models/vital_graph_data.dart';
import 'api_client.dart';
import '../models/general.dart';
import '../models/requests.dart';

// import 'package lib/DiscreteGraph/apps/T008_CGMS/agp_chart/api/model.dart';
// import '../../../../../DiscreteGraph/apps/T008_CGMS/agp_chart/api/model.dart';

class LoginResult {
  final String token;
  LoginResult(this.token);
  static LoginResult fromMap(Map<String, dynamic> resp) {
    return LoginResult(resp['Token'] as String);
  }
}

abstract class SystemApiService {
  // static Future<ApiListResponse<SystemLanguage>> getSystemLanguages() {
  //   return ApiClient.instance.callListApi(
  //     endPoint: 'auth/GetSystemLanguages',
  //     fromJson: SystemLanguage.fromMap,
  //   );
  // }

  static Future<ApiSingleResponse<LoginResult>> login(ApiPayload req) {
    Future<ApiSingleResponse<LoginResult>> v = ApiClient.instance.callObjectApi(
      req: req,
      endPoint: 'auth/Login2',
      fromJson: LoginResult.fromMap,
    );

    return v;
  }

  static Future<ApiSingleResponse<GenericResponse>> forgotPassword(
      ApiPayload req) {
    return ApiClient.instance.callObjectApi(
      req: req,
      endPoint: 'auth/ForgotPassword',
      fromJson: GenericResponse.fromMap,
    );
  }

  static Future<ApiSingleResponse<GenericResponse>> resetPassword(
      ApiPayload req) {
    return ApiClient.instance.callObjectApi(
      req: req,
      endPoint: 'auth/ResetPassword',
      fromJson: GenericResponse.fromMap,
    );
  }

  static Future<ApiSingleResponse<VerificationList>> getUnVerifiedOrders(
      ApiPayload req) {
    return ApiClient.instance.callObjectApi(
      req: req,
      endPoint: 'api/doctor_app/GetUnVerifiedOrders',
      fromJson: VerificationList.fromMap,
    );
  }

  static Future<ApiSingleResponse<ObservationList>> getLiveObservations(
      ApiPayload req) {
    return ApiClient.instance.callObjectApi(
      req: req,
      endPoint: 'api/doctor_app/GetObservationList',
      fromJson: ObservationList.fromMap,
    );
  }

  static Future<ApiSingleResponse<Profile>> getProfile(ApiPayload req) {
    return ApiClient.instance.callObjectApi(
      req: req,
      endPoint: 'api/doctor_app/GetProfile',
      fromJson: Profile.fromMap,
    );
  }

  static Future<ApiSingleResponse<PatientList>> getPatients(ApiPayload req) {
    return ApiClient.instance.callObjectApi(
      req: req,
      endPoint: 'api/doctor_app/GetPatientList',
      fromJson: PatientList.fromMap,
    );
  }

  static Future<ApiSingleResponse<CMSessionList>> getCMSession(ApiPayload req) {
    return ApiClient.instance.callObjectApi(
      req: req,
      endPoint: 'api/doctor_app/GetOngoingCMSessions',
      fromJson: CMSessionList.fromMap,
    );
  }

  static Future<ApiSingleResponse<DummyResponse>> logout() {
    return ApiClient.instance.callObjectApi(
      endPoint: 'api/patient_app2/Logout',
      fromJson: DummyResponse.fromMap,
    );
  }

  static Future<ApiSingleResponse<OrderableList>> getVitalHistoryDataTabular(
      ApiPayload req) {
    return ApiClient.instance.callObjectApi(
        req: req,
        endPoint: 'api/doctor_app/GetVitalHistoryDataTabular',
        fromJson: OrderableList.fromMap,
        convertData: false);
  }

  static Future<ApiSingleResponse<VitalGraphData>> getRecentResultGraphData(
      ApiPayload req) {
    return ApiClient.instance.callObjectApi(
        req: req,
        endPoint: 'api/doctor_app/GetVitalHistoryData',
        fromJson: VitalGraphData.fromJson);
  }

  // static Future<ApiSingleResponse<OrderableList>> getLiveObservations(
  //     ApiPayload req) {
  //   return ApiClient.instance.callObjectApi(
  //       req: req,
  //       endPoint: 'api/doctor_app/GetObservationList',
  //       fromJson: OrderableList.fromMap,
  //       convertData: false);
  // }

  static Future<ApiSingleResponse<DoctorList>> getDoctors(ApiPayload req) {
    return ApiClient.instance.callObjectApi(
      req: req,
      endPoint: 'api/doctor_app/Contacts',
      fromJson: DoctorList.fromMap,
    );
  }

  // static Future<ApiListResponse<Map<String, dynamic>>> getCGMDetailedData(
  //     ApiPayload req) {
  //   return ApiClient.instance.callListApi(
  //     req: req,
  //     endPoint: 'api/cgm_sessions/GetDetailedData',
  //     fromJson: CGMDetailedDataList.fromMap,
  //   );
  // }
}

Future<ApiPayload> loginFunc(
    String siteCode, String username, String password) async {
  // String siteCode = "charms-qa";
  ApiClient.create(siteCode);

  print("hello world ..... ");

  final req = await makeLoginRequest("", username, password);
  ApiSingleResponse<LoginResult> resp1 = await SystemApiService.login(req);
  LoginResult result = resp1.data as LoginResult;
  print('Token : ${result.token}');
  ApiClient.setAuthToken(result.token);
  // print("++++++++++++++++++++++++++++++++++++++++++++++++++");
  // getPatients();
  return req;
}

loginFuncVU() async {
  return await loginFunc('charms-qa', "kashifd", "Kashif123@");
}


// static Future<APIResp> getDoctors(req) async {
//     DoctorList items = DoctorList.fromMap(response["data"]);
//     return APIResp<DoctorList>(
//         true, DoctorList(items.data, items.totalRecords));
//   }