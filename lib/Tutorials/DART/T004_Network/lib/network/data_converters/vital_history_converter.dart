import "package:collection/collection.dart";
import 'dart:convert';
import 'dart:io';

// import 'data_to_convert.dart';
// import 'input.txt';

Map<String, dynamic> dConverter(jsonDatax) {
  var apiData = jsonDatax['data'];
  Map<String, dynamic> recentResultData = {};
  recentResultData['data'] = [];
  var groupBY = {};

  apiData = jsonDecode(jsonEncode(apiData));

  for (var i in apiData.keys) {
    print("key: ==  $i");
    recentResultData['title'] = i;
    var v = apiData[i]['values'];
    // v = v.cast<String, dynamic>();
    // print(v);

    groupBY.addAll(groupBy(v, (var obj) {
      Map<String, dynamic> m = obj as Map<String, dynamic>;
      return m['date_added'];
    }));
    for (var j in groupBY.keys) {
      recentResultData['data']!
          .add({'date_time': j, 'values': getValues(groupBY[j])});
    }

    print(recentResultData['data'].runtimeType);
  }
  // testSort();

  // writeToFile({'data': recentResultData});
  // writeToFile(recentResultData);
  Map<String, dynamic> resp = {
    'data': recentResultData,
    'Status': 'ok',
    // 'ErrorMessage': 'None',
    // 'ErrorCode': 0,
    // 'total_records': 9999,
    'Time': 1641563087.542,
  };

  return resp;
}

List getValues(values) {
  var data = [];
  for (int i = 0; i < values.length; i++) {
    data.add({
      "name": values[i]['measurement_name'],
      "value": values[i]['value'],
      "unit": values[i]['measurement_unit'],
      "status": values[i]['status_to_display']
    });
  }
  return data;
}

void writeToFile(data) {
  JsonEncoder encoder = new JsonEncoder.withIndent('  ');
  String prettyPrintData = encoder.convert(data);
  final fileName = 'data.json';
  File(fileName).writeAsStringSync(prettyPrintData);
}

void testSort() {
  List<Map<String, dynamic>> list = [
    {
      "date_time": 1628575591,
      "values": [
        {"name": "Pulse", "value": 79.0, "unit": "bpm", "status": "Normal"}
      ]
    },
    {
      "date_time": 1628575453,
      "values": [
        {"name": "Pulse", "value": 75.0, "unit": "bpm", "status": "Normal"}
      ]
    },
  ];

  list.sort((a, b) => a['date_time']!.compareTo(b['date_time']));
  print(list);
}

main() {
  // dConverter(jsonData);
}
