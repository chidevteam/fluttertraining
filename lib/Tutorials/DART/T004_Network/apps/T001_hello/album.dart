class Album {
  final int userId;
  final int id;
  final String title;

  Album({
    required this.userId,
    required this.id,
    required this.title,
  });

  static fromJson(Map<String, dynamic> data) {
    Album alb = Album(
      userId: data['userId'],
      id: data['id'],
      title: data['title'],
    );
    return alb;
  }

  String toString() {
    String st = "UserID : ${userId}\n";
    st += "id     : ${id}\n";
    st += "title  : ${title}\n";
    return st;
  }
}
