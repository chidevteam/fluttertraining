// ignore: import_of_legacy_library_into_null_safe
// import 'package:http/http.dart' as http;

// Future<void> main() async {
//   print('Hello, World!');
//   final resp =
//       await http.get(Uri.parse('https://jsonplaceholder.typicode.com/todos/1'));
//   print(resp.body);

// import 'dart:io';
import 'album.dart';

import 'package:http/http.dart';
import 'dart:convert';

fetchAlbum() {
  dynamic v = get(Uri.parse('https://jsonplaceholder.typicode.com/albums/'));
  return v;
}

void main() async {
  List<Album> items = [];

  print("Hello world");
  var album = await fetchAlbum();

  print(album.body);
  // exit(0);

  List<dynamic> list = jsonDecode(album.body);
  print(list[0]);

  for (Map<String, dynamic> m in list) {
    final alb = Album.fromJson(m);
    // print(alb);
    items.add(alb);
  }

  print(items);
  for (Album a in items) {
    print(a);
  }
}
