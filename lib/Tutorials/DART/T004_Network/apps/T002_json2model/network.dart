import 'dart:convert';
import 'package:http/http.dart' as http;
import 'todoModel.dart';

List<Todo> parseProducts(String responseBody) {
  final body = json.decode("[" + responseBody + "]");
  print(body);
  final parsed = body.cast<Map<String, dynamic>>();
  // return parsed.map<Todo>((json) => Todo.fromMap(json)).toList();
  return parsed.map<Todo>(convertFromMap).toList();
}

Todo convertFromMap(json) {
  print("Wow, now I understand!!!!");
  return Todo.fromMap(json);
}

Future<List<Todo>> fetchProducts() async {
  // final x = Uri.https('https://jsonplaceholder.typicode.com', 'todos/1');
  // final response = await http.get(x);

  final response =
      await http.get(Uri.parse('https://jsonplaceholder.typicode.com/todos/1'));
  if (response.statusCode == 200) {
    return parseProducts(response.body);
  } else {
    throw Exception('Unable to fetch products from the REST API');
  }
}
