import 'todoModel.dart';

import 'network.dart';

// Promise/Future, not data
void main() {
  final future = fetchProducts();
  print(future);

  ToDoListWidget widget = ToDoListWidget(future);
  widget.activateFuture();
  print("Future Activated");
}

class ToDoListWidget {
  Future<List<Todo>> future;

  ToDoListWidget(this.future);

  activateFuture() {
    future.then((value) {
      print(value[0].title);
      return;
    }, onError: (e) {
      // Invoked when the future is completed with an error.
      if (e) {
        return 499; // The successor is completed with the value 499.
      } else {
        throw e; // The successor is completed with the error e.
      }
    });
  }
}
