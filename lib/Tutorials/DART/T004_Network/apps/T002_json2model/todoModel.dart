class Todo {
  final int userId;
  final int? id;
  final String title;
  final bool completed;
  final bool? dummy;

  Todo(this.userId, this.id, this.title, this.completed, this.dummy);

  static Todo fromMap(Map<String, dynamic> json) {
    print('---- $json');
    print('---- ${json['id']}');
    return Todo(
      json['userId'],
      json['id1'],
      json['title'],
      json['completed'],
      json['dummy'],
    );
  }

  printObj() {
    print("User ID   : $userId");
    print("id        : $id");
    print("title     : $title");
    print("completed : $completed");
    print("Dummy     : $dummy");
  }
}
