import json
import traceback

from requests import api
from lib.api_client import callAPI
from lib.read_model import readDict
from lib.utils import fileNameFromAPIname, writeJsonToFile, replaceDataDataKeys, writeStringToFile
from lib.write_model import ModelWrite

def processPostman(siteCode, collection, path):
    token = ""
    apiArray = []
    #names = [item["name"] for item in collection]
   
    count = 0
    for item in collection:
        count += 1
        # print("===================================", count)
        
        try:
            
            code, tok = writeModel(siteCode, item, token, path, apiArray)
            if tok != "": token = tok
            
            if code != 200:
                print("Unexpected response ......................... or api not working")
        except:
            traceback.print_exc()
            print("*******************************************************************")
            exit()
        
    return apiArray

def writeModel(siteCode, item, token , path, apiArray):
    name = item["name"]
    if name =="UpdatePassword" or name== "ForgetPassword": return 200, token
    if not (name == "GetVitalHistoryData" or name == "Login"): return 200, token    #to quickly go to the desired API

    print("*" * len(name))
    print(name)
    print("*" * len(name))

    url, req, code, resp  = callAPI(item, siteCode, token)



    if name == "Login":
        token = resp["data"]["Token"]

    folder = path+"models/"+name+"/"

    modelFileName = fileNameFromAPIname(name)
    writeJsonToFile(folder,modelFileName+"_response.json", resp)
    # replaceDataDataKeys(resp, modelFileName)
    modelArray = []
    readDict(-1, resp, "General", modelArray)
    writeJsonToFile(folder,modelFileName+"_req.json", req)
    writeJsonToFile(folder,modelFileName+"_array.json", modelArray)
    model = ModelWrite(modelArray[::-1])
    writeStringToFile(folder,modelFileName+".dart", model.st)

    apiCall = {"name": name, "modelFileName": modelFileName, "url": url, "request":req, "response_code": code, "response":resp}
    apiArray.append(apiCall)    
    return code, token

def main():
    
    path = ""
    # fileName = "postman/CHI_HEALTHBUDDY"
    fileName = "postman/caregiver_postman_kashifdoc"
    f = open(path+fileName+".json", 'r')
    data = f.read()
    pman = json.loads(data)
    collection_name = pman["info"]["name"]
    collection = pman["item"]
    siteCode = "charms-qa"

    apiArray = processPostman(siteCode, collection, path)
    collectionStruct = {"name":collection_name, "siteCode": siteCode, "collection":apiArray}
    writeJsonToFile(path+"models/", fileName.replace("/", "")+"_"+siteCode+".json", collectionStruct)

main()


# for data in dstruct:
#     fName = path+"generated_json/"+data["modelName1"]+".json"
#     f = open(fName, "w")
#     f.write(json.dumps(data, indent=4))
#     f.close()
