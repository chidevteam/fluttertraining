import '../../lib/models/general.dart';
import '../../lib/network/system_api_service.dart';
import '../../lib/network/api_client.dart';
import '../../lib/models/requests.dart';

void main() async {
  String siteCode = "charms-qa";
  ApiClient.create(siteCode);

  print("hello world ..... ");

  final req = await makeLoginRequest("", "kashifd", "Kashif123@");
  ApiSingleResponse<LoginResult> resp1 = await SystemApiService.login(req);
  LoginResult result = resp1.data as LoginResult;
  print('Token : ${result.token}');
  ApiClient.setAuthToken(result.token);

  // getLiveObservations();
  // getUnVerifiedOrders();
  getPatients();
  // getProfile();
  // getCMSession();
  // getVitalHistoryDataTab();
}

getLiveObservations() async {
  final resp = await SystemApiService.getLiveObservations({
    'limit': 30,
    'offset': 0,
  });

  print('${resp.data!.data![0].measurementName}');
}

getUnVerifiedOrders() async {
  final homeCareList = await SystemApiService.getUnVerifiedOrders({
    'limit': 30,
    'offset': 0,
  });
  print("$homeCareList"); // print("${homeCareList.data}");
  for (int i = 0; i < homeCareList.data!.data!.length; i += 1)
    print('${homeCareList.data!.data![i].referenceName}');
}

getPatients() async {
  final patientList = await SystemApiService.getPatients({
    "columns": [
      "user_id",
      "employee_no",
      "image",
      "full_name",
      "user.primary_group.group_name",
      "gender",
      "mobile_phone",
      "date_added",
      "date_updated"
    ],
    "limit": 100,
    "offset": 0,
    "where": {
      "children": [
        {"column": "employee_id", "op": "ne", "search": "15"}
      ],
      "group": "and"
    }
  });
  print("$patientList"); // print("${homeCareList.data}");
  for (int i = 0; i < patientList.data!.data!.length; i += 1)
    print('${patientList.data!.data![i].userName}');
}

getProfile() async {
  final resp = await SystemApiService.getProfile({});

  print('${resp.data!.fullName}');
}

getCMSession() async {
  final cmSessionList = await SystemApiService.getCMSession(
      {"episode_id": 228, "limit": 30, "offset": 0});
  print("$cmSessionList"); // print("${homeCareList.data}");
  for (int i = 0; i < cmSessionList.data!.data!.length; i += 1)
    print('${cmSessionList.data!.data![i].episodePatientFullName}');
}

getVitalHistoryDataTab() async {
  final vitalHistoryList = await SystemApiService.getVitalHistoryDataTabular({
    "episode_id": 171,
    "from": 1627758000,
    "to": 1630436399,
    "start": 1627758000,
    "end": 1627758000,
    "vital_name": "Blood pressure monitor",
    "app_version": "3.1.9-19000",
    "os_name": "iOS",
    "tz_offset": 18000,
    "limit": 1000
  });
  print("$vitalHistoryList"); // print("${homeCareList.data}");
  // for (int i = 0; i < vitalHistoryList.data!.data!.length; i += 1)
  //   print('${vitalHistoryList.data!.data![i]}');
}
