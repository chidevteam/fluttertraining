from lib.utils import typeName, to_camel_case

TypeMap = {"str": "String", "list": "List", 
    "int": "int", "float": "double", 
    "dict": "Map", "NoneType":"dynamic", "bool": "bool"}


def readDict(level, data, modelName, modelArray):
    fields = []
    level += 1
    index = 0
    for key in data.keys():
        index += 1
        extra1 = 20 - len(key)
        keyType = typeName(data[key])
        value = data[key]
        print(" "*(4*level), key[:20], "-" * extra1, keyType) 
        fieldDict = {"fieldName": key, "fieldType": TypeMap[keyType]}

        if keyType == 'list':
            if len(value)>0:
                if typeName(value[0]) == 'dict':
                    tempName = key
                    readDict(level, value[0], tempName, modelArray)
                    tempName = to_camel_case(tempName.capitalize())
                elif typeName(value[0]) == 'str':
                    tempName = "str"
                elif typeName(value[0]) == 'int':
                    tempName = "int"
                elif typeName(value[0]) == 'float':
                    tempName = "double"
                else:
                    tempName = "UNKNOWN"
                    print("====================", key)

            else:
                tempName = "UNKNOWN"
                print("====================", key)

            tName = tempName#to_camel_case(tempName.capitalize())
            fieldDict["listType"] = tName

        if keyType == 'dict':
            tempName = key
            readDict(level, value, tempName, modelArray)
            tName = to_camel_case(tempName.capitalize())
            fieldDict["dictType"] = tName
        
        fields.append(fieldDict)

    mName = to_camel_case(modelName.capitalize())
    modelArray.append({"modelName": mName, "fields": fields})
    
