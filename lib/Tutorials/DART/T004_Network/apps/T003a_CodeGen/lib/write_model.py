
from lib.utils import to_camel_case


class ModelWrite:

    def __init__(self, model):
        self.model = model
        self.st = "\n"


        print("\n=======================================") 
        self.process()
        print("=======================================\n")

    def process(self):

        for classStruct in self.model:
            className = classStruct["modelName"]
            print('=== '+className+': ', (25-len(className))*' ', len(classStruct["fields"]))  
            self.st += "class " + className + " {\n"
            self.writeInstanceVars(classStruct)
            self.writeConstructor(classStruct)
            self.writeMap(classStruct)
            self.st += "}\n\n"

    def writeInstanceVars(self, classStruct):
        for field in classStruct['fields']:
            fieldName = to_camel_case(field["fieldName"])
            fieldType = field["fieldType"]

            if fieldType == "List":
                self.st += "  final List"+"<"+field["listType"]+">? "+fieldName+";\n"
            elif fieldType == "Map":
                self.st += "  final Map<String, "+field["dictType"]+">? "+fieldName+";\n"
            elif fieldType == "dynamic":
                self.st += "  final "+fieldType+" "+fieldName+";\n"
            else:
                self.st += "  final "+fieldType+"? "+fieldName+";\n"

    def writeConstructor(self, classStruct):
        className = classStruct["modelName"]
        self.st += "\n  " + className + "(\n"
        for field in classStruct['fields']:
            fieldName = to_camel_case(field["fieldName"])
            self.st += "    this."+fieldName+",\n"
        self.st += "  );\n"

    def writeMapField(self, field):
        
        fieldType = field["fieldType"]
        fieldName = to_camel_case(field["fieldName"])
        if fieldType == 'List':
            self.st += "      resp['"+field["fieldName"]+"'] "
            self.st += "== null ? null: "+ fieldName+",\n"
        elif fieldType == 'Map':
            typ = fieldType
            self.st += "      "+fieldType+"<String, "+field["dictType"]+">"+".from"+"(resp['"+fieldName+"'].map((item) => "
            if typ == 'String':
                self.st += "item)),\n"
            else:
                
                self.st += field["dictType"]+".fromMap(item))),\n"
        elif fieldType == 'dynamic':
            self.st += "      resp['"+field["fieldName"]+"'] as "+field["fieldType"]+",\n"
        else:
            self.st += "      resp['"+field["fieldName"]+"'] as "+field["fieldType"]+"?,\n"


    def writeMap(self, classStruct):
        className = classStruct["modelName"]
        self.st += "\n  static "+className+" fromMap(Map<String, dynamic> resp) {\n"
        


        for field in classStruct['fields']:
            if field["fieldType"] == "List":
                # fieldType = field["listType"]
                camelNameField = to_camel_case(field['fieldName'])
                listType = field["listType"]
                self.st += "\n    List<"+listType+">? "+camelNameField+";\n"
                self.st += "    if (resp['"+field['fieldName']+"'] != null)\n"
                self.st += "      "+camelNameField+" = List<"+listType+">.from(\n"
                self.st += "          resp['"+field['fieldName']+"'].map((item) => "+listType+".fromMap(item)));\n"


        self.st += "\n    return "+className+"(\n"
        for field in classStruct['fields']:
            self.writeMapField(field)
        self.st += "    );\n"
        self.st += "  }\n"

