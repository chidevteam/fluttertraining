https://www.nylas.com/blog/use-python-requests-module-rest-apis/

url1 = 'https://jsonplaceholder.typicode.com/todos/1'
url2 = 'http://api.open-notify.org/astros.json'
response = requests.get(url2)
v3 = response.json() # This method is convenient when the API returns JSON
print("=============================\n", v3)
query = {'lat':'45', 'lon':'180'}
response = requests.get('http://api.open-notify.org/iss-pass.json', params=query)
print("============================\n",response.json())
# Create a new resource
response = requests.post('https://httpbin.org/post', data = {'key':'value'})
print(response)
# Update an existing resource
# requests.put('https://httpbin.org/put', data = {'key':'value'})
====================================================================

main1_postman: reads from input/postman/....json
               and writes to generated/....json


Screen Name         App Bar        Naming Func    Naming        Extra Elements     Author/Reviewer1/Revieer2
Messages            Y, N, Y              