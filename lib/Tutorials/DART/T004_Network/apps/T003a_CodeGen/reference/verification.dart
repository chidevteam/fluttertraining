class HomeCareList {
  final List<HomeCare> data;
  final int totalRecords;

  HomeCareList(
    this.data,
    this.totalRecords,
  );

  static HomeCareList fromMap(Map<String, dynamic> resp) {
    return HomeCareList(
      List<HomeCare>.from(resp['data'].map((item) => HomeCare.fromMap(item))),
      resp['total_records'] as int,
    );
  }
}

class HomeCare {
  final int patientId;
  final int episodeId;
  final int episodeVitalId;
  final String name;
  final String vitalName;
  final String frequency;
  final int datetime;
  final String icon;
  final String patientName;
  final String instructions;
  final String deviceImage;
  final String deviceName;
  final String deviceModel;
  final String deviceCode;
  final List<MeasurementItem> items;
  final List<HomeCareSchedule> schedules;
  final String referenceName;
  final String recordType;
  final int referenceId;
  final String reportType;
  final String reportUrl;
  final bool isAdvanceSchedule;
  final List<AdvanceSchedule> advanceSchedules;
  final String dosageForm;
  final String strength;
  final String firstNameEnc;
  final String lastNameEnc;
  final String mobilePhoneEnc;

  HomeCare(
    this.patientId,
    this.episodeId,
    this.episodeVitalId,
    this.name,
    this.vitalName,
    this.frequency,
    this.datetime,
    this.icon,
    this.patientName,
    this.instructions,
    this.deviceImage,
    this.deviceName,
    this.deviceModel,
    this.deviceCode,
    this.items,
    this.schedules,
    this.referenceName,
    this.recordType,
    this.referenceId,
    this.reportType,
    this.reportUrl,
    this.isAdvanceSchedule,
    this.advanceSchedules,
    this.dosageForm,
    this.strength,
    this.firstNameEnc,
    this.lastNameEnc,
    this.mobilePhoneEnc,
  );

  static HomeCare fromMap(Map<String, dynamic> resp) {
    return HomeCare(
      resp['patient_id'] as int,
      resp['episode_id'] as int,
      resp['episode_vital_id'] as int,
      resp['name'] as String,
      resp['vital_name'] as String,
      resp['frequency'] as String,
      resp['datetime'] as int,
      resp['icon'] as String,
      resp['patient_name'] as String,
      resp['instructions'] as String,
      resp['device_image'] as String,
      resp['device_name'] as String,
      resp['device_model'] as String,
      resp['device_code'] as String,
      List<MeasurementItem>.from(
          resp['items'].map((item) => MeasurementItem.fromMap(item))),
      List<HomeCareSchedule>.from(
          resp['schedules'].map((item) => HomeCareSchedule.fromMap(item))),
      resp['reference_name'] as String,
      resp['record_type'] as String,
      resp['reference_id'] as int,
      resp['report_type'] as String,
      resp['report_url'] as String,
      resp['is_advance_schedule'] as bool,
      List<AdvanceSchedule>.from(resp['advance_schedules']
          .map((item) => AdvanceSchedule.fromMap(item))),
      resp['dosage_form'] as String,
      resp['strength'] as String,
      resp['first_name_enc'] as String,
      resp['last_name_enc'] as String,
      resp['mobile_phone_enc'] as String,
    );
  }
}

class MeasurementItem {
  final String measurementKey;
  final String measurementIcon;
  final String measurementName;
  final String measurementType;
  final String measurementUnit;
  final String measurementAbbr;

  MeasurementItem(
    this.measurementKey,
    this.measurementIcon,
    this.measurementName,
    this.measurementType,
    this.measurementUnit,
    this.measurementAbbr,
  );

  static MeasurementItem fromMap(Map<String, dynamic> resp) {
    return MeasurementItem(
      resp['measurement_key'] as String,
      resp['measurement_icon'] as String,
      resp['measurement_name'] as String,
      resp['measurement_type'] as String,
      resp['measurement_unit'] as String,
      resp['measurement_abbr'] as String,
    );
  }
}

class HomeCareSchedule {
  final int startTime;
  final String repeatCycle;
  final int repeatUnit;
  final String endType;
  final int endTime;
  final int repeatCount;

  HomeCareSchedule(
    this.startTime,
    this.repeatCycle,
    this.repeatUnit,
    this.endType,
    this.endTime,
    this.repeatCount,
  );

  static HomeCareSchedule fromMap(Map<String, dynamic> resp) {
    return HomeCareSchedule(
      resp['start_time'] as int,
      resp['repeat_cycle'] as String,
      resp['repeat_unit'] as int,
      resp['end_type'] as String,
      resp['end_time'] as int,
      resp['repeat_count'] as int,
    );
  }
}

class AdvanceSchedule {
  final String delta;
  final int startDate;
  final String repeatType;
  final String repeatCycleCustom;
  final String repeatCycle;
  final int repeatUnit;
  final String frequency;
  final List<AdvanceScheduleDays> days;
  final String dosageType;
  final double dosage;
  final String endType;
  final int endValue;
  final String dosageForm;
  bool isHomecare = false;

  AdvanceSchedule(
    this.delta,
    this.startDate,
    this.repeatType,
    this.repeatCycleCustom,
    this.repeatCycle,
    this.repeatUnit,
    this.frequency,
    this.days,
    this.dosageType,
    this.dosage,
    this.endType,
    this.endValue,
    this.dosageForm,
    this.isHomecare,
  );

  static AdvanceSchedule fromMap(Map<String, dynamic> resp) {
    return AdvanceSchedule(
      resp['delta'] as String,
      resp['start_date'] as int,
      resp['repeat_type'] as String,
      resp['repeat_cycle_custom'] as String,
      resp['repeat_cycle'] as String,
      resp['repeat_unit'] as int,
      resp['frequency'] as String,
      List<AdvanceScheduleDays>.from(
          resp['days'].map((item) => AdvanceScheduleDays.fromMap(item))),
      resp['dosage_type'] as String,
      resp['dosage'] as double,
      resp['end_type'] as String,
      resp['end_value'] as int,
      resp['dosage_form'] as String,
      resp['is_homecare'] as bool,
    );
  }
}

class AdvanceScheduleDays {
  bool isHomecare = false;
  final String type;
  final String day;
  final List<AdvanceScheduleTime> times;

  AdvanceScheduleDays(
    this.isHomecare,
    this.type,
    this.day,
    this.times,
  );

  static AdvanceScheduleDays fromMap(Map<String, dynamic> resp) {
    return AdvanceScheduleDays(
      resp['is_homecare'] as bool,
      resp['type'] as String,
      resp['day'] as String,
      List<AdvanceScheduleTime>.from(
          resp['times'].map((item) => AdvanceScheduleTime.fromMap(item))),
    );
  }
}

class AdvanceScheduleTime {
  bool isHomecare = false;
  final int time;
  final double dosage;

  AdvanceScheduleTime(
    this.isHomecare,
    this.time,
    this.dosage,
  );

  static AdvanceScheduleTime fromMap(Map<String, dynamic> resp) {
    return AdvanceScheduleTime(
      resp['is_homecare'] as bool,
      resp['time'] as int,
      resp['dosage'] as double,
    );
  }
}
