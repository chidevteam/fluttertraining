
dart --no-sound-null-safety main.dart

Now I understand async, await, and future

- If a function has "await" in it, we need to put "async" in the function name line
    e.g., 
        ... login() async{
            await ....
        }

8:15 - work really starts.
Target 10:00 -- Call all apis in Dart max 1:00    