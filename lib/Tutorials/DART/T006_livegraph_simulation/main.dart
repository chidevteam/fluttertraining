void main() {
  double devSR = 1.1;
  double playbackSR = .9;
  print('Hello, World442!');
  testSimulator(devSR, playbackSR);
}

void testSimulator(double devSR, double playbackSR) {
  print("$devSR, $playbackSR");
  double residualDataDev = 0;
  int intDataDev = 0; //Data received in samples
  int k = 0;

  for (double t = 0; t < 10000; t += 1.0 / playbackSR) {
    int deviceDataInt = devSR.toInt();
    residualDataDev += devSR - deviceDataInt;
    int temp = residualDataDev.toInt();
    deviceDataInt += temp;
    residualDataDev -= temp;

    intDataDev += deviceDataInt;
    int dataStoreTemp = intDataDev - 0;
    String sResidualDataDev = "${residualDataDev.toStringAsFixed(2)}";
    String sTime = "${t.toStringAsFixed(2)}";
    String sintDataStore = "${intDataDev.toStringAsFixed(2)}";
    String sDataStoreTemp = "${dataStoreTemp.toStringAsFixed(2)}";

    print("$k $sTime:($sResidualDataDev)  -> $sintDataStore $sDataStoreTemp");

    intDataDev = dataStoreTemp;

    k++;
    if (k == 25) break;
  }
}
