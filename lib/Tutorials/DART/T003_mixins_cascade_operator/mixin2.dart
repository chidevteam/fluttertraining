mixin CenterPoint on Shape {
  int x = 0;
  int y = 0;

  sPrint() {
    print("x,y = $x $y");
    draw();
  }
}

class Shape {
  String name;
  Shape(this.name);

  draw() {
    print("Drawing");
  }
}

class Circle extends Shape with CenterPoint {
  double radius = 0;
  Circle(name) : super(name);

  draw() {
    print("drawing circle");
  }
}

main() {
  Circle circle = Circle("Circle1")
    ..x = 5
    ..y = 5;
  circle.sPrint();
}
