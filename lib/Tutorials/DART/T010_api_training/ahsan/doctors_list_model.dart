// import 'package:flutter/material.dart';

// // import '../preferences.dart';

// class GetDoctorsList {
//   final List<GetDoctors> data;
//   final int? totalRecord;
//   GetDoctorsList(this.data, this.totalRecord);

//   static GetDoctorsList fromMap(Map<String, dynamic> resp) {
//     List<GetDoctors> data = [];
//     if (resp['data'] != null) {
//       data = List<GetDoctors>.from(
//           resp['data'].map((item) => GetDoctors.fromMap(item)));
//     }

//     return GetDoctorsList(
//       data,
//       resp['total_records'] as int?,
//     );
//   }
// }

// class GetDoctors {
//   final bool? allowedWaitingAppointments;
//   final int? clinicDoctorId;
//   final int? doctorId;
//   final int? clinicId;
//   final String? fullName;
//   final String? speciality;
//   final String? image;
//   final String? clinicName;
//   final String? clinicAddress;
//   int? nextAvailableDate;
//   final int? selectedAppointmentTypeId;
//   final List<Schedule>? schedules;
//   final List<AppointmentType>? appointmentTypes;
//   GetDoctors(
//     this.allowedWaitingAppointments,
//     this.clinicDoctorId,
//     this.doctorId,
//     this.clinicId,
//     this.fullName,
//     this.speciality,
//     this.image,
//     this.clinicName,
//     this.clinicAddress,
//     this.nextAvailableDate,
//     this.selectedAppointmentTypeId,
//     this.schedules,
//     this.appointmentTypes,
//   );

//   String getSpeciality() {
//     if (speciality == null) {
//       return '--';
//     } else {
//       return speciality!;
//     }
//   }

//   String scheduleTime() {
//     // if (nextAvailableDate != null) {
//     //   return Preferences.timeStampToCustom(
//     //       nextAvailableDate!, 'dd-MMM-yyyy hh:mm a');
//     // } else {
//     return '--';
//     // }
//   }

//   static GetDoctors fromMap(Map<String, dynamic> resp) {
//     List<Schedule>? schedules;
//     if (resp['schedules'] != null) {
//       schedules = List<Schedule>.from(
//           resp['schedules'].map((item) => Schedule.fromMap(item)));
//     }
//     List<AppointmentType>? appointmentTypes;
//     if (resp['appointmentTypes'] != null) {
//       appointmentTypes = List<AppointmentType>.from(resp['appointmentTypes']
//           .map((item) => AppointmentType.fromMap(item)));
//     }

//     return GetDoctors(
//       resp['allowedWaitingAppointments'] as bool?,
//       resp['clinicDoctorId'] as int?,
//       resp['doctorId'] as int?,
//       resp['clinicId'] as int?,
//       resp['fullName'] as String?,
//       resp['speciality'] as String?,
//       resp['image'] as String?,
//       resp['clinicName'] as String?,
//       resp['clinicAddress'] as String?,
//       resp['nextAvailableDate'] as int?,
//       resp['selectedAppointmentTypeId'] as int?,
//       resp['schedules'] == null ? null : schedules,
//       resp['appointmentTypes'] == null ? null : appointmentTypes,
//     );
//   }
// }

// class Schedule {
//   Schedule(
//     this.startTime,
//     this.endTime,
//     this.days,
//   );

//   String scheduleTime() {
//     String localStartTime = 'hh:mm a';
//     // Preferences.timeStampToCustom(startTime!, 'hh:mm a');
//     String localEndTime =
//         'hh:mm a'; //Preferences.timeStampToCustom(endTime, 'hh:mm a');
//     return '$localStartTime - $localEndTime';
//   }

//   bool isDayAvailable(String day) {
//     for (int j = 0; j < days!.length; j++) {
//       if (days![j] == day) {
//         return true;
//       }
//     }
//     return false;
//   }

//   final int? startTime;
//   final int? endTime;
//   final List<String>? days;

//   static Schedule fromMap(Map<String, dynamic> resp) {
//     List<String>? days;
//     if (resp['days'] != null) {
//       days = List<String>.from(resp['days'].map((item) => item));
//     }

//     return Schedule(
//       resp['startTime'] as int?,
//       resp['endTime'] as int?,
//       resp['days'] == null ? null : days,
//     );
//   }
// }

// class AppointmentType {
//   final int? appointmentTypeId;
//   final String? type;
//   bool? selected;
//   bool? isEnable = false;

//   AppointmentType(
//       this.appointmentTypeId, this.type, this.selected, this.isEnable);

//   Color cardBackground() {
//     var c = const Color(0xFF538397);

//     // if (isEnable) {
//     if (selected!) {
//       c = const Color(0xFF387b96);
//     } else {
//       c = Colors.white;
//     }
//     // } else {
//     //   c = const Color(0xFFe6e6e6);
//     // }

//     return c;
//   }

//   Color cardText() {
//     var c = const Color(0xFF538397);

//     if (selected!) {
//       c = Colors.white;
//     } else {
//       c = Colors.black;
//     }
//     return c;
//   }

//   static AppointmentType fromMap(Map<String, dynamic> resp) {
//     return AppointmentType(
//       resp['appointmentTypeId'] as int?,
//       resp['type'] as String?,
//       resp['selected'] as bool?,
//       resp['isEnable'] as bool?,
//     );
//   }
// }
