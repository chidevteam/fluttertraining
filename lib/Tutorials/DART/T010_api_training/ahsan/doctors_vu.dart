// import 'dart:developer';

// import 'package:chi_patient_app/components/appbars.dart';
// import 'package:chi_patient_app/components/images.dart';
// import 'package:chi_patient_app/components/loading_pages.dart';
// import 'package:chi_patient_app/components/padded_text.dart';

// import 'package:flutter/material.dart';
// import 'package:stacked/stacked.dart';

// import 'doctors_vm.dart';

// class DoctorsScreen extends ViewModelBuilderWidget<DoctorsViewModel> {
//   const DoctorsScreen({Key? key}) : super(key: key);

//   @override
//   Widget builder(
//       BuildContext context, DoctorsViewModel viewModel, Widget? child) {
//     return Scaffold(
//       appBar: AppBarBack(title: 'Doctors', addBackButton: true),
//       body: viewModel.isBusy
//           ? const GettingRecords()
//           : viewModel.doctorsList.isEmpty
//               ? const NoRecord('No Results found')
//               : Column(
//                   children: [
//                     SingleChildScrollView(
//                       child: doctorSearchBar(context),
//                     ),
//                     Expanded(
//                       flex: 7,
//                       child: ListView.builder(
//                         itemBuilder: (context, index) {
//                           return Column(
//                             children: [
//                               Padding(
//                                 padding:
//                                     const EdgeInsets.fromLTRB(16, 8, 24, 8),
//                                 child: Row(
//                                   children: [
//                                     doctorImage(viewModel, index),
//                                     const SizedBox(
//                                       width: 16,
//                                     ),
//                                     doctorData(viewModel, index),
//                                     const Spacer(),
//                                     doctorVideoIcon(),
//                                     const SizedBox(
//                                       width: 28,
//                                     ),
//                                     doctorAudioIcon(),
//                                   ],
//                                 ),
//                               ),
//                               const Divider(
//                                 color: Colors.grey,
//                               )
//                             ],
//                           );
//                         },
//                         itemCount: viewModel.doctorsList.length,
//                       ),
//                     ),
//                   ],
//                 ),
//       floatingActionButton: doctorFloatingButtons(),
//     );
//   }

//   Widget doctorSearchBar(BuildContext context, DoctorsViewModel viewModel) {
//     return Padding(
//       padding: const EdgeInsets.fromLTRB(15, 10, 15, 0),
//       child: SizedBox(
//         height: 60,
//         width: double.infinity,
//         child: Stack(
//           children: [
//             TextField(
//                 maxLines: 1,
//                 style: Theme.of(context).textTheme.headline6,
//                 decoration: InputDecoration(
//                     hintText: 'Search',
//                     contentPadding: const EdgeInsets.fromLTRB(10, 10, 50, 25),
//                     hintStyle: TextStyle(
//                         color: Colors.grey[800],
//                         fontWeight: FontWeight.normal,
//                         fontSize: 16),
//                     border: const OutlineInputBorder(
//                         borderRadius: BorderRadius.all(Radius.circular(4)))),
//                 onChanged: (text) {
//                   viewModel.onDoctorListSearch(text, context);
//                 }),
//             Visibility(
//               visible: viewModel.isSearchBusy,
//               child: Positioned(
//                 left: 340,
//                 top: 12,
//                 child: Visibility(
//                   visible: true,
//                   child: SizedBox(
//                     child:
//                         CircularProgressIndicator(color: Colors.grey.shade400),
//                     height: 30.0,
//                     width: 30.0,
//                   ),
//                 ),
//               ),
//             )
//           ],
//         ),
//       ),
//     );
//   }

//   Widget doctorImage(DoctorsViewModel viewModel, int index) {
//     return Expanded(
//       flex: 3,
//       child: CHINetworkImage(
//         viewModel.doctorsList[index].image,
//         height: 48,
//         width: 48,
//       ),
//     );
//   }

//   Widget doctorData(DoctorsViewModel viewModel, int index) {
//     return Expanded(
//       flex: 10,
//       child: Column(
//         crossAxisAlignment: CrossAxisAlignment.start,
//         children: [
//           Text(
//             viewModel.doctorsList[index].fullName.toString(),
//             maxLines: 1,
//             overflow: TextOverflow.ellipsis,
//             style: const TextStyle(
//                 color: Color.fromRGBO(56, 123, 150, 1),
//                 fontSize: 15,
//                 fontWeight: FontWeight.bold),
//           ),
//           PaddedText(
//             maxLines: 1,
//             overflow: TextOverflow.ellipsis,
//             text: viewModel.doctorsList[index].speciality.toString(),
//             style: TextStyle(color: Colors.grey[600]),
//             padding: const EdgeInsets.only(top: 4),
//           ),
//         ],
//       ),
//     );
//   }

//   Widget doctorVideoIcon() {
//     return Expanded(
//         flex: 1,
//         child: IconButton(
//             onPressed: () {},
//             icon: const Icon(
//               Icons.videocam_rounded,
//               size: 24,
//               color: Color.fromRGBO(56, 123, 150, 1),
//             )));
//   }

//   Widget doctorAudioIcon() {
//     return Expanded(
//         flex: 1,
//         child: IconButton(
//             onPressed: () {},
//             icon: const Icon(
//               Icons.call_rounded,
//               size: 24,
//               color: Color.fromRGBO(56, 123, 150, 1),
//             )));
//   }

//   Widget doctorFloatingButtons() {
//     return Row(
//       mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//       children: [
//         Padding(
//           padding: const EdgeInsets.only(left: 36.0),
//           child: FloatingActionButton(
//             heroTag: null,
//             backgroundColor: const Color.fromRGBO(56, 123, 150, 1),
//             onPressed: () {},
//             child: const Icon(
//               Icons.videocam_rounded,
//               color: Colors.white,
//             ),
//           ),
//         ),
//         Padding(
//           padding: const EdgeInsets.only(left: 36.0),
//           child: FloatingActionButton(
//             heroTag: null,
//             backgroundColor: const Color.fromRGBO(56, 123, 150, 1),
//             onPressed: () {},
//             child: const Icon(Icons.call_sharp, color: Colors.white),
//           ),
//         ),
//       ],
//     );
//   }

//   @override
//   DoctorsViewModel viewModelBuilder(BuildContext context) {
//     final vm = DoctorsViewModel();
//     vm.onGetDoctorsList(context);
//     return vm;
//   }
// }
