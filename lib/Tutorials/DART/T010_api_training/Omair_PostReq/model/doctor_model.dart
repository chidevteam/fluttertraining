class Doctor {
  final String? full_name;
  final String? mobile_phone;
  final String? clinics;

  Doctor(this.full_name, this.mobile_phone, this.clinics);

  static Doctor fromMap(Map<String, dynamic> resp) {
    return Doctor(resp['full_name'] as String, resp['mobile_phone'] as String,
        resp['clinics'] as String);
  }

  String toString() {
    String st = '\n=======';
    st += '\nDoctor Name: ' + full_name!;
    st += '\nDoctor Contact: ' + mobile_phone!;
    return st;
  }
}
