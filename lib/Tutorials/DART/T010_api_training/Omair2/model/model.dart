class UserDetail {
  late int id;
  late String name;
  late String email;

  UserDetail({required this.id, required this.name, required this.email});

  UserDetail.fromJson(Map<String, dynamic> map) {
    id = map['id'];
    name = map['name'];
    email = map['email'];
  }

  String toString() {
    String str = '\n=======';
    str += '\n Id ' + id.toString();
    str += '\n Name ' + name;
    str += '\n Email  ' + email;
    return str;
  }
}

class UserDetailList {
  List<UserDetail> userDetails = [];

  UserDetailList(List<Map<String, dynamic>> data) {
    for (int i = 0; i < data.length; i++) {
      Map<String, dynamic> map = data[i];
      UserDetail userDetail =
          UserDetail(id: map['id'], name: map['name'], email: map['email']);
      userDetails.add(userDetail);
    }
  }

  String toString() {
    return userDetails.toString();
  }
}
