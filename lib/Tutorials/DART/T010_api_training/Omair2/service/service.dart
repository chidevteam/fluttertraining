import 'dart:convert';

import 'package:http/http.dart' as http;
import '../model/model.dart';

Future<List<UserDetail>> fetchUser() async {
  final response =
      await http.get(Uri.parse('https://jsonplaceholder.typicode.com/users'));
  if (response.statusCode == 200) {
    var json = jsonDecode(response.body);
    List<UserDetail> users = [];
    for (var data in json) {
      users.add(UserDetail.fromJson(data));
    }
    return users;
  } else
    throw Exception('Failed to get Data');
}
