class Doctor {
  String? fullName;
  String? speciality;
  String? image;
  String? clinic_name;

  Doctor(this.fullName, this.speciality, this.image, this.clinic_name);

  Doctor.fromJson(Map<String, dynamic> map) {
    fullName = map["fullName"];
    speciality = map["speciality"];
    image = map["image"];
    clinic_name = map["clinicName"];
  }

  String toString() {
    String st = "=====================";
    st += "\nName " + fullName!;
    st += "\nSpeciality " + (speciality == null ? "NULL" : speciality!);
    // st += "\nimage " + ;
    st += "\nClinic" + clinic_name!;
    st += "\n=====================\n";
    return st;
  }
}
