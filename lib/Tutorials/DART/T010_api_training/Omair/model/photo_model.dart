class Photo {
  int? id;
  String? author;
  int? width;
  int? height;
  String? url;
  String? download_url;

  Photo(String idx, this.author, this.width, this.height, this.url,
      this.download_url) {
    id = int.parse(idx);
  }

  Photo.fromJson(Map<String, dynamic> map) {
    id = int.parse(map["id"]);
    author = map["author"];
    width = map["width"];
    height = map["height"];
    url = map["url"];
    download_url = map["download_url"];
  }

  String toString() {
    String st = "=====================";
    st += "\nID " + id.toString();
    st += "\nwidth " + width!.toString();
    st += "\nheight " + height!.toString();
    st += "\n=====================\n";
    return st;
  }
}
