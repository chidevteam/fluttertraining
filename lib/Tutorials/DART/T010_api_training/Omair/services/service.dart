import 'dart:convert';
import '../model/photo_model.dart';
import '../model/doctor_model.dart';
import 'package:http/http.dart' as http;
import 'dart:io';

Future<List<Photo>> fetchPhotos() async {
  final response = await http.get(Uri.parse('https://picsum.photos/v2/list'));

  if (response.statusCode == 200) {
    var json = jsonDecode(response.body);
    List<Photo> list = [];
    for (var obj in json) {
      // print(obj);
      list.add(Photo.fromJson(obj));
    }
    // print(list);
    // exit(0);
    return list;
  } else {
    throw Exception('Failed to load album');
  }
}

Map<String, dynamic> req = {
  "username": "daniyalP",
  "password": "Daniyal@1",
  "device_id": "c5d1a020b3f0d620",
  "device_name": "RNE-L21",
  "device_model": "RNE-L21",
  "device_type": "Android",
  "os_name": "Android",
  "os_version": "8.0.0",
  "pn_type": "Firebase",
  "pn_token": "",
  "app_version": "2.3.42-000",
  "app_type": "Patient",
  "is_patient": true,
  "lang": "en"
};

Future<http.Response> login() {
  return http.post(
    Uri.parse('https://charms-qa.cognitivehealthintl.com/auth/Login'),
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    },
    body: jsonEncode(req),
  );
}

Future<List<Doctor>> fetchDoctors(Map req, String mAuthToken) async {
  final response = await http.post(
    Uri.parse(
        'https://charms-qa.cognitivehealthintl.com/api/patient_portal/GetDoctorsList'),
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
      'Authorization': mAuthToken
    },
    body: jsonEncode(req),
  );
  if (response.statusCode == 200) {
    var json = jsonDecode(response.body);
    List<Doctor> list = [];
    for (var obj in json["data"]["data"]) {
      // print(obj.keys);
      // for (String key in obj.keys) {
      //   print(key);
      // }
      // exit(0);
      Doctor doc = Doctor.fromJson(obj);
      list.add(doc);
    }
    // print(list);

    return list;
  } else {
    throw Exception('Failed to load album');
  }
}


// class Service {
//   Future<List<Photo>> fetchData() async {
//     String url = 'https://picsum.photos/v2/list';
//     final response = await http.get(Uri.parse(url));
//     if (response.statusCode == 200) {
//       final json = jsonDecode(response.body) as List<dynamic>;
//       final resultList = json.map((e) => Photo.fromJson(e)).toList();
//       String name = resultList[0].url.toString();
//       print(name);
//       return resultList;
//     } else {
//       throw Exception('Error Fetching Files');
//     }
//   }
// }