import 'dart:convert';

import 'services/service.dart';
import 'model/photo_model.dart';
import 'model/doctor_model.dart';

void main1() async {
  List<Photo>? photos;
  print(" .... Calling API ....");
  photos = await fetchPhotos();
  print(photos);
}

void main() async {
  List<Doctor>? doctors;
  print(" .... Calling API ....");
  var x = await login();
  var json = jsonDecode(x.body);
  String token = json["data"]["Token"];
  print(token);

  Map req = {
    "limit": 30,
    "offset": 0,
    "app_version": "3.2.10-000",
    "os_name": "Android"
  };
  doctors = await fetchDoctors(req, token);
  print(doctors);
}
