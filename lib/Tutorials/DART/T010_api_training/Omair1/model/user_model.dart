class UserModel {
  late int id;
  late String title;

  UserModel(this.id, this.title);
  UserModel.fromJson(Map<String, dynamic> map) {
    id = map['id'];
    title = map['title'];
  }
  String toString() {
    String st = '\n==========';
    st += '\nId   ' + id.toString();
    st += '\nTitle    ' + title;
    return st;
  }
}

class UserModelList {
  List<UserModel> listModel = [];

  UserModelList(List<Map<String, dynamic>> data) {
    for (int i = 0; i < data.length; i++) {
      Map<String, dynamic> map = data[i];
      UserModel userModel = UserModel(map["id"], map["title"]);
      listModel.add(userModel);
    }
  }
  String toString() {
    return listModel.toString();
  }
}
