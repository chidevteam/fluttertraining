import 'model/user_model.dart';
import 'response.dart';
import 'service/service.dart';

Future<void> main() async {
  print('Hello World');
  UserModel obj =
      UserModel(45, 'Junaid'); // calling parameterized consttructor...
  print(obj); //  calling toString method...

  // UserModelList userModelList =
  //     UserModelList(responseData); //   using response.dart
  // print(userModelList);

  List<UserModel> userModel = await fetchData(); //  using actual api
  print(userModel);
}
