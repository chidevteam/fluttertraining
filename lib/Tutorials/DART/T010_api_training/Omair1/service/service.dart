import 'dart:convert';
import '../model/user_model.dart';
import 'package:http/http.dart' as http;

Future<List<UserModel>> fetchData() async {
  final response =
      await http.get(Uri.parse('https://jsonplaceholder.typicode.com/posts'));
  if (response.statusCode == 200) {
    var json = jsonDecode(response.body);
    List<UserModel> users = [];
    for (var data in json) {
      users.add(UserModel.fromJson(data));
    }
    return users;
  } else {
    throw Exception('Failed to load album');
  }
}
