// import 'package:flutter/material.dart';
// import 'photos_vm.dart';
// import 'package:stacked/stacked.dart';

// class PhotosView extends ViewModelBuilderWidget<PhotosViewModel> {
//   PhotosView({Key? key}) : super(key: key);
//   // PhotosViewModel counterViewModel = PhotosViewModel();

//   @override
//   Widget builder(
//       BuildContext context, PhotosViewModel viewModel, Widget? child) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text('Counter App'),
//       ),
//       body: FutureBuilder(
//         future: viewModel.fetchPictures(),
//         builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
//           if (snapshot.connectionState == ConnectionState.waiting) {
//             return Center(
//               child: CircularProgressIndicator(),
//             );
//             // return Center(child: CircularProgressIndicator(),);
//           } else {
//             //return Text('${viewModel.users?[0].id}');
//             return ListView.builder(
//                 shrinkWrap: true,
//                 itemCount: viewModel.users?.length,
//                 itemBuilder: (context, index) {
//                   return Flexible(
//                     child: Card(
//                         child: Padding(
//                       padding: const EdgeInsets.all(15.0),
//                       child: Row(
//                         children: [
//                           Text('$index', style: TextStyle(fontSize: 18)),
//                           Padding(
//                             padding: const EdgeInsets.only(left: 15.0),
//                             child: Text(
//                               '${viewModel.users?[index].author}',
//                               style: TextStyle(fontSize: 18),
//                               overflow: TextOverflow.ellipsis,
//                             ),
//                           ),
//                           Spacer(),
//                           CircleAvatar(
//                               radius: 35,
//                               backgroundImage: NetworkImage(
//                                   '${viewModel.users?[index].downloadUrl}'))
//                         ],
//                       ),
//                     )),
//                   );
//                 });
//           }
//         },
//       ),
//       floatingActionButton: FloatingActionButton(
//         onPressed: () {
//           viewModel.updateValue();
//         },
//         tooltip: 'Increment',
//         child: Icon(Icons.add),
//       ),
//     );
//   }

//   @override
//   PhotosViewModel viewModelBuilder(BuildContext context) {
//     final vm = PhotosViewModel();
//     vm.fetchPictures();
//     return vm;
//     //return CounterViewModel();
//   }

//   // @override
//   // Widget builder(BuildContext context,CounterViewModel viewModel, Widget child) {
//   //   return Scaffold(
//   //     appBar: AppBar(
//   //       title: Text('Counter App'),
//   //     ),
//   //     body: FutureBuilder(
//   //       future: viewModel.fetchPictures(),
//   //       builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
//   //         if(snapshot.connectionState==ConnectionState.waiting) {
//   //           return Center(child: CircularProgressIndicator(),);
//   //          // return Center(child: CircularProgressIndicator(),);
//   //         }
//   //         else{
//   //           //return Text('${viewModel.users?[0].id}');
//   //           return ListView.builder(
//   //               shrinkWrap: true,
//   //               itemCount: viewModel.users?.length,
//   //               itemBuilder: (context,index){
//   //                 return Flexible(
//   //                   child: Card(
//   //                       child: Padding(
//   //                         padding: const EdgeInsets.all(15.0),
//   //                         child: Row(children: [
//   //                             Text('$index',style: TextStyle(fontSize: 18)),
//   //                             Padding(
//   //                               padding: const EdgeInsets.only(left: 15.0),
//   //                               child: Text('${viewModel.users?[index].author}',style: TextStyle(fontSize: 18),
//   //                               overflow: TextOverflow.ellipsis,),
//   //                             ),
//   //                             Spacer(),

//   //                             CircleAvatar(radius:35,backgroundImage:NetworkImage('${viewModel.users?[index].downloadUrl}'))
//   //                           ],
//   //                         ),
//   //                       )
//   //                   ),
//   //                 );
//   //               });
//   //         }
//   //       },

//   //     ),
//   //       floatingActionButton: FloatingActionButton(
//   //         onPressed: (){
//   //           viewModel.updateValue();
//   //         },
//   //         tooltip: 'Increment',
//   //         child: Icon(Icons.add),
//   //       ),
//   //   );
//   // }

//   // @override
//   // CounterViewModel viewModelBuilder(BuildContext context) {
//   //   final vm = CounterViewModel();
//   //   vm.fetchPictures();
//   //   return vm;
//   //   //return CounterViewModel();
//   // }

// }
