class InvoiceItemList {
  List<InvoiceItem> invoiceList = [];

  InvoiceItemList.fromJson(Map<String, dynamic> json) {
    for (Map<String, dynamic> invItem in json["data"]) {
      InvoiceItem iItem = InvoiceItem.fromJson(invItem);
      invoiceList.add(iItem);
    }
  }

  String toString() {
    return invoiceList.toString();
  }
}

class InvoiceItem {
  int? id;
  String? description;
  double? unit_cost;
  bool? taxable;
  String? additional_detail;

  InvoiceItem.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    description = json['description'];
    unit_cost = json['unit_cost'].toDouble();
    taxable = json['taxable'];
    additional_detail = json['additional_detail'];
  }

  String toString() {
    String st = "\n=====================";
    st += "\n ID: $id";
    st += "\n description: $description";
    st += "\n unit_cost: $unit_cost";
    return st;
  }
}
