Map<String, dynamic> response = {
  "pages": 2,
  "count": 12,
  "data": [
    {
      "id": 21456,
      "description": "Chair",
      "unit_cost": 250,
      "taxable": true,
      "additional_detail": "This is a Chair"
    },
    {
      "id": 21455,
      "description": "Table",
      "unit_cost": 5200,
      "taxable": true,
      "additional_detail": "This is a Table"
    },
    {
      "id": 21454,
      "description": "Computer",
      "unit_cost": 65000,
      "taxable": true,
      "additional_detail": "This is a computer"
    },
    {
      "id": 21390,
      "description": "keyboard",
      "unit_cost": 56,
      "taxable": true,
      "additional_detail": ""
    },
    {
      "id": 21078,
      "description": "Refrigerator",
      "unit_cost": 1299,
      "taxable": true,
      "additional_detail": "Waves Brand only"
    },
    {
      "id": 21077,
      "description": "Honda City",
      "unit_cost": 42000,
      "taxable": true,
      "additional_detail": "Can sell Honda"
    },
    {
      "id": 20909,
      "description": "Spirometer",
      "unit_cost": 55.88,
      "taxable": true,
      "additional_detail": ""
    },
    {
      "id": 20871,
      "description": "spirometer",
      "unit_cost": 55.65,
      "taxable": true,
      "additional_detail": ""
    },
    {
      "id": 20870,
      "description": "Spirometer",
      "unit_cost": 55.63,
      "taxable": true,
      "additional_detail": ""
    },
    {
      "id": 20869,
      "description": "Spirometer ",
      "unit_cost": 52.89,
      "taxable": true,
      "additional_detail": ""
    }
  ]
};
