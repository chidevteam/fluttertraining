import 'dart:convert';
import 'services/service.dart';

// void main1() async {
//   List<Photo>? photos;
//   print(" .... Calling API ....");
//   photos = await fetchPhotos();
//   print(photos);
// }

void main() async {
  print(" .... Calling API iLabs ....");
  var x = await login();
  var json = jsonDecode(x.body);
  String token = json["accessToken"];
  print(token);
  // processAddItem(token);
  processGetItems(token);
}

void processAddItem(String token) async {
  Request addItemReq = {
    "taxable": true,
    "description": "Chair",
    "additional_detail": "This is a Chair",
    "unit_cost": "250"
  };

  var item = await addItem(addItemReq, token);
  print(item);
}

void processGetItems(String token) async {
  Request addItemReq = {};

  var items = await getItems(addItemReq, token);
  print(items);
}
