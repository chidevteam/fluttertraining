import 'dart:io' show HttpServer, HttpRequest, WebSocket, WebSocketTransformer;
import 'dart:convert' show json;
import 'dart:async' show Timer;

main() {
  int port = 7000;
  int count = 0;
  // HttpServer.bind('localhost', port).then((HttpServer server) {
  HttpServer.bind('10.8.0.61', port).then((HttpServer server) {
    // print('[+]WebSocket listening at -- ws://localhost:${port}/');
    print('[+]WebSocket listening at -- ws://10.8.0.61:${port}/');
    server.listen((HttpRequest request) {
      WebSocketTransformer.upgrade(request).then((WebSocket ws) {
        count++;
        print("here $count");

        ws.listen(
          (data) {
            print(
                '${request.connectionInfo?.remoteAddress} -- ${Map<String, dynamic>.from(json.decode(data))}');
            Timer(Duration(seconds: 1), () {
              if (ws.readyState == WebSocket.open)
                // checking connection state helps to avoid unprecedented errors
                ws.add(json.encode({
                  'data': 'from server at ${DateTime.now().toString()}',
                }));
            });
          },
          onDone: () {
            count--;
            print('Disconnecting $count');
          },
          onError: (err) => print('[!]Error -- ${err.toString()}'),
          cancelOnError: true,
        );
      }, onError: (err) => print('[!]Error -- ${err.toString()}'));
    }, onError: (err) => print('[!]Error -- ${err.toString()}'));
  }, onError: (err) => print('[!]Error -- ${err.toString()}'));
}
