
10:10

10:16 Starting server integration Start
10:18: integrated

Hardcoded client

10:38 --- middle

10:xx Widget
10:xx complete

Kotlin.
Basic client
PC102 client

Subscribe in Dart

Send message from Dart to Kotlin
Service connection

- server integrate - 2 min
- Widget which connects to server using UI
  - deviceModel, deviceID, deviceName,
  - command text field (D 234 S sdfs)
  - UI
      textview - show received data



- Finally things look very good, going to add the "DIRECTED DATA"
- Going to add the support for DIRECTED DATA in a while
- Make sure connections are "destructed" when client goes away
- Make sure resources are freed properly.

STORY
=====
- There are 3 sides [Dart Server, Kotlin clients, Dart clients]
  > Server has 2 parts, the main IPCServer, and IPCChannels (1-1 for each IPCClient)
    IPCChannel has a DeviceModel, DeviceID, and DeviceName (ChannelID is assigned by server)

List of messages: 
  Client - Server :  ADD_CHANNEL, SUBSCRIBE, UNSUBSCRIBE, 
  Server - Client :  CHANNEL_ADDED, CHANNEL_DELETED, (client will respond accordingly via subscribe)
  Client - Client :  DATA, DIRECTED_DATA

           ==========  S - send, R - Receive  ===============
                     CLIENT            SERVER             REMOTE CLIENT
1. CHANNEL_ID             R       <-        S
2. ADD_CHANNEL            S       ->        R
3. CHANNEL_ADDED                  <-        S         ->          R
4. SUBSCRIBE                                R         <-          S
5. DATA/DIRECTED_DATA     S      ->        R/S        ->          R

- IPCClients connect as IPCChannels to Dart server in 1-1 correspondence
- Server assigns Channel/Client a ChannelID and conveyed to client via CHANNEL_ID message 
  > ChannelID same for IPCChannel/IPCClient
- Client sends the ADD_CHANNEL message with info about model, etc. Server adds it to Channels list.
  > ChannelID as index
- Server conveys the existing list of Channels via CHANNEL_ADDED messages new client
  > Server informs other Channels via same CHANNEL_ADDED message
- Clients can SUBSCRIBE to a channel, list of subscribed channels are maintained in IPCChannel
- Upon leaving of client CHANNEL_REMOVED is sent to ???
- Client can send DIRECTED_DATA messages to a particular client.

Example Usage:
- One of the Dart clients will be ROOTAPP (DeviceModel), which can open graph client
    > graph client will get list of channels and it will subscribe to appropriate channels.
    > Graph client will be informed about new additions/deletion of channels.
  - Most will ignore it (device channels should ignore it)
  - ROOTAPP will open graph client 

SEQUENCE OF IMPLEMENTION TECH DETAILS
=====================================
     
Test Cases:
- UI connected, pc102 comes and goes
- UI connected, pc102 comes, another one comes, first one goes, second goes.
- UI connected, pc102 comes, another one comes, second one goes, first goes.
- UI has to be there, deviceModel = ROOTAPP
- Same device ID can't be repeated
- Add a check if client does not exist then it should not add to subscribed chanels
- UI connected, PC102 comes, UI goes --> PC102 informed

=======================================================================================
T001_simple -- gets two messages in one call if we send messages with 1 microsecond delay
T002_classes -- for actual implementation
=========================================================================================