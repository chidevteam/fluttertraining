import 'dart:io' show WebSocket;
import 'dart:convert' show json;

class IPCClient {
  final int port;
  final Function() readyFunc;
  final Function(dynamic) receiveFunc;
  late int channelID;
  WebSocket? ws = null;

  IPCClient(
      {this.port = 7000, required this.readyFunc, required this.receiveFunc}) {
    String host = 'localhost'; //10.8.0.135
    WebSocket.connect('ws://$host:${port}').then((WebSocket websocket) {
      ws = websocket;

      if (ws!.readyState == WebSocket.open) {
        ws!.listen(
          myReceiveFunc,
          onDone: () => print('Connection Closed By Server :)'),
          onError: (err) => print('[!]Error1 -- ${err.toString()}'),
          cancelOnError: true,
        );
        readyFunc();
      } else
        print('[!]Connection Denied'); // in case, server is not running
    },
        onError: (err) =>
            print('\n[!]Server not running\n -- ${err.toString()}\n\n'));
  }

  bool sendMessage(data) {
    if (ws != null) {
      if (ws!.readyState == WebSocket.open) {
        ws!.add(json.encode(data));
      }
      return true;
    } else
      print("Message Not ready !!");
    return false;
  }

  bool close() {
    ws!.close(); //TO DO: Daniyal, send proper code and message
    return true;
  }

  void myReceiveFunc(message) {
    receiveFunc(json.decode(message));
  }
}
