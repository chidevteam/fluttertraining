// import 'dart:async' show Timer;
import 'client/ipc_client.dart';
// import 'dart:convert' show json;
import 'utils/keyboard.dart';

late IPCClient client;
late int deviceID;
late String deviceModel;
late String deviceName;
late Map<String, dynamic> deviceInfo;
main(List<String> args) {
  if (args.length == 0) {
    args = ['226', 'DHEART', 'Check Me Pro'];
  }

  deviceID = int.parse(args[0]);
  deviceModel = args[1];
  deviceName = args[2];

  deviceInfo = {
    'deviceID': deviceID,
    'deviceModel': deviceModel,
    'deviceName': deviceName,
  };

  client = IPCClient(readyFunc: readyFunc, receiveFunc: recvMsg);
}

readyFunc() {
  Map message = {
    "TYPE": "ADD_CHANNEL",
    "DATA": deviceInfo,
  };
  client.sendMessage(message);

  printMenu();
  keyboardFunc(keyboardData);
}

int recvMsg(msgData) {
  // print(msgData);
  String msgType = msgData["TYPE"];

  switch (msgType) {
    case "CHANNEL_ID":
      print('CHANNEL_ID: ${msgData["DATA"]}');
      client.channelID = msgData["DATA"];
      break;

    case "CHANNEL_ADDED":
      Map data = msgData["DATA"];
      print('ADD: ChannelID:${data["channelID"]} Dev: ${data["deviceModel"]}');
      break;

    case "CHANNEL_DELETED":
      print('DEL: ${msgData["DATA"]}');
      break;

    case "DATA":
      print('DATA: ${msgData["DATA"]}');
      break;

    case "DIRECTED_DATA":
      print(
          'DATA: ${msgData["DATA"]}  SRC: ${msgData["SRC"]} ${msgData["DEST"]}');
      break;

    case "COMMAND":
      print('COMMAND: ${msgData["DATA"]}');
      break;

    default:
      print("=== SHOULD NEVER OCCUR ===MSGTYPE: $msgType ===");
      print(msgData);
      break;
  }

  return 1;
}

void keyboardData(String st) {
  List<String> commands = st.split(" ");
  if (commands.length < 1) {
    print("\n\n???????????????? no space found in command ?????????\n");
    printMenu();
    return;
  }

  String command = commands[0];

  switch (command) {
    case "S":
      subscribeChannel(commands[1]);
      break;
    case "U":
      unsubscribeChannel(commands[1]);
      break;
    case "P":
      printServerStatus(commands[1]);
      break;
    case "D":
      sendData(commands[1]);
      break;
    case "DD":
      sendDirectedData(int.parse(commands[1]), commands[2]);
      break;

    default:
  }
  printMenu();
}

sendData(String data) {
  Map message = {"TYPE": "DATA", "DATA": data};
  client.sendMessage(message);
}

sendDirectedData(int dest, String data) {
  Map message = {
    "TYPE": "DIRECTED_DATA",
    "DATA": data,
    "SRC": client.channelID,
    "DEST": dest
  };
  client.sendMessage(message);
}

subscribeChannel(String id) {
  Map message = {"TYPE": "SUBSCRIBE", "DATA": int.parse(id)};
  client.sendMessage(message);
}

unsubscribeChannel(String id) {
  Map message = {"TYPE": "UNSUBSCRIBE", "DATA": int.parse(id)};
  client.sendMessage(message);
}

printServerStatus(String id) {
  Map message = {"TYPE": "SERVER_STATUS"};
  client.sendMessage(message);
}

printMenu() {
  print("");
  print("Please Enter a Command ============ ");
  print("S no# === Subscribe to channel no: ");
  print("U no# === Unsubscribe to channel no: ");
  print("D Data === To send Data: ");
  print("DD no# Data === To send Data to a channel no: ");
  print("  DD 0 means send to All");

  print("P S    === Print Server Status: ");
  print("");
}



// Timer.periodic(
//     Duration(seconds: 5),
//     (Timer t) => {
//           client.sendMessage(message)
//           // client.close()
//         });