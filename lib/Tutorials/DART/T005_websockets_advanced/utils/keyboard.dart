import 'dart:io';
import 'dart:convert';

// void main() {
//   keyboardFunc(processLine);
//   print("out....");
// }

Stream<String> readLine() {
  return stdin.transform(utf8.decoder).transform(const LineSplitter());
}

void processLine(String line) {
  print('--$line--');
}

void keyboardFunc(Function(String)? processLine) {
  readLine().listen(processLine);
}
