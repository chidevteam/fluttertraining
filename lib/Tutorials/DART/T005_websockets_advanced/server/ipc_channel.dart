import 'dart:io' show WebSocket;
import 'dart:convert' show json;
import 'ipc_server.dart';

class IPCChannel {
  final int channelID;
  late int deviceID;
  late String deviceModel;
  late String deviceName;

  final IPCServer server;
  final WebSocket ws;
  List<int> subscribedChannels = [];

  IPCChannel(
      {required this.server, required this.ws, required this.channelID}) {
    ws.listen(
      receiveMessage,
      onDone: closeChannelFunc,
      onError: errorFunc,
      cancelOnError: true,
    );
  }

  sendMessage(msgData) {
    String message = json.encode(msgData);
    if (ws.readyState == WebSocket.open) ws.add(message);
  }

  receiveMessage(message) {
    var msgData = json.decode(message);
    String msgType = msgData["TYPE"];
    print('MessageType : $msgType    Sender: $channelID');

    switch (msgType) {
      case "ADD_CHANNEL":
        this.deviceID = msgData["DATA"]["deviceID"];
        this.deviceModel = msgData["DATA"]["deviceModel"];
        this.deviceName = msgData["DATA"]["deviceName"];
        server.addChannel(this);
        print("channel added???");
        break;

      // case "UNREGISTER":
      //   break;

      case "DATA":
        print('DATA Model#${deviceModel} ${msgData["DATA"]}');
        server.sendDataToListeners(this, msgData);
        break;

      case "DIRECTED_DATA":
        print('DIR_DATA  ${msgData["DATA"]} DEST: ${msgData["DEST"]}');
        server.sendDirectedData(this, msgData);
        break;

      case "SUBSCRIBE":
        print('SUBSCRIBE ---> ${msgData["DATA"]}');
        subscribedChannels.add(msgData["DATA"]);
        break;

      case "UNSUBSCRIBE":
        print('UNSUBSCRIBE ---> ${msgData["DATA"]}');
        subscribedChannels.remove(msgData["DATA"]);
        break;

      case "SERVER_STATUS":
        print("=== REQUEST FOR SERVER STATUS ===");
        break;

      default:
        print("=== SHOULD NEVER OCCUR ===MSGTYPE: $msgType ===");
        print(msgData);
        break;
    }

    print(server);
  }

  closeChannelFunc() {
    print('=================Disconnecting================');
    server.removeFromListeners(this);
    server.removeChannel(this);
    print(server);
  }

  errorFunc(err) {
    print('[!]Error -- ${err.toString()}');
    print('Disconnecting');
  }

  getChannelInfo() {
    var info = {
      "channelID": channelID,
      "deviceID": deviceID,
      "deviceModel": deviceModel,
      "deviceName": deviceName
    };
    return info;
  }

  String toString() {
    String st = "";
    st += "   ID: " + this.channelID.toString() + "";
    // st += "   deviceID: " + this.deviceID.toString() + "";
    st += "   deviceModel: " + this.deviceModel + "";
    st += "   listenArr: " + this.subscribedChannels.toString();
    return st;
  }
}
