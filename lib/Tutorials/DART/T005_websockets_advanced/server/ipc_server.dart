import 'dart:io' show HttpServer, HttpRequest, WebSocket, WebSocketTransformer;
import 'ipc_channel.dart';

class IPCServer {
  final int port;
  int channelCounter = 0;
  String host = 'localhost'; //'10.8.0.135';
  IPCChannel? debugChannel = null;

  Map<int, dynamic> channels = {};

  IPCServer({this.port = 7000}) {
    HttpServer.bind(host, port).then((HttpServer server) {
      print('[+]WebSocket listening at -- ws://$host:${port}/');
      server.listen((HttpRequest request) {
        WebSocketTransformer.upgrade(request).then(
            (ws) => createSocket(ws, request),
            onError: (err) => handleUpgradeError(err));
      }, onError: (err) => handleRequestError(err));
    }, onError: (err) => handleBindError(err));
  }

  handleUpgradeError(err) {
    print('[!]UpgradeError -- ${err.toString()}');
  }

  handleRequestError(err) {
    print('[!]RequestError -- ${err.toString()}');
  }

  handleBindError(err) {
    print('[!]BindError -- ${err.toString()}');
  }

  createSocket(WebSocket ws, request) {
    channelCounter++;
    // print('${request.connectionInfo?.remoteAddress}');
    // print("Channel Counter: $channelCounter");

    IPCChannel channel =
        IPCChannel(server: this, ws: ws, channelID: channelCounter);

    // Now send the Channel ID message. After that it will send register
    Map<String, dynamic> message = {};
    message["TYPE"] = "CHANNEL_ID";
    message["DATA"] = channel.channelID;
    channel.sendMessage(message);

    relayCurrentChannels(channel, "CHANNEL_ADDED");
  }

  // Current channel is not yet registered
  // loop around all channels and send to currentChannel if it
  // wants to listen
  relayCurrentChannels(IPCChannel currentChannel, String messageType) {
    channels.forEach((keyIndex, chnl) {
      var data = chnl.getChannelInfo();
      Map<String, dynamic> message = {"TYPE": messageType, 'DATA': data};
      currentChannel.sendMessage(message);
    });
  }

  addChannel(IPCChannel channel) {
    channels[channel.channelID] = channel;
    relayChannelChange(channel, "CHANNEL_ADDED");
  }

  removeChannel(IPCChannel channel) {
    channels.remove(channel.channelID);
    relayChannelChange(channel, "CHANNEL_DELETED");
  }

  relayChannelChange(IPCChannel currentChannel, String messageType) {
    var currentChannelID = currentChannel.channelID;
    var data = currentChannel.getChannelInfo();
    channels.forEach((keyIndex, chnl) {
      if (chnl.channelID != currentChannelID) {
        Map<String, dynamic> message = {"TYPE": messageType, 'DATA': data};
        chnl.sendMessage(message);
      }
    });
  }

  removeFromListeners(IPCChannel channel) {
    channels.forEach((keyIndex, chnl) {
      chnl = chnl as IPCChannel;
      chnl.subscribedChannels.remove(channel.channelID);
    });
  }

  sendDataToListeners(IPCChannel channel, msgData) {
    channels.forEach((keyIndex, chnl) {
      chnl = chnl as IPCChannel;
      if (chnl.subscribedChannels.contains(channel.channelID)) {
        chnl.sendMessage(msgData);
      }
    });
  }

  sendDirectedData(IPCChannel channel, msgData) {
    int dest = msgData["DEST"];
    //if dest = 0, send to all, otherwise directed
    if (dest == 0) {
      channels.forEach((keyIndex, chnl) {
        chnl = chnl as IPCChannel;
        if (chnl.channelID != channel.channelID) chnl.sendMessage(msgData);
      });
    } else {
      channels[dest].sendMessage(msgData); //TO DO: Check if channel exists
    }
  }

  String toString() {
    String st = "";
    st += '=============== IPCServer Status ==============\n';
    st += 'NUMBER OF channels:  ${channels.length}\n';
    channels.forEach((keyIndex, channel) {
      st += channel.toString() + "\n";
    });
    return st;
  }
}
