import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';
import '../../lib/ecg.dart';

// void main() => runApp(new ECGWidget());
class ECGApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    print("ECG Widget Started<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>");
    Widget app = new MaterialApp(
        title: "ECG Display Example",
        home: ECGHolder(),
        theme: ThemeData(
          // Add the 5 lines from here...
          appBarTheme: const AppBarTheme(
            backgroundColor: Colors.green,
            foregroundColor: Colors.black,
          ),
        ));
    return app;
  }
}

class ECGHolder extends StatefulWidget {
  @override
  _ECGHolderState createState() => _ECGHolderState();
}

class _ECGHolderState extends State<ECGHolder> {
  List<double> traceSine = [];
  List<double> traceCosine = [];
  double time = 0.0;
  Timer? _timer;

  _generateTrace(Timer t) {
    var sv = sin(time * pi);
    var cv = cos(time * pi);

    setState(() {
      traceSine.add(sv);
      traceCosine.add(cv);
    });
    time += 0.05;
  }

  @override
  initState() {
    //print("State initialized, timer counting >>>>>>>>>>>>>>>");
    super.initState();
    // create our timer to generate test values
    _timer = Timer.periodic(Duration(milliseconds: 10), _generateTrace);
  }

  @override
  void dispose() {
    _timer!.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // Generate the Scaffold
    return Scaffold(
      appBar: AppBar(
        title: Text("ECG Demo 1"),
      ),
      body: makeECGWidgets(),
    );
  }

  Widget makeECGWidgets() {
    print("ECG widget being made again");
    return Column(
      children: [
        Expanded(flex: 1, child: makeECG(traceSine, Colors.green)),
        Expanded(flex: 1, child: makeECG(traceCosine, Colors.red)),
        Expanded(flex: 1, child: makeECG(traceSine, Colors.blue)),
      ],
    );
  }
}

void buttonPressed() {
  print("Floating button pressed");
}

ECG makeECG(List<double> traceData, Color color) {
  // print("Making ECG <<<<<");
  ECG scope = ECG(
    showYAxis: true,
    yAxisColor: Colors.orange,
    margin: EdgeInsets.all(20.0),
    strokeWidth: 4.0,
    backgroundColor: Colors.black,
    traceColor: color,
    yAxisMax: 1.0,
    yAxisMin: -1.0,
    dataSet: traceData,
  );
  return scope;
}
