import 'package:flutter/material.dart';

class LiveGraph extends StatelessWidget {
  const LiveGraph({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Live Graph Title"),
      ),
      body: Center(
        child: ElevatedButton(
          onPressed: () {
            Navigator.pop(context);
          },
          child: const Text('Live Graph'),
        ),
      ),
    );
  }
}
