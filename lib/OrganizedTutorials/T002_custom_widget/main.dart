import 'package:flutter/material.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(home: CustomWidget(color1: Colors.yellow));
  }
}

class CustomWidget extends StatelessWidget {
  // const CustomWidget({ Key? key, }) : super(key: key);

  late final Color? colora;

  CustomWidget(
      {Color? color1 = Colors.red,
      Color? color2 = Colors.green,
      Color? color3 = Colors.blue}) {
    print("CustomWidget Created");
    colora = color1;
  }

  @override
  Widget build(BuildContext context) {
    Container element1 = Container(width: 150, height: 300, color: colora);
    Container element2 =
        Container(width: 150, height: 300, color: Colors.green);

    Row row1 = Row(
        children: [element1, element2],
        mainAxisAlignment: MainAxisAlignment.center);
    Container row2 = Container(width: 300, height: 300, color: Colors.red);

    Column column = Column(children: [row1, row2]);

    return column;
  }
}

createAppBar() {
  return AppBar(
      title: const Text(
        'Welcome to Flutter2221',
      ),
      backgroundColor: Colors.red);
}

createText() {
  return Text('Hello World 4455');
}
