import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class ComplexWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    print("Complex Widget Being Built ============ &&&^% ");
    return Text('Good job3!');
  }
}

class MyApp extends StatelessWidget {
  final ValueNotifier<int> _counter = ValueNotifier<int>(0);
  final Widget cplxConstWidget = ComplexWidget();
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo1q',
      debugShowCheckedModeBanner: true,
      home: Scaffold(
        appBar: AppBar(title: Text("STATELESS UPDATING VALUE")),
        body: ListenerWidget(notifier: _counter, cplxWidget: cplxConstWidget),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.plus_one),
          onPressed: () => _counter.value += 1,
        ),
      ),
    );
  }
}

class ListenerWidget extends StatelessWidget {
  final ValueNotifier<int>? notifier;
  final Widget? cplxWidget;

  const ListenerWidget({Key? key, this.notifier, this.cplxWidget});

  @override
  Widget build(BuildContext context) {
    print("");
    return ValueListenableBuilder<int>(
      builder: (BuildContext context, int value, Widget? complexWgt) {
        // This builder will only get called when the valNotifier is updated.
        return Row(
          children: [
            Text('$value  ------ '),
            complexWgt!, //This complex will not be rebuilt
          ],
        );
      },
      valueListenable: notifier!,
      child: cplxWidget, // Imp: Will not be rebuilt, independent of value
    );
  }
}
