import 'package:flutter/material.dart';

class Point {
  late int x;
  late int y;

  Point(int x, int y) {
    this.x = x;
    this.y = y;
  }
}

class PointNotifier extends ValueNotifier<Point> {
  PointNotifier(Point value) : super(value);

  void incrementx() {
    value.x += 1;
    notifyListeners();
  }

  void incrementy() {
    value.y += 1;
    notifyListeners();
  }
}

void main() => runApp(MyApp());

class ComplexWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    print("Complex Widget Being Built ============ XXXX ");
    return Text('Good job3!');
  }
}

class MyApp extends StatelessWidget {
  final PointNotifier _point = PointNotifier(Point(3, 5));
  final Widget cplxConstWidget = ComplexWidget();
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: true,
      home: Scaffold(
        appBar: AppBar(title: Text("STATELESS UPDATING VALUE")),
        body: ListenerWidget(notifier: _point, cplxWidget: cplxConstWidget),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.plus_one),
          onPressed: () {
            print("Button pressed");
            _point.incrementx();
            _point.incrementy();
          },
        ),
      ),
    );
  }
}

class ListenerWidget extends StatelessWidget {
  final PointNotifier? notifier;
  final Widget? cplxWidget;

  const ListenerWidget({Key? key, this.notifier, this.cplxWidget});

  @override
  Widget build(BuildContext context) {
    print("");
    return ValueListenableBuilder<Point>(
      builder: (BuildContext context, Point value, Widget? complexWgt) {
        // This builder will only get called when the valNotifier is updated.
        return Row(
          children: [
            Text('${value.x} ${value.y}  ------ '),
            complexWgt!, //This complex will not be rebuilt
          ],
        );
      },
      valueListenable: notifier!,
      child: cplxWidget, // Imp: Will not be rebuilt, independent of value
    );
  }
}
