import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';

import '../../../Tutorials/DART/T004_Network/lib/models/general.dart';
import '../../../Tutorials/DART/T004_Network/lib/network/api_client.dart';

typedef ModelFromJsonParse = dynamic Function(Map<String, dynamic> resp);

class GenericDataTableViewModel<T, T1> extends BaseViewModel {
  GenericDataTableViewModel(
      this.items, this.fromJson, this.request, this.listType) {
    // if(items.isEmpty)
    //   {
    //     setBusy(true);
    //   }
    onRefresh();
  }

  onRefresh() {}

  late final List<T> items;
  // late final List<dynamic> items;
  Function(Map<String, dynamic>) fromJson;
  Map<String, dynamic> request;
  Object? listType;
  int offset = 30;
  int totalRecord = 150;
  // final T modelType;
  TextEditingController seachController = TextEditingController();

  void removeItem(int index) {
    items.removeAt(index);
    notifyListeners();
  }

  void createSnackBar(BuildContext context, String message) {
    final snackBar =
        new SnackBar(content: new Text(message), backgroundColor: Colors.red);
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  Future<ApiSingleResponse<T1>> getDataFromApi<T1>() async {
    // setBusy(true);
    // await Future.delayed(Duration(seconds: 2));

    // items.addAll(List<dynamic>.generate(offset, (i) => 'Item ${i + 1}').cast<String>());

    return ApiClient.instance.callObjectApi(
      req: request,
      endPoint: 'api/doctor_app/Contacts',
      fromJson: fromJson,
    );
    // setBusy(false);
  }

  void checkForLazyLoading(int index) async {
    print(
        'Index $index++++++++ Offset $offset-------${offset - 1}//////////${items.length}');
    if (index == (offset - 1)) {
      // await Future.delayed(Duration(seconds: 1));
      // setBusy(true);
      await Future.delayed(Duration(seconds: 2));
      // items.addAll(
      //     List<dynamic>.generate(30, (i) => 'Item ${offset + i + 1}').cast<String>());
      offset += 30;
      // setBusy(false);
      notifyListeners();
    }
  }
}
