import 'package:flutter/material.dart';
import '../../Tutorials/DART/T004_Network/lib/network/api_client.dart';
import 'generic_table/generic_table_vu.dart';
import 'doctor_model.dart';

import 'package:universal/Tutorials/DART/T004_Network/lib/network/system_api_service.dart';

void main() async {
  // ApiClient.create('charms-qa');
  await loginFunc('charms-qa', "kashifd", "Kashif123@");
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // final List<TestModel> items = [];
  final List<Doctor> items = [];
  Widget cellDesign(int index) {
    print('I am in main $index');
    return ListTile(
      title: Text(items[index].full_name!),
    );
  }

  Map<String, dynamic> request = {
    "app_type": "Patient",
    "episode_id": 228,
    "columns": [
      "user_id",
      "employee_no",
      "image",
      "full_name",
      "user.primary_group.group_name",
      "gender",
      "mobile_phone",
      "date_added",
      "date_updated"
    ],
    "limit": 10,
    "offset": 0,
    "app_version": "3.2.10-000",
    "os_name": "Android"
  };
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'CHI DataTable',
      theme: ThemeData.light(),
      debugShowCheckedModeBanner: false,
      home: GenericDataTable<Doctor, DoctorList>(
        title: "Flutter Art",
        search: true,
        dataList: items,
        cellDesign: cellDesign,
        swipeToDelete: true,
        fromJson: DoctorList.fromMap,
        request: request,
        listType: DoctorList,
      ),
    );
  }
}
