import 'generic_table/generic_table_vm.dart';
import 'doctor_model.dart';

class DoctorTableVM extends GenericDataTableViewModel {
  DoctorTableVM(List items, Function(Map<String, dynamic> p1) fromJson,
      Map<String, dynamic> request, Object? listType)
      : super(items, fromJson, request, listType);

  @override
  void onRefresh() async {
    setBusy(true);
    offset = 30;
    items.clear();
    var resp = await getDataFromApi<DoctorList>();
    items.addAll((resp.data!.data!).cast<Doctor>());
    print(resp);
    setBusy(false);
  }
}
