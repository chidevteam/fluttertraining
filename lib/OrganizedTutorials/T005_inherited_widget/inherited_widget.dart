import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext dcontext) {
    Scaffold createHome = Scaffold(
      appBar: createAppBar(),
      body: createBody(),
    );

    return MaterialApp(home: createHome);
  }
}

createAppBar() {
  return AppBar(
    title: const Text('Welcome to Flutter'),
    backgroundColor: Colors.orange,
  );
}

createBody() {
  ButtonStyle style =
      ElevatedButton.styleFrom(textStyle: const TextStyle(fontSize: 20));
  ElevatedButton button = ElevatedButton(
    style: style,
    onPressed: null,
    child: const Text('Disabled'),
  );
  return Column(children: [Text('Hello World 334'), createFrog(), button]);
}

createFrog() {
  return FrogColor(
    color: Colors.green,
    child: Builder(
      builder: (BuildContext innerContext) {
        return Text(
          'Hello Frog 3',
          style: TextStyle(color: FrogColor.of(innerContext).color),
        );
      },
    ),
  );
}

class FrogColor extends InheritedWidget {
  const FrogColor({
    Key? key,
    required this.color,
    required Widget child,
  }) : super(key: key, child: child);

  final Color color;

  static FrogColor of(BuildContext context) {
    final FrogColor? result =
        context.dependOnInheritedWidgetOfExactType<FrogColor>();
    assert(result != null, 'No FrogColor found in context');
    return result!;
  }

  @override
  bool updateShouldNotify(FrogColor old) => color != old.color;
}
