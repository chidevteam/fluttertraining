import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext dcontext) {
    Scaffold createHome = Scaffold(
      appBar: AppBar(
        title: Text('App Bar'),
      ),
      body: Center(child: Text('Hello World')),
    );

    return MaterialApp(home: createHome);
  }
}
