// import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';

import '../../../Tutorials/DART/T004_Network/lib/models/general.dart';
import '../../../Tutorials/DART/T004_Network/lib/network/api_client.dart';
// import '../doctor_model.dart';

typedef ModelFromJsonParse = dynamic Function(Map<String, dynamic> resp);

class GenericDataTableViewModel<T, T1> extends BaseViewModel {
  GenericDataTableViewModel(
      this.items, this.fromJson, this.request, this.endPoint, this.apiResponseType) {
    onRefresh();
  }

  late final List<T> items;
  Function(Map<String, dynamic>) fromJson;
  Map<String, dynamic> request;
  String endPoint;
  var apiResponseType;
  int offset = 30;
  int totalRecord = 150;
  TextEditingController seachController = TextEditingController();

  void removeItem(int index) {
    items.removeAt(index);
    notifyListeners();
  }

  void createSnackBar(BuildContext context, String message) {
    final snackBar =
        new SnackBar(content: new Text(message), backgroundColor: Colors.red);
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  void onRefresh() async {
    setBusy(true);
    offset = 30;
    items.clear();
    var resp = await getDataFromApi();
    apiResponseType = resp.data;
    print('${apiResponseType.runtimeType}');
    print('${apiResponseType.data}');
    items.addAll((apiResponseType.data));
    print(resp);
    setBusy(false);
  }

  Future<ApiSingleResponse<T1>> getDataFromApi() async {
    return ApiClient.instance.callObjectApi(
      req: request,
      endPoint: endPoint,
      fromJson: fromJson,
    );
  }

  void checkForLazyLoading(int index) async {
    print(
        'Index $index++++++++ Offset $offset-------${offset - 1}//////////${items.length}');
    if (index == (offset - 1)) {
      await Future.delayed(Duration(seconds: 2));
      offset += 30;
      notifyListeners();
    }
  }
}
