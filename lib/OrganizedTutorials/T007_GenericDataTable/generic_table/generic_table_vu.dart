import 'dart:async';

import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'package:universal/OrganizedTutorials/T007_GenericDataTable/generic_table/generic_table_vm.dart';

import '../doctor_model.dart';

class GenericDataTable<T, T1>
    extends ViewModelBuilderWidget<GenericDataTableViewModel> {
  GenericDataTable(
      {Key? key,
      required this.title,
      required this.search,
      required this.dataList,
      required this.cellDesign,
      required this.swipeToDelete,
      required this.fromJson,
      required this.request,
      required this.endPoint,
      this.apiResponseType})
      : super(key: key);

  String title;
  bool search;

  List<T> dataList;

  // List<dynamic> dataList;
  Function(int) cellDesign;
  Timer? t = null;
  bool swipeToDelete;
  Function(Map<String, dynamic>) fromJson;
  Map<String, dynamic> request;
  String endPoint;
  var apiResponseType;

  // final T model;

  @override
  Widget builder(BuildContext context, GenericDataTableViewModel viewModel,
      Widget? child) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
        centerTitle: true,
        elevation: 2.0,
      ),
      body: viewModel.isBusy
          ? Container(
              color: Colors.green,
            )
          : Column(
              children: [
                Visibility(
                  visible: search,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Card(
                      child: TextField(
                        controller: viewModel.seachController,
                        cursorColor: Colors.grey,
                        decoration: InputDecoration(
                            hintText: " Search...",
                            border: InputBorder.none,
                            suffixIcon: IconButton(
                              icon: Icon(Icons.search),
                              color: Colors.black,
                              onPressed: () {
                                print(
                                    'Search Button Pressed ${viewModel.seachController.text}');
                              },
                            )),
                        onChanged: (value) {
                          if (t != null) {
                            t!.cancel();
                            print('Canceling timer ${value}');
                          }
                          t = Timer(Duration(seconds: 2), () {
                            print('Currently Written ${value}');
                          });
                        },
                        style: TextStyle(color: Colors.black, fontSize: 15.0),
                      ),
                    ),
                  ),
                ),
                Expanded(
                  child: RefreshIndicator(
                    onRefresh: () async {
                      viewModel.onRefresh();
                    },
                    child: ListView.builder(
                      itemCount: viewModel.items.length,
                      itemBuilder: (context, index) {
                        viewModel.checkForLazyLoading(index);
                        return ((index + 1 == viewModel.items.length) &&
                                (index + 1 != viewModel.totalRecord))
                            ? Center(
                                child: CircularProgressIndicator(),
                              )
                            : swipeToDelete
                                ? Dismissible(
                                    key: UniqueKey(),
                                    background: Container(color: Colors.red),
                                    secondaryBackground: Container(
                                      color: Colors.green,
                                    ),
                                    onDismissed: (DismissDirection direction) {
                                      if (direction ==
                                          DismissDirection.startToEnd) {
                                        print("Add to favorite");
                                      } else {
                                        print('Remove item');
                                      }
                                      viewModel.removeItem(index);
                                    },
                                    confirmDismiss:
                                        (DismissDirection direction) async {
                                      if (direction ==
                                          DismissDirection.startToEnd) {
                                        return await showDialog(
                                          context: context,
                                          builder: (BuildContext context) {
                                            return AlertDialog(
                                              title: const Text(
                                                  "Delete Confirmation"),
                                              content: const Text(
                                                  "Are you sure you want to delete this item?"),
                                              actions: <Widget>[
                                                TextButton(
                                                    onPressed: () =>
                                                        Navigator.of(context)
                                                            .pop(true),
                                                    child:
                                                        const Text("Delete")),
                                                TextButton(
                                                  onPressed: () =>
                                                      Navigator.of(context)
                                                          .pop(false),
                                                  child: const Text("Cancel"),
                                                ),
                                              ],
                                            );
                                          },
                                        );
                                      } else {
                                        viewModel.createSnackBar(
                                            context, 'Added to favorite');
                                      }
                                    },
                                    child: cellDesign(index)
                                    // ListTile(
                                    //   title: Text(viewModel.items[index]),
                                    // ),
                                    )
                                : cellDesign(index);
                      },
                    ),
                  ),
                ),
              ],
            ),
    );
  }

  @override
  GenericDataTableViewModel viewModelBuilder(BuildContext context) {
    return GenericDataTableViewModel<T, T1>(
        dataList, fromJson, request, endPoint, apiResponseType);
  }
}
