import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';

import 'generic_table/generic_table_vu.dart';
import 'home_vm.dart';
import 'doctor_model.dart';

// ignore: must_be_immutable
class HomeVU extends ViewModelBuilderWidget<HomeVM> {
  late HomeVM vm;

  Widget cellDesign(int index) {
    print('I am in main $index');
    return type1CellDesign(textHeading: vm.items[index].full_name!);
  }

  @override
  Widget builder(BuildContext context, HomeVM viewModel, Widget? child) {
    return MaterialApp(
      title: 'CHI DataTable',
      theme: ThemeData.light(),
      debugShowCheckedModeBanner: false,
      home: GenericDataTable<Doctor, DoctorList>(
        title: "Flutter Art",
        search: true,
        dataList: viewModel.items,
        cellDesign: cellDesign,
        swipeToDelete: true,
        fromJson: DoctorList.fromMap,
        request: viewModel.request,
        endPoint: 'api/doctor_app/Contacts',
        apiResponseType: viewModel.mDoctorList,
      ),
    );
  }

  @override
  HomeVM viewModelBuilder(BuildContext context) {
    vm = HomeVM();
    return vm;
  }
}

Widget type1CellDesign(
    {required String textHeading,
    String? urlImage,
    String? textHeading2,
    String? rightLabel,
    String? rightLabel2}) {
  return ListTile(
    title: Text(textHeading),
    subtitle: null,
    trailing: null,
  );
}
