
10:30am: Going to fix the labels x-axis issue.



=======================================


- Move textPainter to parent -- Done
- Finish both landscape and portrait mode with actual data -- Done
- handle value color -- Done
- Make 6500 screen
- handle dial meter value position
- handle reconnection of socket
- why draw is being called several times?(more than 8)
- make configurable screen
- Control the speed of data being drawn
- Handle UltraCritical cases for meter
- Draw Circle as marker if needed
- Self adjusting graph





- generate random number and draw them instead of circles -- Done
- replace animationController with timer -- Done
- draw multiple circles in visualizer example -- Done







- PC-102
- PC-202
- PC-303
- PC-60FW, Yonker
- CheckMePro
- HMS6500
- HeartBook
- Stethoscope