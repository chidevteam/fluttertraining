/*
Made three canvas classes for labels, grids, plotting data
  - First two are stateless are in RepaintBoundary because we only need them to
    rebuild when necessary
  - Third one(MakeGraph) is stateful because we needed orientation change
    callback

Computing EcgGrid params two times because in stateful widget state remains
the same so using build method for computing EcgGrid params

Used LayoutBuilder for getting height and width of it parent
 */

import 'package:flutter/material.dart';
import 'package:universal/Tutorials/T007_ecg/lib/ecg.dart';
import 'ecgGrid.dart';
import 'labels_custom_paint.dart';
import 'graph_custom_paint.dart';
import 'grids_custom_paint.dart';

// ignore: must_be_immutable
class MakeLiveGraph extends StatelessWidget {
  List<num>? dataBuffer;
  String label;
  bool showGrids;
  double sampleRate;
  double yMin;
  double yMax;
  double yGridSmallSpacing;
  double yGridLargeSpacing;
  double xGridSmallSpacing;
  double xGridLargeSpacing;
  double pixelAspecratio;
  Color? graphLineColor;
  Color? labelColor;
  Color? delayColor;
  Color? gridColor;

  MakeLiveGraph({
    Key? key,
    this.dataBuffer,
    required this.label,
    required this.showGrids,
    required this.sampleRate,
    required this.yMin,
    required this.yMax,
    required this.yGridSmallSpacing,
    required this.yGridLargeSpacing,
    required this.xGridSmallSpacing,
    required this.xGridLargeSpacing,
    required this.pixelAspecratio,
    this.graphLineColor = Colors.green,
    this.labelColor = Colors.green,
    this.delayColor = Colors.white,
    this.gridColor = Colors.grey,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    LayoutBuilder layout = LayoutBuilder(builder: (context, constraints) {
      // print('Max Height : ${constraints.maxHeight}');
      // print('Max Width : ${constraints.maxWidth}');

      Rect marginsRect = Rect.fromLTRB(20, 20, 10, 20);

      ECGgrid ecgGrid = ECGgrid(
          marginRect: marginsRect,
          yMin: yMin,
          yMax: yMax,
          canvWidth: constraints.maxWidth,
          canvHeight: constraints.maxHeight,
          yGridSmallSpacing: yGridSmallSpacing,
          yGridLargeSpacing: yGridLargeSpacing,
          xGridSmallSpacing: xGridSmallSpacing,
          xGridLargeSpacing: xGridLargeSpacing,
          pixelAspecratio: pixelAspecratio);

      ecgGrid = computeGridParams(ecgGrid);

      return Stack(children: [
        RepaintBoundary(
            child: MakeGrids(
          marginRect: marginsRect,
          showGrids: showGrids,
          ecgGrid: ecgGrid,
          gridColor: gridColor,
        )),
        RepaintBoundary(
          child: MakeGraph(
            marginRect: marginsRect,
            ecgGrid: ecgGrid,
            dataBuffer: dataBuffer,
            sampleRate: sampleRate,
            yMin: yMin,
            yMax: yMax,
            yGridSmallSpacing: yGridSmallSpacing,
            yGridLargeSpacing: yGridLargeSpacing,
            xGridSmallSpacing: xGridSmallSpacing,
            xGridLargeSpacing: xGridLargeSpacing,
            pixelAspecratio: pixelAspecratio,
            graphLineColor: graphLineColor,
            delayColor: delayColor,
          ),
        ),
        RepaintBoundary(
          child: MakeLabelsAndTitle(
            marginRect: marginsRect,
            label: label,
            labelColor: labelColor,
          ),
        ),
      ]);
    });

    return layout;
  }
}

// ignore: must_be_immutable
class MakeLabelsAndTitle extends StatelessWidget {
  String label;
  Color? labelColor;
  Rect marginRect;

  MakeLabelsAndTitle(
      {required this.marginRect,
      Key? key,
      required this.label,
      required this.labelColor})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.zero,
      child: Container(
          child: CustomPaint(
            painter: LabelsCustomPaint(label, labelColor),
          ),
          color: Color.fromARGB(0, 255, 0, 0),
          width: double.infinity,
          height: double.infinity),
    );
  }
}

// ignore: must_be_immutable
class MakeGrids extends StatelessWidget {
  bool showGrids;
  ECGgrid ecgGrid;
  Color? gridColor;
  Rect marginRect;

  MakeGrids(
      {required this.marginRect,
      Key? key,
      required this.showGrids,
      required this.ecgGrid,
      required this.gridColor})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.zero,
      child: Container(
          child: CustomPaint(
            painter:
                GridsCustomPaint(marginRect, showGrids, ecgGrid, gridColor),
          ),
          color: Color.fromARGB(0, 0, 255, 0),
          width: double.infinity,
          height: double.infinity),
    );
  }
}

// ignore: must_be_immutable
class MakeGraph extends StatefulWidget {
  ValueNotifier<int> notifier = ValueNotifier(0);
  List<num>? dataBuffer;
  double sampleRate;
  Rect marginRect;
  ECGgrid ecgGrid;
  double yMin;
  double yMax;
  double yGridSmallSpacing;
  double yGridLargeSpacing;
  double xGridSmallSpacing;
  double xGridLargeSpacing;
  double pixelAspecratio;
  Color? graphLineColor;
  Color? delayColor;

  MakeGraph({
    required this.marginRect,
    required this.ecgGrid,
    Key? key,
    this.dataBuffer,
    required this.sampleRate,
    required this.yMin,
    required this.yMax,
    required this.yGridSmallSpacing,
    required this.yGridLargeSpacing,
    required this.xGridSmallSpacing,
    required this.xGridLargeSpacing,
    required this.pixelAspecratio,
    required this.graphLineColor,
    required this.delayColor,
  }) : super(key: key);

  @override
  MakeGraphState createState() => new MakeGraphState(
      marginRect,
      ecgGrid,
      notifier,
      dataBuffer,
      sampleRate,
      yMin,
      yMax,
      yGridSmallSpacing,
      yGridLargeSpacing,
      xGridSmallSpacing,
      xGridLargeSpacing,
      pixelAspecratio,
      graphLineColor,
      delayColor);
}

class MakeGraphState extends State
    with WidgetsBindingObserver, SingleTickerProviderStateMixin {
  ValueNotifier<int> notifier;
  List<num>? dataBuffer;
  double sampleRate;
  late GraphCustomPaint graphPainter;
  Rect marginRect;
  ECGgrid ecgGrid;
  double yMin;
  double yMax;
  double yGridSmallSpacing;
  double yGridLargeSpacing;
  double xGridSmallSpacing;
  double xGridLargeSpacing;
  double pixelAspecratio;
  bool isAnimCont = false;
  Color? graphLineColor;
  Color? delayColor;

  late AnimationController _controller;

  MakeGraphState(
      this.marginRect,
      this.ecgGrid,
      this.notifier,
      this.dataBuffer,
      this.sampleRate,
      this.yMin,
      this.yMax,
      this.yGridSmallSpacing,
      this.yGridLargeSpacing,
      this.xGridSmallSpacing,
      this.xGridLargeSpacing,
      this.pixelAspecratio,
      this.graphLineColor,
      this.delayColor);

  @override
  void initState() {
    _controller =
        AnimationController(vsync: this, duration: Duration(days: 365));
    //_controller.addListener(() {setState(() {});}); no need to setState
    _controller.repeat();

    super.initState();
    WidgetsBinding.instance!.addObserver(this);
    print('MakeGraphState created');
  }

  @override
  void dispose() {
    print('MakeGraphState destroyed');
    _controller.dispose();

    WidgetsBinding.instance!.removeObserver(this);
    // dataBuffer!.clear();
    super.dispose();
  }

  @override
  void didChangeMetrics() {
    print('Orientation changed');
    if (isAnimCont) graphPainter.stopTimer();
  }

  @override
  Widget build(BuildContext context) {
    LayoutBuilder layout = LayoutBuilder(builder: (context, constraints) {
      // print('Max Height : ${constraints.maxHeight}');
      // print('Max Width : ${constraints.maxWidth}');

// this.dataBuffer, this.notifier, this.sampleRate, this.ecgGrid
      graphPainter = GraphCustomPaint(marginRect, dataBuffer!, notifier,
          sampleRate, ecgGrid, graphLineColor, delayColor);
      // graphPainter = GraphCustomPaint(
      //     dataBuf: dataBuffer!,
      //     notifier: notifier,
      //     sampleRate: sampleRate,
      //     ecgGrid: ecgGrid,);

      return Padding(
        padding: EdgeInsets.zero,
        child: Container(
            child: CustomPaint(
              painter: graphPainter,
            ),
            color: Color.fromARGB(0, 0, 0, 255),
            width: double.infinity,
            height: double.infinity),
      );
    });
    return layout;
  }
}
