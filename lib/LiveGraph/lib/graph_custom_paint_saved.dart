import 'dart:async';
import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'dart:math' as math;
import 'ecgGrid.dart';
import 'dart:ui';

class GraphCustomPaint extends CustomPainter {
  //Timer related vars
  List<num> dataBuffer;
  Timer? timer = null; // the display timer
  static const int timerRepeatTimeMSec = 66;
  late ValueNotifier<int>? notifier;

  double sampleRate; // 100
  late double samplesPerTimer; // 25
  double numberOfSec =
      2; // 6  TOxDO: THIS SHOULD BE DYNAMIC BASED ON NUMBER OF X BOXES
  late List<num?> chartDataToBeDrawn;
  int count = 0;
  int currentIndex = 0;
  late int sizeOfTrace;
  late double bufferInSeconds;
  ECGgrid ecgGrid;
  final bool isAnimCont;
  int lastTime = -1;
  int counter = 0;
  late List<Path> paths = [];
  double t = 0;
  List<Offset> offsetPts = [];

  // List<double> yy = [];
  List<double> pts = [];
  late Float32List float32list;

  final Animation listenable;
  int cc = 0;

  GraphCustomPaint(
      {required this.dataBuffer,
      required this.isAnimCont,
      this.notifier,
      required this.sampleRate,
      required this.ecgGrid,
      required this.listenable})
      // : super(repaint: isAnimCont ? listenable : notifier) {
      : super(repaint: notifier) {
    print(dataBuffer.length);
    print('Canvas created');

    for (int i = 0; i < 6; i++) paths.add(Path());

    numberOfSec =
        ecgGrid.numXBigBoxes * 0.2; //0.2 sec correspond to one box for ecg

    samplesPerTimer = sampleRate *
        (timerRepeatTimeMSec / 1000.0); // Converted time to seconds
    sizeOfTrace = (sampleRate * numberOfSec).toInt();
    chartDataToBeDrawn = List.filled(sizeOfTrace, -999);
    bufferInSeconds = 0;

    if (isAnimCont) {
      listenable.addListener(() {
        // _consumeData(-999);
        cc++;
        if (cc % 60 == 0) {
          notifier!.value += 1;
        }
      });
    } else {
      startTimer();

      SystemChannels.lifecycle.setMessageHandler((msg) async {
        debugPrint('SystemChannels> $msg');
        if (msg == AppLifecycleState.paused.toString() ||
            msg == AppLifecycleState.inactive.toString() ||
            msg == AppLifecycleState.detached.toString()) timer!.cancel();
        if (msg == AppLifecycleState.resumed.toString()) startTimer();
      });
    }

    initVars();
  }

  void initVars() {
    int k = 0;
    for (double t = 0; t < 1 * 8; t += 1.0 / 256) {
      k++;
      addPointsToPath(paths, t, counter);
    }
    print("$k ----------------------------");
    // float32list = Float32List.fromList(pts);
  }

//////////////////////////////////////////////////////////////////////////////////////////////////
  void addPointsToPath(List<Path> paths, double t, int c) {
    // if (counter > 1000) return;
    int k = 0;
    double f = 2.0;
    double x0 = 50;
    double y0 = 50;
    double r = 10 * math.exp(.05 * t);
    double x = r * math.sin(2 * math.pi * f * t);
    double y = r * math.cos(2 * math.pi * f * t);

    double xa = 200 + 0 * x0;
    double ya = 200 + 0 * y0;
    if (c == 1) if (k < 9) paths[k].moveTo(xa, ya);
    paths[k++].lineTo(xa + x, ya + y);
    pts.add(xa + x);
    pts.add(ya + y);
    offsetPts.add(Offset(xa + x, ya + y));
    // yy.add(ya + y);
  }

  _consumeData(dynamic t) {
    if (!isAnimCont) notifier!.value += 1;
    return;
  }

  @override
  void paint(Canvas canvas, Size size) {
    // print("hrew.....");
    // t += .016;

    counter++;
    if (counter % 15 == 0) {
      int currentTime = DateTime.now().millisecondsSinceEpoch;
      int diff = currentTime - lastTime;
      lastTime = currentTime;
      // print('Difference ++++++++++++++++++++ $diff');
    }
    computePointToPixelAndDrawData2(canvas, size);
    textPaint(canvas, size);
  }

  void computePointToPixelAndDrawData2(Canvas canvas, Size size) {
    Paint paint = Paint()
      ..color = Colors.black
      ..style = PaintingStyle.stroke
      ..strokeWidth = 1.0;

    float32list = Float32List.fromList(pts);

    // for (Path path in paths) {
    //   // canvas.drawPath(path, paint);
    // }
    // Rect rect = Rect.fromLTRB(0, 0, size.width, size.height);
    // canvas.clipRect(rect);

    // if (counter < pts.length) {
    //   pts[2 * counter] = double.infinity;
    //   pts[2 * counter + 1] = double.infinity;
    // }

    // print(count);
    canvas.drawRawPoints(PointMode.polygon, float32list, paint);

    // canvas.drawPoints(PointMode.polygon, offsetPts, paint);
    // print('Xindex++++++++++++++++++++++++');
    Offset s1 = Offset(2 * counter % 400, 0);
    Offset e1 = Offset(2 * counter % 400, size.height);
    canvas.drawLine(s1, e1, paint);

    // var vertices = Vertices(
    //     VertexMode.triangles, [Offset.zero, Offset(100, 0), Offset(100, 100)]);
    // canvas.translate(20, 50);
    // canvas.drawVertices(
    //     vertices, BlendMode.srcOver, Paint()..color = Colors.red);
    // canvas.translate(20, 50);
    // canvas.drawVertices(
    //     vertices, BlendMode.srcOver, Paint()..color = Colors.green);
    // canvas.translate(20, 50);
    // canvas.drawVertices(
    //     vertices, BlendMode.srcOver, Paint()..color = Colors.blue);
  }

/////////////////////////////////////////////////////////////////////////

  void textPaint(Canvas canvas, Size size) {
    final textStyle = TextStyle(
      color: Colors.black,
      fontSize: 10,
    );
    final textSpan = TextSpan(
      text: bufferInSeconds.toStringAsFixed(1) + '(Sec)',
      style: textStyle,
    );
    final textPainter = TextPainter(
      text: textSpan,
      textDirection: TextDirection.ltr,
    );
    textPainter.layout(
      minWidth: 0,
      maxWidth: size.width,
    );
    final offset = Offset(size.width - 40, 3);
    textPainter.paint(canvas, offset);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    // print('shouldRepaint');
    return true;
  }

  void stopTimer() {
    print('Stopping Timer');
    timer?.cancel();
    // dataBuffer.clear();
  }

  void startTimer() {
    print('Start Timer');
    if (timer != null) {
      // print('Stop Timer in Start timer');
      timer?.cancel();
    }

    timer = Timer.periodic(
        Duration(milliseconds: timerRepeatTimeMSec), _consumeData);
  }
}

// if(bufferInSeconds > 1)
//   samplesPerTimer = samplesPerTimer * 1.15;
// if(bufferInSeconds < 1)
//   samplesPerTimer = samplesPerTimer * 0.9;
// if(bufferInSeconds < 0.5)
//   samplesPerTimer = samplesPerTimer * 0.8;
