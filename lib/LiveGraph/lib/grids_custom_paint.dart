import 'package:flutter/material.dart';
import 'ecgGrid.dart';
import 'labels_custom_paint.dart';

class GridsCustomPaint extends CustomPainter {
  Rect marginRect;
  bool showGrids;
  ECGgrid ecgGrid;
  Color? gridColor;

  GridsCustomPaint(
      this.marginRect, this.showGrids, this.ecgGrid, this.gridColor)
      : super(repaint: ecgGrid.gridNotifier);

  @override
  void paint(Canvas canvas, Size size) {
    // T ODO: implement paint
    print('Labels created new2');

    if (showGrids) {
      drawGrids(canvas, size);
    }
  }

  void drawGrids(Canvas canvas, Size size) {
    Paint paint = Paint()
      ..strokeWidth = 1.0
      ..color = gridColor!.withOpacity(0.1);

    // print('ECG Grid ${ecgGrid.printDesc()}');

    drawyAxisGrids(ecgGrid, canvas, size, paint);
    drawxAxisGrids(ecgGrid, canvas, size, paint);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return false;
  }

  void drawyAxisGrids(ECGgrid ecgGrid, Canvas canvas, Size size, Paint paint) {
    for (double y = ecgGrid.yMinPx;
        y <= ecgGrid.yMaxPx;
        y += ecgGrid.yDeltaPx) {
      double yOrigin = y;
      final Offset yStart = Offset(marginRect.left, yOrigin);
      final Offset yEnd = Offset(size.width - marginRect.right, yOrigin);
      paint.strokeWidth = 2.0;
      canvas.drawLine(yStart, yEnd, paint);
    }

    for (double y = ecgGrid.yMinPxSmall;
        y <= ecgGrid.yMaxPxSmall;
        y += ecgGrid.yDeltaPxSmall) {
      double yOrigin = y;
      final Offset yStart = Offset(marginRect.left, yOrigin);
      final Offset yEnd = Offset(size.width - marginRect.right, yOrigin);
      paint.strokeWidth = 1.0;
      canvas.drawLine(yStart, yEnd, paint);
    }
  }

  void drawxAxisGrids(ECGgrid ecgGrid, Canvas canvas, Size size, Paint paint) {
    const double EPSILON = 1e-3; // TO FIX THE 11.000000002 ISSUE

    var i = 0;
    for (double x = marginRect.left;
        x <= size.width - marginRect.right + EPSILON;
        x += (ecgGrid.xDeltaPx * ecgGrid.pixelAspecratio)) {
      double xOrigin = x;
      final Offset xStart = Offset(xOrigin, marginRect.top);
      final Offset xEnd = Offset(xOrigin, size.height - marginRect.bottom);
      paint.strokeWidth = 2.0;
      i++;
      if (i % 5 == 0) {
        textPaint(
            canvas,
            size,
            (i.toDouble() / 5.0).toStringAsFixed(1),
            Colors.green,
            Offset(xOrigin - 10, size.height - marginRect.bottom));
        // print("+++++++++++++++++++++++++++++++vvv+");
      }
      canvas.drawLine(xStart, xEnd, paint);
    }

    for (double x = marginRect.left;
        x <= size.width - marginRect.right + EPSILON;
        x += (ecgGrid.xDeltaPxSmall * ecgGrid.pixelAspecratio)) {
      double xOrigin = x;
      final Offset xStart = Offset(xOrigin, 0 + marginRect.top);
      final Offset xEnd = Offset(xOrigin, size.height - marginRect.bottom);
      paint.strokeWidth = 1.0;
      canvas.drawLine(xStart, xEnd, paint);
    }
  }

  void textPaint(Canvas canvas, Size size, String label, Color labelColor,
      Offset textPos) {
    final textStyle = TextStyle(
      color: labelColor,
      fontSize: 15,
    );
    final textSpan = TextSpan(
      text: label,
      style: textStyle,
    );
    final textPainter = TextPainter(
      text: textSpan,
      textDirection: TextDirection.ltr,
    );
    textPainter.layout(
      minWidth: 0,
      maxWidth: size.width,
    );
    // final offset = Offset(0, 0);
    textPainter.paint(canvas, textPos);
    // print("$label ====");
  }
}
