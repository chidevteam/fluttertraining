import 'package:flutter/material.dart';

import '../../MetersAndDials/lib/dial_meter_holder.dart';
import '../../MetersAndDials/lib/linear_meter_holder.dart';


// ignore: must_be_immutable
class MeterWidget extends StatelessWidget {
  Color color;
  String label;
  List<double> ranges;
  num value;
  String unit;
  bool isMeterRound;

  MeterWidget(
      {required this.color,
      required this.label,
      required this.ranges,
      required this.value,
      required this.unit,
      required this.isMeterRound});

  @override
  Widget build(BuildContext context) {
    LayoutBuilder layout = LayoutBuilder(builder: (context, constraints) {
      // print('Max Height : ${constraints.maxHeight}');
      // print('Max Width : ${constraints.maxWidth}');

      return Container(
        color: color,
        height: constraints.maxHeight,
        width: constraints.maxWidth,
        child: isMeterRound
            ? Center(
                child: DialMeterHolder(
                  ranges: ranges,
                  bgColor: Color(0xFFdddddd),
                  label: label,
                  value: value,
                  unit: unit,
                ),
              )
            : Center(
                child: LinearMeterHolder(
                  ranges: ranges,
                  bgColor: Color(0xFFdddddd),
                  label: label,
                  value: value,
                  unit: unit,
                ),
              ),
      );
    });

    return layout;
  }
}

/*

Center(
          child: Text(
            value == 0 ? '--' : label + ' = $value ' + unit,
            style: TextStyle(color: Colors.white),
          ),
        ),
 */
