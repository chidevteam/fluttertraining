import 'package:flutter/material.dart';
// PROBLEM STATEMENT: ECG GRID COMPUTATIONS
// ========================================================================
// Given the y values of data, and dimensions of Canvas, and gridSpacing:
//      (yMin, yMax, canvWidth, canvHeight, gridSpacing),
// return the values of (yMinPx, yMaxPx, yDeltaPx) so that we can draw grids
// using the following loop:
//
//   for (int y = yMinPx; y <= yMaxPx; y += yDeltaPx)
//   (we will use the same to find the small grid, as well as x-axis)
//
// - We want integer number of big boxes in x-axis
//   y-axis can be made slightly smaller to fit in integer x-axis boxes
//   (i.e. we will CEIL the number of x-boxes)
//
// Grid spacing is .5mV (in general), we will use the X-values based on
// Y values in the application, this function(s) will just compute y stuff.
//
// Draw Y grid lines, 5 steps
// Step 1:  Get the mapping of mV to yPositionPixel
//           Mapping of edge points  .60 -> 0,  -1.25 -> 300
//                                    x1    y1    x2     y2
// Step 2: Compute  (yMinPx, yMaxPx, yDeltaPx)
// Step 3: Compute the number of x-boxes : canvWidth / yDeltaPx
// Step 4: Ceil to next number, and find scaling ratio
// Step 5: Scale the ymin, ymax by above ratio and call step 2 to find
//         updated values, this guarantees that we have integer
//         number of boxes in x-axis
//
//
//----------0 -----------0.60----------------------------------------------
//======    0     ====== 0.50 =============================================
// 1
//------         ------- 0.25 ------------------------------------------------
// 2
//=======         ======= 0.00 =============================================
//3
//------         ------- -0.25 ------------------------------------------------
//4
//=======         ======= -0.50 =============================================
//5
//------         ------- -0.75 ------------------------------------------------
//6
//=======         ======= -1.00 =============================================
// 7
//------   300   ------- -1.25 ------------------------------------------------

// Step 1:
//  m = (y2-y1)/(x2-x1)
//  y = y1 + m * (x - x1)

/*

Added pixel aspect ratio so that user can control how grids and data will be displayed
e.g --> 0.5 is big box size for y axis
        0.2 is big box size for x axis

        then their ratio is 2.5

        if user passes pixelAspectRatio = 2.5 with above box sizes then the
        grid will be square
        if pixelAspectRatio is less than 2.5 than box will have more height
        than width and vice versa if pixelAspectRatio is greater than 2.5
 */

class ECGgrid {
  Rect marginRect;
  double yMin;
  double yMax;
  double canvWidth;
  double canvHeight;
  double yGridSmallSpacing;
  double yGridLargeSpacing;
  double xGridSmallSpacing;
  double xGridLargeSpacing;
  double pixelAspecratio;

  double yMinPx = -1;
  double yMaxPx = -1;
  double yDeltaPx = -1;
  double yMinPxSmall = -1;
  double yMaxPxSmall = -1;
  double yDeltaPxSmall = -1;
  double xDeltaPx = -1;
  double xDeltaPxSmall = -1;
  double scaleRatio = -1; //for debugging
  int numXBigBoxes = -1;
  ValueNotifier<int> gridNotifier = ValueNotifier(0);

  ECGgrid(
      {required this.marginRect,
      required this.yMin,
      required this.yMax,
      required this.canvWidth,
      required this.canvHeight,
      required this.yGridSmallSpacing,
      required this.yGridLargeSpacing,
      required this.xGridSmallSpacing,
      required this.xGridLargeSpacing,
      required this.pixelAspecratio});

  printDesc() {
    print("yMin:            $yMin");
    print("yMax:            $yMax");
    print("canvWidth:       $canvWidth");
    print("canvHeight:      $canvHeight");

    print("yMinPx:          $yMinPx");
    print("yMaxPx:          $yMaxPx");
    print("yDeltaPx:        $yDeltaPx");

    print("yMinPxSmall:     $yMinPxSmall");
    print("yMaxPxSmall:     $yMaxPxSmall");
    print("yDeltaPxSmall:   $yDeltaPxSmall");

    print("xDeltaPx:        $xDeltaPx");
    print("xDeltaPxSmall:   $xDeltaPxSmall");

    print("scaleRatio:      $scaleRatio");

    print("pixelAspecratio: $pixelAspecratio");

    print("numXBigBoxes:    $numXBigBoxes");
  }
}

ECGgrid computeGridParams(ECGgrid ecgGrid) {
  ECGgrid retVal = computeGridVars(ecgGrid);

  ecgGrid.yMin *= ecgGrid.scaleRatio;
  ecgGrid.yMax *= ecgGrid.scaleRatio;

  retVal = computeGridVars(ecgGrid);

  return retVal;
}

ECGgrid computeGridVars(ECGgrid ecgGrid) {
  double x1 = ecgGrid.yMin; // -1.25;
  double y1 = ecgGrid.canvHeight - ecgGrid.marginRect.bottom; //  300;
  double x2 = ecgGrid.yMax; //  0.60
  double y2 = ecgGrid.marginRect.top; //yMinCanv.toDouble(); //  0
  double gridSpacing = ecgGrid.yGridLargeSpacing; //0.5;
  const double EPSILON1 = 1e-3; // TO FIX THE 11.000000002 ISSUE

  double m = (y2 - y1) / (x2 - x1); // (y-y1)= m * (x-x1);

  double yGridMax = snapFloor(x2, gridSpacing); //0.60 snap to 0.5
  double yGridMin = snapCeil(x1, gridSpacing); //-1.25 snap to -1

  // int k = 1;
  // print("__________________BIG GRID POSTIONS___________________");
  // for (double y = yGridMin; y <= yGridMax; y += gridSpacing) {
  //   double yPx = y1 + m * (y - x1);
  //   print('${k++} == $y -> $yPx');
  // }

  ecgGrid.yMinPx = y1 + m * (yGridMax - x1);
  ecgGrid.yMaxPx = y1 + m * (yGridMin - x1) + EPSILON1;
  ecgGrid.yDeltaPx = -m * gridSpacing;
  ecgGrid.xDeltaPx = -m * ecgGrid.xGridLargeSpacing;
  // print('1... ${ecgGrid.yMinPx}, ${ecgGrid.yMaxPx}, ${ecgGrid.yDeltaPx}');

  // double numXboxesD = (ecgGrid.canvWidth / (ecgGrid.yDeltaPx));
  double numXboxesD =
      (ecgGrid.canvWidth / (ecgGrid.xDeltaPx * ecgGrid.pixelAspecratio));
  int numXboxes = numXboxesD.ceil();
  ecgGrid.numXBigBoxes = numXboxes;

  const double EPSILON = 1e-9; // TO FIX THE 11.000000002 ISSUE
  if (ecgGrid.scaleRatio < 0)
    ecgGrid.scaleRatio = (numXboxes / numXboxesD) - EPSILON;
  // print('2... $numXboxesD $numXboxes $scaleRatio');

  double yGridMaxSmall = snapFloor(x2, gridSpacing / 5.0); //0.60 snap to 0.5
  double yGridMinSmall = snapCeil(x1, gridSpacing / 5.0); //-1.25 snap to -1

  // print("__________________SMALL GRID POSTIONS___________________");
  // for (double y = yGridMinSmall; y <= yGridMaxSmall; y += gridSpacing / 5.0) {
  //   double yPx = y1 + m * (y - x1);
  //   print('${k++} == $y -> $yPx');
  // }

  ecgGrid.yMinPxSmall = y1 + m * (yGridMaxSmall - x1);
  ecgGrid.yMaxPxSmall = y1 + m * (yGridMinSmall - x1) + EPSILON1;
  ecgGrid.yDeltaPxSmall = -m * gridSpacing / 5.0;
  ecgGrid.xDeltaPxSmall = -m * ecgGrid.xGridLargeSpacing / 5.0;
  // print('3... ${ecgGrid.yMinPxSmall}, ${ecgGrid.yMaxPxSmall}, ${ecgGrid.yDeltaPxSmall}');

  // return [ecgGrid.scaleRatio, yMinPx, yMaxPx, yDeltaPx];
  return ecgGrid;
}

double snapFloor(double inVal, double delta) {
  double outVal = (inVal / delta).floor() * delta;
  return outVal;
}

double snapCeil(double inVal, double delta) {
  double outVal = (inVal / delta).ceil() * delta;
  return outVal;
}
