import 'dart:async';
import 'dart:typed_data';
import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'dart:math' as math;

import 'ecgGrid.dart';

class GraphCustomPaint extends CustomPainter {
  // External "Large" buffer from which we copy points into traceBuf
  List<num> dataBuf;
  double sampleRate; // 100, required to compute size of trace
  ECGgrid ecgGrid; // To compute x,y scales for drawing on canvas
  final bool isAnimCont;
  late ValueNotifier<int>? notifier;
  final Animation listenable;

  // late List<double> traceBuf; //This is the trace buffer
  int traceIdx = 0; // current Index into the trace buffer
  late Float32List float32list;
  late int sizeOfTrace;

  //Timer related vars
  int desiredFPS = 60;
  Timer? timer = null; // the display timer
  late int timerRepeatTimeMSec;
  late double samplesPerTimer; // Numpts to be copied from dataBuf into traceBuf

  // For computing the actual PaintFrameRate (for debugging). We see how
  // much time we are taking to render the desiredFPS
  int lastTime = -1;
  int paintFrameCounter = 0;

  //Value to scale x, y to Graph
  double canvasWidth = -1;
  late double xScale, m;

  GraphCustomPaint(
      {required this.dataBuf,
      required this.sampleRate,
      required this.ecgGrid,
      required this.isAnimCont,
      this.notifier,
      required this.listenable})
      : super(repaint: isAnimCont ? listenable : notifier) {
    print(dataBuf.length);
    print('Canvas created');
  }

  void initVars() {
    late double numberOfSec;

    timerRepeatTimeMSec = (1000 ~/ desiredFPS);

    //0.2 sec correspond to one box for ecg
    numberOfSec = ecgGrid.numXBigBoxes * 0.2;

    // Converted time to seconds
    samplesPerTimer = sampleRate * (timerRepeatTimeMSec / 1000.0);
    sizeOfTrace = (sampleRate * numberOfSec).toInt();
    sizeOfTrace = sizeOfTrace * 2; // x and y points both are stored.
    // traceBuf = List.filled(sizeOfTrace, double.infinity);

    // Compute the variables for Scale conversion
    this.xScale = (1.0 / (sampleRate * numberOfSec)) * canvasWidth;
    this.m = (ecgGrid.yMinPx - ecgGrid.yMaxPx) / (ecgGrid.yMax - ecgGrid.yMin);

    int k = 0;
    List<double> traceBuf = List.filled(sizeOfTrace, double.infinity);

    for (int i = 0; i < traceBuf.length; i += 2) {
      k++;
      // traceBuf[i] = (i / 2) * .035;
      traceBuf[i] = (i / 2) * this.xScale;
      traceBuf[i + 1] = 100 + 100 * math.sin(2 * math.pi * i * .0001);
    }
    print("$k ----------------------------");

    float32list = Float32List.fromList(traceBuf);
    sizeOfTrace = traceBuf.length;

    if (isAnimCont) {
      listenable.addListener(() {
        _consumeData(-999);
      });
    } else {
      startTimer();
    }
  }

  _consumeData(dynamic t) {
    if (canvasWidth == -1) return;

    // if (dataBuf.length == 0) {
    //   print("============= main buffer empty");
    //   return;
    // }

    notifier!.value += 1;
  }

  copyDataToTraceBuffer() {
    // print("count");
    // print('traceIdx++++++++++++++++++888++$traceIdx');
    // int sizeOfTrace = traceBuf.length;

    // for (int i = 0; i < 400; i += 2)
    //   float32list[(paintFrameCounter * 400 + i + 1) % (sizeOfTrace)] += 5;

    if (traceIdx > 2048) return;

    for (int i = 0; i < samplesPerTimer; i++) {
      if (dataBuf.length == 0) break;
      int currIdx1 = (traceIdx + 1) % 2048;
      float32list[currIdx1] =
          ecgGrid.yMaxPx + this.m * (dataBuf[0] - ecgGrid.yMin);

      // dataBuf.removeAt(0);
      traceIdx += 2;
    }

    // int gapLength = (sampleRate * 0.1).toInt();
    // traceIdx += 2;
    // int gapIndex0 = (traceIdx + gapIndex) % sizeOfTrace;
    // int gapIndex1 = (traceIdx + 1 + i) % sizeOfTrace;
    // traceBuf[gapIndex0] = double.infinity;

    // traceIdx += 2;

    // print("$traceIdx");
    // float32list[(traceIdx + 1) % sizeOfTrace] = 10;
    // traceIdx += 2;
    // float32list[traceIdx + 1] = double.infinity;

    // print(gapIndex1);
    // }
  }

  @override
  void paint(Canvas canvas, Size size) {
    if (canvasWidth == -1) {
      canvasWidth = size.width;
      initVars();
    }

    copyDataToTraceBuffer();
    var paint = Paint()
      ..color = Colors.black
      ..strokeWidth = 1
      ..strokeCap = StrokeCap.round;

    int currentTime = DateTime.now().millisecondsSinceEpoch;

    int diff = currentTime - lastTime;
    if (paintFrameCounter % desiredFPS == 0) {
      lastTime = currentTime;
      print('Difference ++++++++++++++++++++ $diff');
    }

    // float32list[idx + 2] = double.infinity;
    // float32list[idx + 4] = double.infinity;
    paintFrameCounter += 1;
    canvas.drawRawPoints(PointMode.polygon, float32list, paint);

    // double bufferInSec = dataBuf.length / sampleRate;
    // textPaint(canvas, size, 5, 25, bufferInSec);
  }

  void startTimer() {
    print('Start Timer');
    if (timer != null) {
      // print('Stop Timer in Start timer');
      timer?.cancel();
    }

    SystemChannels.lifecycle.setMessageHandler((msg) async {
      debugPrint('SystemChannels> $msg');
      if (msg == AppLifecycleState.paused.toString() ||
          msg == AppLifecycleState.inactive.toString() ||
          msg == AppLifecycleState.detached.toString()) timer!.cancel();
      if (msg == AppLifecycleState.resumed.toString()) startTimer();
    });

    timer = Timer.periodic(
        Duration(milliseconds: timerRepeatTimeMSec), _consumeData);
  }

  void stopTimer() {
    print('Stopping Timer');
    timer?.cancel();
    // dataBuf.clear();
  }

  void textPaint(Canvas canvas, Size size, double x, double y, num value) {
    final textStyle = TextStyle(
      color: Colors.black,
      fontSize: 10,
    );
    final textSpan = TextSpan(
      text: value.toStringAsFixed(1) + '(Sec)',
      style: textStyle,
    );
    final textPainter = TextPainter(
      text: textSpan,
      textDirection: TextDirection.ltr,
    );
    textPainter.layout(
      minWidth: 0,
      maxWidth: size.width,
    );
    final offset = Offset(x, y);
    textPainter.paint(canvas, offset);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    // print('shouldRepaint');
    return false;
  }
}

// if(bufferInSeconds > 1)
//   samplesPerTimer = samplesPerTimer * 1.15;
// if(bufferInSeconds < 1)
//   samplesPerTimer = samplesPerTimer * 0.9;
// if(bufferInSeconds < 0.5)
//   samplesPerTimer = samplesPerTimer * 0.8;

//   void computePointToPixelAndDrawData(Canvas canvas, Size size) {
//   var paint = Paint()
//     ..color = Colors.black
//     ..strokeWidth = 1
//     ..strokeCap = StrokeCap.round;

//   double canvWidth = size.width;

//   double xScale = (1.0 / (sampleRate * numberOfSec)) *
//       canvWidth; //TOxDO: BUFFER SIZE IN SEC

//   double ymax = ecgGrid.yMax; //1;
//   double ymin = ecgGrid.yMin; //-1;

//   double m = (ecgGrid.yMinPx - ecgGrid.yMaxPx) / (ymax - ymin);

//   double xindex = 0;
//   for (int i = 0; i < traceBuf.length - 1; i++) {
//     double x1 = i.toDouble();
//     double y1 = traceBuf[i]!.toDouble();
//     double x2 = (i + 1).toDouble();
//     double y2 = traceBuf[i + 1]!.toDouble();
//     double X1 = x1 * xScale;

//     if (y1 == -999 || y2 == -999) {
//       xindex = X1;
//       continue;
//     }

//     double Y1 = ecgGrid.yMaxPx + m * (y1 - ymin);
//     double X2 = x2 * xScale;
//     double Y2 = ecgGrid.yMaxPx + m * (y2 - ymin);

//     Offset startingPoint = Offset(X1, Y1);
//     Offset endingPoint = Offset(X2, Y2);
//     canvas.drawLine(startingPoint, endingPoint, paint);
//   }

//   Offset s1 = Offset(xindex, 0);
//   Offset e1 = Offset(xindex, size.height);
//   canvas.drawLine(s1, e1, paint);
// }
