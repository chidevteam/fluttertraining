// import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

// import 'ecgGrid.dart';

class CustomPaintBase extends CustomPainter {
  late ValueNotifier<int>? notifier;

  //Value to scale x, y to Graph
  double canvasWidth = -1;

  CustomPaintBase(this.notifier) : super(repaint: notifier) {
    print('Canvas created');

    SystemChannels.lifecycle.setMessageHandler((msg) async {
      debugPrint('SystemChannels> $msg');
      if (msg == AppLifecycleState.paused.toString() ||
          msg == AppLifecycleState.inactive.toString() ||
          msg == AppLifecycleState.detached.toString()) willDisappear();
      if (msg == AppLifecycleState.resumed.toString()) willAppear();
    });
  }

  @override
  void paint(Canvas canvas, Size size) {
    // print("Base paint called");
    if (canvasWidth == -1) {
      canvasWidth = size.width;
      initVars();
    }
  }

  void initVars() {}

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    // print('shouldRepaint');
    return false;
  }

  void willAppear() {}

  void willDisappear() {}

  void textPaint(
      Canvas canvas, Size size, double bufferInSeconds, Color? delayColor) {
    final textStyle = TextStyle(
      color: delayColor,
      fontSize: 10,
    );
    final textSpan = TextSpan(
      text: bufferInSeconds.toStringAsFixed(1) + '(Sec)',
      style: textStyle,
    );
    final textPainter = TextPainter(
      text: textSpan,
      textDirection: TextDirection.ltr,
    );
    textPainter.layout(
      minWidth: 0,
      maxWidth: size.width,
    );
    final offset = Offset(size.width - 40, 3);
    textPainter.paint(canvas, offset);
  }
}
