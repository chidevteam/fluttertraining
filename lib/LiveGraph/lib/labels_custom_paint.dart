import 'package:flutter/material.dart';

class LabelsCustomPaint extends CustomPainter {
  String label;
  Color? labelColor;
  Offset textPos;
  LabelsCustomPaint(this.label, this.labelColor,
      {this.textPos = const Offset(0, 0)});

  @override
  void paint(Canvas canvas, Size size) {
    // TO DO: implement paint
    // print('Labels created new3');
    textPaint(canvas, size);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    // TO DO: implement shouldRepaint
    // throw UnimplementedError();
    return false;
  }

  void textPaint(Canvas canvas, Size size) {
    final textStyle = TextStyle(
      color: labelColor,
      fontSize: 15,
    );
    final textSpan = TextSpan(
      text: label,
      style: textStyle,
    );
    final textPainter = TextPainter(
      text: textSpan,
      textDirection: TextDirection.ltr,
    );
    textPainter.layout(
      minWidth: 0,
      maxWidth: size.width,
    );
    // final offset = Offset(0, 0);
    textPainter.paint(canvas, textPos);
    // print("$label ====");
  }
}
