import 'dart:async';
import 'package:flutter/material.dart';
// import 'package:flutter/services.dart';

import 'custom_paint_base.dart';
import 'ecgGrid.dart';

// TODO:
// 0. Fix the roation double timer issue
// 1. Use double.infinity instead of 999999, and get issue when trace backs
// 2. modulus should be changed to subtraction
// 3. scaling should be done in consumeData
//    only new points will be scaled.

class GraphCustomPaint extends CustomPaintBase {
  // External "Large" buffer from which we copy points into traceBuf
  List<num> dataBuf;
  double sampleRate; // 100, required to compute size of trace
  ECGgrid ecgGrid; // To compute x,y scales for drawing on canvas
  Rect marginRect;

  late List<double> traceBuf; //This is the trace buffer
  int traceIdx = 0;

  //Timer related vars
  int desiredFPS = 15;
  Timer? timer = null; // the display timer
  late int timerRepeatTimeMSec;
  late double samplesPerTimer;
  Color? graphLineColor;

  // For computing the actual PaintFrameRate (for debugging). We see how
  // much time we are taking to render the desiredFPS
  int paintFrameCounter = 0;
  late double xScale, m;

  Color? delayColor;

  double LOW_BUFFER_THRESHOLD = 2.0; //multiply by Sample rate
  double HIGH_BUFFER_THRESHOLD = 6.0; //multiply by Sample rate

  GraphCustomPaint(this.marginRect, this.dataBuf, notifier, this.sampleRate,
      this.ecgGrid, this.graphLineColor, this.delayColor)
      : super(notifier) {
    print('Canvas created');
  }

  @override
  void initVars() {
    //0.2 sec correspond to one box for ecg
    double numberOfSec = ecgGrid.numXBigBoxes * 0.2;

    timerRepeatTimeMSec = (1000 ~/ desiredFPS);
    // Converted time to seconds
    samplesPerTimer = sampleRate * (timerRepeatTimeMSec / 1000.0);
    LOW_BUFFER_THRESHOLD = sampleRate * LOW_BUFFER_THRESHOLD;
    HIGH_BUFFER_THRESHOLD = sampleRate * HIGH_BUFFER_THRESHOLD;
    int sizeOfTrace = (sampleRate * numberOfSec).toInt();
    traceBuf = List.filled(sizeOfTrace, 9999999);
    // Compute the variables for Scale conversion
    this.xScale = (1.0 / (sampleRate * numberOfSec)) *
        (canvasWidth - marginRect.left - marginRect.right);
    this.m = (ecgGrid.yMinPx - ecgGrid.yMaxPx) / (ecgGrid.yMax - ecgGrid.yMin);

    startTimer();
  }

  void startTimer() {
    print('Start Timer');
    if (timer != null) {
      // print('Stop Timer in Start timer');
      timer?.cancel();
    }

    timer = Timer.periodic(
        Duration(milliseconds: timerRepeatTimeMSec), _consumeData);
  }

  void stopTimer() {
    print('Stopping Timer');
    timer?.cancel();
    // dataBuffer.clear();
  }
/*
- 
 */

  _consumeData(Timer t) {
    if (canvasWidth == -1) return;
    if (dataBuf.length == 0) {
      // print("Yes, this is the reason of Jhatka");
      return;
    }
    // print("${dataBuf.length}");
    // print('Timer Running ++++++++++++++++ $samplesPerTimer');
    int sizeOfTrace = traceBuf.length;
    int sampleConsumed = 0;
    double speed = 1.0;
    if (dataBuf.length > HIGH_BUFFER_THRESHOLD) {
      speed = 2.0;
    } else if (dataBuf.length < LOW_BUFFER_THRESHOLD) {
      speed = 0.9;
    }
    print(
        "vv ${dataBuf.length / sampleRate} ${speed.toStringAsFixed(2)}-> ${samplesPerTimer.toStringAsFixed(2)} ${(samplesPerTimer * speed).toStringAsFixed(2)}");

    for (int i = 0; i < samplesPerTimer * speed; i++) {
      if (i >= dataBuf.length) break;
      sampleConsumed++;
      traceBuf[(traceIdx + (sampleRate * 0.1).toInt()) % sizeOfTrace] = 9999999;
      traceBuf[traceIdx % sizeOfTrace] = dataBuf[i].toDouble();
      // dataBuf.removeRange(0, 1);

      traceIdx++;
    }
    if (sampleConsumed > 0) {
      dataBuf.removeRange(0, sampleConsumed);
    }

    notifier!.value += 1;
    // if (notifier!.value % 10 == 0) {
    //   ecgGrid.yMax = 3.0;
    //   computeGridParams(ecgGrid);
    //   ecgGrid.gridNotifier.value += 1;
    // }
  }

  @override
  void paint(Canvas canvas, Size size) {
    super.paint(canvas, size);
    // print("-- traceIdx = $traceIdx");

    computePointToPixelAndDrawDataUsingPath(canvas, size);

    double bufferInSeconds = dataBuf.length / sampleRate;
    textPaint(canvas, size, bufferInSeconds, Colors.green);
  }

  void computePointToPixelAndDrawDataUsingPath(Canvas canvas, Size size) {
    var paint = Paint()
      ..color = graphLineColor!
      ..strokeWidth = 2
      ..style = PaintingStyle.stroke
      ..strokeCap = StrokeCap.round;

    Path path = Path();

    // double ymax = ecgGrid.yMax; //1;
    // double ymin = ecgGrid.yMin; //-1;
    //
    // double m = (ecgGrid.yMinPx - ecgGrid.yMaxPx) / (ymax - ymin);

    bool flag = false;
    for (int i = 0; i < traceBuf.length; i++) {
      if (i == 0) {
        double x1 = i.toDouble();
        double y1 = traceBuf[i].toDouble();
        double X1 = x1 * xScale + marginRect.left;
        double Y1 = ecgGrid.yMaxPx + m * (y1 - ecgGrid.yMin);
        path.moveTo(X1, Y1);
        // print("Y1 $Y1, $y1");
        if (y1 == 9999999) {
          flag = true;
        }
      } else {
        double x2 = i.toDouble();
        double y2 = traceBuf[i].toDouble();

        double X2 = x2 * xScale + marginRect.left;
        double Y2 = ecgGrid.yMaxPx + m * (y2 - ecgGrid.yMin);

        if (y2 == 9999999) {
          flag = true;
        } else {
          if (flag == true) {
            path.moveTo(X2, Y2);
            flag = false;
          }
          path.lineTo(X2, Y2);
        }
      }
    }
    canvas.drawPath(path, paint);
  }

  @override
  void willAppear() {
    startTimer();
  }

  @override
  void willDisappear() {
    stopTimer();
  }
}
