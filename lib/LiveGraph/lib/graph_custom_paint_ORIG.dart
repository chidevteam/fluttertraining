import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'ecgGrid.dart';

class GraphCustomPaint extends CustomPainter {
  // External "Large" buffer from which we copy points into traceBuf
  List<num> dataBuf;
  double sampleRate; // 100, required to compute size of trace
  ECGgrid ecgGrid; // To compute x,y scales for drawing on canvas
  late ValueNotifier<int>? notifier;

  late List<double> traceBuf; //This is the trace buffer
  int traceIdx = 0;

  //Timer related vars
  int desiredFPS = 30;
  Timer? timer = null; // the display timer
  late int timerRepeatTimeMSec;
  late double samplesPerTimer;

  // For computing the actual PaintFrameRate (for debugging). We see how
  // much time we are taking to render the desiredFPS
  int paintFrameCounter = 0;

  //Value to scale x, y to Graph
  double canvasWidth = -1;

  late double xScale, m;


  GraphCustomPaint(
      this.dataBuf, this.notifier, this.sampleRate, this.ecgGrid)
      : super(repaint: notifier) {
    print('Canvas created');

  }

  void initVars() {
    late double numberOfSec;

    timerRepeatTimeMSec = (1000 ~/ desiredFPS);

    //0.2 sec correspond to one box for ecg
    numberOfSec = ecgGrid.numXBigBoxes * 0.2;

    // Converted time to seconds
    samplesPerTimer = sampleRate * (timerRepeatTimeMSec / 1000.0);
    int sizeOfTrace = (sampleRate * numberOfSec).toInt();
    // sizeOfTrace = sizeOfTrace * 2; // x and y points both are stored.
    // traceBuf = List.filled(sizeOfTrace, double.infinity);
    traceBuf = List.filled(sizeOfTrace, -999);
    // Compute the variables for Scale conversion
    this.xScale = (1.0 / (sampleRate * numberOfSec)) * canvasWidth;
    this.m = (ecgGrid.yMinPx - ecgGrid.yMaxPx) / (ecgGrid.yMax - ecgGrid.yMin);

    startTimer();

  }

  @override
  void paint(Canvas canvas, Size size) {
    // print('Labels created new1');
    if (canvasWidth == -1) {
      canvasWidth = size.width;
      initVars();
    }

    computePointToPixelAndDrawDataUsingPath(canvas, size);

    double bufferInSeconds = dataBuf.length / sampleRate;
    textPaint(canvas, size, bufferInSeconds);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    // print('shouldRepaint');
    return false;
  }

  void computePointToPixelAndDrawDataUsingPath(Canvas canvas, Size size) {
    var paint = Paint()
      ..color = Color.fromARGB(255, 0, 100, 0)
      ..strokeWidth = 1
      ..style = PaintingStyle.stroke
      ..strokeCap = StrokeCap.round;

    Path path = Path();

    // double ymax = ecgGrid.yMax; //1;
    // double ymin = ecgGrid.yMin; //-1;
    //
    // double m = (ecgGrid.yMinPx - ecgGrid.yMaxPx) / (ymax - ymin);

    bool flag = false;
    for (int i = 0; i < traceBuf.length; i++) {
      if (i == 0) {
        double x1 = i.toDouble();
        double y1 = traceBuf[i].toDouble();
        double X1 = x1 * xScale;
        double Y1 = ecgGrid.yMaxPx + m * (y1 - ecgGrid.yMin);
        path.moveTo(X1, Y1);
      } else {
        double x2 = i.toDouble();
        double y2 = traceBuf[i].toDouble();

        double X2 = x2 * xScale;
        double Y2 = ecgGrid.yMaxPx + m * (y2 - ecgGrid.yMin);

        if (y2 == -999) {
          flag = true;
        } else {
          if (flag == true) {
            path.moveTo(X2, Y2);
            flag = false;
          }
          path.lineTo(X2, Y2);
        }
      }
    }
    canvas.drawPath(path, paint);
  }

  void startTimer() {
    print('Start Timer');
    if (timer != null) {
      // print('Stop Timer in Start timer');
      timer?.cancel();
    }

    SystemChannels.lifecycle.setMessageHandler((msg) async {
      debugPrint('SystemChannels> $msg');
      if (msg == AppLifecycleState.paused.toString() ||
          msg == AppLifecycleState.inactive.toString() ||
          msg == AppLifecycleState.detached.toString()) timer!.cancel();

      if (msg == AppLifecycleState.resumed.toString()) startTimer();
    });

    timer = Timer.periodic(
        Duration(milliseconds: timerRepeatTimeMSec), _consumeData);
  }

  void stopTimer() {
    print('Stopping Timer');
    timer?.cancel();
    // dataBuffer.clear();
  }

  _consumeData(Timer t) {
    if (canvasWidth == -1) return;
    if (dataBuf.length == 0) return;
    print('Timer Running ++++++++++++++++ $samplesPerTimer');
    int sizeOfTrace = traceBuf.length;
    for (int i = 0; i < samplesPerTimer; i++) {
      if (dataBuf.length == 0) break;
      traceBuf[traceIdx % sizeOfTrace] = dataBuf[0].toDouble();
      dataBuf.removeAt(0); //remover using range, move out of loop
      traceBuf[(traceIdx + (sampleRate * 0.1).toInt()) %
          sizeOfTrace] = -999; //claim by sir ahmed
      traceIdx++;
    }
    notifier!.value += 1;
  }

  void textPaint(Canvas canvas, Size size, double bufferInSeconds) {
    final textStyle = TextStyle(
      color: Colors.black,
      fontSize: 10,
    );
    final textSpan = TextSpan(
      text: bufferInSeconds.toStringAsFixed(1) + '(Sec)',
      style: textStyle,
    );
    final textPainter = TextPainter(
      text: textSpan,
      textDirection: TextDirection.ltr,
    );
    textPainter.layout(
      minWidth: 0,
      maxWidth: size.width,
    );
    final offset = Offset(size.width - 40, 3);
    textPainter.paint(canvas, offset);
  }
}
