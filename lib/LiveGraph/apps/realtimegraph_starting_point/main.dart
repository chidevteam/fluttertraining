// import 'dart:ui';

import 'package:flutter/material.dart';
import 'realtime_graph.dart';
import 'realtime_grid.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    debugPrint("running for first time");
    return MaterialApp(
        title: 'Visualizer',
        theme: ThemeData(
          primarySwatch: Colors.pink,
        ),
        home: Scaffold(
            appBar: AppBar(title: Text("Graph")),
            body: Stack(children: [
              CustomPaint(
                child: Container(
                  width: double.infinity,
                  height: 600,
                  // color: Colors.lightBlue,
                ),
                foregroundPainter: LinePainter(),
              ),
              CustomPaint(
                child: Container(
                  width: double.infinity,
                  height: 600,
                  // color: Colors.lightBlue,
                ),
                foregroundPainter: GridPainter(),
              ),
            ])));
  }
}
