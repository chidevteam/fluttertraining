import 'package:flutter/material.dart';
import 'graph_utils.dart';
import 'dart:math';
import 'dart:io';

class GridPainter extends CustomPainter {
  late double x0, y0, x1, y1;
  double leftMargin = 25;
  double topMargin = 15;
  double rightMargin = 10;
  double bottomMargin = 40;

  // Offset dxy = Offset(0, 0);
  // double sx = 1;
  // double sy = 1;
  void initVars(double w, double h) {
    x0 = leftMargin;
    y0 = topMargin;
    x1 = w - rightMargin;
    y1 = h - bottomMargin;
  }

  @override
  void paint(Canvas canvas, Size size) {
    initVars(size.width, size.height);
    var paint = Paint()
      ..color = Color(0xff995588)
      ..strokeWidth = 2
      ..style = PaintingStyle.stroke;

    // Offset center = Offset(size.width / 2, size.height / 2);

    Rect drawingArea = Rect.fromLTRB(x0, y0, x1, y1);
    // canvas.drawCircle(center, 100, paint);
    canvas.drawLine(Offset(x0, y0), Offset(x1, y1), paint);
    canvas.drawLine(Offset(x1, y0), Offset(x0, y1), paint);
    canvas.drawRect(drawingArea, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}

// class ECGGraphPainter extends CustomPainter {
//   Map<String, dynamic> data;
//   BuildContext context;
//   List<double> timeData = [];
//   List<double> d2 = [];

//   final Offset f1Pt, f2Pt, cPt;

//   late bool longPressMode;
//   // late ECGgridDiscrete ecgGrid;

//   /// Initialize gradient and intecepts, and grid lines via function initVars
//   late double mx = 1.0, my = 1.0, cx, cy;
//   late double yMinPx, yMaxPx, yDeltaPx; //y-grid params
//   late double xMinPx, xMaxPx, xDeltaPx; //y-grid params

//   double leftMargin = 25;
//   double topMargin = 15;
//   double rightMargin = 10;
//   double bottomMargin = 40;
//   late double x0, y0, x1, y1;
//   Offset dxy = Offset(0, 0);
//   double sx = 1;
//   double sy = 1;

//   ECGGraphPainter(this.data, this.f1Pt, this.f2Pt, this.cPt, this.dxy, this.sx,
//       this.sy, this.longPressMode, this.context) {
//     _copyData();
//   }

//   _copyData() {
//     double t = 0;
//     for (double value in data["ecg"]) {
//       timeData.add(t);
//       t += 0.01;
//       d2.add(value);
//     }
//   }

//   @override
//   bool shouldRepaint(covariant CustomPainter oldDelegate) => true;

//   _computeMinMax(Map<String, dynamic> data) {
//     Map<String, double> minMax = {
//       "ymin": minimum(data["ecg"]),
//       "ymax": maximum(data["ecg"]),
//       "xmin": minimum(timeData),
//       "xmax": maximum(timeData),
//     };
//     print(minMax);
//     return minMax;
//   }

//   @override
//   void paint(Canvas canvas, Size size) {
//     initVars(size.width, size.height);

//     Paint paint = makePaint(Colors.black, strokeWidth: 2.0);

//     Rect drawingArea = Rect.fromLTRB(x0, y0, x1, y1);

//     /// Save canvas state for clipping the graph within drawingArea
//     canvas.save();
//     canvas.clipRect(drawingArea);

//     drawMiddleCurve(canvas);
//     canvas.restore(); //to draw text outside the drawing portion

//     canvas.drawRect(drawingArea, paint);
//     drawXGrid(canvas, y0, y1, drawingArea);
//     drawYGrid(canvas, size, drawingArea);
//   }

//   void drawMiddleCurve(Canvas canvas) {
//     Paint paint1 = Paint()
//       ..color = Colors.green.shade600
//       ..style = PaintingStyle.stroke
//       ..strokeWidth = 2;

//     Path p = Path();
//     for (int i = 0; i < timeData.length; i++) {
//       double px = mx * timeData[i] + cx;
//       double py = my * d2[i] + cy;
//       if (i == 0)
//         p.moveTo(px, py);
//       else
//         p.lineTo(px, py);
//     }
//     canvas.drawPath(p, paint1);
//   }

//   void drawPolygonCurve(
//       Canvas canvas, List<List<double>> listData2, Paint paint1) {
//     List<double> y1 = listData2[0];
//     List<double> y2 = listData2[1];

//     Path p = Path();
//     for (int i = 0; i < timeData.length; i++) {
//       double px = mx * timeData[i] + cx;
//       double py = my * y1[i] + cy;
//       if (i == 0)
//         p.moveTo(px, py);
//       else
//         p.lineTo(px, py);
//     }

//     for (int i = timeData.length - 1; i >= 0; i--) {
//       double px = mx * timeData[i] + cx;
//       double py = my * y2[i] + cy;
//       p.lineTo(px, py);
//     }

//     canvas.drawPath(p, paint1);
//   }

//   void initVars(double w, double h) {
//     x0 = leftMargin;
//     y0 = topMargin;
//     x1 = w - rightMargin;
//     y1 = h - bottomMargin;

//     Map<String, double> minMax = _computeMinMax(data);

//     double? ymin = minMax["ymin"]!;
//     double? ymax = minMax["ymax"]!;
//     double? xmin = minMax["xmin"]!;
//     double? xmax = minMax["xmax"]!;

//     Map mp = computeGridAndLineEqs(
//         w, h, xmin, xmax, ymin, ymax, x0, x1, y0, y1, dxy, sx, sy);
//     mx = mp["mx"];
//     cx = mp["cx"];
//     my = mp["my"];
//     cy = mp["cy"];
//     yMinPx = mp["yMinPx"];
//     yMaxPx = mp["yMaxPx"];
//     yDeltaPx = mp["yDelta"];
//     xMinPx = mp["xMinPx"];
//     xMaxPx = mp["xMaxPx"];
//     xDeltaPx = mp["xDelta"];
//   }

//   void drawYGrid(Canvas canvas, Size size, Rect drawingArea) {
//     Path path = Path();
//     Paint paint = makePaint(Colors.black);

//     for (double y = yMaxPx; y <= yMinPx + 1e-3; y -= yDeltaPx) {
//       path.moveTo(leftMargin, y);
//       path.lineTo(size.width - rightMargin, y);
//       double yn = (y - cy) / my;
//       // textPaint(context, canvas, size, yn.toStringAsFixed(2), 0, y - 6);
//     }
//     canvas.save();
//     canvas.clipRect(drawingArea);
//     canvas.drawPath(path, paint);
//     canvas.restore();
//   }

//   void drawXGrid(Canvas canvas, double y0, double y1, Rect drawingArea) {
//     Paint paint = makePaint(Colors.black, strokeWidth: 1.0);
//     Path p = Path();

//     for (double x = xMinPx; x < xMaxPx + 1e-3; x += xDeltaPx) {
//       // double px = mx * x + cx;

//       p.moveTo(x, y0);
//       p.lineTo(x, y1);

//       textPaint(canvas, x.toString(), x - 15, y1 + 15);
//     }
//     canvas.save();
//     canvas.clipRect(drawingArea);

//     canvas.drawPath(p, paint);
//     canvas.restore();
//   }
//   void computeVars(
//       double w, double h, double xmin, double xmax, double ymin, double ymax) {
//     List<double> mxcx =
//         solveLinear(xmin, xmax, leftMargin, w - rightMargin, dxy.dx, sx);
//     mx = mxcx[0];
//     cx = mxcx[1];
//     double desiredNumLines = 4;
//     List<double> resultsX = gridDesignFloat(mxcx[2], mxcx[3], desiredNumLines);
//     xDeltaPx = resultsX[0] * mx;
//     xMinPx = resultsX[1] * mx + cx;
//     xMaxPx = resultsX[2] * mx + cx;

//     List<double> mycy =
//         solveLinear(ymin, ymax, h - bottomMargin, topMargin, dxy.dy, sy);
//     my = mycy[0];
//     cy = mycy[1];

//     List<double> resultsY = gridDesignFloat(mycy[2], mycy[3], desiredNumLines);
//     yDeltaPx = resultsY[0] * my;
//     yMinPx = resultsY[1] * my + cy;
//     yMaxPx = resultsY[2] * my + cy;
//   }

// }

// Map<String, double> computeGridAndLineEqs(
//     double w,
//     double h,
//     double xmin,
//     double xmax,
//     double ymin,
//     double ymax,
//     double x0,
//     double x1,
//     double y0,
//     double y1,
//     Offset dxy,
//     double sx,
//     double sy,
//     {double desiredNumLines = 4}) {
//   late double mx = 1.0, my = 1.0, cx, cy;
//   late double yMinPx, yMaxPx, yDeltaPx; //y-grid params
//   late double xMinPx, xMaxPx, xDeltaPx; //x-grid params
//   if (xmax == xmin) {
//     xmax += 300;
//     xmin -= 300;
//   }
//   List<double> mxcx = solveLinear(xmin, xmax, x0, x1, dxy.dx, sx);
//   mx = mxcx[0];
//   cx = mxcx[1];
//   List<double> resultsX = gridDesignFloat(mxcx[2], mxcx[3], desiredNumLines);
//   xDeltaPx = resultsX[0] * mx;
//   xMinPx = resultsX[1] * mx + cx;
//   xMaxPx = resultsX[2] * mx + cx;

//   List<double> mycy = solveLinear(ymin, ymax, y1, y0, dxy.dy, sy);
//   my = mycy[0];
//   cy = mycy[1];

//   List<double> resultsY = gridDesignFloat(mycy[2], mycy[3], desiredNumLines);
//   yDeltaPx = resultsY[0] * my;
//   yMinPx = resultsY[1] * my + cy;
//   yMaxPx = resultsY[2] * my + cy;

//   Map<String, double> mp = {
//     "mx": mx,
//     "cx": cx,
//     "my": my,
//     "cy": cy,
//     "xMinPx": xMinPx,
//     "xMaxPx": xMaxPx,
//     "xDelta": xDeltaPx,
//     "yMinPx": yMinPx,
//     "yMaxPx": yMaxPx,
//     "yDelta": yDeltaPx,
//   };
//   return mp;
// }

// void printx(v, {dynamic end = ''}) {
//   stdout.write(v + ' ');
// }

// String f2(double v) {
//   // return "{:.3f}".format(v)
//   return (v).toStringAsFixed(3);
// }

// List<double> gridDesignFloat(double vmin, double vmax, double n) {
//   double dx = vmax - vmin;
//   if (dx == 0) {
//     dx = 0.00001;
//   }
//   double d = dx / n;
//   double lg = log(d) / log(10);
//   double flog = lg.floorToDouble();
//   double v = pow(10.0, flog).toDouble();
//   double sc = d / v;

//   //sc is in range 1.xx - 9.999

//   double scv = 0.0000001;
//   if (sc < 1.5) {
//     scv = v * 1;
//   } else if (sc < 2.25) {
//     scv = v * 2.0;
//   } else if (sc < 3.75) {
//     scv = v * 2.5;
//   } else if (sc < 7.5) {
//     scv = v * 5.0;
//   } else {
//     scv = v * 10.0;
//   }

//   double vvmin = (vmin / scv).ceil() * scv;
//   double vvmax = (vmax / scv).floor() * scv;

//   return [scv, vvmin, vvmax];
// }



// List<double> solveLinear(double x1, double x2, double y1, double y2, d, scale) {
//   double mx = (y2 - y1) / (x2 - x1);
//   double cx = y1 - mx * x1;
//   mx = mx * scale;
//   cx = cx * scale + d;
//   double xmin_new = (y1 - cx) / mx;
//   double xmax_new = (y2 - cx) / mx;
//   return [mx, cx, xmin_new, xmax_new];
// }

