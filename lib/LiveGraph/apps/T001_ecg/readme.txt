

- Axis/labels
- speed 
- low and high sample rates
- Internet disconnection
- autoscale
- corner cases


Task, axis labels
==================
10:30am - 1:00pm  
    Refreshed the memory of code,
    Taught Daniyal how the graphs work.

1:20pm: Planning
    We will make marginRect and pass it on to all the concerned painters.
    30min
    Apply appropriate limits
    In the meantime, we will remove unnecesary stuff, and cleanup.

- Took around 2 hours.
======================================================
Autoadjust Grid.
- Ask Gridpaint to redraw, via an notifier
- Somebody changes that notifier (Graphpaint)
4:00pm 










==============================================
Need to make proper widget for value meter(label,width,height,ranges,value,units) Done
use changenotifierprovider for value meter widgets Done
- Show buffer size in seconds(bufferSize/sampleRate) Done


- add a check for showing complete boxes in x and y axis
- Speed control of data to be drawn
- dynamic views(through json)






- PC102 LIVE SCREEN WORKING
> Daniyal     --
 - Fix plot isses 6 sec. Done
 - Graph testing Done
 - Block level screen drawing
    A button in view to show cuff pressure // low priority
    Orientation support(use single instance of viewmodel)


- 6500 ECG SCREEN WORKING
- GENERIC COMPONENT WHICH GETS LAYOUT CONFIG FOR
  PIE CHARTS (/LINE CHARTS) AND LIVEGRAHS


=============================================================================

- Data Travel Path/App Architecture(placement of widgets)

----------------------------------------------------------

- Drawing of actual graph (30 min) Done(bug---detracked for 25 mins)
- handle proper cancellation of timer (on pressing twice 2 timers shouldn't start.)(15 min) Done(15 min)
- divide main into two parts(main, simulated socket)(45 min)

    void main()
    {
        //Socket socket = new Socket(url);
        LiveGraphWidget lg = LiveGraph("wss:/ecgData", 128, Label);
    }


- Timer should stop if app goes to background Done (2 hours)(Searched a lot and decided to go to
                            stateful widget but found a solution and still using stateless widget)
- Graph will be redrawn from beginning on orientation change (canvas is maintaining the position)
- Graph will be redrawn from beginning when app comes from background (canvas is maintaining the position)
- Add limits(ranges)
- Handle UI changes on orientation change(handled graph container size)
- Draw Grids According to provided time/Orientation(done some work needs final touches) and restart graph
- make an init for graphs
- integrate ecg grids
- Show buffer size in seconds(buffersize/samplerate)


----------------------------------------------------------

















-----------------------------------------------------------



ADVANCED
========
- Press a button to pause live graph
    Also pan zoom functionality