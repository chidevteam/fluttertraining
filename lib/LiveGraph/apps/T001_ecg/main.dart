import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import '../../lib/live_graph.dart';
import 'MakeLiveGraphViewModel.dart';

void main() {
  runApp(MyApp());
}

// ignore: must_be_immutable
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    print("My app build called");
    return MaterialApp(
      home: createHome(),
    );
  }
}

Widget createHome() {
  print("create home called 1");

  Scaffold scaffold = Scaffold(
    appBar: AppBar(
      title: Text("Canvas App"),
      backgroundColor: Colors.red,
    ),
    body: MakeGraphContainer(),
    backgroundColor: Colors.black,
  );

  return scaffold;
}

class MakeGraphContainer
    extends ViewModelBuilderWidget<MakeLiveGraphViewModel> {
  MakeGraphContainer({
    Key? key,
  }) : super(key: key);

  @override
  Widget builder(
      BuildContext context, MakeLiveGraphViewModel viewModel, Widget? child) {
    Widget liveGraph = MakeLiveGraph(
      dataBuffer: viewModel.ecgBuffer,
      label: 'ECG',
      showGrids: true,
      sampleRate: viewModel.sampleRate,
      yMin: -1.5,
      yMax: 1.5,
      yGridSmallSpacing: 0.1,
      yGridLargeSpacing: 0.5,
      xGridSmallSpacing: 0.04,
      xGridLargeSpacing: 0.2,
      pixelAspecratio: 2.5,
    );

    Padding liveGraphPadding = Padding(
      child: liveGraph,
      padding: EdgeInsets.all(5),
    );

    ElevatedButton startButton = ElevatedButton(
        onPressed: viewModel.startTimer, child: Text("Connect TO Device"));

    ElevatedButton stopButton = ElevatedButton(
        onPressed: viewModel.stopTimer, child: Text("Disconnect From Device"));

    return OrientationBuilder(
      builder: (context, orientation) {
        return Column(
          children: [
            startButton,
            stopButton,
            Container(
              child: liveGraphPadding,
              color: Color.fromARGB(255, 255, 255, 255),
              width: double.infinity,
              height: 150,
              // orientation == Orientation.portrait ? 645 / 6 : 645 / 12
            ),
          ],
        );
      },
    );
  }

  @override
  MakeLiveGraphViewModel viewModelBuilder(BuildContext context) {
    return MakeLiveGraphViewModel();
  }
}
