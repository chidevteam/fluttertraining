import 'dart:async';
import 'dart:math' as math;

import 'package:stacked/stacked.dart';

class MakeLiveGraphViewModel extends BaseViewModel {
  //Timer related vars
  List<double> ecgBuffer = [];

  Timer? timer = null;

  double sampleRate = (250 * 1); // 100
  double timerFreq = 1;
  double signalFreq = 1.0;
  late int timerRepeatTimeMSec; // 250 milliseconds
  late double samplesPerTimer; // 25
  late double numberOfSec; // 6
  double T = 0;

  MakeLiveGraphViewModel() {
    timerRepeatTimeMSec = (1000.0 ~/ timerFreq).toInt();
    samplesPerTimer = sampleRate * (timerRepeatTimeMSec / 1000.0);
    print("View Model Created  ");
  }

  void _generateData(Timer t) {
    // print("Timer Generated 2");
    for (int i = 0; i < samplesPerTimer; i += 1) {
      T = T + (1.0 / sampleRate);
      ecgBuffer.add(math.sin(2 * 3.141592654 * signalFreq * T)); //2*Pie*f*T
    }
  }

  void startTimer() {
    print('Start Timer Main');

    if (timer != null) {
      timer?.cancel();
    }
    timer = Timer.periodic(
        Duration(milliseconds: timerRepeatTimeMSec), _generateData);
  }

  void stopTimer() {
    print('Stopping Timer Main');
    if (timer != null) {
      timer?.cancel();
      timer = null;
    }
  }

  @override
  void dispose() {
    timer?.cancel();
    super.dispose();
  }
}
