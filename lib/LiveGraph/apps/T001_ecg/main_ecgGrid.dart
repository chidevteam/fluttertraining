// import 'dart:io';
// import 'dart:math';
import '../../lib/ecgGrid.dart';
import 'package:flutter/material.dart';

void main() {
  List data = [
    // [ x1, x2, y2, y1]
    // [-1.5, 1.5, 507, 300],
    [-1.25, 0.6, 500.0, 300.0, 0.1, 0.5, 0.04, 0.2, 2.5],
    [-125.0, 60.0, 507.0, 300.0, 0.1, 50.0, 0.04, 0.2, 250.0],
    [-150.0, 150.0, 330.0, 362.0, 0.1, 50.0, 0.04, 0.2, 250.0],
    [-150.0, 150.0, 330.0, 150.0, 0.1, 50.0, 0.04, 0.2, 250.0],
    // [-1.25, 0.6, 500.0, 300.0],
    // [-1.5, 1.5, 577.0, 346.0],
    // [-1.0, 1.5, 200, 532],
  ];

  // exit(0);

  for (int i = 0; i < data.length; i += 1) {
    double yMin = data[i][0]; // x1 = -1.25 maps to canvHeight
    double yMax = data[i][1]; // x2 = 0.60 maps to yMinCanv
    double canvWidth = data[i][2];
    double canvHeight = data[i][3]; // y1 = 300
    double yGridSmallSpacing = data[i][4];
    double yGridLargeSpacing = data[i][5];
    double xGridSmallSpacing = data[i][6];
    double xGridLargeSpacing = data[i][7];
    double pixelAspecratio = data[i][8];

    ECGgrid ecgGrid = ECGgrid(
        marginRect: Rect.fromLTRB(0, 0, 0, 0),
        yMin: yMin,
        yMax: yMax,
        canvWidth: canvWidth,
        canvHeight: canvHeight,
        yGridSmallSpacing: yGridSmallSpacing,
        yGridLargeSpacing: yGridLargeSpacing,
        xGridSmallSpacing: xGridSmallSpacing,
        xGridLargeSpacing: xGridLargeSpacing,
        pixelAspecratio: pixelAspecratio);
    ecgGrid = computeGridParams(ecgGrid);
    ecgGrid.printDesc();

    // double canvWidth2 = data[i][2].toDouble();
    // const double EPSILON = 1e-3; // TO FIX THE 11.000000002 ISSUE
    // int k = 1;
    // for (double y = retVal[1]; y <= retVal[2]; y += retVal[3]) {
    //   print("${k++} -y-- $y");
    // }

    // k = 0;
    // print("Canvas width 2, $canvWidth2");
    // for (double x = 0; x <= canvWidth2 + EPSILON; x += retVal[3]) {
    //   print("${k++} -x-- $x");
    // }
  }
}


/*

[9:38 AM] Daniyal Altaf
  yMin: -1.6490467922608316
yMax: 1.6490467922608316
canvWidth: 577
canvHeight: 346
yMinPx: 15.636363493223087
yMaxPx: 330.3636365067769
yDeltaPx: 52.45454550225898
yMinPxSmall: 5.145454392771228
yMaxPxSmall: 340.8545456072287
yDeltaPxSmall: 10.490909100451796
scaleRatio: 1.0993645281738877

 */