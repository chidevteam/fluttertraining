import 'package:flutter/material.dart';
import 'package:universal/Tutorials/DART/T004_Network/lib/network/system_api_service.dart';
import 'package:universal/Tutorials/DART/T004_Network/lib/network/api_client.dart';
import 'package:universal/LiveGraph/apps/pc102Screen/liveSessionScreen.dart';

void main() async {
  await loginFunc('charms-qa', "kashifd", "Kashif123@");

  runApp(MyApp());
}

// ignore: must_be_immutable
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    print("My app build called");
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Main(),
    );
  }
}

// ignore: must_be_immutable
class Main extends StatelessWidget {
  final List<String> item = [
    'PC-102',
    'PC-202',
    'PC-303',
    'PC-60FW',
    'YK-81C',
  ];

  String baseUrl = 'wss://charms-qa.cognitivehealthintl.com/ws/cmclient?token=';
  String token = ApiClient.getAuthToken();
  // 'lb3SaRmNR9nSNLxzHgkwWoYscXK1AegHV-CiU8rcFKn7rl5p_XgEYJfheCruFwjcINxTf-xUcITvcsNsKmC2MYpDdpAAjIBHDEvR0nn_aWfiNF4OXOHHwmB9DUXbXRnM15Tk-5HeklErMXHEa8ljjlrrX_ooAmV4fy1SCUQ6wSQ=';
  late String url;

  String observationId = 'G80O59G9KL';
  Main() {
    url = baseUrl + token;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MediaQuery.of(context).orientation == Orientation.landscape
          ? null // show nothing in lanscape mode
          : AppBar(
              title: Center(child: Text('OnGoingSessions')),
              backgroundColor: Colors.red,
            ),
      backgroundColor: Colors.white,
      body: SafeArea(
        child: ListView.separated(
          itemBuilder: (context, position) {
            return ListTile(
              title: Text(item[position]),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>
                            CreateHome(item[position], url, observationId)));
              },
            );
          },
          separatorBuilder: (context, position) {
            return Divider(
              color: Colors.black,
            );
          },
          itemCount: item.length,
        ),
      ),
    );
  }
}
