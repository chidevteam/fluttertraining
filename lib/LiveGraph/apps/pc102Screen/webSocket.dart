import 'dart:async';
import 'dart:convert';

import 'package:stacked/stacked.dart';
import 'package:universal/LiveGraph/apps/pc102Screen/spot_check_monitor_machine.dart';
import 'package:web_socket_channel/status.dart' as status;
import 'package:web_socket_channel/web_socket_channel.dart';

import '../realtimegraph/graph_models/gesture_model.dart';

class WebSocketSimulation extends BaseViewModel {
  //Timer related vars
  List<num> plethBuffer = [];
  List<num> ecgBuffer = [];

  Timer? socketKeepAliveTimer = null;

  SpotCheckMonitor spotCheckMonitorMachine = SpotCheckMonitor();
  GestureModel gestureModel = GestureModel();

  // double sampleRate = 50; // 100

  num spo2 = 0;
  num pulse = 0;
  num systolic = 0;
  num diastolic = 0;
  num temp = 0;
  double count = 0;

  late Map<String, dynamic> map;

  late final channel;

  final String url;
  final String observationId;
  int seconds = 0;
  int accData = 0;

  WebSocketSimulation(this.url, this.observationId) {
    channel = WebSocketChannel.connect(
      Uri.parse(url),
      // Uri.parse('ws://10.8.0.135:7000'),
    );

    var socketOnOpen = {
      "data": {"observation_id": observationId},
      "event": "Start"
    };
    var jsonText = jsonEncode(socketOnOpen);
    channel.sink.add(jsonText);

    startTimer();

    channel.stream.listen((message) {
      DateTime d = DateTime.now();

      if (seconds != d.second) {
        count += 1.0;
        // print("${d.second} data =  ${accData / count}");

        // accData = 0;
      }
      seconds = d.second;
      // print('Received message from socket $message');
      map = jsonDecode(message)['data'];

      if (map.containsKey('SpO2')) {
        spo2 = map['SpO2'];
        // print('SpO2+++++++++++$spo2');
      }
      if (map.containsKey('Pulse')) {
        pulse = map['Pulse'];
        // print('Pulse+++++++++++$pulse');
        notifyListeners();
      }
      if (map.containsKey('Systolic')) {
        systolic = map['Systolic'];
      }
      if (map.containsKey('Diastolic')) {
        diastolic = map['Diastolic'];
        notifyListeners();
      }
      if (map.containsKey('Pleth')) {
        // Iterable<num> list = map['Pleth'].cast<num>();
        // plethBuffer.addAll(list);
        // // print("${d.second} Received ${plethBuffer.length}");
        // accData += plethBuffer.length;
        // // plethBuffer.add((map['Pleth'][0])/100.0);
        // // plethBuffer.add((map['Pleth'][1])/100.0);
        map['Pleth'].cast<num>().forEach((value) {
          // plethBuffer.add(value / 100.0);
          plethBuffer.add(value.toDouble());
          spotCheckMonitorMachine.rtVitalList[0].dataBuf.add(value.toDouble());
        });
      }
      if (map.containsKey('TOCO')) {
        // Iterable<int> list = .cast<int>();
        plethBuffer.add(map['TOCO']);
        // print("${d.second} Received ${plethBuffer.length}");
        spotCheckMonitorMachine.rtVitalList[0].dataBuf
            .add(map['TOCO'].toDouble());
        accData += plethBuffer.length;
        // plethBuffer.add((map['Pleth'][0])/100.0);
        // plethBuffer.add((map['Pleth'][1])/100.0);
      }
      if (map.containsKey('FMOV')) {
        // Iterable<int> list = .cast<int>();

        // print("${d.second} Received ${plethBuffer.length}");
        spotCheckMonitorMachine.rtVitalList[0].dataBuf2
            .add(map['FMOV'].toDouble());
        accData += plethBuffer.length;
        // plethBuffer.add((map['Pleth'][0])/100.0);
        // plethBuffer.add((map['Pleth'][1])/100.0);
      }
    });

    print("View Model Created");
  }

  void startTimer() {
    print('Start Timer Main');

    if (socketKeepAliveTimer != null) {
      socketKeepAliveTimer?.cancel();
    }
    socketKeepAliveTimer =
        Timer.periodic(Duration(milliseconds: 3000), keepAliveData);
  }

  void stopTimer() {
    print('Stopping Timer Main');
    if (socketKeepAliveTimer != null) {
      socketKeepAliveTimer?.cancel();
      socketKeepAliveTimer = null;
    }
  }

  @override
  void dispose() {
    // socketKeepAliveTimer?.cancel();
    stopTimer();
    plethBuffer.clear();
    channel.sink.close(status.goingAway);
    super.dispose();
  }

  void keepAliveData(Timer timer) {
    var keepAlive = {
      'data': {'NetWork Connection': ''},
      'event': 'KeepAlive'
    };
    var jsonText = jsonEncode(keepAlive);

    channel.sink.add(jsonText);
  }
}
