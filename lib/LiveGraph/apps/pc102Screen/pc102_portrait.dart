import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:universal/LiveGraph/lib/value_meter.dart';
import 'package:universal/LiveGraph/apps/pc102Screen/webSocket.dart';
import 'package:universal/LiveGraph/lib/live_graph.dart';

import '../realtimegraph/realtime_graph/realtime_graph.dart';

Widget Pc102PortraitWidget(
    BuildContext context, WebSocketSimulation viewModel, String model) {
  List<Widget> row1 = [];
  List<Widget> row2 = [];
  List<Widget> column = [];

  // Widget liveGraph = MakeLiveGraph(
  //   dataBuffer: viewModel.plethBuffer,
  //   label: 'PLETH',
  //   showGrids: true,
  //   sampleRate: 1,
  //   yMin: -150,
  //   yMax: 150,
  //   yGridSmallSpacing: 0.1,
  //   yGridLargeSpacing: 50,
  //   xGridSmallSpacing: 0.04,
  //   xGridLargeSpacing: 0.2,
  //   pixelAspecratio: 250,
  // );
  Widget liveGraph = RealTimeGraph(
    viewModel.spotCheckMonitorMachine.rtVitalList[0],
    viewModel.gestureModel,
    viewModel.spotCheckMonitorMachine.currTime,
    viewModel.spotCheckMonitorMachine.graphNotifier,
    gridColor: Colors.black12,
    graphLineColor: Colors.red.shade700,
  );
  Padding padding1 = Padding(
    child: liveGraph,
    padding: EdgeInsets.all(2),
  );

  row1.add(Expanded(
    child: Container(
      height: 100,
      child: Consumer<WebSocketSimulation>(builder: (context, model, _) {
        return MeterWidget(
          color: Colors.red,
          label: 'Pulse',
          ranges: [40, 50, 60, 65, 75, 85],
          value: viewModel.pulse,
          unit: 'bpm',
          isMeterRound: false,
        );
      }),
    ),
  ));
  row1.add(VerticalDivider(width: 1.0));
  row1.add(Expanded(
    child: Container(
      height: 100,
      child: Consumer<WebSocketSimulation>(builder: (context, model, _) {
        return MeterWidget(
          color: Colors.blue,
          label: 'SpO2',
          ranges: [85, 90, 95, 100, 101, 102],
          value: viewModel.spo2,
          unit: '%',
          isMeterRound: true,
        );
      }),
    ),
  ));

  if (model == 'PC-303') {
    row1.add(VerticalDivider(width: 1.0));
    row1.add(Expanded(
      child: Container(
        height: 100,
        child: Consumer<WebSocketSimulation>(builder: (context, model, _) {
          return MeterWidget(
            color: Colors.blue,
            label: 'Temperature',
            ranges: [85, 90, 95, 100, 101, 102],
            value: viewModel.temp,
            unit: '°C',
            isMeterRound: false,
          );
        }),
      ),
    ));
  }

  if (model == 'PC-60FW') {
    row1.add(VerticalDivider(width: 1.0));
    row1.add(Expanded(
      child: Container(
        height: 100,
        child: Consumer<WebSocketSimulation>(builder: (context, model, _) {
          return MeterWidget(
            color: Colors.blue,
            label: 'PI',
            ranges: [85, 90, 95, 100, 101, 102],
            value: viewModel.temp,
            unit: '%',
            isMeterRound: false,
          );
        }),
      ),
    ));
  }

  if (model == 'PC-102' || model == 'PC-202' || model == 'PC-303') {
    row2.add(Expanded(
      child: Container(
        height: 100,
        child: Consumer<WebSocketSimulation>(builder: (context, model, _) {
          return MeterWidget(
            color: Colors.red,
            label: 'Systolic',
            ranges: [70, 80, 90, 120, 140, 150],
            value: viewModel.systolic,
            unit: 'mmHg',
            isMeterRound: false,
          );
        }),
      ),
    ));
    row2.add(VerticalDivider(width: 1.0));
    row2.add(Expanded(
      child: Container(
        height: 100,
        child: Consumer<WebSocketSimulation>(builder: (context, model, _) {
          return MeterWidget(
            color: Colors.blue,
            label: 'Diastolic',
            ranges: [50, 60, 70, 100, 110, 120],
            value: viewModel.diastolic,
            unit: 'mmHg',
            isMeterRound: true,
          );
        }),
      ),
    ));
  }

  column.add(Row(
    children: row1,
  ));

  if (model == 'PC-102' || model == 'PC-202' || model == 'PC-303') {
    column.add(Divider(height: 1.0));
    column.add(Row(
      children: row2,
    ));
  }

  column.add(Divider(height: 1.0));
  column.add(Container(
    width: double.infinity,
    height: 150,
    color: Colors.white,
    child: padding1,
  ));

  if (model == 'PC-202' || model == 'PC-303') {
    Widget liveGraph = MakeLiveGraph(
      dataBuffer: viewModel.ecgBuffer,
      label: 'ECG-I',
      showGrids: true,
      sampleRate: 50, ///////////////////////////////////////////
      yMin: -1.5,
      yMax: 1.5,
      yGridSmallSpacing: 0.1,
      yGridLargeSpacing: 0.5,
      xGridSmallSpacing: 0.04,
      xGridLargeSpacing: 0.2,
      pixelAspecratio: 2.5,
    );
    Padding padding2 = Padding(
      child: liveGraph,
      padding: EdgeInsets.all(2),
    );

    column.add(Container(
      width: double.infinity,
      height: 150,
      color: Colors.white,
      child: padding2,
    ));
  }

  return Padding(
    padding: const EdgeInsets.all(8.0),
    child: Container(
      width: double.infinity,
      color: Colors.green,
      child: Padding(
        padding: const EdgeInsets.all(5.0),
        child: Column(
          children: column,
        ),
      ),
    ),
  );
}
