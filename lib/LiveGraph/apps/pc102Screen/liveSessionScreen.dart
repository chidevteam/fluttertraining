import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:universal/LiveGraph/apps/pc102Screen/pc102_landscape.dart';
import 'package:universal/LiveGraph/apps/pc102Screen/pc102_portrait.dart';
import 'package:universal/LiveGraph/apps/pc102Screen/webSocket.dart';

class CreateHome extends StatelessWidget {
  final String model;
  final String url;
  final String observationId;

  const CreateHome(this.model, this.url, this.observationId);

  @override
  Widget build(BuildContext context) {
    WebSocketSimulation viewModel = WebSocketSimulation(url, observationId);
    Scaffold scaffold = Scaffold(
      appBar: MediaQuery.of(context).orientation == Orientation.landscape
          ? null // show nothing in lanscape mode
          : AppBar(
              title: Center(child: Text(model)),
              backgroundColor: Colors.red,
            ),
      backgroundColor: Colors.black,
      body: SafeArea(
          child: MediaQuery.of(context).orientation == Orientation.portrait
              ? Pc102PortraitWidget(
                  context, viewModel, model) // show nothing in lanscape mode
              : Pc102LanscapeWidget(context, viewModel, model)),
    );
    return ChangeNotifierProvider(
        create: (BuildContext context) => viewModel, child: scaffold);
  }
}
