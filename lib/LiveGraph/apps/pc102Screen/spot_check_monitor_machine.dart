import '../realtimegraph/graph_models/rt_machine.dart';
import '../realtimegraph/graph_models/rt_vital.dart';

class SpotCheckMonitor extends RtMachine {
  List<RtVital> rtVitalList = [];

  SpotCheckMonitor() {
    maxTimeBuffer = 1 * 60;
    machineDisplayFPS = 15.0;
    rtVitalList.add(RtVital(
        machine: this,
        vitalName: "TOCO",
        sampleRate: 1.5,
        samplesPerTimer: 1, //used for generation
        yMin: -10,
        yMax: 100,
        autoAdjustGrid: false,
        showGrid: true,
        xMax: maxTimeBuffer, ///////////////////////
        gap: 1.0,
        title: "Spotcheck Monitor",
        units: "%",
        xLabel: "time (sec)",
        yLabel: "Percentage",
        yLabelDigits: 2,
        yLabelDigitsAfterDecimal: 0));
    initVars(rtVitalList);
  }
}
