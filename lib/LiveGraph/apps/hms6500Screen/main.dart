import 'package:flutter/material.dart';
import 'package:universal/LiveGraph/apps/T001_ecg/main.dart';
// import 'package:provider/provider.dart';
// import 'package:universal/LiveGraph/apps/hms6500Screen/hms6500_landscape.dart';
import 'package:universal/LiveGraph/apps/hms6500Screen/hms6500_portrait.dart';
// import 'package:universal/LiveGraph/apps/hms6500Screen/webSocket.dart';
import 'package:universal/chart_performance/chart1/live_line_chart.dart';
import 'package:universal/chart_performance/chart2/live_line_chart.dart';
import 'package:universal/chart_performance/chart3/MyPainter.dart';

void main() {
  runApp(MyApp());
}

// ignore: must_be_immutable
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    print("My app build called");
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: CreateHome(),
    );
  }
}

class CreateHome extends StatelessWidget {
  final List<String> item = [
    'Sir Atiq Chart 1',
    'Sir Atiq Chart 2',
    'Point Tracking Visualizer',
    'Own LiveCharts',
    'Sir Atiq Chart 1 Test',
    'Sir Atiq Chart 2 Test',
    'Point Tracking Visualizer 2',
    'Point Tracking Visualizer Test',
    'BASE'
  ];

  CreateHome({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MediaQuery.of(context).orientation == Orientation.landscape
          ? null // show nothing in lanscape mode
          : AppBar(
              title: Text('HMS6500'),
              backgroundColor: Colors.red,
            ),
      backgroundColor: Colors.white,
      body: SafeArea(
        child: ListView.separated(
          itemBuilder: (context, position) {
            return ListTile(
              title: Text(item[position]),
              onTap: () {
                navigateTo(context, position);
              },
            );
          },
          separatorBuilder: (context, position) {
            return Divider(
              color: Colors.black,
            );
          },
          itemCount: item.length,
        ),
      ),
    );
  }

  void navigateTo(BuildContext context, int index) {
    switch (index) {
      case 0:
        {
          goTo(context, LiveLineChart1(false), false);
        }
        break;
      case 1:
        {
          goTo(context, LiveLineChart2(false), false);
        }
        break;
      case 2:
        {
          goTo(context, MyPainter(false, false), false);
        }
        break;
      case 3:
        {
          goTo(context, Text(''), true);
        }
        break;
      case 4:
        {
          goTo(context, LiveLineChart1(true), false);
        }
        break;
      case 5:
        {
          goTo(context, LiveLineChart2(true), false);
        }
        break;
      case 6:
        {
          goTo(context, MyPainter(false, true), false);
        }
        break;
      case 7:
        {
          goTo(context, MyPainter(true, false), false);
        }
        break;
      case 8:
        {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => createHome()));
          // goTo(context, MyPainter(true, false), false);
        }
        break;
    }
  }

  void goTo(BuildContext context, Widget widget, bool isLiveGraph) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => HMS6500PortraitWidget(widget, isLiveGraph)));
  }
}

// MediaQuery.of(context).orientation == Orientation.portrait
// ? SingleChildScrollView(child: HMS6500PortraitWidget(context, viewModel)) // show nothing in lanscape mode
// : SingleChildScrollView(child: HMS6500LanscapeWidget(context, viewModel))
