import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:universal/LiveGraph/apps/hms6500Screen/webSocket.dart';
import 'package:universal/LiveGraph/lib/value_meter.dart';
import 'package:universal/LiveGraph/lib/live_graph.dart';
// import 'package:universal/chart_performance/chart2/live_line_chart.dart';
// import 'package:universal/chart_performance/chart3/MyPainter.dart';

// ignore: must_be_immutable
class HMS6500PortraitWidget extends StatelessWidget {
  WebSocketSimulation viewModel = WebSocketSimulation();

  Widget widget;
  bool isLiveChart;

  HMS6500PortraitWidget(this.widget, this.isLiveChart);

  @override
  Widget build(BuildContext context) {
    Widget ecgIGraph = MakeLiveGraph(
      dataBuffer: viewModel.ecgIBuffer,
      label: 'ECG-I',
      showGrids: false,
      sampleRate: viewModel.sampleRate,
      yMin: -1.5,
      yMax: 1.5,
      yGridSmallSpacing: 0.1,
      yGridLargeSpacing: 0.5,
      xGridSmallSpacing: 0.04,
      xGridLargeSpacing: 0.2,
      pixelAspecratio: 2.5,
    );
    Padding padding1 = Padding(
      child: ecgIGraph,
      padding: EdgeInsets.all(2),
    );

    Widget ecgIIGraph = MakeLiveGraph(
      dataBuffer: viewModel.ecgIIBuffer,
      label: 'ECG-II',
      showGrids: false,
      sampleRate: viewModel.sampleRate,
      yMin: -1.5,
      yMax: 1.5,
      yGridSmallSpacing: 0.1,
      yGridLargeSpacing: 0.5,
      xGridSmallSpacing: 0.04,
      xGridLargeSpacing: 0.2,
      pixelAspecratio: 2.5,
    );
    Padding padding2 = Padding(
      child: ecgIIGraph,
      padding: EdgeInsets.all(2),
    );

    Widget ecgIIIGraph = MakeLiveGraph(
      dataBuffer: viewModel.ecgIIIBuffer,
      label: 'ECG-III',
      showGrids: false,
      sampleRate: viewModel.sampleRate,
      yMin: -1.5,
      yMax: 1.5,
      yGridSmallSpacing: 0.1,
      yGridLargeSpacing: 0.5,
      xGridSmallSpacing: 0.04,
      xGridLargeSpacing: 0.2,
      pixelAspecratio: 2.5,
    );
    Padding padding3 = Padding(
      child: ecgIIIGraph,
      padding: EdgeInsets.all(2),
    );

    Widget ecgaVRGraph = MakeLiveGraph(
      dataBuffer: viewModel.ecgaVRBuffer,
      label: 'ECG-aVR',
      showGrids: false,
      sampleRate: viewModel.sampleRate,
      yMin: -1.5,
      yMax: 1.5,
      yGridSmallSpacing: 0.1,
      yGridLargeSpacing: 0.5,
      xGridSmallSpacing: 0.04,
      xGridLargeSpacing: 0.2,
      pixelAspecratio: 2.5,
    );
    Padding padding4 = Padding(
      child: ecgaVRGraph,
      padding: EdgeInsets.all(2),
    );

    Widget ecgaVLGraph = MakeLiveGraph(
      dataBuffer: viewModel.ecgaVLBuffer,
      label: 'ECG-aVL',
      showGrids: false,
      sampleRate: viewModel.sampleRate,
      yMin: -1.5,
      yMax: 1.5,
      yGridSmallSpacing: 0.1,
      yGridLargeSpacing: 0.5,
      xGridSmallSpacing: 0.04,
      xGridLargeSpacing: 0.2,
      pixelAspecratio: 2.5,
    );
    Padding padding5 = Padding(
      child: ecgaVLGraph,
      padding: EdgeInsets.all(2),
    );

    Widget ecgaVFGraph = MakeLiveGraph(
      dataBuffer: viewModel.ecgaVFBuffer,
      label: 'ECG-aVF',
      showGrids: false,
      sampleRate: viewModel.sampleRate,
      yMin: -1.5,
      yMax: 1.5,
      yGridSmallSpacing: 0.1,
      yGridLargeSpacing: 0.5,
      xGridSmallSpacing: 0.04,
      xGridLargeSpacing: 0.2,
      pixelAspecratio: 2.5,
    );
    Padding padding6 = Padding(
      child: ecgaVFGraph,
      padding: EdgeInsets.all(2),
    );

    Widget ecgVGraph = MakeLiveGraph(
      dataBuffer: viewModel.ecgVBuffer,
      label: 'ECG-V',
      showGrids: false,
      sampleRate: viewModel.sampleRate,
      yMin: -1.5,
      yMax: 1.5,
      yGridSmallSpacing: 0.1,
      yGridLargeSpacing: 0.5,
      xGridSmallSpacing: 0.04,
      xGridLargeSpacing: 0.2,
      pixelAspecratio: 2.5,
    );
    Padding padding7 = Padding(
      child: ecgVGraph,
      padding: EdgeInsets.all(2),
    );

    Scaffold scaffold = Scaffold(
      appBar: AppBar(
        title: Text('HMS6500 Portrait Mode'),
        backgroundColor: Colors.red,
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              width: double.infinity,
              color: Colors.green,
              child: Padding(
                padding: const EdgeInsets.all(5.0),
                child: isLiveChart
                    ? Column(
                        children: [
                          Row(
                            children: [
                              Expanded(
                                child: Container(
                                  height: 100,
                                  child: Consumer<WebSocketSimulation>(
                                      builder: (context, model, _) {
                                    return MeterWidget(
                                      color: Colors.red,
                                      label: 'Pulse',
                                      ranges: [40, 50, 60, 65, 75, 85],
                                      value: viewModel.pulse,
                                      unit: 'bpm',
                                      isMeterRound: false,
                                    );
                                  }),
                                ),
                              ),
                              VerticalDivider(width: 1.0),
                              Expanded(
                                child: Container(
                                  height: 100,
                                  child: Consumer<WebSocketSimulation>(
                                      builder: (context, model, _) {
                                    return MeterWidget(
                                      color: Colors.blue,
                                      label: 'RR',
                                      ranges: [85, 90, 95, 100, 101, 102],
                                      value: viewModel.rr,
                                      unit: 'bpm',
                                      isMeterRound: true,
                                    );
                                  }),
                                ),
                              ),
                            ],
                          ),
                          Divider(height: 1.0),
                          Row(
                            children: [
                              Expanded(
                                child: Container(
                                  height: 100,
                                  child: Consumer<WebSocketSimulation>(
                                      builder: (context, model, _) {
                                    return MeterWidget(
                                      color: Colors.red,
                                      label: 'Systolic',
                                      ranges: [70, 80, 90, 120, 140, 150],
                                      value: viewModel.systolic,
                                      unit: 'mmHg',
                                      isMeterRound: false,
                                    );
                                  }),
                                ),
                              ),
                              VerticalDivider(width: 1.0),
                              Expanded(
                                child: Container(
                                  height: 100,
                                  child: Consumer<WebSocketSimulation>(
                                      builder: (context, model, _) {
                                    return MeterWidget(
                                      color: Colors.blue,
                                      label: 'Diastolic',
                                      ranges: [50, 60, 70, 100, 110, 120],
                                      value: viewModel.diastolic,
                                      unit: 'mmHg',
                                      isMeterRound: true,
                                    );
                                  }),
                                ),
                              ),
                            ],
                          ),
                          Divider(height: 1.0),
                          Container(
                            width: double.infinity,
                            height: 100,
                            color: Colors.white,
                            child: padding1,
                          ),
                          Divider(height: 1.0),
                          Container(
                            width: double.infinity,
                            height: 100,
                            color: Colors.white,
                            child: padding2,
                          ),
                          Divider(height: 1.0),
                          Container(
                            width: double.infinity,
                            height: 100,
                            color: Colors.white,
                            child: padding3,
                          ),
                          Divider(height: 1.0),
                          Container(
                            width: double.infinity,
                            height: 100,
                            color: Colors.white,
                            child: padding4,
                          ),
                          Divider(height: 1.0),
                          Container(
                            width: double.infinity,
                            height: 100,
                            color: Colors.white,
                            child: padding5,
                          ),
                          Divider(height: 1.0),
                          Container(
                            width: double.infinity,
                            height: 100,
                            color: Colors.white,
                            child: padding6,
                          ),
                          Divider(height: 1.0),
                          Container(
                            width: double.infinity,
                            height: 100,
                            color: Colors.white,
                            child: padding7,
                          ),
                        ],
                      )
                    : Column(
                        children: [
                          SizedBox(
                            width: double.infinity,
                            height: 300,
                            child: widget,
                          ),
                          SizedBox(
                            width: double.infinity,
                            height: 300,
                            child: widget,
                          ),
                          SizedBox(
                            width: double.infinity,
                            height: 300,
                            child: widget,
                          ),
                          SizedBox(
                            width: double.infinity,
                            height: 300,
                            child: widget,
                          ),
                          SizedBox(
                            width: double.infinity,
                            height: 300,
                            child: widget,
                          ),
                          SizedBox(
                            width: double.infinity,
                            height: 300,
                            child: widget,
                          ),
                        ],
                      ),
              ),
            ),
          ),
        ),
      ),
    );

    return ChangeNotifierProvider(
        create: (BuildContext context) => viewModel, child: scaffold);
  }
}
