import 'dart:async';
import 'dart:convert';

import 'package:stacked/stacked.dart';
import 'package:web_socket_channel/status.dart' as status;
import 'package:web_socket_channel/web_socket_channel.dart';

class WebSocketSimulation extends BaseViewModel {
  //Timer related vars
  List<num> plethBuffer = [];
  List<num> ecgIBuffer = [];
  List<num> ecgIIBuffer = [];
  List<num> ecgIIIBuffer = [];
  List<num> ecgaVRBuffer = [];
  List<num> ecgaVLBuffer = [];
  List<num> ecgaVFBuffer = [];
  List<num> ecgVBuffer = [];

  Timer? timer = null;
  Timer? socketKeepAliveTimer = null;

  double sampleRate = 256; // 100
  double timerFreq = 1.0;
  double signalFreq = 1.0;
  late int timerRepeatTimeMSec; // 250 milliseconds
  late double samplesPerTimer; // 25
  late double numberOfSec; // 6
  double T = 0;

  num rr = 0;
  num pulse = 0;
  num systolic = 0;
  num diastolic = 0;

  late Map<String, dynamic> map;

  late final channel;

  WebSocketSimulation() {
    channel = WebSocketChannel.connect(
      Uri.parse(
          'wss://charms-qa.cognitivehealthintl.com/ws/cmclient?token=CUfF7U7QXtdCUl8KHUHbaySM3mVXKK0I4mZJ_awsSjiNzqgr-ssVanEljWJwMDxBN3iXtXYIxnbxS7CsxIC_wzAgJKBYfIF7SBCpyzL-5quc5E_ScagAWEbarHWzevdkWdW-b87FnwIFXF9KCcQy0k9-8DyGyLqAUT8oBSMc9U0='),
          // 'ws://10.8.0.61:7000'),
    );

    timerRepeatTimeMSec = (1000.0 ~/ timerFreq).toInt();
    samplesPerTimer = sampleRate * (timerRepeatTimeMSec / 1000.0);

    var socketOnOpen = {
      "data": {"observation_id": 55141},
      "event": "Start"
    };
    var jsonText = jsonEncode(socketOnOpen);
    channel.sink.add(jsonText);

    startTimer();

    channel.stream.listen((message) {
      // print('Received message from socket $message');
      map = jsonDecode(message)['data'];

      if (map.containsKey('RR')) {
        rr = map['RR'];
        // print('SpO2+++++++++++$spo2');
      }
      if (map.containsKey('HR')) {
        pulse = map['HR'];
        // print('Pulse+++++++++++$pulse');
        // notifyListeners();
      }
      // if (map.containsKey('Systolic')) {
      //   systolic = map['Systolic'];
      // }
      // if (map.containsKey('Diastolic')) {
      //   diastolic = map['Diastolic'];
      //   notifyListeners();
      // }
      // if (map.containsKey('Pleth')) {
      //   plethBuffer.addAll(map['Pleth'].cast<num>());
      //   // plethBuffer.add((map['Pleth'][0])/100.0);
      //   // plethBuffer.add((map['Pleth'][1])/100.0);
      // }
      if (map.containsKey('ECG-I')) {
        ecgIBuffer.addAll(map['ECG-I'].cast<num>());
        ecgIIBuffer.addAll(map['ECG-II'].cast<num>());
        ecgVBuffer.addAll(map['ECG-V'].cast<num>());

        for (int i = 0; i < sampleRate; i++) {
          ecgIIIBuffer.add(map['ECG-II'][i] - map['ECG-I'][i]);
          ecgaVRBuffer.add((-map['ECG-I'][i] - map['ECG-II'][i]) / 2.0);
          ecgaVLBuffer.add((map['ECG-I'][i] - map['ECG-II'][i]) / 2.0);
          ecgaVFBuffer.add((map['ECG-II'][i] - map['ECG-I'][i]) / 2.0);
        }
      }
    });

    print("View Model Created");
  }

  void startTimer() {
    print('Start Timer Main');

    if (timer != null) {
      timer?.cancel();
    }
    socketKeepAliveTimer =
        Timer.periodic(Duration(milliseconds: 3000), keepAliveData);
  }

  void stopTimer() {
    print('Stopping Timer Main');
    if (timer != null) {
      timer?.cancel();
      timer = null;
    }
  }

  @override
  void dispose() {
    timer?.cancel();
    plethBuffer.clear();
    channel.sink.close(status.goingAway);
    super.dispose();
  }

  void keepAliveData(Timer timer) {
    var keepAlive = {
      'data': {'NetWork Connection': ''},
      'event': 'KeepAlive'
    };
    var jsonText = jsonEncode(keepAlive);

    channel.sink.add(jsonText);
  }
}
