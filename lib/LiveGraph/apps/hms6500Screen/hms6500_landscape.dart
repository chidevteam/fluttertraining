import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:universal/LiveGraph/apps/hms6500Screen/webSocket.dart';
import 'package:universal/LiveGraph/lib/value_meter.dart';
import 'package:universal/LiveGraph/lib/live_graph.dart';

Widget HMS6500LandscapeWidget(BuildContext context) {

  WebSocketSimulation viewModel = WebSocketSimulation();
  Widget liveGraph = MakeLiveGraph(
    dataBuffer: viewModel.plethBuffer,
    label: 'PLETH',
    showGrids: true,
    sampleRate: viewModel.sampleRate,
    yMin: -150,
    yMax: 150,
    yGridSmallSpacing: 0.1,
    yGridLargeSpacing: 50,
    xGridSmallSpacing: 0.04,
    xGridLargeSpacing: 0.2,
    pixelAspecratio: 250,
  );

  Padding padding1 = Padding(
    child: liveGraph,
    padding: EdgeInsets.all(2),
  );

  return Padding(
    padding: const EdgeInsets.all(8.0),
    child: Container(
      width: double.infinity,
      height: double.infinity,
      color: Colors.green,
      child: Padding(
        padding: const EdgeInsets.all(5.0),
        child: Row(
          children: [
            Column(
              children: [
                Expanded(
                  child: Container(
                    width: 100,
                    child: Consumer<WebSocketSimulation>(
                        builder: (context, model, _) {
                          return MeterWidget(
                            color: Colors.red,
                            label: 'Pulse',
                            ranges: [40, 50, 60, 65, 75, 85],
                            value: viewModel.pulse,
                            unit: 'bpm',
                            isMeterRound: false,
                          );
                        }),
                  ),
                ),
                Divider(height: 1.0),
                Expanded(
                  child: Container(
                    width: 100,
                    child: Consumer<WebSocketSimulation>(
                        builder: (context, model, _) {
                          return MeterWidget(
                            color: Colors.blue,
                            label: 'RR',
                            ranges: [85, 90, 95, 100, 101, 102],
                            value: viewModel.rr,
                            unit: 'bpm',
                            isMeterRound: true,
                          );
                        }),
                  ),
                ),
                Divider(height: 1.0),
                Expanded(
                  child: Container(
                    width: 100,
                    child: Consumer<WebSocketSimulation>(
                        builder: (context, model, _) {
                          return MeterWidget(
                            color: Colors.red,
                            label: 'Systolic',
                            ranges: [70, 80, 90, 120, 140, 150],
                            value: viewModel.systolic,
                            unit: 'mmHg',
                            isMeterRound: false,
                          );
                        }),
                  ),
                ),
                Divider(height: 1.0),
                Expanded(
                  child: Container(
                    width: 100,
                    child: Consumer<WebSocketSimulation>(
                        builder: (context, model, _) {
                          return MeterWidget(
                            color: Colors.blue,
                            label: 'Diastolic',
                            ranges: [50, 60, 70, 100, 110, 120],
                            value: viewModel.diastolic,
                            unit: 'mmHg',
                            isMeterRound: true,
                          );
                        }),
                  ),
                ),
              ],
            ),
            VerticalDivider(width: 1.0,),
            Expanded(
              child: Container(
                color: Colors.white,
                child: padding1,
              ),
            ),
          ],
        ),
      ),
    ),
  );
}
