import 'package:flutter/material.dart';
// import 'dart:async';
import '../utils/grid_utils.dart';
import '../utils/text_utils.dart';
import '../graph_models/rt_vital.dart';
import '../graph_models/grid_model.dart';
import '../graph_models/gesture_model.dart';
// import 'gap_compute.dart';

class PainterGridGraph extends CustomPainter {
  GestureModel gestureModel;
  GridModel gridModel;
  RtVital rtVital;
  Rect margin;
  ValueNotifier<int> gridNotifier;
  Color gridColor;
  Color titleColor;
  Color labelColor;

  PainterGridGraph(this.gestureModel, this.gridModel, this.rtVital, this.margin,
      this.gridNotifier, this.gridColor, this.titleColor, this.labelColor)
      : super(repaint: gridNotifier);

  bool initVars(double w, double h) {
    Rect dataVal =
        Rect.fromLTRB(0, rtVital.yMin!, rtVital.xMax!, rtVital.yMax!);

    //grid model will store this
    Rect localRect = Rect.fromLTRB(
        margin.left, margin.top, w - margin.right, h - margin.bottom);

    gridModel.computeGrid(dataVal, localRect, Offset(0, 0), Offset(1, 1));

    if (rtVital.isECG == true) {
      gridModel.updateForECG(rtVital);
    }

    return true;
  }

  @override
  void paint(Canvas canvas, Size size) {
    // print("here I am in paint Grid");

    initVars(size.width, size.height);
    var paint = Paint()
      ..color = Color(0xff995588)
      ..strokeWidth = 2
      ..style = PaintingStyle.stroke;

    canvas.drawRect(gridModel.activeRect, paint);
    drawText(canvas, rtVital.vitalName, gridModel.activeRect.left,
        gridModel.activeRect.top / 2, 0,
        fontColor: titleColor);

    drawXGrid(canvas, gridModel, 2, gridColor, labelColor,
        labels: true, vital: rtVital);
    drawYGrid(canvas, gridModel, 2, gridColor, labelColor,
        labels: true, vital: rtVital);
    if (rtVital.isECG == true) {
      drawXGridSmall(canvas, gridModel, 2, gridColor, labelColor,
          labels: true, vital: rtVital);
      drawYGridSmall(canvas, gridModel, 2, gridColor, labelColor,
          labels: true, vital: rtVital);
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}
