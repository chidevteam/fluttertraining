// import 'package:flutter/material.dart';
// // import 'custom_theme.dart';
// import 'realtime_graph.dart';
// import '../graph_models/gesture_model.dart';
// import '../graph_models/rt_vital_model.dart';
// import '../gesture_detector/chi_gesture_detector.dart';

// // ignore: must_be_immutable
// class RealtimeGraphHolder extends StatelessWidget {
//   late ValueNotifier<int> notifier = ValueNotifier(5);

//   double width;
//   double height;
//   final RTvitalModel rtVital;
//   final List<double> dataBuf;
//   GestureModel m_gestureModel = GestureModel();

//   RealtimeGraphHolder(this.rtVital, this.dataBuf,
//       {Key? key,
//       this.width = double.infinity,
//       this.height = double.infinity}) {}

//   @override
//   Widget build(BuildContext context) {
//     // initVars();

//     return Padding(
//       padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
//       child: CHIGestureDetector(
//         onUpdate: (int m, GestureModel gestureModel) {
//           m_gestureModel = gestureModel;
//           notifier.value = m;
//         },
//         child: getValueListenableBuilder(context, notifier),
//       ),
//     );
//   }

//   String toTwoDecimal(double v) {
//     return v.toStringAsFixed(2);
//   }

//   Widget getValueListenableBuilder(ctx, notifier) {
//     // var color = graphStrokes();

//     return ValueListenableBuilder<int>(
//       builder: (BuildContext context, int value, Widget? complexWgt) {
//         return ClipRect(
//           child: Container(
//             width: width,
//             height: height,
//             color: Colors.grey.shade100,
//             child: RealTimeGraph(rtVital, m_gestureModel, 3.2,
//                 height: height, width: width),
//           ),
//         );
//       },
//       valueListenable: notifier,
//     );
//   }
// }
