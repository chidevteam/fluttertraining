// import 'dart:math';
// import "realtime_graph/painter_line_graph.dart";

import '../graph_models/rt_vital.dart';

// main() {
//   print("hello world");
//   double sampleRate = 50;
//   double currTime = 19.1;
//   double windowSize = 6;
//   double gapSize = 1;
//   RTvitalModel rtVital = RTvitalModel.RtPleth();

//   Map res =
//       indexesForLeftRightCurves(currTime, windowSize, gapSize, sampleRate);
//   print(res);
//   int edgeIndex = res["edgeIndex"];
//   int currIndex = res["currIndex"];
//   int startIndex = res["startIndex"];

//   print("X  0 ->> ${currIndex - edgeIndex}");
//   print("DX $edgeIndex ->> ${currIndex - edgeIndex + edgeIndex}");

//   int nStart = currIndex - edgeIndex + (gapSize * rtVital.sampleRate).toInt();
//   int nEnd = (rtVital.xMax! * rtVital.sampleRate).toInt();
//   print("X  $nStart ->> ${nEnd}");
//   print("DX ${nStart + startIndex} ->> ${nEnd + edgeIndex - nEnd}");
// }

Map indexesForLeftRightCurves(
    double currTime, double windowSize, double gapSize, double sampleRate) {
  double startTime = currTime - (windowSize - gapSize);
  int quotient = (currTime / windowSize).floor();
  // print("curr $currTime  prev ${startTime}");
  int quotient2 = (startTime / windowSize).floor();
  // print("$quotient $quotient2");

  double edgeTime = quotient.toDouble() * windowSize;
  // double remainder = currTime - edgeTime;
  // print("rem = $remainder");

  // print("startTime = $startTime");
  int startIndex = (startTime * sampleRate).toInt();
  int currIndex = (currTime * sampleRate).toInt();
  int edgeIndex = (edgeTime * sampleRate).toInt();

  return {
    "startTime": startTime,
    "currTime": currTime,
    "edgeIndex": edgeIndex,
    "currIndex": currIndex,
    "startIndex": startIndex,
    "split": (quotient != quotient2)
  };
}
