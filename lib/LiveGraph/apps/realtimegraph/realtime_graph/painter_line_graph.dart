import 'package:flutter/material.dart';
import 'dart:async';
import '../utils/grid_utils.dart';
import '../utils/text_utils.dart';
import '../graph_models/rt_vital.dart';
import '../graph_models/grid_model.dart';
import '../graph_models/gesture_model.dart';
import 'gap_compute.dart';

class PainterLineGraph extends CustomPainter {
  Rect margin;
  RtVital rtVital;
  ValueNotifier<int> graphNotifier;
  // ValueNotifier<int> gridNotifier;

  GestureModel gestureModel;
  late List<double> dataBuf;

  Timer? timer = null; // the display timer
  GridModel gridModel;

  double currTime; // = 0;////////////////

  late double fps; // = 15.0;
  // late double delayInSec;/////////////
  // late double bufferInSec;////////////
  late Map graphIndices;

  Color labelColor;
  Color graphLineColor;
  Color markerColor;

  PainterLineGraph(
      this.gestureModel,
      this.gridModel,
      this.rtVital,
      this.margin,
      this.graphNotifier,
      this.currTime,
      this.labelColor,
      this.graphLineColor,
      this.markerColor)
      : super(repaint: graphNotifier) {
    dataBuf = rtVital.dataBuf;
  }

  bool initVars(double w, double h) {
    currTime = rtVital.machine.currTime;
    // print("here I am in Line Graph $currTime");
    int startIndex =
        ((currTime - rtVital.xMax! + rtVital.gap) * rtVital.sampleRate).toInt();
    if (startIndex < 0) startIndex = 0;
    int endIndex = (currTime * rtVital.sampleRate).toInt();

    // if (endIndex < 0) endIndex = 0;
    // if (endIndex > dataBuf.length - 1) return false;
    List<double> data = dataBuf.getRange(startIndex, endIndex).toList();
    // print(
    //     "here I am in Line Graph $currTime $endIndex ${data.length} ${dataBuf.length}");
    if (data.length < 1) return false;

    double minD = minimum(data);
    double maxD = maximum(data);
    bool flag = false;
    if (minD != rtVital.yMin!) {
      rtVital.yMin = minD;
      flag = true;
    }
    if (maxD != rtVital.yMax!) {
      rtVital.yMax = maxD;
      flag = true;
    }

    if (flag && rtVital.autoAdjustGrid!) {
      rtVital.drawGrid = flag;
      Rect dataRect =
          Rect.fromLTRB(0, rtVital.yMin!, rtVital.xMax!, rtVital.yMax!);

      //grid model will store this
      Rect localRect = Rect.fromLTRB(
          margin.left, margin.top, w - margin.right, h - margin.bottom);

      gridModel.computeGrid(dataRect, localRect, Offset(0, 0), Offset(1, 1));
    }

    graphIndices = indexesForLeftRightCurves(
        currTime, rtVital.xMax!, rtVital.gap, rtVital.sampleRate);

    return true;
  }

  @override
  void paint(Canvas canvas, Size size) {
    bool success = initVars(size.width, size.height);
    if (success == false) return;

    double delayInSec = rtVital.delayInSec;
    drawText(canvas, "Delay: ${delayInSec.toStringAsFixed(1)}",
        gridModel.activeRect.left + 40, gridModel.activeRect.top / 2, 0,
        fontColor: labelColor);

    double speedFactor = rtVital.machine.speedFactor;
    drawText(canvas, "Speed: $speedFactor", gridModel.activeRect.left + 100,
        gridModel.activeRect.top / 2, 0,
        fontColor: labelColor);

    drawText(canvas, "Buffer:${rtVital.bufferInSec}",
        gridModel.activeRect.right - 80, gridModel.activeRect.top / 2, 0,
        fontColor: labelColor);

    drawCurveSplit(canvas, graphIndices, rtVital.gap);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }

  void drawCurveSplit(Canvas canvas, Map res, double gapSize) {
    Paint paint1 = Paint()
      ..color = graphLineColor
      ..style = PaintingStyle.stroke
      ..strokeWidth = 2;

    Paint paint2 = Paint()
      ..color = markerColor
      ..style = PaintingStyle.stroke
      ..strokeWidth = 2;

    // if (res["split"]) paint1..color = Colors.blue;
    double mx = gridModel.mx;
    double cx = gridModel.cx;
    double my = gridModel.my;
    double cy = gridModel.cy;

    Path p = Path();
    Path p2 = Path();

    int edgeIndex = res["edgeIndex"];
    int currIndex = res["currIndex"];
    int startIndex = res["startIndex"];

    int nGapSize = (gapSize * rtVital.sampleRate).toInt();
    int nStart = currIndex - edgeIndex + nGapSize;
    int nEnd = (rtVital.xMax! * rtVital.sampleRate).toInt();

    int offset = startIndex - edgeIndex;
    List<double> dataBuf2 = rtVital.dataBuf2;
    double xx, px, py;

    if (offset < 0) {
      offset = 0;
      edgeIndex -= 1;
      if (edgeIndex < 0) {
        edgeIndex = 0;
      }
    }
    xx = offset / rtVital.sampleRate;
    px = mx * xx + cx;
    if (offset + edgeIndex >= dataBuf.length) {
      print("Problem found");
    }
    py = my * dataBuf[offset + edgeIndex] + cy;
    p.moveTo(px, py);

    for (int i = offset + 1; i < currIndex - edgeIndex; i++) {
      double xx = i / rtVital.sampleRate;
      px = mx * xx + cx;
      py = my * dataBuf[i + edgeIndex] + cy;
      p.lineTo(px, py);
      if(dataBuf2.isNotEmpty) {
        if (dataBuf2[i + edgeIndex] > 0) {
          p2.moveTo(px, gridModel.activeRect.top);
          p2.lineTo(px, gridModel.activeRect.bottom);
        }
      }
    }

    if ((currIndex < nEnd)) {
      canvas.drawPath(p, paint1);
      canvas.drawPath(p2, paint2);
      return;
    }
    xx = nStart / rtVital.sampleRate;
    px = mx * xx + cx;
    py = my * dataBuf[nStart + (edgeIndex - nEnd)] + cy;

    if (nStart < nEnd) {
      p.moveTo(px, py);
    }

    for (int i = nStart + 1; i < nEnd; i++) {
      xx = i / rtVital.sampleRate;
      px = mx * xx + cx;
      py = my * dataBuf[i + (edgeIndex - nEnd)] + cy;
      p.lineTo(px, py);
      if(dataBuf2.isNotEmpty) {
        if (dataBuf2[i + (edgeIndex - nEnd)] > 0) {
          p2.moveTo(px, gridModel.activeRect.top);
          p2.lineTo(px, gridModel.activeRect.bottom);
        }
      }
    }
    canvas.drawPath(p, paint1);
    canvas.drawPath(p2, paint2);
  }
}
