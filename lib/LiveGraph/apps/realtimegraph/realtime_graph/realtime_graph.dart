import 'package:flutter/material.dart';
import '../graph_models/gesture_model.dart';
import '../graph_models/rt_vital.dart';
import 'painter_line_graph.dart';
import 'painter_grid_graph.dart';
import '../utils/grid_utils.dart';
import '../graph_models/grid_model.dart';

//  gridColor: Colors.black26,
//           titleColor: Colors.black,
//           labelColor: Colors.black,
//           graphLineColor: Colors.red,
// ignore: must_be_immutable
class RealTimeGraph extends StatelessWidget {
  RealTimeGraph(
      this.rtVital, this.gestureModel, this.currTime, this.graphNotifier,
      {this.height = double.infinity,
      this.width = double.infinity,
      this.gridColor = Colors.black26,
      this.titleColor = Colors.black,
      this.labelColor = Colors.black,
      this.markerColor = const Color.fromRGBO(0, 0, 255, 200),
      this.graphLineColor = Colors.red,
      this.backgroundColor = Colors.black12,
      Key? key})
      : super(key: key);
  double height;
  final double width;
  final RtVital rtVital;
  GestureModel gestureModel;
  double currTime;
  // bool drawGrid;

  ValueNotifier<int> graphNotifier; // = ValueNotifier(0);
  // ValueNotifier<int> gridNotifier; // = ValueNotifier(0);
  GridModel gridModel = GridModel();

  final Color gridColor;
  final Color titleColor;
  final Color labelColor;
  final Color markerColor;
  final Color graphLineColor;
  final Color backgroundColor;

  @override
  Widget build(BuildContext context) {
    print("Root Graph called");
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        String maxYlabelString = "9" + "9" * (rtVital.yLabelDigits!);
        Size size = calcTextSize(maxYlabelString, TextStyle(fontSize: 12));
        final Rect margins = Rect.fromLTRB(size.height * 4,//size.width + size.height * 4,
            size.height * 2, size.height, size.height * 2 + size.height);

        if (rtVital.isECG) {
          rtVital.xMax = 3.0;
        }

        double Wt = constraints.maxWidth;//MediaQuery.of(context).size.width; //total screen width
        double Wg = Wt - margins.left - margins.right;
        double xBigBoxes = 5.0; //want 5 big boxes per second
        double yBigBoxes = 2.0; // want 2 big boxes per mV
        double rp = rtVital.xMax! *
            xBigBoxes /
            ((rtVital.yMax! - rtVital.yMin!) *
                yBigBoxes); //check on landscape TODO:
        double Hg = Wg / rp;
        double Ht = Hg + margins.bottom + margins.top;
        if (rtVital.isECG) {
          height = Ht;
        }

        //for x title
        gridModel.textSize = size;


        return Container(
          color: Colors.black12,
          child: Stack(
            children: [
              RepaintBoundary(child: gridGraph(margins)),
              RepaintBoundary(child: lineGraph(margins)),
            ],
          ),
        );
      },
    );

  }

  CustomPaint gridGraph(Rect margins) {
    return CustomPaint(
      child: Container(
        width: width,
        height: height,
        // color: backgroundColor,
      ),
      foregroundPainter: PainterGridGraph(gestureModel, gridModel, rtVital,
          margins, rtVital.gridNotifier, gridColor, titleColor, labelColor),
    );
  }

  CustomPaint lineGraph(Rect margins) {
    return CustomPaint(
      child: Container(
        width: width,
        height: height,
        // color: backgroundColor,
      ),
      foregroundPainter: PainterLineGraph(
          gestureModel,
          gridModel,
          rtVital,
          margins,
          graphNotifier,
          currTime,
          labelColor,
          graphLineColor,
          markerColor),
    );
  }
}
