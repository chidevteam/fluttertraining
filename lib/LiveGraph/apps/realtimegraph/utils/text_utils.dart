import 'package:flutter/material.dart';
import 'grid_utils.dart';

void drawText(Canvas canvas, String name, double x, double y,
    double angleRotationInDegrees,
    {double fontSize = 12, Color fontColor = Colors.black}) {
  double angleRotationInRadians = -angleRotationInDegrees * 3.1415 / 180;
  Size size = calcTextSize(name, TextStyle(fontSize: 12));
  var paint1 = Paint()
    ..color = Color(0xffFFFF00)
    ..style = PaintingStyle.fill;

  // canvas.drawRect(Rect.fromLTWH(x, y, size.width, size.height), paint1);

  canvas.save();
  canvas.translate(x, y);
  canvas.rotate(angleRotationInRadians);
  TextSpan span = new TextSpan(
      style: new TextStyle(
          color: fontColor, fontSize: fontSize, fontFamily: 'Roboto'),
      text: name);
  TextPainter tp = new TextPainter(
      text: span, textAlign: TextAlign.left, textDirection: TextDirection.ltr);
  tp.layout();
  tp.paint(canvas, new Offset(0.0, 0.0));
  canvas.restore();
}
