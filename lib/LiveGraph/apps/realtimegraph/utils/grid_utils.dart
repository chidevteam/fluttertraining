import 'package:flutter/material.dart';
import 'package:universal/LiveGraph/apps/realtimegraph/graph_models/rt_vital.dart';
import 'text_utils.dart';
import '../graph_models/grid_model.dart';

/*
  vital will tell how many places after decimal
  

 */

void drawXGrid(Canvas canvas, GridModel gModel, double strokeWidth,
    Color gridColor, Color labelColor,
    {bool labels = false, RtVital? vital}) {
  Paint paint = Paint()
    ..color = gridColor
    ..strokeWidth = strokeWidth
    ..style = PaintingStyle.stroke;

  Path p = Path();
  double y0 = gModel.activeRect.top;
  double y1 = gModel.activeRect.bottom;

  for (double x = gModel.xMin; x < gModel.xMax + 1e-3; x += gModel.xDelta) {
    double px = x * gModel.mx + gModel.cx;
    // double xMaxPx = xMax * mx + cx;
    // double xDeltaPx = xDelta * mx;
    String x1 = "";
    if (gModel.xMax > 60) {
      int minute = x ~/ 60;
      int sec = x.toInt() % 60;
      x1 = "$minute:${sec.toString().padLeft(2, '0')}";
    } else {
      x1 = x.toStringAsFixed(1);
    }
    p.moveTo(px, y0);
    p.lineTo(px, y1);
    if (labels == true) {
      drawText(canvas, x1, px - 12, y1 + 3, 0, fontColor: labelColor);
    }
  }
  String xLabel = "Time (sec)";
  if (gModel.xMax > 60) xLabel = "Time (min)";
  Size textSize = calcTextSize(xLabel, TextStyle(fontSize: 12));
  drawText(
      canvas,
      xLabel,
      gModel.activeRect.left +
          gModel.activeRect.width / 2 -
          textSize.width / 2.0,
      gModel.activeRect.bottom + 20,
      0,
      fontColor: labelColor);
  canvas.save();
  canvas.clipRect(gModel.activeRect);

  canvas.drawPath(p, paint);
  canvas.restore();
}

void drawYGrid(Canvas canvas, GridModel gModel, double strokeWidth,
    Color gridColor, Color labelColor,
    {bool labels = false, RtVital? vital}) {
  Path path = Path();
  Paint paint = Paint()
    ..color = gridColor
    ..strokeWidth = strokeWidth
    ..style = PaintingStyle.stroke;

  double x0 = gModel.activeRect.left;
  double x1 = gModel.activeRect.right;

  for (double y = gModel.yMin; y <= gModel.yMax + 1e-7; y += gModel.yDelta) {
    double py = y * gModel.my + gModel.cy;

    path.moveTo(x0, py);
    path.lineTo(x1, py);
    if (labels == true) {
      String yLabel = y.toStringAsFixed(vital!.yLabelDigitsAfterDecimal!);
      // TODO: Please fix string width.
      // Size textSize = calcTextSize(yLabel, TextStyle(fontSize: 5));
      // Rect tempRect = Rect.fromLTRB(textSize.width, gModel.activeRect.top,
      //     gModel.activeRect.right, gModel.activeRect.bottom);
      // gModel.activeRect = tempRect;

      drawText(canvas, yLabel, gModel.activeRect.left - gModel.textSize!.width,
          py - 12, 0,
          fontColor: labelColor);
    }
  }
  // canvas.drawRect(gModel.activeRect, paint)
  String yLabel = "${vital!.yLabel!} (${vital.units!}) ";
  Size textSize = calcTextSize(yLabel, TextStyle(fontSize: 12));
  drawText(
      canvas,
      yLabel,
      5,
      gModel.activeRect.top +
          gModel.activeRect.height / 2 +
          textSize.width / 2.0,
      90,
      fontColor: labelColor);

  // drawText(canvas, yLabel, 5, 100, 90, fontColor: labelColor);

  canvas.save();
  canvas.clipRect(gModel.activeRect);
  canvas.drawPath(path, paint);
  canvas.restore();
}

double minimum(List<double> x) {
  return x.reduce((curr, next) => curr < next ? curr : next);
}

double maximum(List<double> x) {
  return x.reduce((curr, next) => curr > next ? curr : next);
}

Size calcTextSize(String text, TextStyle style) {
  final TextPainter textPainter = TextPainter(
    text: TextSpan(text: text, style: style),
    textDirection: TextDirection.ltr,
    textScaleFactor: WidgetsBinding.instance!.window.textScaleFactor,
  )..layout();
  return textPainter.size;
}

void drawXGridSmall(Canvas canvas, GridModel gModel, double strokeWidth,
    Color gridColor, Color labelColor,
    {bool labels = false, RtVital? vital}) {
  Paint paint = Paint()
    ..color = gridColor
    ..strokeWidth = strokeWidth / 2
    ..style = PaintingStyle.stroke;

  Path p = Path();
  double y0 = gModel.activeRect.top;
  double y1 = gModel.activeRect.bottom;

  for (double x = gModel.xMin;
      x < gModel.xMax + 1e-3;
      x += gModel.xDeltaSmall) {
    double px = x * gModel.mx + gModel.cx;
    // double xMaxPx = xMax * mx + cx;
    // double xDeltaPx = xDelta * mx;

    p.moveTo(px, y0);
    p.lineTo(px, y1);
  }
  canvas.drawPath(p, paint);
}

void drawYGridSmall(Canvas canvas, GridModel gModel, double strokeWidth,
    Color gridColor, Color labelColor,
    {bool labels = false, RtVital? vital}) {
  Path path = Path();
  Paint paint = Paint()
    ..color = gridColor
    ..strokeWidth = strokeWidth / 2
    ..style = PaintingStyle.stroke;

  double x0 = gModel.activeRect.left;
  double x1 = gModel.activeRect.right;

  for (double y = gModel.yMin;
      y <= gModel.yMax + 1e-7;
      y += gModel.yDeltaSmall) {
    double py = y * gModel.my + gModel.cy;

    path.moveTo(x0, py);
    path.lineTo(x1, py);
  }
  canvas.drawPath(path, paint);
}
