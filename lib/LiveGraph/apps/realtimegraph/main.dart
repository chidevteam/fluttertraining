import 'package:flutter/material.dart';
// import 'package:universal/LiveGraph/apps/realtimegraph/graph_models/gesture_model.dart';
// import 'graph_models/rt_vital_model.dart';
import 'shifa_bedside_vu.dart';

void main() {
  print("Yes it is running22");
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Visualizer',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: Scaffold(
            appBar: AppBar(title: Text("Graph")),
            body: SingleChildScrollView(
              child: ShifaBedsideVU(),
            )));
  }
}
