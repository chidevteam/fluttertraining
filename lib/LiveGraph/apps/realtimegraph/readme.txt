
TODO:
- 


- Delay in CTG 6 sec.
- CTG markers behaving strangely.
- Data removal (might need to move to rtVital)

- Ecg inverted data in some leads.







-------------------------------------------------------------
- Frequency of signal ?
- removing buffer causes issues, currTime -=
- Grid not updated properly goes upto 3 instead of 2 for 2-2 peak

Problem:
==========
Want to design a realtime graph component which can easily draw graphs of
continuous data coming from a rtVital
- Muliple continuous signals can be coming from the graph
- Extreme Ymin/Ymax will be defined


- Label alignment
- ECGGrid
- Xlabels in case of CTG, etc in Minutes instead of sec
- Discrete Graphs example
- Test on iPad
- signal does not complete one cycle

- yLabel dynamic text-width fix (based on values 1.2, 180, ???)
  > number of digits is already being computed in grid
  > right align
- Colors pass, line color

ECG
- PC102 actual working
- handle the case with no data (stop time in main after some time)
===============================================
- background/foreground
- Duration testing
- Landscape /portrait
- Separate Grid from data for efficiency (in ECG example grid is not redrawn)
------------------
-  pause, resume, stop and rewind
- Scrolling should not create issue
- rotation landscape / portrait
- delay among different graphs which started at same time
- graph created on scroll, we want all to be working
  or least look like they were working (on create they should start
  from proper time)
  Column vs Listview

USER EXPERIENCE / UI/UX
- Selection of graphs to show and not show live sessions.
- One dashboard to show any data from any device (think about design)
  - dials, meters, ecg, ....
- Move things around, resize

Low priority
- In case of CTG, 1.0 sample rate, the graph did not touch the end
  for xmax / trace = 6.0 sec. 
- Gesture Detector

- set fontsize based on boxsize and then make variables which control font displacements.


DESIGN
=======
- Move data generation to main()  
  One timer in main()
  Second timer in Graph painter which refreshes graph
    > after every 10th refresh, it informs grid also

- fix the ygrid not drawn for end boxes (smaller grid not drawn)
- calculate length of string and adjust left margin accordingly.
- if no data dont' draw
=====================================================
GetX
Bloc

Search
Firebase
BaseApp
- Langu

