// import 'machine_model.dart';
import 'package:flutter/material.dart';

class GestureModel {
  Offset f1Pt = Offset.infinite;
  Offset f2Pt = Offset.infinite;
  Offset cPt = Offset.infinite;
  Offset dxy = Offset(0, 0);
  Offset sxy = Offset(1.0, 1.0);
  // double sx = 1.0;
  // double sy = 1.0;
  bool longPressMode = false;
}
