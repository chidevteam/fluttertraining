// import 'package:flutter/material.dart';

import 'package:flutter/material.dart';
import 'rt_machine.dart';

class RtVital {
  RtMachine machine;
  String vitalName;
  double sampleRate;
  int samplesPerTimer; //change name to displayFPS TODO:
  // samplesPerTimer  = sampleRate/displayFPS
  // sampleRate  = samplesPerTimer * displayFPS
  // sampleRate =  samplesPerTimer / timerTimeSec;
  //  sampleRate = samplesPerTimer * 1000/timerTimeMsec

  double? yMin;
  double? yMax;
  bool? autoAdjustGrid;
  bool? showGrid;
  double? xMax; //screen trace times
  late double gap;
  late bool isECG;
  // double screenFPS;
  String? title;
  String? units;
  String? xLabel;
  String? yLabel;
  int? yLabelDigits; //Total digits used for space computation
  int? yLabelDigitsAfterDecimal;
  // late int timerTimeMsec;
  double t_vital = 0;
  List<double> dataBuf = [];
  List<double> dataBuf2 = []; //to draw multiple results on same graph
  late double bufferInSec; // total dataBuffer length
  late double delayInSec; // time between current display and available data
  // ValueNotifier<int> graphNotifier = ValueNotifier(0);
  ValueNotifier<int> gridNotifier = ValueNotifier(0);
  bool drawGrid = false;

  RtVital(
      {required this.machine,
      required this.vitalName,
      required this.sampleRate,
      required this.samplesPerTimer,
      required this.yMin,
      required this.yMax,
      required this.autoAdjustGrid,
      required this.showGrid,
      required this.xMax,
      this.gap = 1.0,
      this.isECG = false,
      // required this.screenFPS,
      this.units = "",
      this.title = "",
      this.xLabel = "",
      this.yLabel = "",
      this.yLabelDigits = 3,
      this.yLabelDigitsAfterDecimal = 2});

  static RtVital RtECG256(RtMachine machine) {
    return RtVital(
        machine: machine,
        vitalName: "ECG",
        sampleRate: 256,
        samplesPerTimer: 256, //used for generation
        yMin: -1.5,
        yMax: 1.5,
        autoAdjustGrid: false,
        showGrid: true,
        isECG: true,
        xMax: 3.0,
        gap: 0.25,
        // screenFPS: 15,
        title: "Virtual ICU (Lead 1)",
        units: "mV",
        xLabel: "time (sec)",
        yLabel: "milli volts",
        yLabelDigits: 3,
        yLabelDigitsAfterDecimal: 2);
  }

  static RtVital RtPleth(RtMachine machine) {
    return RtVital(
        machine: machine,
        vitalName: "Pleth",
        sampleRate: 50,
        samplesPerTimer: 2, //used for generation
        yMin: -20,
        yMax: 20,
        autoAdjustGrid: false,
        showGrid: true,
        xMax: 6.0,
        gap: 1.0,
        // screenFPS: 15,
        title: "Spotcheck Monitor",
        units: "%",
        xLabel: "time (sec)",
        yLabel: "Percentage",
        yLabelDigits: 2,
        yLabelDigitsAfterDecimal: 2);
  }

  static RtVital RtCTG(RtMachine machine) {
    return RtVital(
        machine: machine,
        vitalName: "CTG",
        sampleRate: 1,
        samplesPerTimer: 1,
        yMin: 0,
        yMax: 100,
        autoAdjustGrid: true,
        showGrid: true,
        xMax: 30 * 60,
        gap: 1 / 8.0 * 30 * 60.0,
        // screenFPS: 1.0,
        title: "Spotcheck Monitor",
        units: "%",
        xLabel: "time (sec)",
        yLabel: "Percentage",
        yLabelDigits: 4,
        yLabelDigitsAfterDecimal: 2);
  }
}
