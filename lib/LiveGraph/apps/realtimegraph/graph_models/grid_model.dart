// import 'machine_model.dart';
import 'package:flutter/material.dart';
import 'dart:math';

import 'package:universal/LiveGraph/apps/realtimegraph/graph_models/rt_vital.dart';

class GridModel {
  late double mx, cx, my, cy;
  late double xMin, xMax, xDelta;
  late double yMin, yMax, yDelta;

  late double xMinSmall, xMaxSmall, xDeltaSmall;
  late double yMinSmall, yMaxSmall, yDeltaSmall;

  late Rect activeRect;
  Size? textSize;

  void computeGrid(Rect dataVal, Rect dataPx, Offset dxy, Offset scale) {
    double xmin = dataVal.left;
    double ymin = dataVal.top;
    double xmax = dataVal.right;
    double ymax = dataVal.bottom;

    double x0 = dataPx.left;
    double y0 = dataPx.top;
    double x1 = dataPx.right;
    double y1 = dataPx.bottom;
    activeRect = dataPx;

    List<double> mxcx = solveLinear(xmin, xmax, x0, x1, dxy.dx, scale.dx);
    mx = mxcx[0];
    cx = mxcx[1];
    // print(mxcx);

    List<double> mycy = solveLinear(ymin, ymax, y1, y0, dxy.dy, scale.dy);
    my = mycy[0];
    cy = mycy[1];
    // print(mycy);

    double desiredNumLinesX = 6;
    List<double> resultsX = gridDesignFloat(mxcx[2], mxcx[3], desiredNumLinesX);
    xDelta = resultsX[0];
    xMin = resultsX[1];
    xMax = resultsX[2];

    double desiredNumLinesY = 4;
    List<double> resultsY = gridDesignFloat(mycy[2], mycy[3], desiredNumLinesY);
    yDelta = resultsY[0];
    yMin = resultsY[1];
    yMax = resultsY[2];
  }

  List<double> solveLinear(
      double x1, double x2, double y1, double y2, d, scale) {
    double dx = (x2 - x1);
    if (dx.abs() < .00001) dx = .00001;
    double mx = (y2 - y1) / dx;
    double cx = y1 - mx * x1;
    mx = mx * scale;
    cx = cx * scale + d;
    double xmin_new = (y1 - cx) / mx;
    double xmax_new = (y2 - cx) / mx;
    return [mx, cx, xmin_new, xmax_new];
  }

  List<double> gridDesignFloat(double vmin, double vmax, double n) {
    double dx = vmax - vmin;
    if (dx == 0) {
      dx = 0.00001;
    }

    double d = dx / n;
    if (d < 0) d = .000001;
    double lg = log(d) / log(10);
    double flog = lg.floorToDouble();
    double v = pow(10.0, flog).toDouble();
    double sc = d / v;

    //sc is in range 1.xx - 9.999

    double scv = 0.0000001;
    if (sc < 1.5) {
      scv = v * 1;
    } else if (sc < 2.25) {
      scv = v * 2.0;
    } else if (sc < 3.75) {
      scv = v * 2.5;
    } else if (sc < 7.5) {
      scv = v * 5.0;
    } else {
      scv = v * 10.0;
    }

    double vvmin = (vmin / scv).ceil() * scv;
    double vvmax = (vmax / scv).floor() * scv;

    return [scv, vvmin, vvmax];
  }

  updateForECG(RtVital rtVital) {
    xDelta = 0.2;
    xMin = 0;
    xMax = rtVital.xMax!;

    yDelta = 0.5;
    yMin = rtVital.yMin!;
    yMax = rtVital.yMax!;

    xDeltaSmall = 0.2 / 5;

    yDeltaSmall = 0.5 / 5;
  }

  String toString() {
    String st = "";
    st += "mx = $mx\n";
    st += "cx = $cx\n";
    st += "my = $my\n";
    st += "cy = $cy\n";
    st += "xMin = $xMin\n";
    st += "xMax = $xMax\n";
    st += "xDelta = $xDelta\n";
    st += "yMin = $yMin\n";
    st += "yMax = $yMax\n";
    st += "yDelta = $yDelta\n";
    return st;
  }
}
