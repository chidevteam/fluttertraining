import 'dart:async';
import 'rt_vital.dart';
import 'hms6500.dart';
import 'dart:math';

class HMS6500DataGenerator {
  HMS6500 hms6500;
  double startTime = 0;

  HMS6500DataGenerator(this.hms6500) {
    startTime = DateTime.now().millisecondsSinceEpoch / 1000.0;
    for (RtVital vital in hms6500.rtVitalList) {
      int timerTimeMsec =
          ((1000.0 / vital.sampleRate) * vital.samplesPerTimer).toInt();
      Timer.periodic(Duration(milliseconds: timerTimeMsec), (timer) {
        vitalGen(vital, timerTimeMsec, 1.0, 1.0);
        vital.t_vital += timerTimeMsec / 1000.0;
        double newTime =
            DateTime.now().millisecondsSinceEpoch / 1000.0 - startTime;

        // print("${vital.t_vital - newTime}");
      });
    }
  }

  void vitalGen(RtVital rtVital, int timerTimeMsec, double amp, double f) {
    double samplingTime = 1.0 / rtVital.sampleRate;
    double tStart = rtVital.t_vital;
    double ejectionTime = timerTimeMsec / 1000.0;
    double tEnd = rtVital.t_vital + ejectionTime;

    f = rtVital.sampleRate / 50;

    if (rtVital.t_vital > 1.5 && rtVital.t_vital < 4) {
      return;
    }
    int beforeSize = rtVital.dataBuf.length;

    for (double t = tStart; t < tEnd - 1e-5; t += samplingTime) {
      double PI = 3.1415926;
      double s1 = .5 * sin(2 * PI * f * t);
      // double s2 = amp * sin(2 * PI * f / 20.0 * t);
      rtVital.dataBuf.add(s1);
      if (s1 > 0) {
        rtVital.dataBuf2.add(0.0);
      } else {
        rtVital.dataBuf2.add(1.0);
      }
    }
    int afterSize = rtVital.dataBuf.length;
    int samples = afterSize - beforeSize;
    if (rtVital.vitalName == "Pleth") {
      // print("samples: ${samples}");
      if (samples != 2) {
        print("case found!");
      }
    }

    if (rtVital.vitalName == "CTG") {
      // print("samples: ${samples}");
      if (samples != 1) {
        print("case found!");
      }
    }

    // dataTime = (rtVital.dataBuf.length / rtVital.sampleRate).toStringAsFixed(2);

    // print("A time ${rtVital.vitalName}: $vTime  $dataTime ");
  }
}

// String vTime = rtVital.t_vital.toStringAsFixed(2);
// String dataTime =
//     (rtVital.dataBuf.length / rtVital.sampleRate).toStringAsFixed(2);
// print("B time ${rtVital.vitalName}: $vTime  $dataTime ");
// print("   time ${rtVital.vitalName}: ${rtVital.t.toStringAsFixed(2)}");
