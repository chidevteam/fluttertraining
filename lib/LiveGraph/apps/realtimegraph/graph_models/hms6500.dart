import 'rt_machine.dart';
import 'rt_vital.dart';

class HMS6500 extends RtMachine {
  HMS6500() {
    machineDisplayFPS = 15.0;
    rtVitalList.add(RtVital.RtECG256(this));
    // rtVitalList.add(RtVital.RtPleth(this));
    rtVitalList.add(RtVital.RtPleth(this));
    rtVitalList.add(RtVital.RtPleth(this));

    // rtVitalList.add(RtVital.RtCTG(this));
    initVars(rtVitalList);
  }
}
