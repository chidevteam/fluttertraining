import 'package:flutter/material.dart';
import 'dart:async';
import 'rt_vital.dart';

/*
CurrTime can stop if no data
It is used to find location in the vitalBuffer

RtVitalGraph is a SINK component, so timer will be at 1/machineDisplayFPS

machineDisplayFPS -> displayFPS
drawGrid -> recomputeGrid and draw
*/

class RtMachine {
  double maxTimeBuffer = 6.0;
  double currTime = 0;
  double speedFactor = 1.0;
  double machineDisplayFPS = -1;
  double LOW_THRESHOLD = 2.0;
  double HIGH_THRESHOLD = 6.0;
  ValueNotifier<int> graphNotifier = ValueNotifier(0);
  List<RtVital> rtVitalList = [];

  initVars(List<RtVital> rtVitalList) {
    int timerTime = (1000.0 ~/ machineDisplayFPS); //in millisec

    Timer.periodic(Duration(milliseconds: timerTime), (timer) {
      // deletePreviousData(rtVitalList);
      double minDelay = 9999999;
      double minBufferSec = 9999999;
      currTime += timerTime / 1000.0 * speedFactor;
      graphNotifier.value += 1;
      for (RtVital rtVital in rtVitalList) {
        if (rtVital.drawGrid) {
          rtVital.gridNotifier.value += 1;
          rtVital.drawGrid = false;
        }

        //Don't allow to go further than data available
        double dataSizeSec = rtVital.dataBuf.length / rtVital.sampleRate;
        if (currTime > dataSizeSec) currTime = dataSizeSec;
        computeVitalDelay(rtVital);
        // print("current Time =: $currTime");

        rtVital.bufferInSec = rtVital.dataBuf.length / rtVital.sampleRate;
        if (rtVital.delayInSec < minDelay) minDelay = rtVital.delayInSec;
        if (rtVital.bufferInSec < minBufferSec)
          minBufferSec = rtVital.bufferInSec;
      }
      speedFactor = 1.0;
      if (minDelay < LOW_THRESHOLD) speedFactor = .5;
      if (minDelay > HIGH_THRESHOLD) speedFactor = 2.0;

      if (currTime > 3 * maxTimeBuffer) {
        currTime -= maxTimeBuffer;
        for (RtVital rtVital in rtVitalList) {
          rtVital.dataBuf
              .removeRange(0, (rtVital.sampleRate * maxTimeBuffer).toInt());
        }
      }
    });
  }

  void computeVitalDelay(RtVital rtVital) {
    int currIndex = (currTime * rtVital.sampleRate).toInt();
    double bufferInSamples = (rtVital.dataBuf.length - currIndex).toDouble();
    rtVital.delayInSec = bufferInSamples / rtVital.sampleRate;
  }
}
