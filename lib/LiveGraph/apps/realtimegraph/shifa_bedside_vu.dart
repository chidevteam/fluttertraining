import 'package:flutter/material.dart';
import 'package:universal/LiveGraph/apps/realtimegraph/graph_models/rt_vital.dart';
import 'realtime_graph/realtime_graph.dart';
import 'graph_models/hms6500.dart';
import 'graph_models/hms6500DataGen.dart';
import 'graph_models/gesture_model.dart';

// ignore: must_be_immutable
class ShifaBedsideVU extends StatelessWidget {
  ShifaBedsideVU({Key? key}) : super(key: key) {
    hms6500 = HMS6500();
    dataGen = HMS6500DataGenerator(hms6500);
  }

  late HMS6500 hms6500;
  late HMS6500DataGenerator dataGen;
  GestureModel gestureModel = GestureModel();

  @override
  Widget build(BuildContext context) {
    List<RealTimeGraph> rtWidgetList = [];

    // double height = MediaQuery.of(context).size.height;

    for (RtVital vital in hms6500.rtVitalList) {
      RealTimeGraph rtg = RealTimeGraph(
        vital,
        gestureModel,
        hms6500.currTime,
        hms6500.graphNotifier,
        height: 150,
      );
      rtWidgetList.add(rtg);
    }

    return Column(children: rtWidgetList);
  }
}
