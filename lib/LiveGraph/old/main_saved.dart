// // import 'package:canvas/LiveGraphViewModel.dart';
// // import 'package:flutter/material.dart';
// // import 'package:provider/provider.dart';
// //
// // final Color darkBlue = Color.fromARGB(255, 18, 32, 47);
// //
// // void main() {
// //   runApp(MyApp());
// // }
// //
// // class MyApp extends StatelessWidget {
// //   @override
// //   Widget build(BuildContext context) {
// //     print('root build called');
// //     return ChangeNotifierProvider(
// //       create: (BuildContext context) => LiveGraphViewModel(),
// //       child: MaterialApp(
// //         debugShowCheckedModeBanner: false,
// //         theme: ThemeData.dark().copyWith(scaffoldBackgroundColor: darkBlue),
// //         home: SafeArea(
// //           child: Scaffold(
// //             // Outer white container with padding
// //             body: Container(
// //               width: double.infinity,
// //               height: double.infinity,
// //               padding: const EdgeInsets.fromLTRB(5, 10, 15, 20),
// //               color: Colors.white,
// //               // Inner yellow container
// //               child: Stack(
// //                 children: [
// //                   Container(
// //                     width: double.infinity,
// //                     height: double.infinity,
// //                     color: Colors.yellow,
// //                   ),
// //                   AspectRatio(
// //                     aspectRatio: 1 / 0.45,
// //                     child: Container(
// //                       color: Colors.red,
// //                       padding: const EdgeInsets.fromLTRB(5, 20, 15, 30),
// //                       child: Stack(
// //                         children: [
// //                           Container(
// //                               width: double.infinity,
// //                               height: double.infinity,
// //                               color: Colors.blue,
// //                               child: RepaintBoundary(
// //                                   child: CustomPaint(
// //                                 painter: GridPainter(),
// //                                 isComplex: true,
// //                                 willChange: false,
// //                               ))),
// //                           Padding(
// //                             padding: const EdgeInsets.all(8.0),
// //                             child: Container(
// //                                 width: double.infinity,
// //                                 height: double.infinity,
// //                                 color: const Color.fromARGB(128, 0, 255, 0),
// //                                 child: CustomPaint(painter: DataPainter())),
// //                           ),
// //                           TextButton(
// //                             onPressed: () {
// //                               final _item = Provider.of<LiveGraphViewModel>(context,listen: false);
// //                               _item.onTap();},
// //                             child: Consumer<LiveGraphViewModel>(
// //                               builder: (context, model, _) {
// //                                 return Text(model.name);
// //                               },
// //                             ),
// //                           ),
// //                         ],
// //                       ),
// //                     ),
// //                   ),
// //                 ],
// //               ),
// //             ),
// //           ),
// //         ),
// //       ),
// //     );
// //   }
// // }
// //
// // class GridPainter extends CustomPainter {
// //   final List<num?> chartData = List.filled(300, null);
// //   int currentTime = 0;
// //
// //   @override
// //   void paint(Canvas canvas, Size size) {
// //     print('paint Called........................................');
// //     var paint = Paint()
// //       ..color = Colors.black
// //       ..strokeWidth = 5
// //       ..strokeCap = StrokeCap.round;
// //
// //     // textPaint(canvas, size);
// //
// //     // Offset startingPoint = Offset(0, size.height / 2);
// //     // Offset endingPoint = Offset(size.width, size.height / 2);
// //
// //     // canvas.drawLine(startingPoint, endingPoint, paint);
// //
// //     var paintH = Paint()
// //       ..color = const Color.fromARGB(80, 0, 0, 0)
// //       ..strokeWidth = .5
// //       ..strokeCap = StrokeCap.round;
// //
// //     drawGrid(canvas, size, paintH);
// //   }
// //
// //   void drawGrid(Canvas canvas, Size size, Paint paint) {
// //     int Nx = 8;
// //     int Ny = 4;
// //
// //     double deltaY = size.height / Ny;
// //     for (int i = 0; i < Ny + 1; i++) {
// //       Offset startingPoint = Offset(0, i * deltaY);
// //       Offset endingPoint = Offset(size.width, i * deltaY);
// //       canvas.drawLine(startingPoint, endingPoint, paint);
// //     }
// //
// //     double deltaX = size.width / Nx;
// //     for (int i = 0; i < Nx + 1; i++) {
// //       Offset startingPoint = Offset(i * deltaX, 0);
// //       Offset endingPoint = Offset(i * deltaX, size.height);
// //       canvas.drawLine(startingPoint, endingPoint, paint);
// //     }
// //   }
// //
// //   @override
// //   bool shouldRepaint(GridPainter oldDelegate) {
// //     print('In shouldRepaint +++++++++++++++++++++++++++++++++');
// //     return true;
// //   }
// //
// //   void textPaint(Canvas canvas, Size size) {
// //     final textStyle = TextStyle(
// //       color: Colors.black,
// //       fontSize: 30,
// //     );
// //     final textSpan = TextSpan(
// //       text: 'Hello, world.',
// //       style: textStyle,
// //     );
// //     final textPainter = TextPainter(
// //       text: textSpan,
// //       textDirection: TextDirection.ltr,
// //     );
// //     textPainter.layout(
// //       minWidth: 0,
// //       maxWidth: size.width,
// //     );
// //     final offset = Offset(0, 0);
// //     textPainter.paint(canvas, offset);
// //   }
// // }
// //
// // class DataPainter extends CustomPainter {
// //   final List<num?> chartData = List.filled(300, null);
// //   int currentTime = 0;
// //
// //   @override
// //   void paint(Canvas canvas, Size size) {
// //     print('paint Called DataPainter........................................');
// //     var paint = Paint()
// //       ..color = Colors.black
// //       ..strokeWidth = 5
// //       ..strokeCap = StrokeCap.round;
// //   }
// //
// //   @override
// //   bool shouldRepaint(DataPainter oldDelegate) {
// //     print('In shouldRepaint DataPainter +++++++++++++++++++++++++++++++++');
// //     return true;
// //   }
// //
// // // void addData(int val) {
// // //   chartData[currentTime] = val;
// // //   currentTime++;
// // //   // print('Add  Data');
// // //   if (currentTime > chartData.length - 1) {
// // //     currentTime = 0;
// // //   }
// // //   notifier.value = currentTime.toDouble();
// // // }
// // //
// // // void addAll(List<num?> values) {
// // //
// // // }
// // }

// import 'package:flutter/material.dart';
// import 'package:percent_indicator/linear_percent_indicator.dart';
// import 'package:provider/provider.dart';

// import 'LiveGraphViewModel.dart';

// // class MyCounter with ChangeNotifier {
// //   int _num = 0;
// //
// //   int get num => _num;
// //
// //   set num(int n) {
// //     _num = n;
// //     notifyListeners();
// //   }
// //
// //   void increment() {
// //     _num = _num + 1;
// //     notifyListeners();
// //   }
// //
// //   void decrement() {
// //     _num = _num - 1;
// //     notifyListeners();
// //   }
// // }

// void main() {
//   runApp(MyApp());
// }

// class MyApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     print('root build called');
//     LiveGraphViewModel viewModel = LiveGraphViewModel();
//     return ChangeNotifierProvider(
//         // builder: (context) => MyCounter(),
//         create: (_) => viewModel,
//         child: MaterialApp(
//           title: 'MyAppJan',
//           home: SafeArea(
//             child: Scaffold(
//               // Outer white container with padding
//               body: Container(
//                 width: double.infinity,
//                 height: double.infinity,
//                 padding: const EdgeInsets.fromLTRB(5, 10, 15, 20),
//                 color: Colors.white,
//                 // Inner yellow container
//                 child: Stack(
//                   children: [
//                     Container(
//                       width: double.infinity,
//                       height: double.infinity,
//                       color: Colors.yellow,
//                     ),
//                     AspectRatio(
//                       aspectRatio: 1 / 0.45,
//                       child: Container(
//                         color: Colors.red,
//                         padding: const EdgeInsets.fromLTRB(5, 20, 15, 30),
//                         child: Stack(
//                           children: [
//                             Container(
//                                 width: double.infinity,
//                                 height: double.infinity,
//                                 color: Colors.blue,
//                                 child: RepaintBoundary(
//                                     child: CustomPaint(
//                                   painter: GridPainter(),
//                                   isComplex: true,
//                                   willChange: false,
//                                 ))),
//                             Padding(
//                               padding: const EdgeInsets.all(8.0),
//                               child: Container(
//                                 width: double.infinity,
//                                 height: double.infinity,
//                                 color: const Color.fromARGB(0, 0, 255, 0),
//                                 child: Consumer<LiveGraphViewModel>(
//                                     builder: (context, myCounter, _) {
//                                   // print('Consumer ++++++++++++++++++++++++++++');
//                                   return CustomPaint(
//                                       painter: DataPainter(viewModel));
//                                 }),
//                               ),
//                             ),
//                             // MyCounterText(),
//                           ],
//                         ),
//                       ),
//                     ),
//                     LinearPercentIndicator(
//                       width: 170.0,
//                       animation: true,
//                       animationDuration: 10,
//                       lineHeight: 20.0,
//                       leading: new Text("Buffer Size"),
//                       // trailing: new Text("right content"),
//                       percent: 0.2,
//                       center: Text("20.0%"),
//                       linearStrokeCap: LinearStrokeCap.butt,
//                       progressColor: Colors.green[900],
//                     ),
//                   ],
//                 ),
//               ),
//             ),
//           ),
//           theme: ThemeData(primarySwatch: Colors.orange),
//         ));
//   }
// }

// class MyCounterText extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     print('MyCounterText');
//     return Consumer<LiveGraphViewModel>(
//       builder: (context, myCounter, _) {
//         return Text(myCounter.name);
//       },
//     );
//   }
// }

// class MyIncreaseButton extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     final _items = Provider.of<LiveGraphViewModel>(context, listen: false);
//     print('MyIncreaseButton');
//     return RaisedButton(
//       child: Text('Increase ++'),
//       onPressed: () => _items.onTap(),
//     );
//   }
// }

// class GridPainter extends CustomPainter {
//   final List<num?> chartData = List.filled(300, null);
//   int currentTime = 0;

//   @override
//   void paint(Canvas canvas, Size size) {
//     print('paint Called........................................');
//     var paint = Paint()
//       ..color = Colors.black
//       ..strokeWidth = 5
//       ..strokeCap = StrokeCap.round;

//     // textPaint(canvas, size);

//     // Offset startingPoint = Offset(0, size.height / 2);
//     // Offset endingPoint = Offset(size.width, size.height / 2);

//     // canvas.drawLine(startingPoint, endingPoint, paint);

//     var paintH = Paint()
//       ..color = const Color.fromARGB(80, 0, 0, 0)
//       ..strokeWidth = .5
//       ..strokeCap = StrokeCap.round;

//     drawGrid(canvas, size, paintH);
//   }

//   void drawGrid(Canvas canvas, Size size, Paint paint) {
//     int Nx = 8;
//     int Ny = 4;

//     double deltaY = size.height / Ny;
//     for (int i = 0; i < Ny + 1; i++) {
//       Offset startingPoint = Offset(0, i * deltaY);
//       Offset endingPoint = Offset(size.width, i * deltaY);
//       canvas.drawLine(startingPoint, endingPoint, paint);
//     }

//     double deltaX = size.width / Nx;
//     for (int i = 0; i < Nx + 1; i++) {
//       Offset startingPoint = Offset(i * deltaX, 0);
//       Offset endingPoint = Offset(i * deltaX, size.height);
//       canvas.drawLine(startingPoint, endingPoint, paint);
//     }
//   }

//   @override
//   bool shouldRepaint(GridPainter oldDelegate) {
//     // print('In shouldRepaint +++++++++++++++++++++++++++++++++');
//     return true;
//   }

//   void textPaint(Canvas canvas, Size size) {
//     final textStyle = TextStyle(
//       color: Colors.black,
//       fontSize: 30,
//     );
//     final textSpan = TextSpan(
//       text: 'Hello, world.',
//       style: textStyle,
//     );
//     final textPainter = TextPainter(
//       text: textSpan,
//       textDirection: TextDirection.ltr,
//     );
//     textPainter.layout(
//       minWidth: 0,
//       maxWidth: size.width,
//     );
//     final offset = Offset(0, 0);
//     textPainter.paint(canvas, offset);
//   }
// }

// class DataPainter extends CustomPainter {
//   final List<num?> chartData = List.filled(600, null);
//   int currentTime = 0;
//   late LiveGraphViewModel model;

//   DataPainter(LiveGraphViewModel viewModel) {
//     model = viewModel;
//   }

//   @override
//   void paint(Canvas canvas, Size size) {
//     // print('paint Called DataPainter........................................');
//     // print(model.chartDataToBeDrawn.toString());
//     var paint = Paint()
//       ..color = Colors.black
//       ..strokeWidth = 1
//       ..strokeCap = StrokeCap.round;
//     double width = size.width;
//     double height = size.height;
//     double xScale = (1.0 / (model.sampleRate * model.numberOfSec)) * width;
//     double ymax = 1;
//     double ymin = -1;

//     double m = (0 - height) / (ymax - ymin);

//     for (int i = 0; i < model.chartDataToBeDrawn.length - 1; i++) {
//       double x1 = i.toDouble();
//       double y1 = model.chartDataToBeDrawn[i]!.toDouble();
//       double x2 = (i + 1).toDouble();
//       double y2 = model.chartDataToBeDrawn[i + 1]!.toDouble();
//       if (y1 == 0 || y2 == 0) continue;
//       double X1 = x1 * xScale;
//       double Y1 = height + m * (y1 - ymin);
//       double X2 = x2 * xScale;
//       double Y2 = height + m * (y2 - ymin);
//       //
//       // print('y1+++++++++++$y1');
//       // print('y2+++++++++++$y2');
//       // print('Y1+++++++++++$Y1');
//       // print('Y2+++++++++++$Y2');

//       Offset startingPoint = Offset(X1, Y1);
//       Offset endingPoint = Offset(X2, Y2);
//       canvas.drawLine(startingPoint, endingPoint, paint);
//     }
//   }

//   @override
//   bool shouldRepaint(DataPainter oldDelegate) {
//     // print('In shouldRepaint DataPainter +++++++++++++++++++++++++++++++++');
//     return true;
//   }

//   void addData(int val) {
//     chartData[currentTime] = val;
//     currentTime++;
//     print('Add  Data');
//     if (currentTime > chartData.length - 1) {
//       currentTime = 0;
//     }
//   }

//   void addAll(List<num?> values) {}
// }
