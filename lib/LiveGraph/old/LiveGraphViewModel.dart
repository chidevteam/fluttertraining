// import 'dart:async';
// import 'dart:math' as math;

// import 'package:stacked/stacked.dart';

// class LiveGraphViewModel extends BaseViewModel {
//   //with ChangeNotifier {
//   double sampleRate = 100; // 100
//   double timerFreqSec = (1.0 / 4.0); // 0.25 sec
//   late double timerFreqMilliSec; // 250 milliseconds
//   late double samplesPerTimer; // 25
//   late double numberOfSec; // 6

//   late Timer _timer;
//   late Timer _timerMilli;
//   late List<num?> chartData;
//   late List<num?> chartDataToBeDrawn;
//   String name = 'Kashif';
//   int count = 0;
//   int currentIndex = 0;
//   late int sizeOfTrace;
//   late double f;
//   late double T;
//   late double bufferPercent;

//   LiveGraphViewModel() {
//     timerFreqMilliSec = timerFreqSec * 1000;
//     samplesPerTimer = sampleRate * timerFreqSec;
//     numberOfSec = 6;
//     print('LiveGraphViewModel +++++++++++++++++++++++++++++++++++++++');
//     _timer = Timer.periodic(
//         Duration(milliseconds: timerFreqMilliSec.toInt()), _generateTrace);
//     _timerMilli = Timer.periodic(Duration(milliseconds: 10), _putData);
//     chartData = [];
//     sizeOfTrace = (sampleRate * numberOfSec).toInt();
//     chartDataToBeDrawn = List.filled(sizeOfTrace, 0);
//     f = 1;
//     T = 0;
//     bufferPercent = 0;
//   }

//   void onTap() {
//     print('onTap ///////////////////////////////////////');
//     count++;
//     name = 'Kashif $count';
//     notifyListeners();
//   }

//   _generateTrace(Timer t) {
//     // print('+++++++++++++++++++++++++++++++++++++++ ${chartData.length}');
//     for (int i = 0; i < samplesPerTimer; i++) {
//       T = T + (1.0 / sampleRate);
//       chartData.add(math.sin(2 * 3.141592654 * f * T));
//     }
//     // print(chartData.toString());
//   }

//   _putData(Timer t) {
//     // print('+++++++++++++++++++++++++++++++++++++++');
//     // for (int i = 0; i < chartData.length; i++) {
//     //   chartDataToBeDrawn[currentIndex % sizeOfTrace] = math.Random().nextInt(60) + 30;
//     // T = T + (1.0/100);
//     if (chartData.length == 0) return;
//     chartDataToBeDrawn[currentIndex % sizeOfTrace] = chartData[0];
//     chartData.removeAt(0);
//     chartDataToBeDrawn[(currentIndex + 5) % sizeOfTrace] =
//         0; //claim by sir ahmed
//     currentIndex++;
//     bufferPercent = chartData.length / chartDataToBeDrawn.length;
//     // print('Buffer Percent $bufferPercent');
//     // }
//     notifyListeners();
//   }
// }
