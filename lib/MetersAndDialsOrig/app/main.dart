import 'package:flutter/material.dart';
// /import '../lib/dial_meter_holder.dart';
import '../lib/linear_meter_holder.dart';

void main() {
  runApp(MaterialApp(title: 'Material App', home: MyApp()));
}

class MyApp extends StatelessWidget {
  final List<double> ranges = [30, 40, 50, 55, 70, 80];

  @override
  Widget build(BuildContext context) {
    print('root build called1');
    Scaffold scaffold = Scaffold(
      appBar: AppBar(
        title: Text("Meter App2333"),
        backgroundColor: Colors.red,
      ),
      body: Column(children: [
        // DialMeterHolder(),
        LinearMeterHolder(ranges),
      ]),
      backgroundColor: Colors.black,
    );
    return scaffold;
  }
}
