limits = [30, 40, 50, 55, 70, 80];
N = limits.length;
(mL = 0.1), (mR = 0.1), (d = 0.01), (uL = 0.05), (uR = 0.05);
fracRange = 1.0 - mL - mR - uL - uR - N * d;
console.log(fracRange);
console.log("Hello");

var canvasElement = document.getElementById("myCanvas");

window.addEventListener("resize", resizeFunc, true);

function drawLine() {
  var ctx = canvasElement.getContext("2d");
  w = canvasElement.offsetWidth;
  // print(w);

  canvasElement.setAttribute("width", w);

  ctx.beginPath();
  ctx.moveTo(10, 200);
  ctx.lineTo(w - 10, 200);
  ctx.strokeStyle = "#FF0000";
  ctx.lineWidth = 20;
  ctx.stroke();

  ctx.beginPath();
  w = canvasElement.offsetWidth;
  x = w / 2;
  y = 100;
  r = 50;
  start = 0;
  end = (270 * Math.PI) / 180;
  ctx.arc(100, 75, 50, start, end, true);
  ctx.stroke();
  console.log(w);
}

function resizeFunc(event) {
  drawLine();
  console.log("resizing");
}

drawLine();
