import 'dart:math' as math;
import 'package:flutter/material.dart';

class LinearMeter extends CustomPainter {
  final List<Color> rangeColors;
  late List<double> ranges;

  static const FRAC2RAD = 360.0 * math.pi / 180;

  LinearMeter(this.ranges, this.rangeColors) {
    print('88888here000000000000000');
  }

  @override
  void paint(Canvas canvas, Size size) {
    drawArc(canvas, size, Color(0xFFFF00ff), 15.0, StrokeCap.round);
    drawLine(canvas, size, rangeColors[1], 15.0, StrokeCap.round);

    // drawLine(canvas, size, deltaRanges[5], ucRight, rangeColors[7], 15.0,
    //     StrokeCap.round);

    // for (int i = 0; i < deltaRanges.length - 1; i++) {
    //   double diff = deltaRanges[i + 1] - deltaRanges[i] - d;
    //   drawLine(canvas, size, deltaRanges[i], diff, rangeColors[i + 2], 15.0,
    //       StrokeCap.round);
    // }
  }

  void drawLine(Canvas canvas, Size size, Color color, double strokeWidth,
      StrokeCap strokeCap) {
    var paint = Paint()
      ..color = color
      ..style = PaintingStyle.stroke
      ..strokeWidth = strokeWidth;
    // ..strokeCap = strokeCap;

    Offset p1 = Offset(10, 250);
    Offset p2 = Offset(50, 250);
    canvas.drawLine(p1, p2, paint);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return false;
  }

  void drawArc(Canvas canvas, Size size, Color color, double strokeWidth,
      StrokeCap strokeCap) {
    double start = 1 / 8.0;
    double end = 4 / 8.0;

    double tempEnd = 1 - end;

    var paint = Paint()
      ..color = color
      ..style = PaintingStyle.stroke
      ..strokeWidth = strokeWidth;
    // ..strokeCap = strokeCap;
    end = end - start;
    Rect rect = Rect.fromLTRB(50, 50, 200, 200);
    double theta1 = start * FRAC2RAD + math.pi / 2;
    double theta2 = tempEnd * FRAC2RAD;

    canvas.drawArc(rect, theta1, theta2, false, paint);
  }
}
