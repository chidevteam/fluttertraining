// import 'dart:math' as math;
import 'package:flutter/material.dart';
import 'linear_meter.dart';

// ignore: must_be_immutable
class LinearMeterHolder extends StatelessWidget {
  late List<Color> rangeColors = [
    //Color(0xff000000),
    Color(0xFF800000),
    Color(0xFFFF0000),
    Color(0xFFFFA000),
    Color(0xFF00a000),
    Color(0xFFFFA000),
    Color(0xFFFF0000),
    Color(0xFF800000),
    // Color(0xff000000)
  ];
  final List<double> ranges;

  LinearMeterHolder(this.ranges);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 300.0,
      padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
      color: Colors.yellow,
      child: Container(
          width: double.infinity,
          height: double.infinity,
          color: const Color.fromARGB(0, 0, 255, 0),
          child: CustomPaint(painter: LinearMeter(ranges, rangeColors))),
    );
  }
}
