package com.chi.webcalls.comms

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.telephony.PhoneStateListener
import android.telephony.TelephonyManager
import android.util.Log
import androidx.localbroadcastmanager.content.LocalBroadcastManager


class PhoneCallStateReceiver : BroadcastReceiver() {
    private var mTelephonyManager: TelephonyManager? = null
    val TAG = "PhoneCallStateReceiver"
    override fun onReceive(context: Context, intent: Intent?) {

        // for outgoing call
        val outgoingPhoneNo: String? = intent!!.getStringExtra(Intent.EXTRA_PHONE_NUMBER)
        Log.e(TAG, "onReceive: Outgoing Phone #: $outgoingPhoneNo")
        // prevent outgoing call
        // resultData = null

        mTelephonyManager = context.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
        val mPhoneStateListener: PhoneStateListener = object : PhoneStateListener() {
            override fun onCallStateChanged(state: Int, incomingNumber: String) {
                super.onCallStateChanged(state, incomingNumber)
                when (state) {
                    TelephonyManager.CALL_STATE_IDLE -> Log.e(TAG, "onCallStateChanged: CALL_STATE_IDLE")
                    TelephonyManager.CALL_STATE_RINGING -> Log.e(TAG, "onCallStateChanged: CALL_STATE_RINGING")
                    TelephonyManager.CALL_STATE_OFFHOOK -> {
                        Log.e(TAG, "onCallStateChanged: CALL_STATE_OFFHOOK")

                        val i = Intent(CommsConstants.ActionEndCall)
                        LocalBroadcastManager.getInstance(context).sendBroadcast(i)
                    }
                }
            }
        }
//        if (!isListening) {
            mTelephonyManager!!.listen(mPhoneStateListener, PhoneStateListener.LISTEN_CALL_STATE)
//            isListening = true
//        }
    }

//    companion object {
//        var isListening = false
//    }
}