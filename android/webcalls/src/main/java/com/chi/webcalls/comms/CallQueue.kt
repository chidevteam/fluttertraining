package com.chi.webcalls.comms

import android.util.Log
import java.util.ArrayList


interface ActiveCallListener
{
    fun onActiveCallChanged(call: CommsCall?, oldCall: CommsCall?)
    fun onCallQueueChanged()
}


class CallQueue(private val mListener: ActiveCallListener)
{
    private val items: ArrayList<CommsCall> = ArrayList()
    private var mActiveCall: CommsCall? = null

    var activeCall: CommsCall?
        get() = mActiveCall
        set(value) {

            val oldCall = mActiveCall
            mActiveCall = value ?: items.firstOrNull()

            mListener.onActiveCallChanged(mActiveCall, oldCall)
        }

//    fun setActiveCall
    fun swapActiveCall(call: CommsCall)
    {
        if(mActiveCall != null)
        {
            items.remove(call)
            items.add(mActiveCall!!)

            activeCall = call

            mListener.onCallQueueChanged()
        }
    }

    fun ensureFirstInQueue(call: CommsCall)
    {
        if(items.size == 0) return
        if(items[0] == call) return

        items.remove(call)
        items.add(0, call)
    }

    fun addCall(call: CommsCall)
    {
        if(activeCall == null)
        {
            activeCall = call
            return
        }

        val call2 = findCall(call.RoomId)
        if(call2 == null)
        {
            items.add(call)
        }

        mListener.onCallQueueChanged()
    }

    fun removeCall(roomId: String)
    {
        var call = findCall(roomId) // ?: return

        if(call == null)
        {
            Log.e("CallQueue", "Removing a non-existing call")
            return
        }

        if (call == mActiveCall)
        {
            if(items.size > 0)
            {
                call = items[0]
                items.remove(call)

                activeCall = call
            }
            else
            {
                activeCall = null
            }
        }
        else
        {
            items.remove(call)
        }

        mListener.onCallQueueChanged()
    }

    fun findCall(roomId: String): CommsCall?
    {
        if(mActiveCall != null && mActiveCall!!.RoomId == roomId)
            return mActiveCall

        return items.find { it.RoomId == roomId }
    }

    fun getItems(): ArrayList<CommsCall>
    {
        return items
    }
}