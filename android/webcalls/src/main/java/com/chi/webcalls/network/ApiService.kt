package com.chi.webcalls.network

import com.chi.webcalls.comms.*
import com.codegini.lib.network.*
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

interface ApiService
{
    @POST("/public/api/versions/AppVersion")
    fun checkAppVersion(@Body request: AppVersionReq): Call<ApiSingleResponse<AppVersionResp>>


    @POST("/api/doctor_app/GetProfile")
    fun GetProfile(): Call<ApiSingleResponse<Doctor>>

    @POST("/api/doctor_app/GetPatientList")
    fun GetPatients(@Body request: ListRequest): Call<ApiSingleResponse<PatientList>>

    @POST("/api/doctor_app/Contacts")
    fun Contact(@Body request: ListRequest): Call<ApiSingleResponse<DoctorList>>
}
