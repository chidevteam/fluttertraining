package com.chi.webcalls.ui

import android.content.Context
import android.graphics.Paint
import android.graphics.Rect
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import android.widget.Toast
import com.chi.webcalls.R

class CommsToast private constructor(context: Context): Toast(context)
{
    companion object
    {
        fun show(context: Context, message: String)
        {
            val view: View = LayoutInflater.from(context).inflate(
                R.layout.comms_toast_layout,
                null, false
            )

            val textView: TextView = view.findViewById(R.id.commsToastText)

            textView.text = message

            var width = 0
            val paint= Paint()
            val rect = Rect()

            paint.textSize = textView.textSize
            paint.getTextBounds(message, 0, message.length, rect)

            width += view.paddingLeft
            width += getPixel(context, 36f)
            width += getPixel(context, 8f)
            width += rect.width()
            width += view.paddingRight

            view.minimumWidth = width.coerceAtMost(getScreenWidth(context) * 2 / 3)

            val toast = CommsToast(context)
            toast.duration = Toast.LENGTH_LONG
            toast.view = view

//            toast.setGravity(Gravity.BOTTOM, 0, 50);
            toast.show()
        }

        private fun getPixel(context: Context, dp: Float): Int
        {
            return (context.resources.displayMetrics.density * dp + 0.5).toInt()
        }

        private fun getScreenWidth(context: Context): Int
        {
            return context.resources.displayMetrics.widthPixels
        }
    }
}