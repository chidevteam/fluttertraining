package com.chi.webcalls.ui

import android.annotation.SuppressLint
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.media.AudioDeviceInfo
import android.media.AudioManager
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.*
import android.widget.ProgressBar
import androidx.fragment.app.Fragment
import com.chi.webcalls.R
import com.chi.webcalls.comms.*
import com.chi.webcalls.deserialize
import com.chi.webcalls.serialize
import java.net.URLEncoder
import java.util.*
import kotlin.collections.ArrayList


class WebCallFragment: Fragment(), AudioDeviceChangeListener
{
    var mSiteCode: String? = null
    var mToken: String? = null
    var mCanInvite = false
    var mCanRecord = false
    var mActiveCall: CommsCall? = null

    private var mWebView: WebView? = null
    private var mProgressBar: ProgressBar? = null
    private var mContext: Context? = null
    private var mPageLoaded = false
    private var mBaseUrl: String = ""

    var mDialog: DoctorInviteDialog? = null
    private var mJoinedParticipants: ArrayList<Int> = ArrayList()

    private var savedVolumeControlStream: Int = -1

    lateinit var mAudioInOutChangeReceiver: AudioInOutChangeReceiver
    private var mConnectedDevices = ArrayList<AudioDevice>()
    private var mFirstTime = true

    var mSpeakerDevice: AudioDevice = AudioDevice(JS_EVENT_SPEAKER, false)
    var mEarpieceDevice: AudioDevice = AudioDevice(JS_EVENT_EARPIECE, false)

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)

        savedVolumeControlStream = activity!!.volumeControlStream
        activity!!.volumeControlStream = AudioManager.STREAM_VOICE_CALL

        WebView.setWebContentsDebuggingEnabled(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
    {
        val view = inflater.inflate(R.layout.comms_web_call_fragment, container, false)

        mWebView = view.findViewById(R.id.commsWebView)
        mProgressBar = view.findViewById(R.id.commsPbLoading)

        val siteCode = mSiteCode ?: return view
        mBaseUrl = "https://$siteCode.cognitivehealthintl.com"

        mConnectedDevices.add(mSpeakerDevice)
        mConnectedDevices.add(mEarpieceDevice)

        Log.e(TAG, "************* onCreateView: $mConnectedDevices *************" )

        setWebView()

        return view
    }

    override fun onAttach(context: Context)
    {
        super.onAttach(context)
        mContext = context
        mAudioInOutChangeReceiver = AudioInOutChangeReceiver(mContext, this@WebCallFragment)
    }

    override fun onResume()
    {
        mContext!!.registerReceiver(mAudioInOutChangeReceiver, IntentFilter(Intent.ACTION_HEADSET_PLUG))
        mContext!!.registerReceiver(mAudioInOutChangeReceiver, IntentFilter(BluetoothDevice.ACTION_ACL_CONNECTED))
        mContext!!.registerReceiver(mAudioInOutChangeReceiver, IntentFilter(BluetoothDevice.ACTION_ACL_DISCONNECTED))
        mContext!!.registerReceiver(mAudioInOutChangeReceiver, IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED))

        super.onResume()
    }

    override fun onPause()
    {
        mContext!!.unregisterReceiver(mAudioInOutChangeReceiver.removeAudioController())
        mAudioInOutChangeReceiver.setDefaultSetting()

        super.onPause()
    }

    override fun onDestroy()
    {
        activity?.volumeControlStream = savedVolumeControlStream
        mAudioInOutChangeReceiver.shutdown()

        val webView = mWebView!!

        CookieManager.getInstance().removeAllCookies(null)
        CookieManager.getInstance().flush()
        webView.clearHistory()
        webView.clearCache(true)
        webView.loadUrl("about:blank")
        webView.onPause()
        webView.removeAllViews()
        webView.destroy()

        super.onDestroy()
    }

    fun startCall()
    {
        if (!mPageLoaded)
        {
            Handler().postDelayed({
                startCall()
            }, 200)

            return
        }

        val call = mActiveCall ?: return
        val webView = mWebView ?: return

        val config = CallsAppConfig(
            MainServer = mBaseUrl,
            RoomId = call.RoomId,
            CallType = call.CallType,
            UserId = call.caller.user_id,
            UserName = call.caller.name,
            UserImage = call.caller.image,
            InviteToken = call.InviteToken,
            CanInvite = mCanInvite,
            CanRecord = mCanRecord
        )

        val params = config.serialize()
//        val params = "{\"actionType\": \"InitCall\", \"data\": ${data}}"
        webView.loadUrl("javascript:StartWebRtcCall($params)")

        changeAudioRoute(call.CallType == "Video")
    }

    fun stopCall()
    {
        val webView = mWebView ?: return
        webView.loadUrl("javascript:StopWebRtcCall()")
    }

    fun onUserInvited(user: CommsUser)
    {
        val webView = mWebView ?: return
        val params = user.serialize()

        webView.loadUrl("javascript:onUserInvited($params)")
    }

    fun onInviteAccepted(user: CommsUser)
    {
        val webView = mWebView ?: return
        val params = user.serialize()

        webView.loadUrl("javascript:onInviteAccepted($params)")
    }

    fun onInviteRejected(user: CommsUser)
    {
        val webView = mWebView ?: return
        val params = user.serialize()

        webView.loadUrl("javascript:onInviteRejected($params)")
    }

    fun onInviteNoResponse(user: CommsUser)
    {
        val webView = mWebView ?: return
        val params = user.serialize()

        webView.loadUrl("javascript:onInviteNoResponse($params)")
    }

    fun onRecipientRinging(user: CommsUser)
    {
        val webView = mWebView ?: return
        val params = user.serialize()

        webView.loadUrl("javascript:onRecipientRinging($params)")
    }

    fun inviteUser(doctor: Doctor)
    {
        val inviteToken = UUID.randomUUID().toString().replace("-", "")

        val user = CommsUser(
            user_id = doctor.user_id!!,
            name = doctor.full_name!!,
            image = doctor.image,
            invite_token = inviteToken
        )

        (mContext as CallActivity).inviteUser(user, mActiveCall!!)
    }

    fun inviteCC()
    {
        val inviteToken = UUID.randomUUID().toString().replace("-", "")

        (mContext as CallActivity).inviteCC(inviteToken, mActiveCall!!)
    }

    fun onRecordingStarted(rec_meta: CommsMapAny)
    {
        val webView = mWebView ?: return
        val params = rec_meta.serialize()

        webView.loadUrl("javascript:RecordingStarted($params)")
    }

    fun onRecordingStopped(rec_meta: CommsMapAny)
    {
        val webView = mWebView ?: return
        val params = rec_meta.serialize()

        webView.loadUrl("javascript:RecordingStopped($params)")
    }

    fun onStartRecSuccess()
    {
        val webView = mWebView ?: return
        webView.loadUrl("javascript:onStartRecSuccess()")
    }

    fun onStartRecFailed()
    {
        val webView = mWebView ?: return
        webView.loadUrl("javascript:onStartRecFailed()")
//        Log.e(TAG, "startRecording: step 5")
    }

    fun onStopRecSuccess()
    {
        val webView = mWebView ?: return
        webView.loadUrl("javascript:onStopRecSuccess()")
//        Log.e(TAG, "stopRecording: step 5")
    }

    fun onStopRecFailed()
    {
        val webView = mWebView ?: return
        webView.loadUrl("javascript:onStopRecFailed()")
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun setWebView()
    {
        val token = URLEncoder.encode(mToken, "utf-8")
        val webView = mWebView ?: return
        val siteCode = mSiteCode ?: return

        val settings: WebSettings = webView.settings

        settings.javaScriptEnabled = true
        settings.useWideViewPort = true
        settings.loadWithOverviewMode = true
        settings.builtInZoomControls = true
        settings.domStorageEnabled = true
        settings.allowContentAccess = true
        settings.databaseEnabled = true
        settings.displayZoomControls = false
        settings.allowFileAccessFromFileURLs = true
        settings.allowUniversalAccessFromFileURLs = true
        settings.mediaPlaybackRequiresUserGesture = false
        settings.setSupportZoom(true)
        settings.builtInZoomControls = true

        webView.setLayerType(View.LAYER_TYPE_HARDWARE, null)
        webView.clearCache(true)
        webView.clearHistory()

        val cookieManager = CookieManager.getInstance()
        cookieManager.setAcceptThirdPartyCookies(webView, true)
        cookieManager.setAcceptCookie(true)

        webView.webChromeClient = object : WebChromeClient() {

            override fun onProgressChanged(view: WebView?, progress: Int) {
                if (progress == 100) {
                    Log.e("WEB", "webChromeClient progress 100 ...")
                }
            }

            override fun onPermissionRequest(request: PermissionRequest?)
            {
                if(request != null)
                {
                    Log.e("WEB", "Permission Requested for " + request.resources[0])
                    request.grant(request.resources)
                }
            }
        }

        webView.webViewClient = object: WebViewClient() {

            override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean
            {
                return false
            }

            override fun onPageFinished(view: WebView, url: String) {

                mProgressBar?.visibility = View.GONE
                webView.visibility = View.VISIBLE

                mPageLoaded = true
            }


            override fun onLoadResource(view: WebView, url: String?) {
                view.loadUrl("javascript:document.getElementsByName(\"viewport\")[0].setAttribute(\"content\", \"width=device-width, initial-scale=1.0, maximum-scale=5.0, user-scalable=yes\");")
                super.onLoadResource(view, url)
            }
        }

        webView.isVerticalScrollBarEnabled = false
        webView.isHorizontalScrollBarEnabled = false
        webView.addJavascriptInterface(this, "Android")

        val url = "$mBaseUrl/comms/JoinMeeting?token=$token"
        Log.e(TAG, "setWebView: Token $token")
        webView.loadUrl(url)
    }

    @JavascriptInterface
    fun receiveSignal(data: String)
    {
        Log.e(TAG, "JavascriptInterface->receiveSignal ==== > $data")

        if(data.contains(JS_EVENT_WEBRTC_STOPPED))
        {
            Log.e(TAG, "JavascriptInterface -> Event could be JS_EVENT_WEBRTC_STOPPED")
        }

        val event = data.deserialize(CommsJavascriptEvent::class.java)
        if(event == null)
        {
            Log.e(TAG, "JavascriptInterface -> Event is Null")
            return
        }

        val eventType = event.type
        if(eventType == null)
        {
            Log.e(TAG, "JavascriptInterface -> EventType is Null")
            return
        }

        val activity = mContext as CallActivity

        when(eventType)
        {
            JS_EVENT_INVITE -> {
                Log.e(TAG, "receiveSignal: Handle Invite Signal")
                activity.runOnUiThread {
                    mDialog =  DoctorInviteDialog(mJoinedParticipants, mActiveCall!!.caller.user_id)
                    mDialog!!.setListener(object : DoctorInviteDialog.DoctorInviteListener
                    {
                        override fun onDoctorSelected(doctor: Doctor) {
                            mDialog!!.dismissAllowingStateLoss()
                            mDialog = null
                            inviteUser(doctor)
                        }

                        override fun onInviteCC() {
                            Log.e(TAG, "onInviteCC: Callback Received")
                            mDialog!!.dismissAllowingStateLoss()
                            mDialog = null
                            inviteCC()
                        }

                        override fun onDialogClose(){
                            mDialog!!.dismissAllowingStateLoss()
                            mDialog = null
                        }
                    })

                    mDialog!!.show(childFragmentManager, "DoctorInviteDialog")
                }
            }

            JS_EVENT_END_CALL -> {
                activity.runOnUiThread {
                    stopCall()
                    activity.endCall(mActiveCall!!.RoomId)
                }
            }

            JS_EVENT_START_RECORDING -> {
                activity.runOnUiThread {
                    activity.startRecording(mActiveCall!!)
                }
            }

            JS_EVENT_STOP_RECORDING -> {
                activity.runOnUiThread {
                    activity.stopRecording(mActiveCall!!)
                }
            }

            JS_EVENT_CHANGE_SPK -> {

                when(event.data)
                {
                    JS_EVENT_EARPIECE -> mAudioInOutChangeReceiver.setDefaultCallSetting(false)
                    JS_EVENT_SPEAKER -> mAudioInOutChangeReceiver.playSpeaker()
                    JS_EVENT_HEADPHONE -> mAudioInOutChangeReceiver.playHeadPhone()
                    JS_EVENT_BLUETOOTH -> mAudioInOutChangeReceiver.playBluetooth()
                }

                Log.e(TAG, "receiveSignal: JS_EVENT_CHANGE_SPK ++++++++++++++++++++++ $event")
            }

            JS_EVENT_CAM_STATE -> {
                val camOn = event.data == "1"
//                activity.cameraStateChanged(camOn)

                if(camOn)
                {
                    Handler().postDelayed({
                        changeAudioRoute(true, report = true)
                    }, 200)
                }
                Log.e(TAG, "receiveSignal: JS_EVENT_CAM_STATE ++++++++++++++++++++++ $event")
            }

            JS_EVENT_WEBRTC_STOPPED -> {
                Log.e(TAG, "receiveSignal: JS_EVENT_WEBRTC_STOPPED ++++++++++++++++++++++ $event")
//                activity.onWebRtcCallEnded(mActiveCall!!.RoomId) // TODO: Sir Atiq will uncomment this line
            }

            JS_EVENT_UPDATE_DEVICES -> {
                Log.e(TAG, "receiveSignal: JS_EVENT_UPDATE_DEVICES ++++++++++++++++++++++ $event")
                val webView = mWebView ?:return
                val params = mConnectedDevices.serialize()
                activity.runOnUiThread {
                    Log.e(TAG, "receiveSignal: JS_EVENT_UPDATE_DEVICES Devices +++++++++++++++++++++ $params")
                    webView.loadUrl("javascript:onSpeakerDeviceChanged($params)")
                }
            }

            else -> {
                Log.e(TAG, "Unhandled JS EVent: $eventType")
            }
        }
    }

    private fun changeAudioRoute(speakerOn: Boolean, report: Boolean = false)
    {
        val webView = mWebView ?: return
        mAudioInOutChangeReceiver.setSpeakerState(speakerOn)

        for (device in mConnectedDevices)
        {
            if (device.device == JS_EVENT_SPEAKER)
                device.is_active = speakerOn
            else if (device.device == JS_EVENT_EARPIECE)
                device.is_active = !speakerOn
        }

        if (!mAudioInOutChangeReceiver.getHeadPhoneStatus() && !mAudioInOutChangeReceiver.getBluetoothStatus())
        {
            mAudioInOutChangeReceiver.setDefaultCallSetting(speakerOn)
        }

        Log.e(TAG, "---------------------- changeAudioRoute: $speakerOn ----------------------")

        if (report)
        {
            activity?.runOnUiThread {
                if (speakerOn)
                {
                    webView.loadUrl("javascript:OnAudioDeviceChanged($JS_EVENT_SPEAKER)")
                }
                else
                {
                    webView.loadUrl("javascript:OnAudioDeviceChanged($JS_EVENT_EARPIECE)")
                }
            }
        }
    }

    override fun onDeviceChange(device: AudioDevice, isConnected: Boolean) {
        Log.e(TAG, "********** onDeviceChange: 1 Device: $device    isConnected: $isConnected **********")
        val webView = mWebView ?: return

        activity?.runOnUiThread {
            if (isConnected)
            {
                mConnectedDevices.remove(mEarpieceDevice)

                for(dev in mConnectedDevices)
                {
                    dev.is_active = false
                }

                mConnectedDevices.add(device)

                if (device.device == JS_EVENT_BLUETOOTH)
                    mAudioInOutChangeReceiver.playBluetooth()
                else if (device.device == JS_EVENT_HEADPHONE)
                    mAudioInOutChangeReceiver.playHeadPhone()

            }
            else
            {
                mConnectedDevices.remove(device)

                if (mAudioInOutChangeReceiver.getBluetoothStatus())
                    mAudioInOutChangeReceiver.playBluetooth()
                else if (mAudioInOutChangeReceiver.getHeadPhoneStatus())
                    mAudioInOutChangeReceiver.playHeadPhone()
                else
                {
                    mConnectedDevices.add(mEarpieceDevice)
                    mAudioInOutChangeReceiver.setDefaultCallSetting(mAudioInOutChangeReceiver.getSpeakerState())

                    for (dev in mConnectedDevices)
                    {
                        if (dev.device == JS_EVENT_SPEAKER)
                            dev.is_active = mAudioInOutChangeReceiver.getSpeakerState()
                        else if (dev.device == JS_EVENT_EARPIECE)
                            dev.is_active = !mAudioInOutChangeReceiver.getSpeakerState()
                    }
                }
            }

            val params = mConnectedDevices.serialize()
//
//            if (mFirstTime)
//            {
//                mFirstTime = false
//                Handler().postDelayed({
//                    webView.loadUrl("javascript:onSpeakerDeviceChanged($params)")
//                }, 5000)
//            }
//            else
//            {
                webView.loadUrl("javascript:onSpeakerDeviceChanged($params)")
//            }

            Log.e(TAG, "********** onDeviceChange: 2  params: $params **********")
        }
    }

    companion object
    {
        private const val TAG = "WebCallFragment"

        private const val JS_EVENT_INVITE = "invite"
        private const val JS_EVENT_END_CALL = "end-call"
        private const val JS_EVENT_START_RECORDING = "start-recording"
        private const val JS_EVENT_STOP_RECORDING = "stop-recording"
        private const val JS_EVENT_CHANGE_SPK = "change-speaker"
        private const val JS_EVENT_CAM_STATE = "camera-state"
        private const val JS_EVENT_WEBRTC_STOPPED = "webrtc_stopped"
        private const val JS_EVENT_UPDATE_DEVICES = "update-speaker-devices"
        private const val JS_EVENT_BLUETOOTH = "Bluetooth"
        private const val JS_EVENT_HEADPHONE = "Headphone"
        private const val JS_EVENT_SPEAKER = "Speaker"
        private const val JS_EVENT_EARPIECE = "Earpiece"
    }
}
