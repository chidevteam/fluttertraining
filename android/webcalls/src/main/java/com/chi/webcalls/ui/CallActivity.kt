package com.chi.webcalls.ui

import android.Manifest
import android.content.*
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.media.MediaPlayer
import android.os.*
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.FrameLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.RecyclerView
import com.chi.webcalls.R
import com.chi.webcalls.comms.*
import com.chi.webcalls.deserialize
import com.chi.webcalls.network.ApiClient
import com.chi.webcalls.network.ApiService
import com.chi.webcalls.serialize
import com.chi.webcalls.services.CallsService
import com.chi.webcalls.toInteger
import com.codegini.lib.core.BaseActivity
import com.google.gson.internal.LinkedTreeMap
import java.lang.ref.WeakReference
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.Map
import kotlin.collections.get


class CallActivity : BaseActivity(), ActiveCallListener
{
    private lateinit var mMessenger: Messenger

    private var mService: Messenger? = null
    private lateinit var mCallUiFragment: CallUiFragment
    private lateinit var mWebCallFragment: WebCallFragment

    private val mCallQueue: CallQueue = CallQueue(this)
    private var mStartOutgoing = false
    private var mSiteCode: String? = null
    private var mToken: String? = null
    private var mCanInvite = false
    private var mCanRecord = false

    private lateinit var mCallQueueAdapter: CallQueueAdapter
    private var mCallUiContainer: FrameLayout? = null
    private var mWebCallContainer: FrameLayout? = null
    private var rvCallQueue: RecyclerView? = null

    lateinit var apiService: ApiService
    private var isMessengerNull = false

    val RC_ALL_PERMISSIONS = 9005
    private val mPermissionsNotGranted: MutableList<String> = java.util.ArrayList()

    private val mCallStatusReceiver = object : BroadcastReceiver() {

        override fun onReceive(context: Context?, intent: Intent?)
        {
            if (intent == null) {
                Log.e(TAG, "mCallStatusReceiver-> intent is null")
                return
            }

            if (intent.action == null) {
                Log.e(TAG, "mCallStatusReceiver-> action is null")
                return
            }

            Log.e(TAG, "mCallStatusReceiver-> action =  ${intent.action}")

            if (intent.action == CommsConstants.ActionEndCall)
            {
                Log.e(TAG, "========== mCallStatusReceiver EndCall BC Received ==========")
                endCall(mCallQueue.activeCall!!.RoomId)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.comms_call_activity)

        mCallUiContainer = findViewById(R.id.commsCallUi)
        mWebCallContainer = findViewById(R.id.commsWebCall)
        rvCallQueue = findViewById(R.id.rvCallsQueue)

        mMessenger = Messenger(ServiceMessageHandler(this))

        setupWindow()

        val action = intent.action ?: return
        val bundle = intent.extras!!

        mSiteCode = bundle.getString(CallsService.K_CALLS_SITE_CODE)
        mToken = bundle.getString(CallsService.K_CALLS_TOKEN)

        val callUi = CallUiFragment()
        val webCall = WebCallFragment()
        mCallUiFragment = callUi
        mWebCallFragment = webCall

        supportFragmentManager.beginTransaction()
            .add(R.id.commsCallUi, mCallUiFragment)
            .add(R.id.commsWebCall, mWebCallFragment)
            .commit()

        val call = mCallQueue.activeCall
        callUi.mActiveCall = call

        webCall.mActiveCall = call
        webCall.mSiteCode = mSiteCode
        webCall.mToken = mToken

        mCallQueueAdapter = CallQueueAdapter(mCallQueue.getItems(), this)
        rvCallQueue!!.adapter = mCallQueueAdapter

        apiService = ApiClient.apiService(this, mSiteCode!!)
        ApiClient.setAuthToken(mToken)

        Handler().postDelayed({

            initializeCall(action, bundle)

        }, 100)

        allPermissionGranted()

        val filter = IntentFilter()
        filter.addAction(CommsConstants.ActionEndCall)
        LocalBroadcastManager.getInstance(this).registerReceiver(mCallStatusReceiver, filter)
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onPause() {
        super.onPause()
    }

    override fun onDestroy()
    {
        Log.e(TAG, "Call Activity Destroyed")
        mCallUiFragment.stopRinger()

        if (mService != null)
        {
            if(!isMessengerNull)
            {
                val msg = Message.obtain(null, CallsService.MSG_UNREGISTER_CALL)
                mService!!.send(msg)
            }
        }

        LocalBroadcastManager.getInstance(this).unregisterReceiver(mCallStatusReceiver)

        super.onDestroy()
    }

    override fun onStart() {

        Intent(this, CallsService::class.java).also {
            bindService(it, mServiceConnection, 0)
        }
        super.onStart()
    }

    override fun onStop() {

        if (mService != null)
        {
            unbindService(mServiceConnection)
        }

        super.onStop()
    }

    override fun onBackPressed() {

    }

    fun acceptCall(call: CommsCall)
    {
        mCallUiFragment.stopRinger()

        if(mCallQueue.activeCall != call)
        {
            Log.e(TAG, "How come accepted call is not active call?")
        }

        val bundle = Bundle()
        bundle.putString("call", call.serialize())

        sendActionToService(CallsService.MSG_ACTION_ACCEPT_CALL, bundle)

        mCallUiContainer?.visibility = View.GONE
        mWebCallContainer?.visibility = View.VISIBLE

        mWebCallFragment.mActiveCall = call
        mWebCallFragment.mCanInvite = mCanInvite
        mWebCallFragment.mCanRecord = mCanRecord
        mWebCallFragment.startCall()

        call.State = CommsCallState.Connected
        updateCallState(call)
    }

    fun rejectCall(call: CommsCall)
    {
        mCallUiFragment.stopRinger()
        val bundle = Bundle()
        bundle.putString("call", call.serialize())

        sendActionToService(CallsService.MSG_ACTION_REJECT_CALL, bundle)

        mCallQueue.removeCall(call.RoomId)
    }

    fun endCall(roomId: String, playError: Boolean = true)
    {
        mCallUiFragment.stopRinger()

        val call = mCallQueue.activeCall ?: return

        if (call.RoomId != roomId)
        {
            Log.e(TAG, "How come end call room-id mismatch?")
        }

        mWebCallContainer?.visibility = View.GONE

        val bundle = Bundle()
        bundle.putString("call", call.serialize())

        sendActionToService(CallsService.MSG_ACTION_END_CALL, bundle)

        if(playError)
            finishCall(call.RoomId, "Call Ended", 100)
    }

    fun inviteUser(user: CommsUser, call: CommsCall)
    {
        val bundle = Bundle()
        bundle.putString("user", user.serialize())
        bundle.putString("call", call.serialize())

        sendActionToService(CallsService.MSG_ACTION_INVITE_USER, bundle)
    }

    fun inviteCC(inviteToken: String, call: CommsCall)
    {
        val bundle = Bundle()
        bundle.putString("invite_token", inviteToken)
        bundle.putString("call", call.serialize())

        sendActionToService(CallsService.MSG_ACTION_INVITE_CC, bundle)
    }

    fun startRecording(call: CommsCall)
    {
//        Log.e(TAG, "startRecording: step 1")
        val bundle = Bundle()
        bundle.putString("call", call.serialize())

        sendActionToService(CallsService.MSG_ACTION_START_RECORDING, bundle)
    }

    fun stopRecording(call: CommsCall)
    {
//        Log.e(TAG, "stopRecording: step 1")
        val bundle = Bundle()
        bundle.putString("call", call.serialize())

        sendActionToService(CallsService.MSG_ACTION_STOP_RECORDING, bundle)
    }

    //
    fun onWebRtcCallEnded(roomId: String)
    {
        Log.e(TAG, "onWebRtcCallEnded $roomId")
        mCallQueue.removeCall(roomId)
    }

    private fun initializeCall(action: String, bundle: Bundle)
    {
        val siteCode = bundle.getString(CallsService.K_CALLS_SITE_CODE) ?: return
        val token = bundle.getString(CallsService.K_CALLS_TOKEN) ?: return

        mCanInvite = bundle.getBoolean(CallsService.K_INVITE_ALLOWED, false)
        mCanRecord = bundle.getBoolean(CallsService.K_RECORD_ALLOWED, false)
        mSiteCode = siteCode
        mToken = token

        Log.e(TAG, "Initializing Call, CanInvite: $mCanInvite")

        if (action == CallsService.ACTION_CALL_USER)
        {
            startNewCall(bundle)
        }
        else if (action == CallsService.ACTION_CALL_CC)
        {
            startNewCCCall(bundle)
        }
        else if (action == CallsService.ACTION_INCOMING_CALL)
        {
            onIncomingCall(bundle)
        }
    }

    private fun startNewCall(bundle: Bundle)
    {
        val callType = bundle.getString(CallsService.K_CALL_TYPE) ?: return
        val myself = bundle.getString(CallsService.K_CALL_CALLER) ?: return
        val callerOrRecipient = bundle.getString(CallsService.K_CALL_RECIPIENT) ?: return

        val callUser = callerOrRecipient.deserialize(CommsUser::class.java)
        val caller = myself.deserialize(CommsUser::class.java)

        mStartOutgoing = true
        val call = CommsCall.makeOutGoingCall(
            callType,
            caller = caller
        )

        call.addRecipient(callUser, CommsCallState.Outgoing)

        mCallQueue.addCall(call)
    }

    private fun startNewCCCall(bundle: Bundle)
    {
        val callType = bundle.getString(CallsService.K_CALL_TYPE) ?: return
        val myself = bundle.getString(CallsService.K_CALL_CALLER) ?: return

        val caller = myself.deserialize(CommsUser::class.java)

        mStartOutgoing = true
        val call = CommsCall.makeOutGoingCall(
            callType,
            caller = caller,
            toCC = true
        )

        mCallQueue.addCall(call)
    }

    private fun onIncomingCall(bundle: Bundle)
    {
        val callObj = bundle.getString(CallsService.K_CALL) ?: return
        val call = callObj.deserialize(CommsCall::class.java)

        mCallQueue.addCall(call)
    }

    fun acceptQueueCall(call: CommsCall)
    {
        mCallUiFragment.stopRinger()

        val activeCall = mCallQueue.activeCall
        if (activeCall != null)
        {
            if(activeCall.State == CommsCallState.Incoming)
            {
                mCallQueue.swapActiveCall(call)

                acceptCall(call)
                return
            }

            call.State = CommsCallState.NextAccepted
            mCallQueue.ensureFirstInQueue(call)

            if(mWebCallFragment.mActiveCall?.RoomId == activeCall.RoomId)
            {
                mWebCallFragment.stopCall()
            }

            endCall(activeCall.RoomId, false)

            val handler = Handler() // Todo: Sir Atiq Will Remove this Handler delay after removing following line
            handler.postDelayed({
                mCallQueue.removeCall(activeCall.RoomId) // Todo: Sir Atiq Will Remove this line
            }, 2500)
        }
        else
        {
            call.State = CommsCallState.NextAccepted
            mCallQueue.activeCall = call
        }
    }

    private fun setupWindow()
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O_MR1)
        {
            setTurnScreenOn(true)
            setShowWhenLocked(true)
        }
        else
        {
            window.addFlags(
                WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED or
                        WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD or
                        WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON or
                        WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON or
                        WindowManager.LayoutParams.FLAG_ALLOW_LOCK_WHILE_SCREEN_ON
            )

            val params = window.attributes
            params.screenBrightness = 1.0f
            window.attributes = params
        }
    }

    private fun onServiceConnected()
    {
        Log.e(TAG, "onServiceConnected: Starting outgoing call ====================================== 1" )
        if (mStartOutgoing)
        {
            Log.e(TAG, "onServiceConnected: Starting outgoing call ====================================== 2" )

            mStartOutgoing = false

            val call = mCallQueue.activeCall ?: return

            call.ServiceConnected = true
            mCallUiFragment.updateUI()

            val bundle = Bundle()
            bundle.putString("call", call.serialize())
            if (call.IsToCC)
                sendActionToService(CallsService.MSG_ACTION_CALL_CC, bundle)
            else
                sendActionToService(CallsService.MSG_ACTION_CALL_USER, bundle)

            updateCallState(call)
        }
    }

    private fun sendActionToService(action: Int, bundle: Bundle)
    {
        Log.e(TAG, "Sending Call Action")
        val msg = Message.obtain(null, action)
        msg.data = bundle
        msg.replyTo = mMessenger

        mService?.send(msg)
    }

    private fun onServiceMessage(msg: Message): Boolean
    {
        Log.e(TAG, "onServiceMessage what: ${msg.what}")
        var retVal = false

        when(msg.what)
        {
            CallsService.CLIENT_MSG_CALL_RESPONSE -> {
                onCallActionResponse(msg.data)
                retVal = true
            }

            CallsService.CLIENT_MSG_SERVER_EVENT -> {
                onServerEvent(msg.data)
                retVal = true
            }

            CallsService.CLIENT_MSG_SHOW_ERROR -> {
                onShowError(msg.data)
                retVal = true
            }

            CallsService.CLIENT_MSG_END_ACTIVITY -> {
                isMessengerNull = true
//                onFinishingNonEndedCalls()
                finish()
                Log.e(TAG, "onServiceMessage: CLIENT_MSG_END_ACTIVITY Called....................." )
            }

            CallsService.CLIENT_MSG_START_REC_RESPONSE -> {
                onStartRecResponse(msg.data)
                Log.e(TAG, "onServiceMessage: CLIENT_MSG_START_REC_RESPONSE Called....................." )
//                Log.e(TAG, "startRecording: step 4")
            }

            CallsService.CLIENT_MSG_STOP_REC_RESPONSE -> {
                onStopRecResponse(msg.data)
                Log.e(TAG, "onServiceMessage: CLIENT_MSG_STOP_REC_RESPONSE Called....................." )
//                Log.e(TAG, "stopRecording: step 4")
            }
        }

        return retVal
    }

    private fun onCallActionResponse(bundle: Bundle)
    {
        val roomId = bundle.getString("room_id") ?: return
        val sResult = bundle.getString("result", null)
        val sError = bundle.getString("error", null)

        val call = mCallQueue.findCall(roomId) ?: return

        if (sError != null)
        {
            return
        }

        if(sResult == null)
        {
            return
        }

        val result: CommsMapAny = sResult.deserialize()
        val status = result["Status"] ?: return
        if (status == "Ok") return

        call.ErrorMessage = result["ErrorMessage"] as String
        if (call == mCallQueue.activeCall)
        {
            call.State = CommsCallState.Error
            mCallUiFragment.updateUI()

            scheduleErrorAndFinish()
        }
    }

    private fun onServerEvent(bundle: Bundle)
    {
        val data = bundle.getString("event")
        val action = bundle.getString("action")
        if (data == null && action == null)
        {
            Log.e(TAG, "onServerEvent, event & action both null")
            return
        }

        if(action != null)
        {
            initializeCall(action, bundle)
            return
        }

        if(data == null)
        {
            Log.e(TAG, "onServerEvent, event is null")
            return
        }

        Log.e(TAG, "Server Event: $data")

        val event = data.deserialize(CommsIpcAction::class.java)

        val roomId = event.params["room_id"] as String? ?: return

        when(event.method)
        {
            CommsEvents.EVENT_INCOMING_CALL -> {
                Log.e(TAG, "Incoming Call")
            }

            CommsEvents.EVENT_RECIPIENT_RINGING -> {
                onEventParticipantRinging(event.params)
            }

            CommsEvents.EVENT_CALL_REJECTED -> {
                finishCall(roomId, "Recipient Rejected the Call", 100)
            }

            CommsEvents.EVENT_CALL_ALL_CC_BUSY -> {
                onAllCCUsersBusy(roomId)
            }

            CommsEvents.EVENT_CALL_ENDED -> {
                onEventCallEnded(roomId)
            }

            CommsEvents.EVENT_PARTICIPANT_CALL_ENDED -> {

            }

            CommsEvents.EVENT_CALL_ACCEPTED -> {
                Log.e(TAG, "onServerEvent: +++++++++++++++++++++++++++++++ EVENT_CALL_ACCEPTED +++++++++++++++++++++++++++    1")
                onCallAccepted(event.params)
            }

            CommsEvents.EVENT_CALL_EXPIRED -> {
                finishCall(roomId, "Call is no longer valid", 100)
            }

            CommsEvents.EVENT_CALL_FAILED -> {

            }

            CommsEvents.EVENT_CALL_NO_RESPONSE -> {
                finishCall(roomId, "No Response from Recipient", 100)
            }

            CommsEvents.EVENT_SESSION_EXPIRED -> {

            }

            CommsEvents.EVENT_USER_INVITED -> {

                val fragment = mWebCallFragment ?: return
                if (fragment.mActiveCall!!.RoomId != roomId) return

                @Suppress("UNCHECKED_CAST")
                val invited = event.params["invited"] as CommsMapAny

                fragment.onUserInvited(CommsUser.fromMap(invited))
            }

            CommsEvents.EVENT_INVITE_REJECTED -> {

                val fragment = mWebCallFragment ?: return
                if (fragment.mActiveCall!!.RoomId != roomId) return

                val userId = event.params["user_id"]!!.toInteger()
                val invToken = event.params["invite_token"] as String
                val name = event.params["name"] as String? ?: ""

                mCallUiFragment.stopRinger()

                fragment.onInviteRejected(CommsUser(
                    user_id = userId,
                    name = name,
                    image = null,
                    invite_token = invToken
                ))
            }

            CommsEvents.EVENT_INVITE_ACCEPTED -> {
                val fragment = mWebCallFragment ?: return
                if (fragment.mActiveCall!!.RoomId != roomId) return

                val call = mCallQueue.findCall(roomId) ?: return

                val userId = event.params["user_id"]!!.toInteger()
                val invToken = event.params["invite_token"] as String

                val recipient = event.params["recipient"] as Map<*, *>
                val name = recipient["name"] as String
                var isUserAdded = false

                for (res in call.recipients)
                {
                    if (res.user_id == userId)
                        isUserAdded = true
                }

                if (!isUserAdded)
                {
                    call.addRecipient(CommsUser(user_id = userId, name = name), CommsCallState.Connected)
                }

                mCallUiFragment.stopRinger()

                fragment.onInviteAccepted(CommsUser(
                    user_id = userId,
                    name = "",
                    image = null,
                    invite_token = invToken
                ))
            }

            CommsEvents.EVENT_INVITE_NO_RESPONSE -> {
                val fragment = mWebCallFragment ?: return
                if (fragment.mActiveCall!!.RoomId != roomId) return

                val userId = event.params["user_id"]!!.toInteger()
                val invToken = event.params["invite_token"] as String

                mCallUiFragment.stopRinger()

                fragment.onInviteNoResponse(CommsUser(
                    user_id = userId,
                    name = "",
                    image = null,
                    invite_token = invToken
                ))
            }

            CommsEvents.EVENT_RECORDING_STARTED -> {
                val fragment = mWebCallFragment ?: return
                if (fragment.mActiveCall!!.RoomId != roomId) return

                fragment.onRecordingStarted(event.params)
            }

            CommsEvents.EVENT_RECORDING_STOPPED -> {
                val fragment = mWebCallFragment ?: return
                if (fragment.mActiveCall!!.RoomId != roomId) return

                fragment.onRecordingStopped(event.params)
            }
        }
    }

    private fun onShowError(bundle: Bundle)
    {
        val message = bundle.getString("message")!!

        CommsToast.show(this, message)
    }

    private fun onEventParticipantRinging(params: CommsMapAny)
    {
        Log.e(TAG, "Received eventRecipientRinging")
        val roomId = params["room_id"] as String? ?: return

        val call = mCallQueue.findCall(roomId) ?: return
        val userId = params["user_id"]?.toInteger() ?: return

        call.setRecipientState(userId, CommsCallState.Ringing)

        if(call == mCallQueue.activeCall)
        {
            mCallUiFragment.updateUI()
            if (!mCallUiFragment.isRingerPlaying())
                mCallUiFragment.startRinger(R.raw.ringback_tone)
        }

        val invToken = params["invite_token"] as String? ?: return
        val fragment = mWebCallFragment ?: return
        if (fragment.mActiveCall!!.RoomId != roomId) return

        fragment.onRecipientRinging(CommsUser(
            user_id = userId,
            name = "",
            image = null,
            invite_token = invToken
        ))
    }

    private fun onEventCallEnded(roomId: String)
    {
        val call = mCallQueue.findCall(roomId) ?: return

        if (call == mCallQueue.activeCall)
        {
            runOnUiThread {
                mWebCallContainer?.visibility = View.GONE
                mWebCallFragment.stopCall()
            }

            val message = if(call.State == CommsCallState.Incoming) "Call Missed" else "Call Ended"
            finishCall(roomId, message, 100)
        }
        else
        {
            mCallQueue.removeCall(roomId)
        }
    }

    private fun onCallAccepted(params: CommsMapAny)
    {
        val roomId = params["room_id"] as String? ?: return
        var isCcCall = params["is_cc_call"] as Boolean?
        if (isCcCall == null) isCcCall = false

        val call = mCallQueue.findCall(roomId) ?: return

        mCallUiFragment.stopRinger()

        mCallUiContainer?.visibility = View.GONE
        mWebCallContainer?.visibility = View.VISIBLE

        mWebCallFragment.mActiveCall = call
        mWebCallFragment.mCanInvite = mCanInvite
        mWebCallFragment.mCanRecord = mCanRecord

        if (isCcCall)
        {
            val recipient = params["recipient"] as Map<*, *>
            val userId: Int = (recipient["user_id"] as Double).toInt()
            val name = recipient["name"] as String
            call.addRecipient(CommsUser(user_id = userId, name = name), CommsCallState.Connected)
        }

        mWebCallFragment.startCall()

        call.State = CommsCallState.Connected

        updateCallState(call)
    }

    private fun onAllCCUsersBusy(roomId: String)
    {
        finishCall(roomId, "All Command center users are busy", 100)
//        val call = mCallQueue.findCall(roomId) ?: return
//
//        call.ErrorMessage = "All Command center users are busy"
//        if (call == mCallQueue.activeCall)
//        {
//            call.State = CommsCallState.Error
//            mCallUiFragment?.updateUI()
//            mCallUiContainer?.visibility = View.VISIBLE
//            mCallUiFragment?.stopRinger()
//
//            scheduleErrorAndFinish(100)
//        }
//
//        mCallQueue.removeCall(roomId)
    }

    private fun onStartRecResponse(bundle: Bundle)
    {
        val status = bundle.getString("status")
        if (status != "Ok")
        {
            runOnUiThread {
                mWebCallFragment.onStartRecFailed()
            }
        }
        else
        {
            runOnUiThread {
                mWebCallFragment.onStartRecSuccess()
            }
        }
    }

    private fun onStopRecResponse(bundle: Bundle)
    {
        val status = bundle.getString("status")
        if (status != "Ok")
        {
            runOnUiThread {
                mWebCallFragment.onStopRecFailed()
            }
        }
        else
        {
            runOnUiThread {
                mWebCallFragment.onStopRecSuccess()
            }
        }
    }


    companion object
    {
        private const val TAG = "CallActivity"
    }

    private val mServiceConnection = object: ServiceConnection
    {
        override fun onServiceConnected(name: ComponentName?, service: IBinder?)
        {
            if(service == null)
            {
                Log.e(TAG, "Failed to connect to calls service, is service initialised?")
                return
            }

            Log.e(TAG, "Connected to calls service")
            mService = Messenger(service)

            try
            {
                val msg = Message.obtain(null, CallsService.MSG_REGISTER_CALL)
                msg.replyTo = mMessenger
                mService!!.send(msg)

                Handler().postDelayed({

                    onServiceConnected()

                }, 100)
            }
            catch (ex: RemoteException)
            {
                Log.e(TAG, "Failed to register with calls service, is service initialised?")
            }
        }

        override fun onServiceDisconnected(name: ComponentName?)
        {
            Log.e(TAG, "Disconnected from calls service")
            mService = null
        }
    }

    private class ServiceMessageHandler(activity: CallActivity) : Handler()
    {
        val activityReference = WeakReference(activity)

        override fun handleMessage(msg: Message)
        {
            Log.e(TAG, "Message Received from Service")
            val activity = activityReference.get() ?: return

            if(!activity.onServiceMessage(msg))
                super.handleMessage(msg)
        }
    }

    private fun finishCall(roomId: String, message: String, delay: Long = 1000L)
    {
        val call = mCallQueue.findCall(roomId) ?: return

        call.ErrorMessage = message
        if (call == mCallQueue.activeCall)
        {
            call.State = CommsCallState.Error
            mCallUiFragment.updateUI()
            mCallUiContainer?.visibility = View.VISIBLE
            mCallUiFragment.stopRinger()

            scheduleErrorAndFinish(delay)
        }
        else
        {
            mCallQueue.removeCall(call.RoomId)
        }
    }

    private fun scheduleErrorAndFinish(delay: Long = 1000L)
    {
        Handler().postDelayed({
            playErrorAndFinish()
        }, delay)
    }

    private fun playErrorAndFinish()
    {
        val player = MediaPlayer.create(this, R.raw.error_tone)
        player.start()

        player.setOnCompletionListener {
            Log.e(TAG, "playErrorAndFinish: Error tone finished ")
        }

        // TODO : Sir Atiq below code took out from player listener because few causes were generating due to delay
        val call = mCallQueue.activeCall
        if(call != null)
        {
            mCallQueue.removeCall(call.RoomId)
        }
    }

    private fun onActiveCallChanged()
    {
        val callUi = mCallUiFragment ?: return

        val call = mCallQueue.activeCall!!
        Log.e(TAG, "New Active Call ${call.RoomId}")

        callUi.mActiveCall = call
        callUi.updateUI()

        if(call.State == CommsCallState.Incoming || call.State == CommsCallState.Outgoing)
        {
            mCallUiContainer!!.visibility = View.VISIBLE
        }
        else
        {
            mWebCallContainer!!.visibility = View.VISIBLE
        }

        val webCall = mWebCallFragment
        webCall.mActiveCall = call

        if(call.State == CommsCallState.NextAccepted)
        {
            acceptCall(call)
        }
    }

    private fun updateCallState(call: CommsCall?)
    {
        val bundle = Bundle()
        bundle.putString("call", call?.serialize())
        sendActionToService(CallsService.MSG_ACTIVE_CALL_CHANGED, bundle)
    }

    private fun sendCallQueueToService()
    {
        val list = ArrayList<CommsCall>()

        if (mCallQueue.activeCall != null)
            list.add(mCallQueue.activeCall!!)

        list.addAll(mCallQueue.getItems())

        val intent = Intent(this@CallActivity, CallsService::class.java)
        intent.action = CallsService.UPDATE_SERVICE_CALL_QUEUE
        intent.putExtra("call_queue", list.serialize())
        startService(intent)
    }

    override fun onActiveCallChanged(call: CommsCall?, oldCall: CommsCall?)
    {
        updateCallState(call)
        sendCallQueueToService()

        if (call == null)
        {
            if (mService != null) {
                val msg = Message.obtain(null, CallsService.MSG_UNREGISTER_CALL)
                mService!!.send(msg)
            }
        }
        else
        {
            Log.e(TAG, "Another call became active ${call.RoomId}")
            onActiveCallChanged()
        }
    }

    override fun onCallQueueChanged()
    {
        Log.e(TAG, "onCallQueueChanged")
        mCallQueueAdapter.notifyDataSetChanged()

        sendCallQueueToService()
    }

    override fun onConfigurationChanged(newConfig: Configuration)
    {
        super.onConfigurationChanged(newConfig)
    }

    private fun allPermissionGranted(): Boolean
    {
        mPermissionsNotGranted.clear()
        cameraCheckGranted()
        micCheckGranted()
        phoneCheckGranted()

        if(mPermissionsNotGranted.size == 0)
        {
            return true
        }

        ActivityCompat.requestPermissions(
            this@CallActivity,
            mPermissionsNotGranted.toTypedArray(), RC_ALL_PERMISSIONS)

        return false
    }

    private fun cameraCheckGranted()
    {
        val res = ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
        if (res == PackageManager.PERMISSION_GRANTED)
            return

        mPermissionsNotGranted.add(Manifest.permission.CAMERA)
    }

    private fun micCheckGranted()
    {
        val res = ActivityCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO)
        if (res == PackageManager.PERMISSION_GRANTED)
            return

        mPermissionsNotGranted.add(Manifest.permission.RECORD_AUDIO)
    }

    private fun phoneCheckGranted()
    {
        val res = ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE)
        if (res == PackageManager.PERMISSION_GRANTED)
            return

        mPermissionsNotGranted.add(Manifest.permission.READ_PHONE_STATE)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray)
    {
        var deniedAny = false
        val call = mCallQueue.activeCall

        if(requestCode == RC_ALL_PERMISSIONS)
        {
            for(i in grantResults)
            {
                if(i != PackageManager.PERMISSION_GRANTED)
                    deniedAny = true
            }

            if(deniedAny)
            {
                if(call!!.State == CommsCallState.Incoming)
                {
                    rejectCall(call)
                }
                else
                {
                    endCall(call.RoomId)
                }
            }
        }
    }
}
