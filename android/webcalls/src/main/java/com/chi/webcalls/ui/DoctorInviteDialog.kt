package com.chi.webcalls.ui

import android.content.Context
import android.content.res.ColorStateList
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import com.chi.webcalls.R
import com.chi.webcalls.common.GenericAdapter
import com.chi.webcalls.comms.Doctor
import com.chi.webcalls.comms.DoctorList
import com.chi.webcalls.comms.Patient
import com.chi.webcalls.comms.PatientList
import com.chi.webcalls.databinding.InviteDialogBinding
import com.chi.webcalls.network.ApiCallback
import com.codegini.lib.controls.datatable.DataTableFragment
import com.codegini.lib.network.ApiSingleResponse
import com.codegini.lib.network.CgThrowable
import com.codegini.lib.network.ListRequest
import com.codegini.lib.network.WhereData

class DoctorInviteDialog(participants: ArrayList<Int>, user_id: Int) : DialogFragment()
{
    lateinit var binding: InviteDialogBinding
    private lateinit var mListener: DoctorInviteListener
    protected var mContext: Context? = null
    protected var mActivity: CallActivity? = null
    private var mDoctorsList: ArrayList<Doctor> = ArrayList()
    private var mPatientsList: ArrayList<Doctor> = ArrayList()
    private lateinit var doctorsAdapter: GenericAdapter<Doctor>
    private lateinit var patientsAdapter: GenericAdapter<Doctor>
    var mCheckedId = R.id.btnDoctors
    var status = "STATUS_DOCTOR"
    var totalRecords = 0
    var offset = 0
    internal var bIsLoadingData: Boolean = false
    var mParticipants = participants
    val mUserId = user_id

    val mDataTable: DataTableFragment<Doctor> = DataTableFragment.newInstance<Doctor>("doctors")


    override fun onAttach(context: Context)
    {
        super.onAttach(context)

        mContext = context
        if(context is CallActivity)
        {
            mActivity = context
        }
        else
        {
            throw Exception("Should implement Listner")
        }
    }

    fun setListener(listener: DoctorInviteListener)
    {
        mListener = listener
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
    {
        binding = DataBindingUtil.inflate<InviteDialogBinding>(
            inflater,
            R.layout.invite_dialog,
            container,
            false
        )

        doctorsAdapter = GenericAdapter(mDoctorsList, mContext!!, R.layout.invite_dialog_cell)
        { view: View, it: Doctor ->
            mListener.onDoctorSelected(it)
        }
        binding.rvDoctors.adapter = doctorsAdapter

        patientsAdapter = GenericAdapter(mPatientsList, mContext!!, R.layout.invite_dialog_cell)
        { view: View, it: Doctor ->
            mListener.onDoctorSelected(it)
        }
        binding.rvPatients.adapter = patientsAdapter

        binding.btnToggleGroup.check(R.id.btnDoctors)
        binding.btnToggleGroup.addOnButtonCheckedListener { group, checkedId, isChecked ->

            if (isChecked)
            {
                mCheckedId = checkedId
                when (checkedId)
                {
                    R.id.btnPatients ->
                    {
                        status = "STATUS_PATIENT"
                        loadDataPatient(binding.txtSearch.editText!!.text.toString())
                        updateUI(true)
                    }
                    R.id.btnDoctors ->
                    {
                        status = "STATUS_DOCTOR"
                        loadDataDoctors(binding.txtSearch.editText!!.text.toString())
                        updateUI(false)
                    }
                }
            }
            else
            {
                if (mCheckedId == checkedId)
                {
                    group.check(checkedId)
                }
            }
        }

        binding.txtFieldSearch.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

            }

            override fun afterTextChanged(editable: Editable) {
                if (!bIsLoadingData)
                {
                    if (binding.txtSearch.editText!!.text.isNotEmpty())
                    {
                        binding.txtSearch.error = null
                        when(status)
                        {
                            "STATUS_PATIENT" -> loadDataPatient(binding.txtSearch.editText!!.text.toString())
                            "STATUS_DOCTOR" -> loadDataDoctors(binding.txtSearch.editText!!.text.toString())
                        }

                    }
                    else
                    {
                        when(status)
                        {
                            "STATUS_PATIENT" -> loadDataPatient(null)
                            "STATUS_DOCTOR" -> loadDataDoctors(null)
                        }
                    }
                }
            }
        })

        loadDataDoctors(null)
//        loadDataPatient(null)
//        updateUI(false)
//        addScrollListener()

        binding.handler = this
        return binding.root
    }

    override fun onStart() {
        super.onStart()

        val dialog = getDialog()
        if (dialog != null && dialog!!.getWindow() != null) {
            val width = ViewGroup.LayoutParams.MATCH_PARENT
            val height = ViewGroup.LayoutParams.MATCH_PARENT

            dialog!!.getWindow()!!.setLayout(width, height)
//            dialog.window!!.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        }
    }

    override fun onResume() {
        super.onResume()
    }

    fun onAppbarClose(view: View)
    {
        mListener.onDialogClose()
    }

    fun onBtnInviteCC(view: View)
    {
        mListener.onInviteCC()
    }

//    private fun addScrollListener() {
//        binding.rvDoctors.addOnScrollListener(object : RecyclerView.OnScrollListener() {
//            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
//                super.onScrolled(recyclerView, dx, dy)
//
//                if (bIsLoadingData)
//                    return
//
//                if (doctorsAdapter.getItemCount() < totalRecords && dy != 0)
//                {
//                    loadDataDoctors()
//                }
//            }
//        })
//    }

    fun loadDataDoctors(search: String?)
    {
        Log.e("DoctorDialog", "loadDataDoctors: $search +++++++++++++++++++++")
        bIsLoadingData = true
        mDoctorsList.clear()
        val req =  ListRequest()
        req.columns = listColumns()
        req.search = search
        req.offset = 0
        val where = WhereData()
        req.where = where
        where.group = "and"
        where.children = arrayOf(
            WhereData("user_id", mUserId, "ne")
        )
        WhereData("user.primary_group.group_name", "Doctor", "like")

        val call = mActivity!!.apiService.Contact(req)
        call.enqueue(object : ApiCallback<ApiSingleResponse<DoctorList>>(mContext!!)
        {
            override fun onSuccess(resp: ApiSingleResponse<DoctorList>)
            {
//                setButtonsEnableToggle()
                bIsLoadingData = false

                if (resp.data.data.isEmpty())
                {
                    binding.rvDoctors.visibility = View.GONE
                    binding.rvPatients.visibility = View.GONE
                    binding.locNoRecord.visibility = View.VISIBLE
                    doctorsAdapter.notifyDataSetChanged()
                    return
                }

                updateUI(false)
                totalRecords = resp.data.total_records
//                offset += resp.data.data.size
                mDoctorsList.addAll(getDoctorsList(resp.data.data))
                doctorsAdapter.notifyDataSetChanged()
            }

            override fun onError(t: CgThrowable)
            {
                bIsLoadingData = false
                mActivity!!.showAlert(mActivity!!.getString(R.string.error_loading_data), t.ErrorMessage)
            }
        })
    }

    private fun loadDataPatient(search: String?) {
        bIsLoadingData = true
        mPatientsList.clear()
        val req = ListRequest()
        req.offset = 0
        req.limit = 30
        req.search = search

        val call = mActivity!!.apiService.GetPatients(req)
        call.enqueue(object : ApiCallback<ApiSingleResponse<PatientList>>(mContext!!) {
            @RequiresApi(Build.VERSION_CODES.O)
            override fun onSuccess(resp: ApiSingleResponse<PatientList>) {

//                setButtonsEnableToggle()
                bIsLoadingData = false

                if (resp.data.data.isEmpty())
                {
                    binding.rvDoctors.visibility = View.GONE
                    binding.rvPatients.visibility = View.GONE
                    binding.locNoRecord.visibility = View.VISIBLE
                    patientsAdapter.notifyDataSetChanged()
                    return
                }

                updateUI(true)
                mPatientsList.addAll(getPatientList(resp.data.data))
                patientsAdapter.notifyDataSetChanged()
            }

            override fun onError(t: CgThrowable) {
                bIsLoadingData = false
                mActivity!!.showAlert(
                    mActivity!!.getString(R.string.error_loading_data),
                    t.ErrorMessage
                )
            }
        })
    }

    private fun updateUI(isPatient: Boolean)
    {
        if (isPatient)
        {
            binding.rvPatients.visibility = View.VISIBLE
            binding.locNoRecord.visibility = View.GONE
            binding.rvDoctors.visibility = View.GONE
        }
        else
        {
            binding.rvDoctors.visibility = View.VISIBLE
            binding.locNoRecord.visibility = View.GONE
            binding.rvPatients.visibility = View.GONE
        }
    }

    private fun getPatientList(list: List<Patient>): ArrayList<Doctor> {

        val patientList = ArrayList<Doctor>()
        for (item in list)
        {
            if (!mParticipants.contains(item.patient_user_id))
                patientList.add(Doctor(item.id, item.patient_user_id, item.name, item.image))
        }

        return patientList
    }

    private fun getDoctorsList(list: List<Doctor>): ArrayList<Doctor> {

        val doctorList = ArrayList<Doctor>()
        for (item in list)
        {
            if (!mParticipants.contains(item.user_id))
                doctorList.add(item)
        }

        return doctorList
    }

    private fun listColumns(): Array<String> {
        return arrayOf(
            "user_id",
            "employee_no",
            "image",
            "full_name",
            "user.primary_group.group_name",
            "gender",
            "mobile_phone",
            "date_added",
            "date_updated"
        )
    }

    private fun setButtonsDisableToggle()
    {
        binding.btnPatients.isClickable = false
        binding.btnDoctors.isClickable = false
    }

    fun setButtonsEnableToggle()
    {
        when (status)
        {
            "STATUS_PATIENT" ->
            {
                binding.btnPatients.isClickable = false
                binding.btnDoctors.isClickable = true
            }
            "STATUS_DOCTOR" ->
            {
                binding.btnDoctors.isClickable = false
                binding.btnPatients.isClickable = true
            }
        }
    }

    fun onTypeChanged(view: View) {
        when (view.tag)
        {
            getString(R.string.patients) ->
            {
                status = "STATUS_PATIENT"
                loadDataPatient(binding.txtSearch.editText!!.text.toString())
                updateUI(true)
            }
            getString(R.string.employees) ->
            {
                status = "STATUS_DOCTOR"
                loadDataDoctors(binding.txtSearch.editText!!.text.toString())
                updateUI(false)
            }
        }
    }

    interface DoctorInviteListener
    {
        fun onDoctorSelected(doctor: Doctor)
        fun onInviteCC()
        fun onDialogClose()
    }
}
