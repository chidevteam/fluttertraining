package com.chi.webcalls.comms

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothHeadset
import android.bluetooth.BluetoothProfile
import android.bluetooth.BluetoothProfile.ServiceListener
import android.content.Context
import android.util.Log

interface BluetoothHeadSetListener
{
    fun connectedToProxy(aBluetoothHeadset: BluetoothHeadset?)
    fun disconnectedToProxy()
}

class BluetoothHeadsetManager(context: Context?)
{
    private var mBluetoothHeadset: BluetoothHeadset? = null

    private var mBluetoothAdapter: BluetoothAdapter? = null

    private val mContext: Context? = context

    private var mCallBackListener: BluetoothHeadSetListener? = null

    init
    {
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
    }

    fun addListener(aListener: BluetoothHeadSetListener)
    {
        mCallBackListener = aListener
    }

    fun hasEnableBluetooth(): Boolean
    {
        return mBluetoothAdapter!!.isEnabled
    }

    fun connectionToProxy()
    {
        mBluetoothAdapter!!.getProfileProxy(mContext, mProfileListener, BluetoothProfile.HEADSET)
    }

    fun disconnectProxy()
    {
        mBluetoothAdapter!!.closeProfileProxy(BluetoothProfile.HEADSET, mBluetoothHeadset)
    }

    private fun getBTHandSetInstance(): BluetoothHeadset?
    {
        return mBluetoothHeadset
    }

    private val mProfileListener: ServiceListener = object : ServiceListener
    {
        override fun onServiceConnected(profile: Int, proxy: BluetoothProfile)
        {
            Log.e("BluetoothHeadsetManager", "onServiceConnected: 1")
            if (profile == BluetoothProfile.HEADSET)
            {
                Log.e("BluetoothHeadsetManager", "onServiceConnected: 2")
                mBluetoothHeadset = proxy as BluetoothHeadset
                mCallBackListener!!.connectedToProxy(getBTHandSetInstance())
            }
        }

        override fun onServiceDisconnected(profile: Int)
        {
            Log.e("BluetoothHeadsetManager", "onServiceDisconnected: 1")
            if (profile == BluetoothProfile.HEADSET)
            {
                Log.e("BluetoothHeadsetManager", "onServiceDisconnected: 2")
                mBluetoothHeadset = null
                mCallBackListener!!.disconnectedToProxy()
            }
        }
    }
}