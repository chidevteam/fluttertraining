package com.chi.webcalls.network

import android.content.Context
import com.google.gson.GsonBuilder
import com.google.gson.JsonDeserializer
import com.google.gson.JsonPrimitive
import com.google.gson.JsonSerializer
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*
import java.util.concurrent.TimeUnit


class ApiClient(context: Context, private val mSiteCode: String)
{
    var bIsAuthenticated: Boolean = false
    private var m_AuthToken: String? = null

    private val m_Cookie = "2|0a03a1c2|c45f715c5df81edf12b3a274d39cad5c|%d"
    private var m_XSRF: String? = null
    private val m_XSRFTime: Long = 0

    var mBaseUrl: String? = null

    val apiService: ApiService


    init
    {
//        API_BASE_URL = com.chi.doctorapp.utils.Preferences.getString(context,context.getString(R.string.App_base_url))
//        if (API_BASE_URL == null)
//            API_BASE_URL = BuildConfig.BaseUrl
        mBaseUrl = String.format(Locale.ENGLISH, "https://%s.cognitivehealthintl.com", mSiteCode)

        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

        val okHttpClient = OkHttpClient.Builder()
            .readTimeout(25, TimeUnit.SECONDS)
            .connectTimeout(25, TimeUnit.SECONDS)
            .addInterceptor(loggingInterceptor)
            .addInterceptor { chain ->
                var request: okhttp3.Request = chain.request()

                val ts = Date().time / 1000
                if (ts - m_XSRFTime > 3600)
                {
                    m_XSRF = String.format(Locale.ENGLISH, m_Cookie, ts)
                }

                val xsrf = m_XSRF!!

                if (m_AuthToken != null)
                {
                    val builder = request.newBuilder()
                        .addHeader("Authorization", m_AuthToken!!)
                        .addHeader("X-XSRFToken", xsrf)
                        .addHeader("Cookie", "_xsrf=$xsrf")

                    request = builder.build()
                }
                else
                {
                    val builder = request.newBuilder()
                        .addHeader("X-XSRFToken", xsrf)
                        .addHeader("Cookie", "_xsrf=$xsrf")

                    request = builder.build()
                }

                chain.proceed(request)

            }.build()


        val builder = GsonBuilder()
        @Suppress("UNUSED_PARAMETER")
        builder.registerTypeAdapter(Date::class.java, JsonDeserializer<Date> { json, _, _ ->
            Date(json.getAsJsonPrimitive().getAsLong() * 1000)
        })

        builder.registerTypeAdapter(Date::class.java, JsonSerializer<Date> { date, _, _ ->
            JsonPrimitive(date.getTime() / 1000)
        })

        builder.setLenient()
        val gson = builder.create()

        val retrofit = Retrofit.Builder()
            .baseUrl(mBaseUrl!!)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()

        apiService = retrofit.create(ApiService::class.java)

    }

    companion object
    {
        private var _apiClient: ApiClient? = null

        fun setAuthToken(token: String?)
        {
            if (_apiClient == null)
                return

            _apiClient!!.m_AuthToken = token
            _apiClient!!.bIsAuthenticated = token != null
        }

        fun apiService(context: Context, siteCode: String): ApiService
        {
            _apiClient = ApiClient(context, siteCode)
            return _apiClient!!.apiService
        }

        fun getAuthToken(): String?
        {
            if(_apiClient == null)
                return null

            return _apiClient!!.m_AuthToken
        }

        fun getSocketUrl(): String
        {
            return  _apiClient!!.mBaseUrl!!.replace("http", "ws")
        }
    }
}