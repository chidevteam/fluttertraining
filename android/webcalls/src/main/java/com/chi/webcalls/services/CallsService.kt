package com.chi.webcalls.services

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.*
import android.util.Log
import androidx.core.app.NotificationCompat
import com.chi.webcalls.R
import com.chi.webcalls.comms.*
import com.chi.webcalls.deserialize
import com.chi.webcalls.serialize
import com.chi.webcalls.toInteger
import com.chi.webcalls.ui.CallActivity
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.lang.ref.WeakReference


class CallsService : Service()
{
    private val mClients = ArrayList<Messenger>()

    private var mCommsManager: CommsManager? = null
    private var mMessenger: Messenger? = null
    private var mCallMessenger: Messenger? = null
    private var mSiteCode: String? = null
    private var mToken: String? = null
    private var mCanInvite = false
    private var mCanRecord = false

    private val mCallQueue: ArrayList<CommsCall> = ArrayList()

    override fun onCreate()
    {
        super.onCreate()

        Log.e(TAG, "onCreate")

        mMessenger = Messenger(ClientMessageHandler(this))

        startForeground(NOTIFICATION_ID, getNotification())
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int
    {
        super.onStartCommand(intent, flags, startId)

        Log.e(TAG, "onStartCommand")

        if (intent == null)
        {
            Log.e(TAG, "No Intent?")
            return START_STICKY
        }

        Log.e(TAG, "Action: ${intent.action}")

        when(intent.action)
        {
            ACTION_INIT_CALLS -> {
                initializeService(intent)
            }

            ACTION_CALL_USER -> {
                callUser(intent)
            }

            ACTION_CALL_CC -> {
                callCcUser(intent)
            }

            UPDATE_SERVICE_CALL_QUEUE -> {
                updateCallQueue(intent)
            }
        }

        return START_STICKY
    }

    override fun onDestroy()
    {
        mCommsManager?.shutdown()

        stopSelf()

        super.onDestroy()
    }

    override fun onTaskRemoved(rootIntent: Intent?) {
        super.onTaskRemoved(rootIntent)
        onFinishingNonEndedCalls()
        mCallMessenger = null
        mCommsManager?.setActiveCall(null)
    }

    override fun onBind(intent: Intent): IBinder?
    {
        if(mCommsManager == null)
        {
            Log.e(TAG, "Client trying to bind before service is initialised")
            return null
        }

        return mMessenger?.binder
    }

    // public methods, in service?
    fun onConnectionStateChanged(connected: Boolean)
    {
        if (connected)
            startForeground(NOTIFICATION_ID, getNotification("Call Signals Connected", false))
        else
            startForeground(NOTIFICATION_ID, getNotification("Call Signals Disconnected", true))
    }

    fun onIncomingCall(event: CommsEvent)
    {
        val params = event.params ?: return

        val call = CommsCall.makeIncomingCall(params)

        val bundle = Bundle()
        bundle.putString(K_CALL, call.serialize())
        bundle.putString(K_CALLS_SITE_CODE, mSiteCode)
        bundle.putString(K_CALLS_TOKEN, mToken)

        startOrSendToCallActivity(ACTION_INCOMING_CALL, bundle)

        mCommsManager?.sendReplyToServer(event)
    }

    fun onServerEvent(event: CommsIpcAction)
    {
        Log.e(TAG, "onServerEvent\n${event.serialize()}")
        val messenger = mCallMessenger ?: return

        val msg = Message.obtain(null, CLIENT_MSG_SERVER_EVENT)
        val bundle = Bundle()
        bundle.putString("event", event.serialize())

        msg.data = bundle
        messenger.send(msg)
    }

    private fun initializeService(intent: Intent)
    {
        if(mCommsManager == null)
        {
            val siteCode = intent.getStringExtra(K_CALLS_SITE_CODE)!!
            val token = intent.getStringExtra(K_CALLS_TOKEN)!!

            mCommsManager = CommsManager(this, siteCode, token)
            mCommsManager!!.connect()

            mCanInvite = intent.getBooleanExtra(K_INVITE_ALLOWED, false)
            mCanRecord = intent.getBooleanExtra(K_RECORD_ALLOWED, false)
            Log.e(TAG, "Initializing Service, CanInvite: $mCanInvite")
            mSiteCode = siteCode
            mToken = token
        }
    }

    private fun callUser(intent: Intent)
    {
        if (mCommsManager == null)
        {
            Log.e(TAG, "CommsManager is null")
            return
        }

        val callType = intent.getStringExtra(K_CALL_TYPE)!!
        val recipient = intent.getStringExtra(K_CALL_RECIPIENT)!!
        val caller = intent.getStringExtra(K_CALL_CALLER)!!


        val bundle = Bundle()
        bundle.putString(K_CALL_TYPE, callType)
        bundle.putString(K_CALL_CALLER, caller)
        bundle.putString(K_CALL_RECIPIENT, recipient)
        bundle.putString(K_CALLS_SITE_CODE, mSiteCode)
        bundle.putString(K_CALLS_TOKEN, mToken)

        startOrSendToCallActivity(ACTION_CALL_USER, bundle)
    }

    private fun callCcUser(intent: Intent)
    {
        if (mCommsManager == null)
        {
            Log.e(TAG, "CommsManager is null")
            return
        }

        val callType = intent.getStringExtra(K_CALL_TYPE)!!
        val caller = intent.getStringExtra(K_CALL_CALLER)!!


        val bundle = Bundle()
        bundle.putString(K_CALL_TYPE, callType)
        bundle.putString(K_CALL_CALLER, caller)
        bundle.putString(K_CALLS_SITE_CODE, mSiteCode)
        bundle.putString(K_CALLS_TOKEN, mToken)

        startOrSendToCallActivity(ACTION_CALL_CC, bundle)
    }

    private fun updateCallQueue(intent: Intent)
    {
        val data = intent.getStringExtra("call_queue") ?: return
        val itemType = object : TypeToken<ArrayList<CommsCall>>() {}.type
        val callQueue = Gson().fromJson<ArrayList<CommsCall>>(data, itemType)

        mCallQueue.clear()
        mCallQueue.addAll(callQueue)
        Log.e(TAG, "updateCallQueue: ${mCallQueue.size} +++++++++++++++++++++++++++++++++ ")
    }

    private fun startOrSendToCallActivity(action: String, bundle: Bundle)
    {
        val messenger = mCallMessenger

        bundle.putBoolean(K_INVITE_ALLOWED, mCanInvite)
        bundle.putBoolean(K_RECORD_ALLOWED, mCanRecord)

        if (messenger == null)
        {
            Log.e(TAG, "Starting CallActivity")

            val i2 = Intent(this, CallActivity::class.java)

            i2.action = action
            i2.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP)
            i2.putExtras(bundle)

            startActivity(i2)
        }
        else
        {
            Log.e(TAG, "Sending Message to CallActivity")

            bundle.putString("action", action)
            val msg = Message.obtain(null, CLIENT_MSG_SERVER_EVENT)
            msg.data = bundle
            messenger.send(msg)
        }
    }

    private fun onClientActionCallUser(msg: Message)
    {
        val manager = mCommsManager ?: return
        val callObj = msg.data.getString("call") ?: return

        val call = callObj.deserialize(CommsCall::class.java)

        manager.onCallUser(call) { result: CommsMapAny?, error: CommsMap? ->

            Log.e(TAG, "callUser Response: Result: $result ======= Error: $error" )

            val bundle = Bundle()
            bundle.putString("room_id", call.RoomId)
            bundle.putString("result", result?.serialize())
            bundle.putString("error", error?.serialize())

            val resp = Message.obtain(null, CLIENT_MSG_CALL_RESPONSE)
            resp.data = bundle

            if (mCallMessenger == null)
                Log.e(TAG, "mCallMessenger is null" )
            else
                mCallMessenger!!.send(resp)
        }
    }

    private fun onClientActionCallCC(msg: Message)
    {
        val manager = mCommsManager ?: return
        val callObj = msg.data.getString("call") ?: return

        val call = callObj.deserialize(CommsCall::class.java)

        manager.onCallCc(call) { result: CommsMapAny?, error: CommsMap? ->

            Log.e(TAG, "onCallCc Response: Result: $result ======= Error: $error" )

            val bundle = Bundle()
            bundle.putString("room_id", call.RoomId)
            bundle.putString("result", result?.serialize())
            bundle.putString("error", error?.serialize())

            val resp = Message.obtain(null, CLIENT_MSG_CALL_RESPONSE)
            resp.data = bundle

            if (mCallMessenger == null)
                Log.e(TAG, "mCallMessenger is null" )
            else
                mCallMessenger!!.send(resp)
        }
    }

    private fun onClientActionRejectCall(msg: Message)
    {
        val manager = mCommsManager ?: return
        val callObj = msg.data.getString("call") ?: return

        val call = callObj.deserialize(CommsCall::class.java)

        manager.onRejectCall(call) { result: CommsMapAny?, error: CommsMap? ->
            Log.e(TAG, "onRejectCall Response: Result: $result ======= Error: $error" )
        }
    }

    private fun onClientActionEndCall(msg: Message)
    {
        val manager = mCommsManager ?: return
        val callObj = msg.data.getString("call") ?: return

        val call = callObj.deserialize(CommsCall::class.java)

        manager.onEndCall(call) { result: CommsMapAny?, error: CommsMap? ->
            Log.e(TAG, "onEndCall Response: Result: $result ======= Error: $error" )
        }
    }

    private fun onClientActionAcceptCall(msg: Message)
    {
        val manager = mCommsManager ?: return
        val callObj = msg.data.getString("call") ?: return

        val call = callObj.deserialize(CommsCall::class.java)

        manager.onAcceptCall(call) { result: CommsMapAny?, error: CommsMap? ->
            Log.e(TAG, "onAcceptCall Response: Result: $result ======= Error: $error" )
        }
    }

    private fun onClientActionInviteUser(msg: Message)
    {
        val manager = mCommsManager ?: return
        val callObj = msg.data.getString("call") ?: return
        val userObj = msg.data.getString("user") ?: return

        val call = callObj.deserialize(CommsCall::class.java)
        val user = userObj.deserialize(CommsUser::class.java)

        manager.onInviteUser(user, call) { result: CommsMapAny?, error: CommsMap? ->
            Log.e(TAG, "onInviteUser Response: Result: $result ======= Error: $error" )

            if(result != null)
            {
                val status = result["Status"] as String

                if (status != "Ok")
                {
                    sendErrorToClient(result["ErrorCode"]!!.toInteger(), result["ErrorMessage"] as String)
                }
            }
        }
    }

    private fun onClientActionInviteCC(msg: Message)
    {
        val manager = mCommsManager ?: return
        val callObj = msg.data.getString("call") ?: return
        val inviteToken = msg.data.getString("invite_token") ?: return

        val call = callObj.deserialize(CommsCall::class.java)

        manager.onInviteCC(call, inviteToken) { result: CommsMapAny?, error: CommsMap? ->
            Log.e(TAG, "onInviteUser Response: Result: $result ======= Error: $error" )

            if(result != null)
            {
                val status = result["Status"] as String

                if (status != "Ok")
                {
                    sendErrorToClient(result["ErrorCode"]!!.toInteger(), result["ErrorMessage"] as String)
                }
            }
        }
    }

    private fun onClientActionStartRecording(msg: Message)
    {
        val manager = mCommsManager ?: return
        val callObj = msg.data.getString("call") ?: return
        val call = callObj.deserialize(CommsCall::class.java)

        manager.onStartRecording(call) { result: CommsMapAny?, error: CommsMap? ->
            Log.e(TAG, "onStartRecording Response: Result: $result ======= Error: $error" )

            if(result != null)
            {
//                Log.e(TAG, "startRecording: step 3")

                val status = result["Status"] as String

                if (status != "Ok")
                {
                    sendErrorToClient(result["ErrorCode"]!!.toInteger(), result["ErrorMessage"] as String)
                }

                val bundle = Bundle()
                bundle.putString("status", status)

                val resp = Message.obtain(null, CLIENT_MSG_START_REC_RESPONSE)
                resp.data = bundle

                if (mCallMessenger == null)
                    Log.e(TAG, "mCallMessenger is null" )
                else
                    mCallMessenger!!.send(resp)
            }
        }
    }

    private fun onClientActionStopRecording(msg: Message)
    {
        val manager = mCommsManager ?: return
        val callObj = msg.data.getString("call") ?: return
        val call = callObj.deserialize(CommsCall::class.java)

        manager.onStopRecording(call) { result: CommsMapAny?, error: CommsMap? ->
            Log.e(TAG, "onStopRecording Response: Result: $result ======= Error: $error" )

            if(result != null)
            {
//                Log.e(TAG, "stopRecording: step 3")

                val status = result["Status"] as String

                if (status != "Ok")
                {
                    sendErrorToClient(result["ErrorCode"]!!.toInteger(), result["ErrorMessage"] as String)
                }

                val bundle = Bundle()
                bundle.putString("status", status)

                val resp = Message.obtain(null, CLIENT_MSG_STOP_REC_RESPONSE)
                resp.data = bundle

                if (mCallMessenger == null)
                    Log.e(TAG, "mCallMessenger is null" )
                else
                    mCallMessenger!!.send(resp)
            }
        }
    }

    private fun onClientActiveCallChanged(msg: Message)
    {
        val manager = mCommsManager ?: return
        val callObj = msg.data.getString("call")

        if(callObj == null)
        {
            Log.e(TAG, "Client Active Call Changed, Call = null")
            manager.setActiveCall(null)
        }
        else
        {
            val call = callObj.deserialize(CommsCall::class.java)
            Log.e(TAG, "Client Active Call Changed, Call = ${call.RoomId}")

            manager.setActiveCall(call)
        }
    }

    private fun onFinishingActivity()
    {
        val resp = Message.obtain(null, CLIENT_MSG_END_ACTIVITY)

        if (mCallMessenger == null)
            Log.e(TAG, "mCallMessenger is null" )
        else
            mCallMessenger!!.send(resp)
    }

    private fun onFinishingNonEndedCalls()
    {
        Log.e(TAG, "onFinishingNonEndedCalls: ${mCallQueue.size}  +++++++++++++++++++++++++++++++++ ")
        if (mCallQueue.size > 0)
        {
            for (call in mCallQueue)
            {
                Log.e(TAG, "onFinishingNonEndedCalls: RoomId.............. ${call.RoomId}")
                val manager = mCommsManager ?: return
                manager.onEndCall(call) { result: CommsMapAny?, error: CommsMap? ->
                    Log.e(TAG, "onEndCall Response: Result: $result ======= Error: $error" )
                }
            }
        }
    }

    private fun getNotification(): Notification
    {
        return getNotification("")
    }

    private fun getNotification(message: String, error: Boolean = false): Notification
    {
        val name = getString(R.string.app_name)

        val builder = NotificationCompat.Builder(this, CHANNEL_ID)
            .setContentTitle("CHI Calls")
            .setContentText(message)

        if (error)
            builder.setSmallIcon(R.drawable.ic_disconnected)
        else
            builder.setSmallIcon(R.drawable.chi_logo)

        val channel = NotificationChannel(CHANNEL_ID, name, NotificationManager.IMPORTANCE_LOW)
        channel.description = "CHI Calls Channel"
        channel.setShowBadge(false)

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.createNotificationChannel(channel)

        return builder.setPriority(NotificationCompat.PRIORITY_LOW)
            .setCategory(Notification.CATEGORY_SERVICE)
            .build()
    }

    private fun sendErrorToClient(code: Int, message: String)
    {
        val messenger = mCallMessenger ?: return

        val msg = Message.obtain(null, CLIENT_MSG_SHOW_ERROR)
        val bundle = Bundle()
        bundle.putInt("code", code)
        bundle.putString("message", message)

        msg.data = bundle
        messenger.send(msg)
    }


    companion object
    {
        const val ACTION_INIT_CALLS = "InitCalls"
        const val ACTION_CALL_USER = "CallUser"
        const val ACTION_CALL_CC = "CallCC"
        const val ACTION_INCOMING_CALL = "IncomingCall"

        const val K_CALL = "call"
        const val K_CALLS_SITE_CODE = "siteCode"
        const val K_CALLS_TOKEN = "token"
        const val K_CALL_TYPE = "callType"
        const val K_CALL_RECIPIENT = "callRecipient"
        const val K_CALL_CALLER = "callCaller"
        const val K_INVITE_ALLOWED = "inviteAllowed"
        const val K_RECORD_ALLOWED = "recordAllowed"

        const val MSG_REGISTER_CLIENT = 10
        const val MSG_UNREGISTER_CLIENT = 20
        const val MSG_REGISTER_CALL = 30
        const val MSG_UNREGISTER_CALL = 40

        const val MSG_ACTION_CALL_USER = 50
        const val MSG_ACTION_REJECT_CALL = 51
        const val MSG_ACTION_END_CALL = 52
        const val MSG_ACTION_ACCEPT_CALL = 53
        const val MSG_ACTION_INVITE_USER = 54
        const val MSG_ACTIVE_CALL_CHANGED = 55
        const val MSG_ACTION_CALL_CC = 56
        const val MSG_ACTION_INVITE_CC = 57

        const val MSG_ACTION_START_RECORDING= 58
        const val MSG_ACTION_STOP_RECORDING= 59

//        const val CLIENT_MSG_CALL_USER = 100
        const val CLIENT_MSG_CALL_RESPONSE = 101
        const val CLIENT_MSG_SERVER_EVENT = 102
        const val CLIENT_MSG_SHOW_ERROR = 103
        const val CLIENT_MSG_END_ACTIVITY = 104
        const val CLIENT_MSG_START_REC_RESPONSE = 105
        const val CLIENT_MSG_STOP_REC_RESPONSE = 106

        const val UPDATE_SERVICE_CALL_QUEUE = "ServiceCallQueue"

        private const val TAG = "CallsService"
        private const val NOTIFICATION_ID = 3001
        private const val CHANNEL_ID = "chi_call_channel_01"
    }


    private class ClientMessageHandler(service: CallsService) : Handler()
    {
        private val serviceReference = WeakReference(service)

        override fun handleMessage(msg: Message)
        {
            val service = serviceReference.get() ?: return

            when(msg.what)
            {
                MSG_REGISTER_CLIENT -> {
                    service.mClients.add(msg.replyTo)
                }

                MSG_UNREGISTER_CLIENT -> {
                    service.mClients.remove(msg.replyTo)
                }

                MSG_REGISTER_CALL -> {
                    service.mCallMessenger = msg.replyTo

                    Log.e(TAG, "CallActivity Registered")

                    if(msg.replyTo == null)
                        Log.e(TAG, "CallActivity Registered - ReplyTo is Null")
                    else
                        Log.e(TAG, "CallActivity Registered - ReplyTo is NOT Null")
                }

                MSG_UNREGISTER_CALL -> {
                    service.onFinishingActivity()

                    service.mCallMessenger = null

                    Log.e(TAG, "CallActivity Unregistered")

                    service.mCommsManager?.setActiveCall(null)
                }

                MSG_ACTION_CALL_USER -> {
                    service.onClientActionCallUser(msg)
                }

                MSG_ACTION_REJECT_CALL -> {
                    service.onClientActionRejectCall(msg)
                }

                MSG_ACTION_END_CALL -> {
                    service.onClientActionEndCall(msg)
                }

                MSG_ACTION_ACCEPT_CALL -> {
                    service.onClientActionAcceptCall(msg)
                }

                MSG_ACTION_INVITE_USER -> {
                    service.onClientActionInviteUser(msg)
                }

                MSG_ACTIVE_CALL_CHANGED -> {
                    service.onClientActiveCallChanged(msg)
                }

                MSG_ACTION_CALL_CC -> {
                    service.onClientActionCallCC(msg)
                }

                MSG_ACTION_INVITE_CC -> {
                    service.onClientActionInviteCC(msg)
                }

                MSG_ACTION_START_RECORDING -> {
//                    Log.e(TAG, "startRecording: step 2")
                    service.onClientActionStartRecording(msg)
                }

                MSG_ACTION_STOP_RECORDING -> {
//                    Log.e(TAG, "stopRecording: step 2")
                    service.onClientActionStopRecording(msg)
                }

                else -> {
                    super.handleMessage(msg)
                }
            }
        }
    }


}
