package com.chi.webcalls.comms



import android.content.Context
import android.content.Intent
import android.os.Handler
import android.os.Looper
import android.util.Log
import androidx.localbroadcastmanager.content.LocalBroadcastManager

import com.chi.webcalls.R
import com.chi.webcalls.serialize
import com.chi.webcalls.services.CallsService
import com.chi.webcalls.ui.CallActivity


class CommsManager(private val mService: CallsService, siteCode: String, token: String): WSMessageListener
{
    internal val mConnection = CommsConnection(mService, siteCode, token)

    companion object
    {
        private const val TAG = "CommsManager"
    }

    init {
        mConnection.setOnMessageListener(this)
    }

    fun connect()
    {
        Log.e(TAG, "Connect Called")
        mConnection.connect()
    }

    fun shutdown()
    {
        Log.e(TAG, "WS Disconnect Called")
        mConnection.disconnect()
    }

    fun setActiveCall(call: CommsCall?)
    {
        if(call == null)
        {
            mConnection.mCurrentCallId = null
            mConnection.mCurrentCallState = null
        }
        else
        {
            mConnection.mCurrentCallId = call.RoomId
            mConnection.mCurrentCallState = call.State.toString()
        }
    }

    fun onCallUser(call: CommsCall, callback: CommsRequestCallback)
    {
        val params = mapOf(
            "room_id" to call.RoomId,
            "call_type" to call.CallType,
            "recipients" to call.recipients
        )

        mConnection.sendRpcRequest(CommsActions.ACTION_CALL_USER, params, 10000, callback)
    }

    fun onCallCc(call: CommsCall, callback: CommsRequestCallback)
    {
        val params = mapOf(
            "room_id" to call.RoomId,
            "call_type" to call.CallType
        )

        mConnection.sendRpcRequest(CommsActions.ACTION_CALL_CC, params, 10000, callback)
    }

    fun onEndCall(call: CommsCall, callback: CommsRequestCallback)
    {
        val params = mapOf(
            "room_id" to call.RoomId,
            "invite_token" to call.InviteToken
        )

        mConnection.sendRpcRequest(CommsActions.ACTION_END_CALL, params, 3000, callback)
    }

    fun onRejectCall(call: CommsCall, callback: CommsRequestCallback)
    {
        val params = mapOf(
            "room_id" to call.RoomId,
            "invite_token" to call.InviteToken
        )

        mConnection.sendRpcRequest(CommsActions.ACTION_REJECT_CALL, params, 3000, callback)
    }

    fun onAcceptCall(call: CommsCall, callback: CommsRequestCallback)
    {
        val params = mapOf(
            "room_id" to call.RoomId,
            "invite_token" to call.InviteToken
        )

        mConnection.sendRpcRequest(CommsActions.ACTION_ACCEPT_CALL, params, 3000, callback)
    }

    fun onInviteUser(user: CommsUser, call: CommsCall, callback: CommsRequestCallback)
    {
        val params = mapOf(
            "room_id" to call.RoomId,
            "call_type" to call.CallType,
            "recipients" to listOf(user)
        )

        mConnection.sendRpcRequest(CommsActions.ACTION_INVITE_USER, params, 3000, callback)
    }

    fun onInviteCC(call: CommsCall, inviteToken: String, callback: CommsRequestCallback)
    {
        val params = mapOf(
            "room_id" to call.RoomId,
            "call_type" to call.CallType,
            "invite_token" to inviteToken
        )

        mConnection.sendRpcRequest(CommsActions.ACTION_INVITE_CC, params, 3000, callback)
    }

    fun onStartRecording(call: CommsCall, callback: CommsRequestCallback)
    {
        val params = mapOf(
            "room_id" to call.RoomId
        )

        mConnection.sendRpcRequest(CommsActions.ACTION_START_RECORDING, params, 3000, callback)
    }

    fun onStopRecording(call: CommsCall, callback: CommsRequestCallback)
    {
        val params = mapOf(
            "room_id" to call.RoomId
        )

        mConnection.sendRpcRequest(CommsActions.ACTION_STOP_RECORDING, params, 3000, callback)
    }

    fun sendReplyToServer(event: CommsEvent)
    {
        val params = event.params!!
        val result: HashMap<String, Any?> = hashMapOf(
            "room_id" to params["room_id"] as String,
            "invite_token" to params["invite_token"] as String?
        )

        when(event.method)
        {
            CommsEvents.EVENT_INCOMING_CALL,
            CommsEvents.EVENT_RECIPIENT_RINGING -> {
                result["call_state"] = "Ringing"
            }

            CommsEvents.EVENT_CALL_ACCEPTED -> {
                result["call_state"] = "Connecting"
            }

            CommsEvents.EVENT_CALL_ENDED,
            CommsEvents.EVENT_CALL_EXPIRED,
            CommsEvents.EVENT_CALL_REJECTED -> {
                result["call_state"] = "Ended"
            }
        }

        mConnection.sendRpcResponse(event.id!!, result, null)
    }

    override fun onServerEvent(event: CommsEvent)
    {
        val method = event.method ?: return
        val params = event.params ?: return

        if (method == CommsEvents.EVENT_INCOMING_CALL)
        {
            mService.onIncomingCall(event)
            return
        }

        val ev = CommsIpcAction(
            eventId = event.id,
            method = method,
            params = params
        )

        mService.onServerEvent(ev)
        sendReplyToServer(event)
    }

    override fun onConnectionStateChanged(connected: Boolean)
    {
        mService.onConnectionStateChanged(connected)
    }
}
