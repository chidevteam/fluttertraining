package com.chi.webcalls.ui

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.chi.webcalls.R
import com.chi.webcalls.comms.CommsCall

class CallQueueAdapter(private var items: List<CommsCall>, private val context: Context) : RecyclerView.Adapter<CallQueueAdapter.ViewHolder>()
{

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder
    {
        val inflater = LayoutInflater.from(parent.context)
        val view: View = inflater.inflate(R.layout.comms_calls_queue_cell, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int)
    {
        holder.lblName.text = items[position].getRecipientName()
    }

    override fun getItemCount() = items.size

//    class ViewHolder(val view: View, val context: Context) : RecyclerView.ViewHolder(view.rootView)
//    {
//
//        val lblName : TextView = view.findViewById(R.id.lblName)
//        val btnAccept : ImageButton = view.findViewById(R.id.btnAcceptCall)
//        val btnReject : ImageButton = view.findViewById(R.id.btnRejectCall)
//    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view)
    {
        val TAG = "CallQueueAdapter"
        val lblName: TextView = view.findViewById(R.id.lblName)
        val btnAccept: ImageButton = view.findViewById(R.id.btnAcceptCall)
        val btnReject: ImageButton = view.findViewById(R.id.btnRejectCall)

        init
        {
            btnAccept.setOnClickListener {
                Log.e(TAG, "onBindViewHolder: onBtnAccept Clicked")
                (view.context as CallActivity).acceptQueueCall(items[adapterPosition])
            }

            btnReject.setOnClickListener {
                Log.e(TAG, "onBindViewHolder: btnReject Clicked")
                (view.context as CallActivity).rejectCall(items[adapterPosition])
            }
        }
    }
}