package com.chi.webcalls.network

import android.content.Context
import com.codegini.lib.network.CgThrowable
import com.codegini.lib.network.GenericResponse
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException

abstract class ApiCallback<T : GenericResponse>(val context: Context) : Callback<T>
{
    override fun onResponse(call: Call<T>, response: Response<T>)
    {
        if (response.code() != 200)
        {
            var message = "Unknown Error"
            try
            {
                val resp = JSONObject(response.errorBody()!!.string())

                message = resp.getString("ErrorMessage")
            }
            catch (e: IOException)
            {
                e.printStackTrace()
            }
            catch (e: JSONException)
            {
                e.printStackTrace()
            }

            val t = CgThrowable(message, response.code())
            onFailure(call, t)

            return
        }

        val resp = response.body()

        if (resp == null) {
            val t = CgThrowable("Unknown error, no body?", -1)

            onFailure(call, t)
            return
        }

        if (!resp.isSuccess)
        {
            val t = CgThrowable(resp.ErrorMessage, resp.ErrorCode)
            onFailure(call,t)
            return
        }

        val runnable = Runnable{ onSuccess(resp) }
        runnable.run()
    }

    override fun onFailure(call: Call<T>, t: Throwable)
    {
        val runnable = Runnable{
            val t2 = CgThrowable(t.message, -1)
            onError(t2)
        }
        runnable.run()
    }

    fun onFailure(call: Call<T>, t: CgThrowable)
    {
        val runnable = Runnable{ onError(t) }

        runnable.run()
    }

    abstract fun onSuccess(resp: T)
    abstract fun onError(t: CgThrowable)

}