package com.chi.webcalls.ui

import android.content.Context
import android.media.MediaPlayer
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.LinearLayout
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.chi.webcalls.R
import com.chi.webcalls.comms.CommsCall
import com.chi.webcalls.comms.CommsCallState


class CallUiFragment: Fragment()
{
    private lateinit var lblRecipientName: TextView
    private lateinit var lblCallTitle: TextView
    private lateinit var lblErrorMessage: TextView
    private lateinit var lblBtnDecline: TextView

    private lateinit var lolBtnReject: LinearLayout
    private lateinit var lolBtnAccept: LinearLayout

    private var mContext: Context? = null

    var mActiveCall: CommsCall? = null

    private var mPlayer: MediaPlayer? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
    {
        val view = inflater.inflate(R.layout.comms_call_ui_fragment, container, false)

        lblCallTitle = view.findViewById(R.id.lblCallTitle)
        lblBtnDecline = view.findViewById(R.id.lblBtnDecline)
        lblErrorMessage = view.findViewById(R.id.lblErrorMessage)
        lblRecipientName = view.findViewById(R.id.lblRecipientName)

        lolBtnReject = view.findViewById(R.id.lolBtnReject)
        lolBtnAccept = view.findViewById(R.id.lolBtnAccept)

        val btnEndCall: ImageButton = view.findViewById(R.id.btnEndCall)
        val btnAcceptCall: ImageButton = view.findViewById(R.id.btnAcceptCall)

        btnEndCall.setOnClickListener {
            val call = mActiveCall!!

            if(call.State == CommsCallState.Incoming)
            {
                (mContext as CallActivity).rejectCall(call)
            }
            else
            {
                (mContext as CallActivity).endCall(call.RoomId)
            }
        }

        btnAcceptCall.setOnClickListener {
            (mContext as CallActivity).acceptCall(mActiveCall!!)
        }

        updateUI()

        return view
    }

    override fun onAttach(context: Context)
    {
        super.onAttach(context)
        mContext = context
    }

    override fun onDetach() {
        super.onDetach()
        stopRinger()
    }

    override fun onDestroy() {
        super.onDestroy()
        stopRinger()
    }

    fun startRinger(id: Int)
    {
        stopRinger()

        mPlayer = MediaPlayer.create(mContext,id)
        mPlayer!!.isLooping = true
        mPlayer!!.start()
        Log.e("CallUiFragment", "+++++++++++++++++++++++ startRinger: $id +++++++++++++++++++++++++")
    }

    fun stopRinger()
    {
        if (mPlayer == null) return

        if (mPlayer!!.isPlaying)
            mPlayer!!.stop()

        mPlayer = null
    }

    fun isRingerPlaying() : Boolean
    {
        if (mPlayer == null) return false

        return mPlayer!!.isPlaying
    }

    fun updateUI()
    {
        val call = mActiveCall ?: return

        when(call.State)
        {
            CommsCallState.Outgoing -> {
                updateOutCallState(call)
            }

            CommsCallState.Incoming -> {
                updateInCallState(call)
            }

            CommsCallState.Error -> {
                updateErrorCallState(call)
            }
        }
    }

    private fun updateOutCallState(call: CommsCall)
    {
        if (call.IsToCC)
        {
            lblRecipientName.text = "Command Center"
//            lblCallTitle.text = "Calling Command Center"
            val str = if (call.ServiceConnected) {
                "Calling Command Center"
            } else {
                "Connecting Command Center"
            }
            lblCallTitle.text = str
        }
        else
        {
            val recipient = call.recipients[0]

            var str = "${call.CallType} Calling ${recipient.name}"
            lblRecipientName.text = str

            str = if (call.ServiceConnected) {
                "${recipient.call_state} ${recipient.name}"
            } else {
                "Connecting ${recipient.name}"
            }
            lblCallTitle.text = str
        }

        lblErrorMessage.visibility = View.GONE
        lolBtnAccept.visibility = View.GONE
        lblBtnDecline.text = "End"
    }

    private fun updateInCallState(call: CommsCall)
    {
        var str = "${call.getRecipientName()} Calling"
        lblRecipientName.text = str

        str = "Incoming ${call.CallType} Call"
        lblCallTitle.text = str

        lblErrorMessage.visibility = View.GONE
        lolBtnAccept.visibility = View.VISIBLE
        lolBtnReject.visibility = View.VISIBLE
        lblBtnDecline.text = "Decline"

        startRinger(R.raw.phone_ring)
    }

    private fun updateErrorCallState(call: CommsCall)
    {
        lolBtnAccept.visibility = View.GONE
        lolBtnReject.visibility = View.GONE

        lblErrorMessage.visibility = View.VISIBLE
        lblErrorMessage.text = call.ErrorMessage
    }
}