package com.chi.webcalls.comms

import android.annotation.SuppressLint
import android.app.AlarmManager
import android.app.PendingIntent
import android.app.Service
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.PowerManager
import android.util.Log
import com.chi.webcalls.deserialize
import com.chi.webcalls.serialize
import okhttp3.*
import java.util.*
import kotlin.collections.HashMap



internal class CommsConnection(private val mContext: Context, siteCode: String, token: String):
    WebSocketListener()
{
    var mCurrentCallId: String? = null
    var mCurrentCallState: String? = null

    private lateinit var mSocketUrl: String
    private val mSiteCode = siteCode
    private val mToken = token

    private val mOkHttpClient = OkHttpClient()
    private var mWebSocket: WebSocket? = null

    private var mConnecting: Boolean = false
    private var mConnected: Boolean = false
    private var mStayConnected = false
    private var mHasPingStarted = false
    private var mHasConnectStarted = false
    private var mHasConnectScheduled = false

    private var mMessageListener: WSMessageListener? = null

    private var mPingPendingIntent: PendingIntent? = null
    private var mReconnectPendingIntent: PendingIntent? = null

    private val mCommsPingAlarmReceiver = CommsPingAlarmReceiver()
    private val mCommsReconnectAlarmReceiver = CommsReconnectAlarmReceiver()

    private var mWakeLock: PowerManager.WakeLock? = null

    private var mCommandId: Int = 1
    private val mRequests: HashMap<Int, ClientRequestItem> = HashMap()
    private val mPendingMessages: MutableList<String> = arrayListOf()


    companion object
    {
        const val TAG = "CommsConnection"
        const val ACTION_COMMS_SIGNAL_PING = "ACTION_COMMS_SIGNAL_PING"
        const val ACTION_COMMS_CONNECT = "ACTION_COMMS_CONNECT"

        const val PING_INTERVAL_MS: Long = 5000L
        // const val PING_RESPONSE_WAIT_MS: Long = 5000L
        const val RECONNECT_DELAY = 5000L
    }

    fun setOnMessageListener(listener: WSMessageListener)
    {
        this.mMessageListener = listener
    }

    fun connect()
    {
        if(!mHasConnectStarted)
        {
            mHasConnectStarted = true

            mContext.registerReceiver(mCommsReconnectAlarmReceiver, IntentFilter(
                ACTION_COMMS_CONNECT
            ))
            mReconnectPendingIntent = PendingIntent.getBroadcast(mContext, 0, Intent(
                ACTION_COMMS_CONNECT
            ), PendingIntent.FLAG_UPDATE_CURRENT)
        }

        mStayConnected = true

        acquireWakeLock()
        scheduleConnect()
    }

    fun disconnect()
    {
        mStayConnected = false

        releaseWakeLock()

        val alarmManager: AlarmManager = mContext.getSystemService(Service.ALARM_SERVICE) as AlarmManager

        if(mHasConnectStarted)
        {
            mHasConnectStarted = false

            if(mPingPendingIntent != null)
            {
                alarmManager.cancel(mPingPendingIntent)
                mPingPendingIntent = null
            }

            try
            {
                mContext.unregisterReceiver(mCommsReconnectAlarmReceiver)
            }
            catch (e: IllegalArgumentException)
            {
            }
        }

        if(mReconnectPendingIntent != null)
        {
            alarmManager.cancel(mReconnectPendingIntent)
            mReconnectPendingIntent = null
        }

        mPendingMessages.clear()
        stopPing()

        if(mWebSocket != null)
        {
            mWebSocket!!.close(1000, "Requested Disconnect")
        }
    }

    fun sendRpcResponse(event_id: Int, result: CommsMapAny?, error: CommsMap?)
    {
        val resp = CommsEvent(event_id, result = result, error = error)
        val respJson = resp.serialize()

        if (!mConnected)
        {
            if(!mPendingMessages.contains(respJson))
                mPendingMessages.add(respJson)
        }
        else if (!mWebSocket!!.send(respJson))
        {
            if(!mPendingMessages.contains(respJson))
                mPendingMessages.add(respJson)

            scheduleConnect()
        }
        else
        {
            Log.e(TAG, "sendRpcResponse: $respJson Sent")
        }
    }

    fun sendRpcRequest(method: String, params: CommsMapAny, timeout: Long, callback: CommsRequestCallback)
    {
        val req = CommsEvent(mCommandId++, method, params)

        val item =  ClientRequestItem(req, timeout, callback)
        mRequests[req.id!!] = item

        if (!mConnected)
        {
            if (method != CommsActions.ACTION_PING)
                mPendingMessages.add(item.jsonReq)
        }
        else if (!mWebSocket!!.send(item.jsonReq))
        {
            if (method != CommsActions.ACTION_PING)
                mPendingMessages.add(item.jsonReq)

            scheduleConnect()
        }
        else
        {
            Log.e(TAG, "sendRpcRequest: ${item.jsonReq}")
        }

        item.startTimer() {
            mRequests.remove(it.req.id!!)
            mPendingMessages.remove(it.jsonReq)

            it.callback(null, mapOf(
                "message" to "Request Timed Out"
            ))
        }
    }


    // Ping Alarms
    private fun startPing()
    {
        stopPing()

        if (!mHasPingStarted)
        {
            mHasPingStarted = true

            Log.e(TAG, "Starting Comms Ping Alarms")
            mContext.registerReceiver(mCommsPingAlarmReceiver, IntentFilter(ACTION_COMMS_SIGNAL_PING))
            mPingPendingIntent = PendingIntent.getBroadcast(mContext, 0, Intent(
                ACTION_COMMS_SIGNAL_PING
            ), PendingIntent.FLAG_UPDATE_CURRENT)

            schedulePing()
        }
        else
        {
            Log.e(TAG, "!! Comms Ping Alarms Already Started?")
        }
    }

    private fun stopPing()
    {
        if (mHasPingStarted)
        {
            mHasPingStarted = false

            Log.e(TAG, "Stopping Comms Ping Alarms")

            if(mPingPendingIntent != null)
            {
                val am: AlarmManager = mContext.getSystemService(Service.ALARM_SERVICE) as AlarmManager
                am.cancel(mPingPendingIntent)

                mPingPendingIntent = null
            }

            try
            {
                mContext.unregisterReceiver(mCommsPingAlarmReceiver)
            }
            catch (e: IllegalArgumentException)
            {
            }
        }
        else
        {
            Log.e(TAG, "!! Comms Ping Alarms Already Stopped?")
        }
    }

    private fun schedulePing()
    {
//        Log.e(TAG, "Comms Ping Scheduled")

        val nextAlarmInMilliseconds: Long = System.currentTimeMillis() + PING_INTERVAL_MS
        val am: AlarmManager = mContext.getSystemService(Service.ALARM_SERVICE) as AlarmManager

        am.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, nextAlarmInMilliseconds, mPingPendingIntent)
    }

    private fun sendPing()
    {
        if(!mStayConnected || mConnecting)
            return

        if(!mConnected || mWebSocket == null)
        {
            scheduleConnect()
            return
        }

        // acquireWakeLock()

        val message = mapOf("interval" to "5000")

        sendRpcRequest(CommsActions.ACTION_PING, message, 3000 ) { result: CommsMapAny?, error: CommsMap? ->

//            Log.e(TAG, "onPingResponse: Result: $result ======= Error: $error" )
            if (error != null)
            {
                scheduleConnect()
            }
            else
            {
//                Log.e(TAG, "Ping - Server Replied")

                if(mHasConnectScheduled)
                {
                    cancelConnect()
                }

                if (mHasPingStarted)
                {
                    schedulePing()
                }
            }
        }
    }

    // Connection Related Privates
    private fun connectInner()
    {
        Log.e(TAG, "Will Connect WS")
        if(mConnecting)
        {
            Log.e(TAG, "Already Connecting, skipping now")
            return
        }

        mConnecting = true
        mConnected = false

        if (mHasPingStarted)
        {
            stopPing()
        }

        val roomId = mCurrentCallId ?: "null"
        val callState = mCurrentCallState ?: "null"

        mSocketUrl = String.format(
            Locale.ENGLISH,
            "wss://%s.cognitivehealthintl.com/comms/ws/calls-router/call-signals?token=%s&call=%s&state=%s",
            mSiteCode,
            mToken,
            roomId,
            callState
        )

        val existingWebSocket = mWebSocket

        val request = Request.Builder().url(mSocketUrl).build()

        mWebSocket = mOkHttpClient.newWebSocket(request, this)
        Log.e(TAG, "Created New WS - ${mWebSocket!!.hashCode()}")

        if(existingWebSocket != null)
        {
            Log.e(TAG, "Cancelling Existing WS - ${existingWebSocket.hashCode()}")

            existingWebSocket.cancel()
        }
    }

    private fun scheduleConnect()
    {
        if(!mStayConnected)
            return

        if(mHasConnectScheduled)
        {
            Log.e(TAG, "!! Comms Connect Already Scheduled")
            return
        }

        mHasConnectScheduled = true

        Log.e(TAG, "Comms Connect Scheduled")

        val nextAlarmInMilliseconds: Long = System.currentTimeMillis() + RECONNECT_DELAY
        val am: AlarmManager = mContext.getSystemService(Service.ALARM_SERVICE) as AlarmManager

        am.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, nextAlarmInMilliseconds, mReconnectPendingIntent)
    }

    private fun cancelConnect()
    {
        if(mHasConnectScheduled)
        {
            mHasConnectScheduled = false

            if(mReconnectPendingIntent != null)
            {
                val alarmManager: AlarmManager = mContext.getSystemService(Service.ALARM_SERVICE) as AlarmManager
                alarmManager.cancel(mReconnectPendingIntent)
            }
        }
    }

    // TODO: Review this method, timeout should not be cleared here
    private fun sendPendingMessages()
    {
        while (mPendingMessages.size > 0)
        {
            if (!mConnected) break

            val jsonReq = mPendingMessages[0]

            if (!mWebSocket!!.send(jsonReq)) break
            else {
                this.mPendingMessages.remove(jsonReq)
            }
        }
    }

    // WebSocketListener Implementation
    override fun onOpen(webSocket: WebSocket, response: Response?)
    {
        Log.e(TAG, "WS Opened - ${webSocket.hashCode()}")
        Log.e(TAG, "WS Opened response - ${response}")

        mConnected = true
        mConnecting = false

        if(mWebSocket == null)
        {
            mWebSocket = webSocket
        }

        // stopping previous ping functionality
        if(mStayConnected) startPing()

        sendPendingMessages();

        mMessageListener?.onConnectionStateChanged(true)
        // releaseWakeLock()
    }

    override fun onClosed(webSocket: WebSocket?, code: Int, reason: String?)
    {
        Log.e(TAG, "WS Closed - ${webSocket.hashCode()}")
        mConnected = false
        mConnecting = false

        if(mStayConnected && (mWebSocket == null || mWebSocket == webSocket))
            scheduleConnect()

        mMessageListener?.onConnectionStateChanged(false)
    }

    override fun onFailure(webSocket: WebSocket?, t: Throwable, response: Response?)
    {
        Log.e(TAG, "WS Failure - ${webSocket.hashCode()}")
        mConnected = false
        mConnecting = false

        if(mStayConnected && (mWebSocket == null || mWebSocket == webSocket))
            scheduleConnect()

        mMessageListener?.onConnectionStateChanged(false)
    }

    override fun onMessage(webSocket: WebSocket, text: String)
    {
        Log.e(TAG, "WS Message: $text")

        val message: CommsEvent = text.deserialize(CommsEvent::class.java)

        if (message.method != null)
        {
            mMessageListener?.onServerEvent(message)
            return
        }
        else if (message.id != null)
        {
            val requestItem = mRequests[message.id] ?: return

            requestItem.cancelTimer()
            mRequests.remove(message.id)
            mPendingMessages.remove(requestItem.jsonReq)

            requestItem.callback(message.result, message.error)
        }
    }


    @SuppressLint("WakelockTimeout")
    private fun acquireWakeLock()
    {
        if (mWakeLock == null)
        {
            val pm = mContext.getSystemService(Service.POWER_SERVICE) as PowerManager
            mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "Comms:ConnectionWakeLoc")
        }

        mWakeLock?.acquire()
    }

    private fun releaseWakeLock()
    {
        if (mWakeLock != null && mWakeLock!!.isHeld)
        {
            mWakeLock!!.release()
        }
    }


    inner class CommsPingAlarmReceiver: BroadcastReceiver()
    {
        @SuppressLint("WakelockTimeout")
        override fun onReceive(context: Context, Intent: Intent?)
        {
            if (mWakeLock != null && !mWakeLock!!.isHeld)
                mWakeLock!!.acquire()

//            Log.e(TAG, "CommsPingAlarm Received")
            sendPing()
        }
    }

    inner class CommsReconnectAlarmReceiver: BroadcastReceiver()
    {
        @SuppressLint("WakelockTimeout")
        override fun onReceive(context: Context, Intent: Intent?)
        {
            if (mWakeLock != null && !mWakeLock!!.isHeld)
                mWakeLock!!.acquire()

            Log.e(TAG, "CommsReconnectAlarm Received")

            if(mHasConnectScheduled)
            {
                mHasConnectScheduled = false
                connectInner()
            }
        }
    }


}