package com.chi.webcalls.comms

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothHeadset
import android.bluetooth.BluetoothProfile
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.media.AudioDeviceInfo
import android.media.AudioManager
import android.os.Handler
import android.text.TextUtils
import android.util.Log
import android.widget.Toast

interface AudioDeviceChangeListener
{
    fun onDeviceChange(device: AudioDevice, isConnected: Boolean)
}

class AudioInOutChangeReceiver(context: Context?, listener: AudioDeviceChangeListener) : BroadcastReceiver()
{
    var TAG = "AudioInOutChangeReceiver"

    private var mInstance: AudioInOutChangeReceiver? = null

//	private AudioHeadSetController mAudioController;

    //	private AudioHeadSetController mAudioController;
    private val ACTION_HEADSET_PLUG_STATE = "android.intent.action.HEADSET_PLUG"

    private var mAudioManager: AudioManager? = null

    private var mBTHeadSetManager: BluetoothHeadsetManager? = null

    private var mContext: Context? = context

    private var mBluetoothHeadset: BluetoothHeadset? = null

    private var mAudioDeviceListener: AudioDeviceChangeListener? = null

    var mSpeakerState: Boolean = false

    var mHeadphoneDevice: AudioDevice = AudioDevice("Headphone", false)
    var mBluetoothDevice: AudioDevice = AudioDevice("Bluetooth", false)

    var mHeadsetCallback: BluetoothHeadSetListener = object : BluetoothHeadSetListener
    {
        override fun disconnectedToProxy()
        {
            Log.e(TAG, "disconnectedToProxy: ")
        }

        override fun connectedToProxy(aBluetoothHeadset: BluetoothHeadset?)
        {
            Log.e(TAG, "connectedToProxy: ")
            mBluetoothHeadset = aBluetoothHeadset
            Log.e(TAG, "connectedToProxy: Connected devices " + mBluetoothHeadset!!.connectedDevices.size)
            if (mBluetoothHeadset!!.connectedDevices.size > 0)
            {
                mBluetoothDevice.is_active = true
                mAudioDeviceListener!!.onDeviceChange(mBluetoothDevice, true)
                Log.e(TAG, "connectedToProxy: Bluetooth State Already Connected" )
            }
        }
    }

    init
    {

        Log.e(TAG, "AudioInOutChangeReceiver()")

        if (mInstance == null)
        {
            mInstance = this
        }

        mAudioManager = mContext!!.getSystemService(Context.AUDIO_SERVICE) as AudioManager
        mAudioDeviceListener = listener

        initiBTHeadSetConfiguration()
    }

    fun getInstance(aContext: Context?, alistener: AudioDeviceChangeListener): AudioInOutChangeReceiver?
    {
        if (mInstance == null)
        {
            mInstance = AudioInOutChangeReceiver(aContext, alistener)
        }
        return mInstance
    }

    fun voidInstance()
    {
        mInstance = null
    }

    fun getActiveContext(): Context?
    {
        return mContext
    }

    fun removeAudioController(): AudioInOutChangeReceiver?
    {
        Log.e(TAG, "removeAudioController()")

        // reset BT Head set Configuration
        resetBTHeadSetConfiguration()
        return mInstance
    }

    private fun initiBTHeadSetConfiguration()
    {
        if (mBTHeadSetManager == null) mBTHeadSetManager =
            BluetoothHeadsetManager(mContext)
        mBTHeadSetManager!!.addListener(mHeadsetCallback)

        // if BT is turn on then we move next step
        if (mBTHeadSetManager!!.hasEnableBluetooth())
        {
            mBTHeadSetManager!!.connectionToProxy()
        }
    }

    private fun resetBTHeadSetConfiguration()
    {
        mBTHeadSetManager!!.disconnectProxy()
    }

    override fun onReceive(context: Context, intent: Intent)
    {
//		if(BirdersApplication.getInstance().getDebug_mode())
//			StorageManager.dumpIntent(getTAG(),intent);
        val action = intent.action
        // check action should not empty
        if (TextUtils.isEmpty(action)) return

        //Broadcast Action: Wired Headset plugged in or unplugged.
        if (action == ACTION_HEADSET_PLUG_STATE)
        {
            updateHeadSetDeviceInfo(context, intent)
        }
        else if (action == BluetoothDevice.ACTION_ACL_CONNECTED  || action == BluetoothDevice.ACTION_ACL_DISCONNECTED || action == BluetoothAdapter.ACTION_STATE_CHANGED)
        {
            updateBTHeadSetDeviceInfo(context, intent)
        }
    }

    fun updateHeadSetDeviceInfo(context: Context, intent: Intent)
    {
        //0 for unplugged, 1 for plugged.
        val state = intent.getIntExtra("state", -1)
        var stateName: StringBuilder? = null

        //Headset type, human readable string
        val name = intent.getStringExtra("name")

        // - 1 if headset has a microphone, 0 otherwise, 1 mean h2w
        val microPhone = intent.getIntExtra("microphone", -1)
        when (state)
        {
            0 ->
            {
                stateName = StringBuilder("Headset is unplugged ==>  $name")
                Log.e(TAG, stateName.toString())
//                checkHeadSetConnectivity()
                mHeadphoneDevice.is_active = false
                mAudioDeviceListener!!.onDeviceChange(mHeadphoneDevice, false)
            }
            1 ->
            {
                stateName = StringBuilder("Headset is plugged ==>  $name")
                Log.e(TAG, stateName.toString())
                mHeadphoneDevice.is_active = true
                mAudioDeviceListener!!.onDeviceChange(mHeadphoneDevice, true)
            }
            else ->
            {
                Log.e(TAG, "I have no idea what the headset state is")
            }
        }
    }

    fun updateBTHeadSetDeviceInfo(context: Context, intent: Intent?)
    {
        Log.e(TAG, "updateBTHeadSetDeviceInfo: 1")
        if (intent != null)
        {
            val device = intent.getParcelableExtra<BluetoothDevice>(BluetoothDevice.EXTRA_DEVICE)
            if (device != null)
            {
                Handler().postDelayed({
                    Log.e(TAG, "updateBTHeadSetDeviceInfo: 2")
                    verifyHeadSetState(device)
                }, 1500)
            }
        }
    }

    private fun verifyHeadSetState(device: BluetoothDevice)
    {
//		//TODO Wait for other wise result getting wrong
//		try {
//			Thread.sleep(500);
//		} catch (InterruptedException e) {
//			e.printStackTrace();
//		}
        var state = 0

        // STATE_DISCONNECTED	0
        // STATE_CONNECTED 		2

        // STATE_CONNECTING		1
        // STATE_DISCONNECTING	3
        state = mBluetoothHeadset!!.getConnectionState(device)
        if (state == BluetoothProfile.STATE_CONNECTED || state == BluetoothProfile.STATE_CONNECTING)
        {
            mBluetoothDevice.is_active = true
            mAudioDeviceListener!!.onDeviceChange(mBluetoothDevice, true)

        }
        else if (state == BluetoothProfile.STATE_DISCONNECTED)
        {
//            checkHeadSetConnectivity()
            mBluetoothDevice.is_active = false
            mAudioDeviceListener!!.onDeviceChange(mBluetoothDevice, false)
        }

        Log.e(TAG, "Bluetooth State :$state    Name: ${device.name}    UUID  ${device.uuids}   Address ${device.address}   Type  ${device.type}")
    }

    fun checkHeadSetConnectivity()
    {
        if (mAudioManager == null && mBluetoothHeadset == null)
            return

        if (!getBluetoothStatus() && !getHeadPhoneStatus())
        {
            mAudioManager!!.mode = AudioManager.MODE_IN_COMMUNICATION or AudioManager.MODE_IN_CALL or AudioManager.MODE_RINGTONE
            mAudioManager!!.stopBluetoothSco()
            mAudioManager!!.isBluetoothScoOn = false
            mAudioManager!!.isSpeakerphoneOn = mSpeakerState
        }
    }

    fun getHeadPhoneStatus(): Boolean
    {
        var isHeadPhone = false
        for (device in mAudioManager!!.getDevices(AudioManager.GET_DEVICES_OUTPUTS))
        {
            if (device.type == AudioDeviceInfo.TYPE_WIRED_HEADPHONES || device.type == AudioDeviceInfo.TYPE_WIRED_HEADSET)
            {
                isHeadPhone = true
            }
        }
        return isHeadPhone
    }


    fun getBluetoothStatus(): Boolean
    {
        return if (mBluetoothHeadset == null) false
        else mBluetoothHeadset!!.connectedDevices.size > 0
    }

    fun playBluetooth()
    {
        if (getBluetoothStatus())
        {
            Log.e(TAG, "playBluetooth: ")
            // 1. case - bluetooth device
            mAudioManager!!.mode = AudioManager.MODE_IN_COMMUNICATION or AudioManager.MODE_IN_CALL or AudioManager.MODE_RINGTONE
            mAudioManager!!.startBluetoothSco()
            mAudioManager!!.isBluetoothScoOn = true
        }
    }

    fun playHeadPhone()
    {
        if (getHeadPhoneStatus())
        {
            Log.e(TAG, "playHeadPhone: ")
            // 2. case - wired device
            mAudioManager!!.mode = AudioManager.MODE_IN_COMMUNICATION or AudioManager.MODE_IN_CALL or AudioManager.MODE_RINGTONE
            mAudioManager!!.stopBluetoothSco()
            mAudioManager!!.isBluetoothScoOn = false
            mAudioManager!!.isSpeakerphoneOn = false
        }
    }

    fun playSpeaker()
    {
        Log.e(TAG, "playSpeaker: ")
        // 3. case - phone speaker
        mAudioManager!!.mode = AudioManager.MODE_IN_COMMUNICATION or AudioManager.MODE_IN_CALL or AudioManager.MODE_RINGTONE
        mAudioManager!!.stopBluetoothSco()
        mAudioManager!!.isBluetoothScoOn = false
        mAudioManager!!.isSpeakerphoneOn = true
    }

    fun setDefaultCallSetting(isSpeakerOn: Boolean)
    {
        mAudioManager!!.mode = AudioManager.MODE_IN_COMMUNICATION or AudioManager.MODE_IN_CALL or AudioManager.MODE_RINGTONE
        mAudioManager!!.stopBluetoothSco()
        mAudioManager!!.isBluetoothScoOn = false
        mAudioManager!!.isSpeakerphoneOn = isSpeakerOn
    }

    fun setDefaultSetting()
    {
        mAudioManager!!.mode = AudioManager.MODE_NORMAL
        mAudioManager!!.isSpeakerphoneOn = false
    }

    fun setSpeakerState(isSpeakerOn: Boolean)
    {
       mSpeakerState = isSpeakerOn
    }

    fun getSpeakerState() : Boolean
    {
        return mSpeakerState
    }

    fun shutdown()
    {
        setDefaultSetting()
        mAudioManager = null
        mBTHeadSetManager = null
    }
}