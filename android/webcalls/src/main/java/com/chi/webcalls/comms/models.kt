package com.chi.webcalls.comms


import android.os.Handler
import com.chi.webcalls.serialize
import com.chi.webcalls.toInteger
import java.util.*
import kotlin.collections.ArrayList
import kotlin.math.roundToInt

typealias CommsMapAny = Map<String, Any?>
typealias CommsMap = Map<String, String>

typealias CommsRequestCallback = (result: CommsMapAny?, error: CommsMap?) -> Unit
typealias CommsRequestTimedOut = (item: ClientRequestItem) -> Unit

interface WSMessageListener
{
    fun onServerEvent(event: CommsEvent)
    fun onConnectionStateChanged(connected: Boolean)
}


object CommsConstants
{
    const val ActionCallStatus = "ActionCallStatus"
    const val ActionEndCall = "ActionEndCall"
    const val ExtraCallInfo = "ExtraCallInfo"
    const val ExtraIsOutgoingCall = "ExtraIsOutgoingCall"

    const val K_MESSAGE_CALLS = 8033
}

object CommsActions
{
    const val ACTION_PING = "ping"

    const val ACTION_CALL_USER = "CallUser"
    const val ACTION_CALL_CC = "CallCC"

    const val ACTION_INVITE_USER = "InviteUser"
    const val ACTION_INVITE_CC = "InviteCC"

    const val ACTION_ACCEPT_CALL = "AcceptCall"
    const val ACTION_ACCEPT_CC_CALL = "AcceptCCCall"

    const val ACTION_REJECT_CALL = "RejectCall"
    const val ACTION_REJECT_CC_CALL = "RejectCCCall"

    const val ACTION_END_CALL = "EndCall"
    const val ACTION_END_CC_CALL = "EndCCCall"

    const val ACTION_START_RECORDING = "StartRecording"
    const val ACTION_STOP_RECORDING = "StopRecording"

}

object CommsEvents
{
    const val EVENT_CALL_ACK = "localEventCallAcknowledged"

    const val EVENT_INCOMING_CALL = "eventIncomingCall"
    const val EVENT_RECIPIENT_RINGING = "eventRecipientRinging"
    const val EVENT_CALL_REJECTED = "eventCallRejected"
    const val EVENT_CALL_ALL_CC_BUSY = "eventAllCCUsersBusy"
    const val EVENT_CALL_ENDED = "eventCallEnded"
    const val EVENT_PARTICIPANT_CALL_ENDED = "eventParticipantCallEnded"
    const val EVENT_CALL_ACCEPTED = "eventCallAccepted"
    const val EVENT_CALL_EXPIRED = "eventCallExpired"

    const val EVENT_CALL_FAILED = "eventCallFailed"
    const val EVENT_CALL_NO_RESPONSE = "eventNoResponse"
    const val EVENT_SESSION_EXPIRED = "eventSessionExpired"

    const val EVENT_USER_INVITED = "eventUserInvited"
    const val EVENT_INVITE_REJECTED = "eventInviteRejected"
    const val EVENT_INVITE_ACCEPTED = "eventInviteAccepted"
    const val EVENT_INVITE_NO_RESPONSE = "eventInviteNoResponse"

    const val EVENT_RECORDING_STARTED = "eventRecordingStarted"
    const val EVENT_RECORDING_STOPPED = "eventRecordingStopped"
}


enum class CommsCallState
{
    Outgoing,
    Incoming,
    Connected,
    Ringing,
    Error,
    NextAccepted
}

data class CommsUser(
    val user_id: Int,
    val name: String,
    val image: String? = null,
    val invite_token: String? = null
)
{
    companion object
    {
        fun fromMap(user: CommsMapAny): CommsUser
        {
            val userId = user["user_id"]!!.toInteger()
            val name = user["name"] as String
            val image = user["image"] as String?
            val inviteToken = user["invite_token"] as String?

            return CommsUser(userId, name, image, inviteToken)
        }
    }
}

data class CallRecipient(
    var call_state: CommsCallState,
    val user_id: Int,
    val name: String,
    val image: String? = null,
    val invite_token: String? = null
)
{
    companion object
    {
        fun fromCommsUser(user: CommsUser, state: CommsCallState): CallRecipient
        {
            return CallRecipient(
                call_state = state,
                user_id = user.user_id,
                name = user.name,
                image = user.image
            )
        }
    }
}


class CommsCall(
    val RoomId: String,
    val CallType: String,

    val IsOutGoing: Boolean,
    val IsToCC: Boolean,

    var State: CommsCallState,
    val caller: CallRecipient,

    val recipients: MutableList<CallRecipient> = ArrayList(),
    val InviteToken: String? = null,

    var ServiceConnected: Boolean = false,
    var ErrorMessage: String = ""
)
{
    companion object
    {
        @Suppress("UNCHECKED_CAST")
        fun makeIncomingCall(params: CommsMapAny): CommsCall
        {
            val user = CommsUser.fromMap(params["recipient"] as CommsMapAny)

            val call = CommsCall(
                RoomId = params["room_id"] as String,
                CallType = params["call_type"] as String,
                IsOutGoing = false,
                IsToCC = false,
                State = CommsCallState.Incoming,
                caller = CallRecipient.fromCommsUser(user, CommsCallState.Incoming),
                InviteToken = params["invite_token"] as String?
            )

            val caller = params["caller"] as CommsMapAny
            call.addRecipient(
                user_id = caller["user_id"]!!.toInteger(),
                name = caller["name"] as String,
                image = caller["image"] as String?,
                state = CommsCallState.Incoming
            )

            return call
        }

        fun makeOutGoingCall(callType: String, caller: CommsUser, toCC: Boolean = false): CommsCall
        {
            val call = CommsCall(
                RoomId = UUID.randomUUID().toString().replace("-", ""),
                CallType = callType,
                IsOutGoing = true,
                IsToCC = toCC,
                State = CommsCallState.Outgoing,
                caller = CallRecipient.fromCommsUser(caller, CommsCallState.Outgoing)
            )

//            call.addRecipient(recipient, CommsCallState.Outgoing)

            return call
        }

        fun makeOutGoingCall_unused(action: CommsIpcAction, user: CommsUser, toCC: Boolean = false): CommsCall
        {
            val params = action.params

            val call = CommsCall(
                RoomId = params["room_id"] as String,
                CallType = params["call_type"] as String,
                IsOutGoing = true,
                IsToCC = toCC,
                State = CommsCallState.Outgoing,
                caller = CallRecipient.fromCommsUser(user, CommsCallState.Outgoing)
            )

            call.addRecipient(
                user_id = (params["user_id"] as String).toInt(),
                name = params["name"] as String,
                image = params["image"] as String?,
                state = CommsCallState.Outgoing
            )

            return call
        }
    }

    fun addRecipient(recipient: CommsUser, state: CommsCallState)
    {
        addRecipient(recipient.user_id, recipient.name, recipient.image, state)
    }

    fun addRecipient(user_id: Int, name: String, image: String?, state: CommsCallState)
    {
        val curr = findRecipient(user_id)

        if (curr == null)
            recipients.add(CallRecipient(state, user_id, name, image))
    }

    fun getRecipientName(): String
    {
        if (recipients.size == 0) return ""

        return recipients[0].name
    }

    fun findRecipient(user_id: Int): CallRecipient?
    {
        return recipients.find { user_id == user_id }
    }

    fun setRecipientState(user_id: Int, state: CommsCallState)
    {
        val recipient = findRecipient(user_id) ?: return

        recipient.call_state = state
    }

    fun addRecipient(callUser: CommsUser) {

    }
}

data class CommsIpcAction(
    val eventId: Int? = null,
    val method: String,
    val params: CommsMapAny
)


class CommsEvent(
    val id: Int? = null,

    var method: String? = null,
    val params: CommsMapAny? = null,

    var result: CommsMapAny? = null,
    var error: CommsMap? = null
)

class ClientRequestItem(
    val req: CommsEvent,
    private val timeout: Long,
    val callback: CommsRequestCallback
)
{
    val jsonReq: String = req.serialize()

    private val timer: Handler? = if (timeout > 0) Handler() else null
    private var timeoutRunnable: Runnable? = null


    fun startTimer(onTimeout: CommsRequestTimedOut)
    {
        if(timer == null)
            return

        timeoutRunnable = Runnable {
            onTimeout(this)
        }

        timer.postDelayed(timeoutRunnable!!, timeout)
    }

    fun cancelTimer()
    {
        if(timer == null)
            return

        if (timeoutRunnable != null)
        {
            timer.removeCallbacks(timeoutRunnable!!)
            timeoutRunnable = null
        }
    }
}

data class CommsJavascriptEvent(
    val type: String? = null,
    val data: String? = null
)

data class CallsAppConfig(
    val MainServer: String,
    val RoomId: String,
    val CallType: String,
    val UserId: Int,
    val UserName: String,
    val UserImage: String?,
    val InviteToken: String? = null,
    val CanInvite: Boolean = true,
    val CanRecord: Boolean = true
)

data class InviteMeta
    (
    val id: String,
    val metadata: MetaDataInvite
)

data class MetaDataInvite
    (
    val user_id: Int,
    val user_name: String,
    val is_recorder: Boolean
)

data class Doctor(
    val employee_id: String? = null,
    val user_id: Int? = null,
    var full_name: String? = null,
    var gender: String? = null,
    var image: String? = null
)

data class DoctorList(
    val data: ArrayList<Doctor>,
    val total_records: Int
)

data class PatientList(
    val data: List<Patient>,
    val total_records: Int
)

data class Patient(
    val id: String,
    val patient_user_id: Int,
    var image: String? = null,
    var name: String? = null
)

data class AudioDevice(
    var device: String,
    var is_active: Boolean
)

data class AppVersionReq(
    val bundle_id: String,
    val app_version: String
)

data class AppVersionResp(
    val bundle_id: String,
    val app_version: String,
    val app_build: String,
    val store_version: String,
    val should_update: Boolean,
    val release_date: Long
)