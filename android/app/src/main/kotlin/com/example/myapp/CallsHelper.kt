package com.example.myapp

import android.content.Context
import android.content.Intent
import android.util.Log

import com.chi.webcalls.comms.CommsUser
import com.chi.webcalls.serialize
import com.chi.webcalls.services.CallsService
import io.flutter.embedding.android.FlutterActivity



object CallsHelper
{
    private const val TAG = "CallsHelper"

    fun callUser(activity: FlutterActivity, callType: String, recipient: CommsUser, caller: CommsUser)
    {
        Log.e(TAG, "Called: callUser")

        val intent = Intent(activity, CallsService::class.java)
//        val intent = Intent(activity, CallActivity::class.java)

        intent.action = CallsService.ACTION_CALL_USER
        intent.putExtra(CallsService.K_CALL_TYPE, callType)
        intent.putExtra(CallsService.K_CALL_CALLER, caller.serialize())
        intent.putExtra(CallsService.K_CALL_RECIPIENT, recipient.serialize())

//        activity.startActivity(intent)

        activity.startService(intent)
    }

    fun callCommandCenter(activity: FlutterActivity, callType: String, caller: CommsUser)
    {
        Log.e(TAG, "Called: callCommandCenter")

        val intent = Intent(activity, CallsService::class.java)

        intent.action = CallsService.ACTION_CALL_CC
        intent.putExtra(CallsService.K_CALL_TYPE, callType)
        intent.putExtra(CallsService.K_CALL_CALLER, caller.serialize())

        activity.startService(intent)
    }

    fun startCallsService(activity: FlutterActivity, siteCode: String, token: String, canInvite: Boolean, canRecord: Boolean)
    {
        val callsServiceIntent = Intent(activity, CallsService::class.java)
        callsServiceIntent.action = CallsService.ACTION_INIT_CALLS

        callsServiceIntent.putExtra(CallsService.K_CALLS_SITE_CODE, siteCode)
        callsServiceIntent.putExtra(CallsService.K_CALLS_TOKEN, token)
        callsServiceIntent.putExtra(CallsService.K_INVITE_ALLOWED, canInvite)
        callsServiceIntent.putExtra(CallsService.K_RECORD_ALLOWED, canRecord)

        activity.startForegroundService(callsServiceIntent)
    }

    fun stopCallsService(context: Context)
    {
        context.stopService(Intent(context, CallsService::class.java))
    }

}