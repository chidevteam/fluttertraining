package com.example.universal

import android.os.Handler
import android.os.Looper
import android.util.Log
import androidx.annotation.NonNull
import androidx.databinding.DataBindingUtil
import com.chi.webcalls.comms.CommsUser
import com.chi.webrtcsdk.core.*
import com.chi.webrtcsdk.databinding.ActiveCallParticipantViewBinding
import com.chi.webrtcsdk.ui.CallUiUser
import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodCall

//import com.chi.webcalls.CallsHelper
// import com.codegini.lib.core.BaseActivity
import com.example.myapp.CallsHelper

class MainActivity: FlutterActivity(), CallUiDelegate {

    private val TAG = "FlutterActivity"

    private lateinit var appConfig: RtcAppConfig
    private var rtcClient: WebRtcClient? = null

    private lateinit var localUser: CallUiUser
    private lateinit var remoteUser: CallUiUser

    private val mHandler: Handler = Handler(Looper.getMainLooper())

    override fun configureFlutterEngine(@NonNull flutterEngine: FlutterEngine)
    {
        super.configureFlutterEngine(flutterEngine)
        MethodChannel(flutterEngine.dartExecutor.binaryMessenger,
            HOST_BASE_CHANNEL).setMethodCallHandler { call, result ->

            when(call.method)
            {
                "startCallsService" -> startCallsService(call, result)
                "stopCallsService" -> stopCallsService(call, result)
                "callUser" -> callUser(call, result)
                "callCommandCenter" -> callCommandCenter(call, result)
                "setupNativeCall" -> setupNativeCall(call, result)

                else -> {
                    result.error("400", "Unknown host-base-channel method!", null)
                }
            }
        }
    }

    private fun startCallsService(call: MethodCall, result: MethodChannel.Result)
    {
        val token = call.argument<String?>("token")
        val siteCode = call.argument<String?>("siteCode")
        if (token == null)
        {
            result.error("401", "Authorisation code is required.", null)
            return
        }
        if (siteCode == null)
        {
            result.error("400", "Site code is required.", null)
            return
        }

        CallsHelper.startCallsService(this, siteCode, token, canInvite = false, canRecord = false)
        result.success(mapOf("status" to "Ok"))

    }

    private fun stopCallsService(call: MethodCall, result: MethodChannel.Result)
    {
        CallsHelper.stopCallsService(this)
        result.success(mapOf("status" to "Ok"))

    }


    private fun callUser(call: MethodCall, result: MethodChannel.Result) {
        val callType = call.argument<String>("callType")
        if (callType == null) {
            result.error("400", "callType is required.", null)
            return
        }
        val d_user_id = call.argument<Int>("d_user_id")
        if (d_user_id == null) {
            result.error("400", "d_user_id is required.", null)
            return
        }
        val d_full_name = call.argument<String>("d_full_name")
        if (d_full_name == null) {
            result.error("400", "d_full_name is required.", null)
            return
        }
        val d_image = call.argument<String>("d_image")
        if (d_image == null) {
            result.error("400", "d_image is required.", null)
            return
        }
        val p_user_id = call.argument<Int>("p_user_id")
        if (p_user_id == null) {
            result.error("400", "p_user_id is required.", null)
            return
        }
        val p_full_name = call.argument<String>("p_full_name")
        if (p_full_name == null) {
            result.error("400", "p_full_name is required.", null)
            return
        }
        val p_image = call.argument<String>("p_image")
        if (p_image == null) {
            result.error("400", "p_image is required.", null)
            return
        }

        CallsHelper.callUser(this, callType,
            recipient = CommsUser(user_id = p_user_id, name = p_full_name, image = p_image),
            caller = CommsUser(d_user_id, d_full_name, d_image)
        )
        result.success(mapOf("status" to "Ok"))
    }

    private fun callCommandCenter(call: MethodCall, result: MethodChannel.Result) {
        val callType = call.argument<String>("callType")
        if (callType == null) {
            result.error("400", "callType is required.", null)
            return
        }
        val d_user_id = call.argument<Int>("d_user_id")
        if (d_user_id == null) {
            result.error("400", "d_user_id is required.", null)
            return
        }
        val d_full_name = call.argument<String>("d_full_name")
        if (d_full_name == null) {
            result.error("400", "d_full_name is required.", null)
            return
        }
        val d_image = call.argument<String>("d_image")
        if (d_image == null) {
            result.error("400", "d_image is required.", null)
            return
        }

        CallsHelper.callCommandCenter(this, callType,
            caller = CommsUser(d_user_id, d_full_name, d_image))
        result.success(mapOf("status" to "Ok"))
    }

    private fun setupNativeCall(call: MethodCall, result: MethodChannel.Result) {

        val server = call.argument<String>("server")
        if (server == null) {
            result.error("400", "server url is required.", null)
            return
        }

        val my_user_id = call.argument<String>("my_user_id")
        if (my_user_id == null) {
            result.error("400", "my_user_id is required.", null)
            return
        }

        val my_name = call.argument<String>("my_name")
        if (my_name == null) {
            result.error("400", "my_name is required.", null)
            return
        }

        val room_id = call.argument<String>("room_id")
        if (room_id == null) {
            result.error("400", "room_id is required.", null)
            return
        }

        startNativeCall(server, my_name, my_user_id, room_id)

        result.success(mapOf("status" to "Ok"))
    }

    private fun startNativeCall(server: String, userName: String, userId: String, roomId: String)
    {
//        val localParticipant: ActiveCallParticipantViewBinding =
//            DataBindingUtil.inflate(layoutInflater, R.layout.active_call_participant_view, binding.participantsView, false)

        val callConfig = CallConfig(
            mainServer = server,
            roomId = roomId,
            callType = "Video",
            auth_token = "",
            my_user_id = userId,
            my_name = userName,
            my_image = null,
            inviteToken = null
        )

        appConfig = RtcAppConfig(callConfig)
        rtcClient = WebRtcClient(appConfig)

        localUser = CallUiUser(
            metadata = appConfig.userMetaData,
            deviceState = ParticipantDeviceState(
                audioActive = true,
                videoActive = appConfig.callType == "Video",
                screenActive = false
            ),
            binding = null,
            isLocal = true
        )

//        localUser.binding.user = localUser

        localUser.initRenderer(rtcClient!!.eglBase, true)

//        binding.localPartView.addView(localParticipant.root)

        rtcClient!!.initialize(this@MainActivity, this)
    }


    override fun getLocalView(): ActiveCallParticipantViewBinding? {
        return localUser.binding
    }

    override fun updateParticipant(msg: CallParticipant) {
        Log.e(TAG, "updateParticipant: $msg")
    }

    override fun participantJoined(msg: CallParticipant) {
        Log.e(TAG, "participantJoined: $msg")

        mHandler.post {

//            val userBinding: ActiveCallParticipantViewBinding =
//                DataBindingUtil.inflate(layoutInflater, R.layout.active_call_participant_view, binding.participantsView, false)

            remoteUser = CallUiUser(
                metadata = msg.metadata,
                deviceState = msg.deviceState!!,
                binding = null
            )

//            userBinding.user = remoteUser

//            binding.participantsView.addView(userBinding.root)

            remoteUser.initRenderer(rtcClient!!.eglBase, false)

//        remoteUsers[msg.id] = user

//           val renderer = remoteUser.binding.surfaceView
//           renderer.visibility = if(remoteUser.deviceState.videoActive) View.VISIBLE else View.GONE
        }
    }

    override fun participantLeft(id: String) {
        Log.e(TAG, "participantLeft: $id")
    }

    override fun addVideoTrack(connectionId: String, track: org.webrtc.VideoTrack) {
        Log.e(TAG, "addVideoTrack: $connectionId")

        //TODO uncomment this for video call
//        mHandler.post {
//            val renderer = remoteUser.binding.surfaceView
//
//            Log.e(TAG, "🔔 Added Video Track to Renderer")
//            track.addSink(renderer)
////            renderer.visibility = if(remoteUser.deviceState.videoActive) View.VISIBLE else View.GONE
//
//            localUser.binding.surfaceView.bringToFront()
//        }
    }

    override fun userDeviceStateChanged(msg: ParticipantDeviceStateMessage) {
        Log.e(TAG, "userDeviceStateChanged: $msg")
    }

    override fun userScreenStateChanged(msg: ScreenStateMessage) {
        Log.e(TAG, "userScreenStateChanged: $msg")
    }

    override fun endCall() {
        Log.e(TAG, "endCall: ")
    }

    override fun participantConnectionProblem(id: String) {
        Log.e(TAG, "participantConnectionProblem: ")
    }

    override fun participantConnectionRecovered(id: String) {
        Log.e(TAG, "participantConnectionRecovered: ")
    }

    override fun stopUI() {
        Log.e(TAG, "stopUI:")
    }

    override fun autoCallEnded() {
        Log.e(TAG, "autoCallEnded:")
    }

    override fun updateConnectivityState(isConnected: Boolean) {
        Log.e(TAG, "updateConnectivityState: $isConnected")
    }


    companion object {
        const val HOST_BASE_CHANNEL = "com.chi.calls/host.base"
    }
}
