package com.chi.webrtcsdk.core

import android.content.Context
import android.os.Handler
import android.os.Looper
import android.util.Log
import org.webrtc.*
import org.webrtc.audio.JavaAudioDeviceModule


class WebRtcClient(private val appConfig: RtcAppConfig): SignalSocketDelegate
{
    private lateinit var factory: PeerConnectionFactory
    private lateinit var iceServers: List<PeerConnection.IceServer>

    private var connectionId: String = ""
    private var authSession: AuthSession? = null
    private var localMedia: LocalMedia? = null

    private var socket: SignalsWebSocket? = null

    private val connections: MutableMap<String, RemotePeer> = mutableMapOf()

    private val rootEglBase: EglBase = EglBase.create()
    private var callUiDelegate: CallUiDelegate? = null
    private var disconnectHandler: Handler? = null
    private var connectivityStateHandler: Handler? = null

    val eglBase: EglBase
        get() = rootEglBase

    fun initialize(context: Context, uiDelegate: CallUiDelegate)
    {
        Log.e("RTC-C", "initialize: ")

        PeerConnectionFactory.initialize(
            PeerConnectionFactory.InitializationOptions.builder(context)
//                .setFieldTrials(fieldTrials)
                .setEnableInternalTracer(true)
                .createInitializationOptions()
        )

        val adm = JavaAudioDeviceModule.builder(context)
            .setUseHardwareAcousticEchoCanceler(true)
            .setUseHardwareNoiseSuppressor(true)
            .createAudioDeviceModule()

        val encoderFactory: VideoEncoderFactory = DefaultVideoEncoderFactory(
            rootEglBase.eglBaseContext, true, true
        )

        val decoderFactory: VideoDecoderFactory = DefaultVideoDecoderFactory(
            rootEglBase.eglBaseContext
        )

        factory = PeerConnectionFactory.builder()
            .setOptions(PeerConnectionFactory.Options())
            .setAudioDeviceModule(adm)
            .setVideoEncoderFactory(encoderFactory)
            .setVideoDecoderFactory(decoderFactory)
            .createPeerConnectionFactory()

        socket = SignalsWebSocket(appConfig, this)
        socket!!.start()

        Log.e("RTC-C", "web rtc socket started..: ")

        callUiDelegate = uiDelegate

        localMedia = LocalMedia(factory)
//        localMedia?.start(uiDelegate.getLocalView().surfaceView, rootEglBase)
        localMedia?.start(null, rootEglBase)

        if(appConfig.callType != "Video")
        {
            localMedia?.videoTrack?.setEnabled(false)
        }

//        factory.createLocalMediaStream()
        adm.release()

    }

    fun stop()
    {
        val connectionIds: MutableList<String> = ArrayList()

        socket?.prepareStop()

        connections.keys.forEach {
            val connection = connections[it]

            if(connection != null)
            {
                connectionIds.add(it)
                connection.stop()
            }
        }

        connectionIds.forEach {
            connections.remove(it)
        }

        callUiDelegate?.let {
//            localMedia?.stop(it.getLocalView().surfaceView) //TODO uncomment this for video call
            localMedia?.stop(null)
        }

        callUiDelegate?.endCall()

        val req = PingRequest()
        socket?.sendRpcRequest("leaveRoom", req) {
            socket?.stop()
        }
    }

    fun changeMicState(enabled: Boolean)
    {
        localMedia?.audioTrack?.setEnabled(enabled)

        socket?.updateDeviceState(
            connectionId = connectionId,
            device = "audio",
            state = enabled
        )
    }

    fun changeCamState(enabled: Boolean)
    {
        localMedia?.videoTrack?.setEnabled(enabled)

        socket?.updateDeviceState(
            connectionId = connectionId,
            device = "video",
            state = enabled
        )
    }

    fun switchCamera(id: String)
    {
        localMedia?.switchCameraDevice(id)
    }

    fun startConnectivityHandler()
    {
        if (connectivityStateHandler != null)
            return

        connectivityStateHandler = Handler(Looper.getMainLooper())
        connectivityStateHandler?.postDelayed(
            {
                callUiDelegate?.updateConnectivityState(false)
                connectivityStateHandler = null

            }, 3000)
    }

    override fun onSocketConnected()
    {
        Log.e("RTC-C", "onSocketConnected")

        disconnectHandler?.removeCallbacksAndMessages(null)
        disconnectHandler = null

        connectivityStateHandler?.removeCallbacksAndMessages(null)
        connectivityStateHandler = null

        callUiDelegate?.updateConnectivityState(true)

    }

    override fun onSocketDisconnected(closing: Boolean)
    {
        Log.e("RTC-C", "onSocketDisconnected Closing: $closing")

        startConnectivityHandler()

        if (disconnectHandler != null)
            return

        disconnectHandler = Handler(Looper.getMainLooper())
        disconnectHandler?.postDelayed(
            {
                callUiDelegate?.autoCallEnded()
                disconnectHandler = null

                Log.e("RTC-C", "onSocketDisconnected: autoCallEnded called")

            }, 30000)
    }

    override fun onAuthenticated(authSession: AuthSession)
    {
        Log.e("RTC-C", "onAuthenticated")
        this.authSession = authSession
        connectionId = authSession.connectionId
    }

    override fun onSessionAuthenticationFailed(errorMessage: String)
    {
        Log.e("RTC-C", "onAuthenticated")
    }

    override fun onRoomJoined(msg: JoinRoomResponse)
    {
        Log.e("RTC-C", "onRoomJoined")

        authSession?.auth_token = msg.auth_token

        val list: MutableList<PeerConnection.IceServer> = ArrayList()

        msg.ice_servers.forEach {

            it.urls.forEach{ url ->

                val builder = PeerConnection.IceServer.builder(url)

                if (it.credential != null)
                {
                    builder.setUsername(it.username!!)
                    builder.setPassword(it.credential)
                }

                list.add(builder.createIceServer())
            }
        }

        this.iceServers = list

        connections.keys.forEach {
            connections[it]?.reconnectRtc()
        }

        msg.value.forEach {
            callUiDelegate?.updateParticipant(it)
        }
    }

    override fun onParticipantRequestedOffer(msg: CallParticipant)
    {
        Log.e("RTC-C", "onParticipantRequestedOffer")
    }

    override fun onParticipantJoined(msg: CallParticipant)
    {
        Log.e("RTC-C", "onParticipantJoined")

        if(msg.metadata.is_recorder == true) return

        callUiDelegate?.participantJoined(msg)

        if(!connections.containsKey(msg.id))
        {
            val connection = RemotePeer(
                connectionId = msg.id,
                client = socket!!,
                factory = factory,
                uiDelegate = callUiDelegate!!,
                iceServers = iceServers
            )

            connections[msg.id] = connection
            connection.start(localMedia = localMedia!!)
        }
        else
        {
            val connection = connections[msg.id]
            connection?.reconnectRtc()
        }
    }

    override fun onParticipantLeft(msg: CallParticipant)
    {
        Log.e("RTC-C", "onParticipantLeft")

        val connection = connections[msg.id] ?: return
        connections.remove(msg.id)

        callUiDelegate?.participantLeft(msg.id)

        connection.stop()
    }

    override fun onParticipantConnectionProblem(msg: ParticipantGenericMessage)
    {
        Log.e("RTC-C", "onParticipantConnectionProblem")

        callUiDelegate?.participantConnectionProblem(msg.id)
    }

    override fun onParticipantConnectionRecovered(msg: ParticipantGenericMessage)
    {
        Log.e("RTC-C", "onParticipantConnectionRecovered")

        callUiDelegate?.participantConnectionRecovered(msg.id)

        val connection = connections[msg.id] ?: return

        connection.reconnectRtc()
    }

    override fun onParticipantIceCandidate(msg: RemoteIceCandidate)
    {
        Log.e("RTC-C", "onParticipantIceCandidate")

        val connection = connections[msg.senderConnectionId] ?: return

        connection.addIceCandidate(msg)
    }

    override fun onParticipantRequestedMedia(msg: RemoteMediaRequest)
    {
        Log.e("RTC-C", "onParticipantRequestedMedia")

        var connection = connections[msg.endpointName]

        callUiDelegate?.participantJoined(
            CallParticipant(
            id = msg.endpointName,
            createdAt = 0,
            metadata = msg.metadata,
            streams = null,
            deviceState = msg.deviceState
        )
        )

        if(connection == null)
        {
            connection = RemotePeer(
                connectionId = msg.endpointName,
                client = socket!!,
                factory = factory,
                uiDelegate = callUiDelegate!!,
                iceServers = iceServers
            )

            connections[msg.endpointName] = connection

            // connection.start(localMedia = localMedia!!)
            Handler(Looper.getMainLooper()).postDelayed({
                connection.start(localMedia!!)
            }, 2000)

        }

        connection.onOfferReceived(msg)
    }

    override fun onParticipantMediaStateChanged(msg: ParticipantDeviceStateMessage)
    {
        Log.e("RTC-C", "onParticipantMediaStateChanged")
        callUiDelegate?.userDeviceStateChanged(msg)
    }

    override fun onScreenStateAction(msg: ScreenStateMessage)
    {
        Log.e("RTC-C", "onScreenStateAction")
        callUiDelegate?.userScreenStateChanged(msg)
    }
}