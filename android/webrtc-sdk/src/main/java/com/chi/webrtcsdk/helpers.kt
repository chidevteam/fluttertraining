package com.chi.webrtcsdk

import com.google.gson.Gson
import org.webrtc.SdpObserver
import org.webrtc.SessionDescription
import java.lang.reflect.Type

internal fun Any.toJson(): String
{
    val gson = Gson()
    return gson.toJson(this)
}


internal object Serializer
{
    internal fun <T> deserialize(str: String, clazz: Class<T>): T
    {
        val gson = Gson()
        val obj = gson.fromJson(str, clazz)

        return obj as T
    }

    internal fun <T> deserialize(str: String, typeOfT: Type): T?
    {
        val gson = Gson()

        return gson.fromJson(str, typeOfT)
    }
}



class SimpleSdpObserver: SdpObserver
{
    override fun onCreateSuccess(p0: SessionDescription?) {

    }

    override fun onSetSuccess() {

    }

    override fun onCreateFailure(p0: String?) {

    }

    override fun onSetFailure(p0: String?) {

    }

}