package com.chi.webrtcsdk.ui

import android.annotation.SuppressLint
import android.app.Activity
import android.content.res.ColorStateList
import android.content.res.Resources
import android.graphics.Color
import android.os.CountDownTimer
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.*
import com.chi.webrtcsdk.R
import com.chi.webrtcsdk.databinding.ActiveCallParticipantViewBinding
import com.chi.webrtcsdk.core.*
import org.webrtc.EglBase
import org.webrtc.RendererCommon
import org.webrtc.VideoTrack
import java.util.*
import kotlin.collections.ArrayList


class CallUiUser(
    val metadata: UserMeta,
    var deviceState: ParticipantDeviceState,
    val binding: ActiveCallParticipantViewBinding?, //TODO undo nullable when testing done.
    val isLocal: Boolean = false
    )
{
    var showLocalPreview: Boolean = (isLocal && deviceState.videoActive)

    val initials: String = if(metadata.user_name.isEmpty()) {
        ""
    } else {
        val parts = metadata.user_name.replace(Regex("[ ]+")," ").split(" ")

        if(parts.size == 1) {
            metadata.user_name.substring(0, 2).uppercase()
        } else {
            "${parts[0].substring(0, 1)}${parts[1].substring(0, 1)}".uppercase()
        }
    }

//    val minEnabled: Boolean get() = deviceState.audioActive
//    val camEnabled: Boolean get() = deviceState.videoActive

    fun updateMicState(enabled: Boolean)
    {
        deviceState.audioActive = enabled

        binding?.user = this
    }

    fun updateCamState(enabled: Boolean, hasUsers: Boolean)
    {
        deviceState.videoActive = enabled

        binding?.user = this
//        binding.surfaceView.visibility = if(enabled) View.VISIBLE else View.GONE //TODO uncomment this for video call

        if(isLocal)
        {
            if (binding != null)
                binding.root.visibility = if(enabled) View.VISIBLE else View.GONE
        }
    }

    fun updateScreenState(enabled: Boolean)
    {
        deviceState.screenActive = enabled

        binding?.user = this

        if(!isLocal)
        {
//            binding.surfaceView.visibility = if(enabled) View.VISIBLE else View.GONE //TODO uncomment this for video call
        }
    }

    fun initRenderer(eglBase: EglBase, mirror: Boolean)
    {
        //TODO uncomment this for video call
//        val renderer = binding.surfaceView
//        renderer.init(eglBase.eglBaseContext, object: RendererCommon.RendererEvents
//        {
//            override fun onFirstFrameRendered()
//            {
//                renderer.setBackgroundColor(Color.TRANSPARENT)
//            }
//
//            override fun onFrameResolutionChanged(videoWidth: Int, videoHeight: Int, rotation: Int)
//            {
//
//            }
//        })
//
//        renderer.setBackgroundColor(Color.BLACK)
//        renderer.setScalingType(RendererCommon.ScalingType.SCALE_ASPECT_FILL)
//        renderer.setEnableHardwareScaler(true)
//        renderer.setMirror(mirror)
    }
}


class CallUiManager(
    private val parent: FrameLayout,
    private val localParent: FrameLayout,
    private val appConfig: RtcAppConfig,
    private val eglBase: EglBase,
    private val callUIManagerListener: CallUIManagerListener
    ): CallUiDelegate
{

    val localUser: CallUiUser

    private val remoteUsers: MutableMap<String, CallUiUser> = mutableMapOf()
    private val mHandler: Handler = Handler(Looper.getMainLooper())
    private val mCallEndHandler: Handler = Handler(Looper.getMainLooper())

    private var mTimerTask: TimerTask? = null
    private var mSecondsTimer: Timer? = null
    private var mCountDownTimer: CountDownTimer? = null
    private var mSeconds = 0

    companion object
    {
        const val TAG = "UIM"
    }

    init {

        val localParticipant: ActiveCallParticipantViewBinding =
            ActiveCallParticipantViewBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )

        val params = FrameLayout.LayoutParams(
            FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT
        )

        localUser = CallUiUser(
            metadata = appConfig.userMetaData,
            deviceState = ParticipantDeviceState(
                audioActive = true,
                videoActive = appConfig.callType == "Video",
                screenActive = false
            ),
            binding = localParticipant,
            isLocal = true
        )

        localUser.binding?.user = localUser
        localUser.binding?.root?.layoutParams = params

        localUser.initRenderer(eglBase, true)

        localParent.addView(localParticipant.root)

        calcLayout()
    }

    fun updateLocalCamState(enabled: Boolean)
    {
        localUser.updateCamState(enabled, remoteUsers.isNotEmpty())
    }

    private fun getPresentingUser(): CallUiUser?
    {
        val users = remoteUsers.filterValues { it.deviceState.screenActive }

        if (users.isEmpty())
            return null

        return users.values.first()
    }

    private fun calcLayout()
    {
        if (remoteUsers.isEmpty())
        {
            val params = FrameLayout.LayoutParams(-1, -1)
            params.topMargin = 0
            params.leftMargin = 0

            localUser.binding?.root?.layoutParams = params


            Log.e("UIM", "Just Local Layout")
            return
        }

        val screenMetrics = Resources.getSystem().displayMetrics

        if(screenMetrics.heightPixels > screenMetrics.widthPixels)
            doPortraitLayout()
        else
            doLandscapeLayout()
    }

    private fun doPortraitLayout()
    {
        val screenMetrics = Resources.getSystem().displayMetrics

        val localWidth: Int = (81 * screenMetrics.density).toInt()
        val localHeight: Int = (144 * screenMetrics.density).toInt()

//        localUser.layoutParams.width = localWidth
//        localUser.layoutParams.height = localHeight
//
//        localUser.layoutParams.topMargin = (20 * screenMetrics.density).toInt()
//        localUser.layoutParams.leftMargin = (screenMetrics.widthPixels - localWidth - (20 * screenMetrics.density)).toInt()

        val lParams = FrameLayout.LayoutParams(localWidth, localHeight)
        lParams.topMargin = (20 * screenMetrics.density).toInt()
        lParams.leftMargin = (screenMetrics.widthPixels - localWidth - (20 * screenMetrics.density)).toInt()

        localUser.binding?.root?.layoutParams = lParams

        Log.e("UIM", "Portrait Local Layout: ($localWidth, $localHeight)")

        val presentingUser = getPresentingUser()

        if(presentingUser != null)
        {
            val params = FrameLayout.LayoutParams(-1, -1)
            params.topMargin = 0
            params.leftMargin = 0
            presentingUser.binding?.root?.layoutParams = params

            //TODO uncomment this for video call
//            val renderer = presentingUser.binding.surfaceView
//            renderer.setScalingType(RendererCommon.ScalingType.SCALE_ASPECT_FIT)

            localUser.binding?.root?.visibility = View.GONE

            if(remoteUsers.size == 1)
                return

            val topMargin = screenMetrics.heightPixels - 80
            var leftMargin = 0
            val vHeight = 80
            val vWidth = 80

            remoteUsers.values.forEach { user: CallUiUser ->

                if (user != presentingUser)
                {
                    val params2 = FrameLayout.LayoutParams(vWidth, vHeight)
                    params2.topMargin = topMargin
                    params2.leftMargin = leftMargin
                    user.binding?.root?.layoutParams = params2

                    leftMargin += vWidth
                }
            }
        }
        else
        {
            localUser.binding?.root?.visibility = if (localUser.showLocalPreview) View.VISIBLE else View.GONE

            when (remoteUsers.size) {
                1 -> {
                    val user = remoteUsers.values.first()

                    val params = FrameLayout.LayoutParams(-1, -1)
                    params.topMargin = 0
                    params.leftMargin = 0
                    user.binding?.root?.layoutParams = params

                    //TODO uncomment this for video call
//                    val renderer = user.binding.surfaceView
//                    renderer.setScalingType(RendererCommon.ScalingType.SCALE_ASPECT_FILL)

                    Log.e("UIM", "One User Layout")

                }
                2 -> {
                    var topMargin = 0
                    val vHeight = screenMetrics.heightPixels / 2

                    remoteUsers.values.forEach { user: CallUiUser ->

                        val params = FrameLayout.LayoutParams(-1, vHeight)
                        params.topMargin = topMargin
                        params.leftMargin = 0
                        user.binding?.root?.layoutParams = params

                        topMargin += vHeight
                        Log.e("UIM", "Two User Layout")

                        //TODO uncomment this for video call
//                        val renderer = user.binding.surfaceView
//                        renderer.setScalingType(RendererCommon.ScalingType.SCALE_ASPECT_FILL)

                    }
                }
                3 -> {
                    var topMargin = 0
                    val vHeight = screenMetrics.heightPixels / 2
                    val vWidth = screenMetrics.widthPixels / 2

                    var idx = 0

                    remoteUsers.values.forEach { user: CallUiUser ->

                        val params = FrameLayout.LayoutParams(
                            if(idx == 2) -1 else vWidth,
                            vHeight
                        )
                        params.topMargin = topMargin
                        params.leftMargin = 0
                        user.binding?.root?.layoutParams = params

                        //TODO uncomment this for video call
//                        val renderer = user.binding.surfaceView
//                        renderer.setScalingType(RendererCommon.ScalingType.SCALE_ASPECT_FILL)

                        if (idx == 1)
                            topMargin += vHeight

                        idx++

                    }
                }
            }
        }

    }

    private fun doLandscapeLayout()
    {
        val screenMetrics = Resources.getSystem().displayMetrics

        val localWidth: Int = (144 * screenMetrics.density).toInt()
        val localHeight: Int = (81 * screenMetrics.density).toInt()

        Log.e("UIM", "Landscape Local Layout: ($localWidth, $localHeight)")

        val lParams = FrameLayout.LayoutParams(localWidth, localHeight)
        lParams.topMargin = (20 * screenMetrics.density).toInt()
        lParams.leftMargin = (screenMetrics.widthPixels - localWidth - (20 * screenMetrics.density)).toInt()

        localUser.binding?.root?.layoutParams = lParams

        when (remoteUsers.size) {
            1 -> {
                val user = remoteUsers.values.first()

                val params = FrameLayout.LayoutParams(-1, -1)
                params.topMargin = 0
                params.leftMargin = 0
                user.binding?.root?.layoutParams = params

                Log.e("UIM", "One User Layout")

            }
            2 -> {
                var leftMargin = 0
                val vWidth = screenMetrics.widthPixels / 2

                remoteUsers.values.forEach { user: CallUiUser ->

                    val params = FrameLayout.LayoutParams(vWidth, -1)
                    params.topMargin = 0
                    params.leftMargin = leftMargin
                    user.binding?.root?.layoutParams = params

                    leftMargin += vWidth
                    Log.e("UIM", "Two User Layout")
                }
            }
            3 -> {
                var topMargin = 0
                val vHeight = screenMetrics.heightPixels / 2
                val vWidth = screenMetrics.widthPixels / 2

                var idx = 0

                remoteUsers.values.forEach { user: CallUiUser ->

                    val params = FrameLayout.LayoutParams(vWidth, vHeight)
                    params.topMargin = 0
                    params.leftMargin = topMargin
                    user.binding?.root?.layoutParams = params

                    if (idx == 1)
                        topMargin += vHeight

                    idx++

                }
            }
        }
    }


    // CallUiDelegate
    override fun getLocalView(): ActiveCallParticipantViewBinding?
    {
        return  localUser.binding
    }

    override fun updateParticipant(msg: CallParticipant)
    {
        Log.e(TAG, "updateParticipant: 1 $msg" )
        if(remoteUsers.containsKey(msg.id))
        {
            msg.deviceState?.let { state: ParticipantDeviceState ->

                Log.e(TAG, "updateParticipant: 2 $state")
                remoteUsers[msg.id]?.let { user: CallUiUser ->
                    mHandler.post {
                        Log.e(TAG, "updateParticipant: 3 $user")
                        user.deviceState = state
                        user.binding?.user = user
                    }
                }
            }
        }
    }

    override fun participantJoined(msg: CallParticipant)
    {

        Log.e(TAG, "--------------> participantJoined: $msg")

        if(msg.metadata.is_recorder == true) return

        if(remoteUsers.containsKey(msg.id))
        {
            return
        }

        mCallEndHandler.removeCallbacksAndMessages(null)

        val view = (parent.parent as View).findViewById(R.id.layoutConnectivity) as LinearLayout
        val counterView = (parent.parent as View).findViewById(R.id.lblTimer) as TextView
        view.visibility = View.GONE

        val menuView = (parent.parent as View).findViewById(R.id.layoutMenu) as LinearLayout
        val lblDuration = (menuView as View).findViewById(R.id.lblDuration) as TextView
        lblDuration.visibility = View.VISIBLE

        if (appConfig.is_teleconsult) counterView.visibility = View.VISIBLE

        setTeleconsultUI(appConfig.is_teleconsult, counterView)

        if (mSecondsTimer == null)
            startTimer(lblDuration)

        mHandler.post {
            val userBinding: ActiveCallParticipantViewBinding =
                ActiveCallParticipantViewBinding.inflate(
                    LayoutInflater.from(parent.context), parent, false
                )

            val user = CallUiUser(
                metadata = msg.metadata,
                deviceState = msg.deviceState!!,
                binding = userBinding
            )

            userBinding.user = user

            parent.addView(userBinding.root)

            user.initRenderer(eglBase, false)

            remoteUsers[msg.id] = user

            //TODO uncomment this for video call
//            val renderer = user.binding.surfaceView
//            renderer.visibility = if(user.deviceState.videoActive) View.VISIBLE else View.GONE

            calcLayout()
        }
    }

    override fun participantLeft(id: String)
    {
        if(!remoteUsers.containsKey(id))
            return

        val user = remoteUsers[id] ?: return

        mHandler.post {
            parent.removeView(user.binding?.root)

            remoteUsers.remove(id)

            calcLayout()

            if (remoteUsers.isEmpty())
            {
                mCallEndHandler.postDelayed({

                    Log.e(TAG, "mCallEndHandler: ending call.....")
                    callUIManagerListener.autoCallEnded()

                },30000)
            }
        }
    }

    override fun addVideoTrack(connectionId: String, track: VideoTrack)
    {
        if(!remoteUsers.containsKey(connectionId))
        {
            Log.e(TAG, "🔔 Add Video Track - $connectionId not found")
            return
        }

        val user = remoteUsers[connectionId] //?: return

        if (user == null)
        {
            Log.e(TAG, "🔔 Add Video Track - user with $connectionId not found")
            return
        }

        //TODO uncomment this for video call
//        mHandler.post {
//
//            val renderer = user.binding.surfaceView
//
//            Log.e(TAG, "🔔 Added Video Track to Renderer")
//            track.addSink(renderer)
//            renderer.visibility = if(user.deviceState.videoActive) View.VISIBLE else View.GONE
//
//            localUser.binding.surfaceView.bringToFront()
//        }
//
//        mHandler.postDelayed({
//            localUser.binding.surfaceView.visibility = View.GONE
//            localUser.binding.surfaceView.visibility = View.VISIBLE
//        }, 2000)
    }

    override fun userDeviceStateChanged(msg: ParticipantDeviceStateMessage)
    {
        if(!remoteUsers.containsKey(msg.endpointName))
            return

        val user = remoteUsers[msg.endpointName] ?: return
        mHandler.post {
            user.updateMicState(msg.audio)
            user.updateCamState(msg.video, false)
        }
    }

    override fun userScreenStateChanged(msg: ScreenStateMessage) {

        Log.e(TAG, "userScreenStateChanged: remoteUsers: $remoteUsers  connectionId: ${msg.connectionId} ", )

        if(!remoteUsers.containsKey(msg.connectionId))
            return

        val user = remoteUsers[msg.connectionId] ?: return

        mHandler.post {
            if (msg.action == "fullscreen")
            {
                user.updateScreenState(true)
            }
            else if (msg.action == "exit_fullscreen")
            {
                user.updateScreenState(false)
            }

            calcLayout()
        }
    }

    override fun endCall()
    {
        mCallEndHandler.removeCallbacksAndMessages(null)

        mHandler.post {

            val connectionIds: MutableList<String> = ArrayList()

            remoteUsers.keys.forEach {
                val user = remoteUsers[it]

                if(user != null)
                {
                    connectionIds.add(it)

                    parent.removeView(user.binding?.root)
//                    user.binding.surfaceView.release() //TODO uncomment this for video call
                }
            }

            connectionIds.forEach {
                remoteUsers.remove(it)
            }

            localParent.removeView(localUser.binding?.root)
//            localUser.binding.surfaceView.release() //TODO uncomment this for video call
        }
    }

    override fun participantConnectionProblem(id: String) {
        if(!remoteUsers.containsKey(id))
            return

        val user = remoteUsers[id] ?: return

        mHandler.post {
            user.binding?.colErrorMessage?.visibility = View.VISIBLE
        }
    }

    override fun participantConnectionRecovered(id: String) {
        if(!remoteUsers.containsKey(id))
            return

        val user = remoteUsers[id] ?: return

        mHandler.post {
            user.binding?.colErrorMessage?.visibility = View.GONE
        }
    }

    override fun stopUI()
    {
        mSecondsTimer?.cancel()
        mCountDownTimer?.cancel()
//        mTimerTask?.cancel()
    }

    override fun autoCallEnded() {
        callUIManagerListener.autoCallEnded()
    }

    override fun updateConnectivityState(isConnected: Boolean) {
        mHandler.post {
            val view = (parent.parent as View).findViewById(R.id.layoutConnectivity) as LinearLayout
            val warningIcon = view.findViewById(R.id.ivWarning) as ImageView
            val lblErrorMessage = view.findViewById(R.id.lblErrorMessage) as TextView
            val leaveCallBtn = view.findViewById(R.id.imgBtnLeaveCall) as ImageButton
            val progressBar = view.findViewById(R.id.pbLoading) as ProgressBar

            lblErrorMessage.text = if (isConnected) parent.context.getString(R.string.waiting_for_other_participant_to_join)
                                   else parent.context.getString(R.string.please_check_your_internet_connection)

            view.visibility = if (isConnected) View.GONE else View.VISIBLE
            warningIcon.visibility = if (isConnected) View.GONE else View.VISIBLE
            leaveCallBtn.visibility = if (isConnected) View.VISIBLE else View.GONE
            progressBar.visibility = if (isConnected) View.GONE else View.VISIBLE
        }
    }

    fun setTeleconsultUI(isTeleconsult: Boolean, lblTimer: TextView)
    {
        if (!isTeleconsult) return

        startCountDownTimer(lblTimer)
    }

    fun startCountDownTimer(lblTimer: TextView)
    {
        Handler(Looper.getMainLooper()).post {
            mCountDownTimer = object : CountDownTimer(appConfig.tc_config!!.duration!!.toLong() * 1000, 1000) {
                override fun onTick(millisUntilFinished: Long) {
                    val minutes = millisUntilFinished / 60000
                    val seconds = (millisUntilFinished % 60000)/1000

                    Log.e(TAG, "onTick: minutes $minutes   seconds $seconds")

                    lblTimer.text = String.format("%02d : %02d", minutes, seconds)
                    if (appConfig.tc_config!!.enforce_call_duration)
                    {
                        if ((millisUntilFinished/1000) < appConfig.tc_config!!.red_alert_delta!!)
                        {
                            animateBackground(true)
                        }
                        else if ((millisUntilFinished/1000) < appConfig.tc_config!!.yellow_alert_delta!!)
                        {
                            animateBackground(false)
                        }
                    }
                }

                override fun onFinish() {
                    lblTimer.backgroundTintList = ColorStateList.valueOf(parent.context.getColor(R.color.red))
                }
            }.start()
        }
    }

    fun startTimer(lblDuration: TextView)
    {
        mSecondsTimer = Timer()
        initializeTimerTask(lblDuration)

        //schedule the timer, to wake up every 1 second
        mSecondsTimer?.schedule(mTimerTask, 1000, 1000)
    }

    private fun initializeTimerTask(lblDuration: TextView)
    {
        mTimerTask = object : TimerTask() {
            override fun run() {

                val activity = parent.context as Activity

                activity.runOnUiThread {
                    val hours = ++mSeconds / 3600
                    val minutes = (mSeconds % 3600) / 60

                    // Log.e(TAG, "TimerTask: hours $hours   minutes $minutes   seconds ${mSeconds % 60}")

                    lblDuration.text = String.format("%02d:%02d:%02d",hours, minutes, mSeconds % 60)
                }
            }
        }
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    fun animateBackground(isCritical: Boolean)
    {
        Log.e(TAG, "animateBackground: isCritical : $isCritical")

        val background = (parent.parent as View).findViewById(R.id.screenShadow) as LinearLayout
        background.clearAnimation()

        if (isCritical)
        {
            val animation1: Animation = AnimationUtils.loadAnimation(parent.context, R.anim.blink)
            background.background = parent.context.getDrawable(R.drawable.screen_shadow_red)
            background.animation = animation1

        }
        else
        {
            val animation1: Animation = AnimationUtils.loadAnimation(parent.context, R.anim.blink)
            background.background = parent.context.getDrawable(R.drawable.screen_shadow_warning)
            background.animation = animation1
        }

    }
}