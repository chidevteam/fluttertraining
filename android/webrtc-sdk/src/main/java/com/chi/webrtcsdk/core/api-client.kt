package com.chi.webrtcsdk.core

import android.util.Log
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.POST
import retrofit2.http.Query
import java.util.*
import java.util.concurrent.TimeUnit


internal class ApiClient(private val mBaseUrl: String)
{
    internal val apiService: RtcApiService
    private var m_AuthToken: String? = null
    private var m_ViewSessionID: String? = null

    init {
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

        val okHttpClient = OkHttpClient.Builder()
            .readTimeout(25, TimeUnit.SECONDS)
            .connectTimeout(25, TimeUnit.SECONDS)
            .addInterceptor(loggingInterceptor)
            .addInterceptor { chain ->
                var request: okhttp3.Request = chain.request()

                val builder = request.newBuilder()
                builder.addHeader("Authorization", m_AuthToken!!)
                if (m_ViewSessionID != null)
                {
                    builder.addHeader("X-SESSION-ID", m_ViewSessionID!!)
                }
                request = builder.build()
                chain.proceed(request)

            }.build()

        val builder = GsonBuilder()

        builder.setLenient()
        val gson = builder.create()

        val retrofit = Retrofit.Builder()
            .baseUrl(mBaseUrl)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()

        apiService = retrofit.create(RtcApiService::class.java)
    }

    companion object {
        private var mApiClient: ApiClient? = null

        fun createClient(baseUrl: String): ApiClient
        {
            mApiClient = ApiClient(baseUrl)

            return mApiClient as ApiClient
        }

        fun apiService(): RtcApiService
        {
            return mApiClient!!.apiService
        }

        fun setAuthToken(token: String?)
        {
            if (mApiClient == null)
                return

            mApiClient!!.m_AuthToken = token
        }

        fun setViewSessionId(viewSessionId: String?)
        {
            if (viewSessionId == null) return
            if (mApiClient == null) return

            mApiClient!!.m_ViewSessionID = viewSessionId
        }
    }
}

interface RtcApiService
{
    @POST("/comms/calls-router/call-authorization/Request")
    fun getToken(@Query("token") token: String, @Body request: AuthRequest): Call<AuthResponse>
}