package com.chi.webrtcsdk.ui

import android.animation.LayoutTransition
import android.content.Context
import android.content.res.ColorStateList
import android.hardware.Camera
import android.hardware.camera2.CameraManager
import android.media.AudioDeviceInfo
import android.media.AudioManager
import android.media.MediaPlayer
import android.os.Bundle
import android.os.CountDownTimer
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.chi.webrtcsdk.R
import com.chi.webrtcsdk.Serializer
import com.chi.webrtcsdk.core.*
import com.chi.webrtcsdk.databinding.ActiveCallFragmentBinding
import com.chi.webrtcsdk.databinding.CallMenuFragmentBinding
import com.chi.webrtcsdk.toJson
import org.webrtc.Camera1Enumerator


class ActiveCallFragment: Fragment(),CallUIManagerListener
{
    private lateinit var mContext: Context

    private var isSpeakerOnOff = false
    private var isMicEnabled = true
    private var isCameraEnabled = false

    var isBlueToothConnected = false

    private var _binding: ActiveCallFragmentBinding? = null
    private var mMenuViewBinding: CallMenuFragmentBinding? = null

    private val binding get() = _binding!!

    private var mCallConfig: CallConfig? = null
    private var rtcClient: WebRtcClient? = null

    private lateinit var callUiManager: CallUiManager
    private lateinit var appConfig: RtcAppConfig

    private var listener: CallStateListener?  = null

    val roomId: String? get() = mCallConfig?.roomId

    lateinit var mAudioManager: AudioManager
    private val mHandler: Handler = Handler(Looper.getMainLooper())
    private val mRecordingHandler: Handler = Handler(Looper.getMainLooper())

    var speakerAdapter: ArrayAdapter<String>? = null
    var micAdapter: ArrayAdapter<String>? = null
    var cameraAdapter: ArrayAdapter<String>? = null

    val audioDevices = arrayOf(
        "TYPE_UNKNOWN",
        "TYPE_BUILTIN_EARPIECE",
        "TYPE_BUILTIN_SPEAKER",
        "TYPE_WIRED_HEADSET",
        "TYPE_WIRED_HEADPHONES",
        "TYPE_LINE_ANALOG",
        "TYPE_LINE_DIGITAL",
        "TYPE_BLUETOOTH_SCO",
        "TYPE_BLUETOOTH_A2DP",
        "TYPE_HDMI",
        "TYPE_HDMI_ARC",
        "TYPE_USB_DEVICE",
        "TYPE_USB_ACCESSORY",
        "TYPE_DOCK",
        "TYPE_FM",
        "TYPE_BUILTIN_MIC",
        "TYPE_FM_TUNER",
        "TYPE_TV_TUNER",
        "TYPE_TELEPHONY",
        "TYPE_AUX_LINE",
        "TYPE_IP",
        "TYPE_BUS",
        "TYPE_USB_HEADSET",
        "TYPE_HEARING_AID",
        "TYPE_BUILTIN_SPEAKER_SAFE",
        "TYPE_REMOTE_SUBMIX",
        "TYPE_BLE_HEADSET",
        "TYPE_BLE_SPEAKER",
        "TYPE_ECHO_REFERENCE",
        "TYPE_HDMI_EARC"
    ) //TODO added this list to show String types of devices

    var mSpeakerList: ArrayList<String> = ArrayList()
    var mMicList: ArrayList<String> = ArrayList()
//    val mCameraList = arrayOf("Front", "Back")
    val mCameraList = ArrayList<String>()

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)

        arguments?.let {
            mCallConfig = Serializer.deserialize(it.getString(ARG_CALL_CONFIG)!!, CallConfig::class.java)
        }

        Log.e(TAG, "Fragment onCreate")

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View
    {
        _binding = ActiveCallFragmentBinding.inflate(inflater, container, false)

        binding.handler = this

        Log.e(TAG, "Fragment onCreateView")

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        super.onViewCreated(view, savedInstanceState)

        Log.e(TAG, "Fragment onViewCreated")

        binding.participantsView.layoutTransition.enableTransitionType(LayoutTransition.CHANGING)
        mMenuViewBinding = CallMenuFragmentBinding.inflate(LayoutInflater.from(binding.root.context), binding.colMain, false)
//        binding.localPartView.layoutTransition.enableTransitionType(LayoutTransition.CHANGING)

        mCallConfig?.let {

            appConfig = RtcAppConfig(it)

            isCameraEnabled = it.callType == "Video"
            val icon: Int = if (isCameraEnabled)
                R.drawable.ic_videocam_on
            else
                R.drawable.ic_videocam_off

            binding.imgBtnCamera.setImageDrawable(ContextCompat.getDrawable(mContext, icon))

            rtcClient = WebRtcClient(appConfig)

            callUiManager = CallUiManager(
                binding.participantsView,
                binding.localPartView,
                appConfig,
                rtcClient!!.eglBase,
            this)

            rtcClient!!.initialize(mContext, callUiManager)
            callUiManager.updateLocalCamState(isCameraEnabled)

        }

        setupMenuUi()
    }


    override fun onAttach(context: Context)
    {
        super.onAttach(context)

        Log.e(TAG, "Fragment onAttach")
        mContext = context

        if(context is CallStateListener)
        {
            listener = context
        }
    }

    override fun onDetach()
    {
        listener = null

        Log.e(TAG, "Fragment onDetach")

        super.onDetach()
    }

    override fun onDestroyView()
    {
        mHandler.removeCallbacksAndMessages(null)

        callUiManager.stopUI()

        rtcClient?.stop()
        _binding = null

        Log.e(TAG, "Fragment onDestroyView")

        super.onDestroyView()
    }

    fun setupMenuUi()
    {
        mAudioManager = mContext.getSystemService(Context.AUDIO_SERVICE) as AudioManager
        mSpeakerList = getOutputDevices()
        mMicList = getMic()

        speakerAdapter = ArrayAdapter(mContext, android.R.layout.simple_spinner_item, mSpeakerList)
        speakerAdapter!!.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        mMenuViewBinding!!.spSpeaker.adapter = speakerAdapter

        micAdapter = ArrayAdapter(mContext, android.R.layout.simple_spinner_item, mMicList)
        micAdapter!!.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        mMenuViewBinding!!.spMic.adapter = micAdapter

        cameraAdapter = ArrayAdapter(mContext, android.R.layout.simple_spinner_item, mCameraList)
        cameraAdapter!!.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        mMenuViewBinding!!.spCamera.adapter = cameraAdapter

        mMenuViewBinding!!.spSpeaker.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                Log.e(TAG, "onCreateView: Speaker onItemSelected $position" )
                val item = mSpeakerList.get(position)
                val deviceType = ArrayList(listOf(*audioDevices)).indexOf(item)
                changeSpeakerDevice(deviceType)

            }
            override fun onNothingSelected(parent: AdapterView<*>) {}
        }

        mMenuViewBinding!!.spMic.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                Log.e(TAG, "onCreateView: Mic onItemSelected $position" )
                val item = mMicList.get(position)
                val deviceType = ArrayList(listOf(*audioDevices)).indexOf(item)
            }
            override fun onNothingSelected(parent: AdapterView<*>) {}
        }

        mMenuViewBinding!!.spCamera.onItemSelectedListener = object : AdapterView.OnItemSelectedListener
        {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long)
            {
                val deviceId = mCameraList[position]

                Log.e(TAG, "onCreateView: Camera onItemSelected $position, Device: $deviceId" )

                rtcClient?.switchCamera(deviceId)
            }

            override fun onNothingSelected(parent: AdapterView<*>) {}
        }

        getCameraDevices()

        mHandler.postDelayed({
            binding.layoutMenu.visibility = View.VISIBLE
        }, 3000)
    }

    fun getCameraDevices()
    {
        mCameraList.clear()

        val list = Camera1Enumerator(true).deviceNames
        mCameraList.addAll(list)
        cameraAdapter?.notifyDataSetChanged()
        mMenuViewBinding!!.spCamera.setSelection(1)
    }

    fun getOutputDevices(): ArrayList<String> {

        val devices = mAudioManager.getDevices(AudioManager.GET_DEVICES_OUTPUTS)
        var list = ArrayList<String>()

        for (device in devices) {
            list.add(audioDevices[device.type])
        }

        return list
    }

    fun getMic(): ArrayList<String> {

        val devices = mAudioManager.getDevices(AudioManager.GET_DEVICES_INPUTS)
        var list = ArrayList<String>()

        for (device in devices) {
            list.add(audioDevices[device.type])
        }

        return list
    }

    fun onShowMenu(view: View)
    {

    }

    fun onCameraOnOff()
    {
        Log.e(TAG, "onCameraOnOff ------------------")

        isCameraEnabled = !isCameraEnabled

        val icon: Int = if (isCameraEnabled)
            R.drawable.ic_videocam_on
        else
            R.drawable.ic_videocam_off

        binding.imgBtnCamera.setImageDrawable(ContextCompat.getDrawable(mContext, icon))

        rtcClient?.changeCamState(isCameraEnabled)
        callUiManager.updateLocalCamState(isCameraEnabled)
    }

    fun onMicOnOff(view: View)
    {
        Log.e(TAG, "onMicOnOff ------------------")

        isMicEnabled = !isMicEnabled

        val icon = if(isMicEnabled)
            R.drawable.ic_mic_on
        else
            R.drawable.ic_mic_off

        binding.imgBtnMic.setImageDrawable(ContextCompat.getDrawable(mContext, icon))

        rtcClient?.changeMicState(isMicEnabled)
        callUiManager.localUser.updateMicState(isMicEnabled)
    }

    fun onMore(view: View)
    {
        Log.e(TAG, "============== onMore: $listener ==============")

        binding.callMenuView.removeAllViews()

        binding.callMenuView.addView(mMenuViewBinding!!.root)

        mMenuViewBinding!!.btnClose.setOnClickListener {
            binding.callMenuView.removeView(mMenuViewBinding!!.root)
        }
    }

    fun onEndCall(view: View?)
    {
        rtcClient?.stop()
        listener?.callEnded()

        mAudioManager.mode = AudioManager.MODE_NORMAL
        mAudioManager.isSpeakerphoneOn = true

        if (isBlueToothConnected)
            mAudioManager.stopBluetoothSco()
    }

    fun onHideRecordingView(view: View)
    {
        binding.layoutRecording.visibility = View.GONE
        mRecordingHandler.removeCallbacksAndMessages(null)
    }

    fun changeSpeakerDevice(type : Int)
    {
        if (type == AudioDeviceInfo.TYPE_BUILTIN_SPEAKER || type == AudioDeviceInfo.TYPE_BUILTIN_SPEAKER_SAFE)
        {
            mAudioManager.mode = AudioManager.MODE_IN_COMMUNICATION
            mAudioManager.isSpeakerphoneOn = true

            if (isBlueToothConnected)
                mAudioManager.stopBluetoothSco()
        }
        else if (type == AudioDeviceInfo.TYPE_BUILTIN_EARPIECE)
        {
            mAudioManager.mode = AudioManager.MODE_IN_COMMUNICATION
            mAudioManager.isSpeakerphoneOn = false

            if (isBlueToothConnected)
                mAudioManager.stopBluetoothSco()
        }
        else if (type == AudioDeviceInfo.TYPE_BLUETOOTH_SCO || type == AudioDeviceInfo.TYPE_BLUETOOTH_A2DP)
        {
            isBlueToothConnected = true
            mAudioManager.mode = AudioManager.MODE_IN_COMMUNICATION
            mAudioManager.isSpeakerphoneOn = false
            mAudioManager.startBluetoothSco()
        }
    }

    override fun autoCallEnded() {
        onEndCall(null)
    }

    fun recordingStateChange(isStarted: Boolean, name: String)
    {
        if (isStarted)
        {
            binding.layoutRecording.visibility = View.VISIBLE
            binding.ivRecording.visibility = View.VISIBLE
            binding.lblRecordingMessage.text = "${name} started recording "

            binding.ivRecording.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.blink))
        }
        else
        {
            binding.layoutRecording.visibility = View.VISIBLE
            binding.lblRecordingMessage.text = "Recording stopped"
            binding.ivRecording.visibility = View.INVISIBLE
            binding.ivRecording.clearAnimation()

            mRecordingHandler.postDelayed({
                binding.layoutRecording.visibility = View.GONE
            },4000)
        }
    }

    companion object
    {
        const val TAG: String = "CallFragment"
        const val ARG_CALL_CONFIG = "CallConfig"

        @JvmStatic
        fun newInstance(callConfig: CallConfig) =
            ActiveCallFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_CALL_CONFIG, callConfig.toJson())
                }
            }

    }
}