package com.chi.webrtcsdk.core

import android.util.Log
import android.view.View
import org.webrtc.*
import java.util.*
import org.webrtc.CameraVideoCapturer


class LocalMedia(private val factory: PeerConnectionFactory)
{
    var videoCapturer: VideoCapturer? = null
    var videoTrack: VideoTrack? = null
    var audioTrack: AudioTrack? = null

    var selectedCameraDevice: String = ""

//    fun start(localRender: SurfaceViewRenderer, eglBase: EglBase) //TODO uncomment this for video call
    fun start(localRender: SurfaceViewRenderer?, eglBase: EglBase)
    {
        audioTrack = createAudioTrack()

        if (localRender != null)
            videoTrack = createVideoTrack(localRender, eglBase)
    }

//    fun stop(localRender: SurfaceViewRenderer) //TODO uncomment this for video call
    fun stop(localRender: SurfaceViewRenderer?)
    {
        audioTrack?.dispose()
        audioTrack = null

        videoTrack?.removeSink(localRender)
        videoTrack?.dispose()
        videoTrack = null

        videoCapturer?.dispose()
    }


    private fun createAudioTrack(): AudioTrack
    {
        val audioConstraints = MediaConstraints()
        val audioSource = factory.createAudioSource(audioConstraints)

        val trackId = UUID.randomUUID().toString()

        return factory.createAudioTrack(trackId, audioSource)
    }

    private fun createVideoTrack(localRender: SurfaceViewRenderer, eglBase: EglBase): VideoTrack
    {
        val enumerator = Camera1Enumerator(true)
        val deviceNames = enumerator.deviceNames

//        val formats = enumerator.getSupportedFormats(deviceNames[1])
//        Log.e("LOCAL", "Formats = $formats")

        val deviceName = deviceNames[1]
        selectedCameraDevice = deviceName
        Log.e("LOCAL", "Camera = $deviceName")

        videoCapturer = enumerator.createCapturer(deviceName, null)

        val videoSource = factory.createVideoSource(false)
        val threadName = Thread.currentThread().name
        val surfaceTextureHelper = SurfaceTextureHelper.create(
            threadName, eglBase.eglBaseContext
        )

        videoCapturer!!.initialize(surfaceTextureHelper, localRender.context, videoSource.capturerObserver)
        videoCapturer?.startCapture(640, 480, 30)

        videoSource.adaptOutputFormat(640, 480, 30)

        val trackId = UUID.randomUUID().toString()
        val track = factory.createVideoTrack(trackId, videoSource)
        track.setEnabled(true)

        track.addSink(localRender)
        localRender.visibility = View.VISIBLE

        return track
    }

    fun switchCameraDevice(cameraId: String)
    {
        Log.e("LOCAL", "Switching to Camera = $cameraId")

        if (selectedCameraDevice == cameraId)
        {
            Log.e("LOCAL", "Not switching camera")
            return
        }

        selectedCameraDevice = cameraId

        val cameraVideoCapturer = videoCapturer as CameraVideoCapturer
        cameraVideoCapturer.switchCamera(object: CameraVideoCapturer.CameraSwitchHandler
        {
            override fun onCameraSwitchDone(isFrontCamera: Boolean)
            {
                Log.e("LOCAL", "onCameraSwitchDone, isFrontCamera = $isFrontCamera")
            }

            override fun onCameraSwitchError(error: String?)
            {
                Log.e("LOCAL", "onCameraSwitchError = $error")
            }

        }, cameraId)
    }
}
