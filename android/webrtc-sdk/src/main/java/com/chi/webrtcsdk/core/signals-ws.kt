package com.chi.webrtcsdk.core

import android.os.Handler
import android.os.Looper
import okhttp3.*
import retrofit2.Call
import retrofit2.Callback
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type
import android.util.Log

import com.chi.webrtcsdk.Serializer
import com.chi.webrtcsdk.toJson


internal class SignalsWebSocket(
    private val appConfig: RtcAppConfig,
    private val peerDelegate: SignalSocketDelegate
): WebSocketListener(), RpcClient
{
    private val okHttpClient = OkHttpClient()
    private var webSocket: WebSocket? = null

    private var connecting: Boolean = false
    private var connected: Boolean = false
    private var closing: Boolean = false
    private var closed: Boolean = false

    private var commandId: Int = 0
    private var socketToken: String? = null

    private var authSession: AuthSession? = null

//    private val peerDelegate: SignalSocketDelegate

    private var rpcRequests: MutableMap<Int, RpcRequestEntry> = mutableMapOf()

    private var mPendingPack: ArrayList<String> = ArrayList()

    companion object
    {
        private const val TAG = "SWS"
        private const val kParticipantJoined = "participantJoined"
        private const val kParticipantLeft = "participantLeft"
        private const val kParticipantConnectionProblem = "participantConnectionProblem"
        private const val kParticipantConnectionRecovered = "participantConnectionRecovered"
        private const val kIceCandidate = "iceCandidate"
        private const val kMediaRequest = "mediaRequest"
        private const val kRequestForOffer = "requestForOffer"
        private const val kOnPresentationAction = "onPresentationAction"
        private const val kParticipantDeviceStateChanged = "participantDeviceStateChanged"
    }

    fun start()
    {
        closing = false
        connect()
    }

    fun prepareStop()
    {
        closing = true
    }

    fun stop()
    {
        if(closed)
            return

        closed = true

        val ws = webSocket ?: return

        webSocket = null
        ws.close(200, "Closing")
    }

    fun updateDeviceState(connectionId: String, device: String, state: Boolean)
    {
        val req = DeviceStateChangedRequest(
            endpointName = connectionId,
            device = device,
            state = state
        )

        sendRpcRequest("deviceState", req) {

        }

    }

    override fun <T> sendRpcRequest(method: String, params: T, completion: RpcCompletion)
    {
        val message = RpcRequest(cmdId, method, params)
        val msg = message.toJson()

        val ent = RpcRequestEntry(message, completion)
        rpcRequests[message.id] = ent

        if (webSocket == null)
        {
            mPendingPack.add(msg)
        }
        else
        {
            webSocket!!.send(msg)
        }
    }

    override fun <T> sendRpcResponse(refId: Int, endPoint: String, result: T, completion: RpcCompletion)
    {
        val message = RpcResponse(null, method = "rpcResponse", result = result,
            params = null, endpointName = endPoint, ref_id = refId)

        val msg = message.toJson()

        if (webSocket == null)
        {
            mPendingPack.add(msg)
        }
        else
        {
            webSocket!!.send(msg)
        }

        completion(RpcResult(""))
    }

    private val cmdId: Int
        get() {
            commandId += 1

            return commandId
        }

    private fun schedulePing()
    {
        if (closing || connecting || !connected) return

        Handler(Looper.getMainLooper()).postDelayed({

            if (!closing) {
                val req = PingRequest()

                sendRpcRequest("ping", req) {

                    schedulePing()
                }
            }
        }, 1000)
    }

    private fun connect()
    {
        if(authSession == null)
        {
            getToken()
        }
        else
        {
            connectSocket()
        }
    }

    private fun reconnect()
    {
        if(closing || closed || connected)
            return

        Handler(Looper.getMainLooper()).postDelayed({
            connect()
        }, 1000)
    }

    private fun connectSocket()
    {
        if(connecting)
            return

        if (authSession == null)
            return

        connecting = true
        if(webSocket != null)
        {
            webSocket!!.close(1000, "Closing socket if open on reconnect")
            webSocket = null
        }

        val session: AuthSession = authSession!!

        Log.e(TAG, "Connecting Signals Socket")
        val socketUrl = "${session.socketUrl}?token=$socketToken"
        val request = Request.Builder().url(socketUrl).build()

        webSocket = okHttpClient.newWebSocket(request, this)
    }

    private fun joinRoom()
    {
        val aus = authSession ?: return

        Log.e(TAG, "Joining the room")
        val req = JoinRoomRequest(room_id = aus.room_id, connectionId = aus.connectionId,
            has_video = appConfig.callType == "Video",
            video_active = appConfig.callType == "Video",
            metadata = appConfig.userMetaData)

        sendRpcRequest("joinRoom", req) { result: RpcResult ->
            connecting = false

            Log.e(TAG, "Room Joined")
            val typeOfT: Type = object : TypeToken<RpcResponse<JoinRoomResponse>>() {}.type
            val resp: RpcResponse<JoinRoomResponse>? = Serializer.deserialize(result.message, typeOfT)

            authSession?.auth_token = resp!!.result!!.auth_token
            peerDelegate.onRoomJoined(resp.result!!)

            schedulePing()

            sendPendingMessages()
        }

    }

    private fun sendPendingMessages()
    {
        val ws = webSocket ?: return

        for (msg in mPendingPack)
        {
            ws.send(msg)
        }

        mPendingPack.clear()
    }

    private fun getToken()
    {
        ApiClient.createClient(appConfig.mainServer)
        ApiClient.setAuthToken(appConfig.authToken)
        ApiClient.setViewSessionId(appConfig.viewSessionId)
        val call = ApiClient.apiService().getToken(
            appConfig.authToken,
            AuthRequest(appConfig.roomId, appConfig.userMetaData.user_id, appConfig.userMetaData.user_name, appConfig.inviteToken)
        )

        call.enqueue(object: Callback<AuthResponse>
        {
            override fun onResponse(
                call: Call<AuthResponse>,
                response: retrofit2.Response<AuthResponse>)
            {
                val resp = response.body() ?: return

                authSession = resp.session
                socketToken = authSession!!.auth_token

                peerDelegate.onAuthenticated(authSession!!)
                connectSocket()
            }

            override fun onFailure(call: Call<AuthResponse>, t: Throwable)
            {
                Log.e(TAG, "Failed to get Token. Error: $t")
            }
        })
    }


    override fun onOpen(webSocket: WebSocket?, response: Response?)
    {
        this.connecting = false
        this.connected = true

        peerDelegate.onSocketConnected()

        joinRoom()
    }

    override fun onMessage(webSocket: WebSocket, text: String)
    {
        val message = Serializer.deserialize(text, RpcResponseHeader::class.java)

        if (message.id != null && rpcRequests.containsKey(message.id))
        {
            val request = rpcRequests[message.id]

            rpcRequests.remove(message.id)

            request?.completion?.invoke(RpcResult(text))
        }

        Log.e(TAG, "onMessage: $text")

        if(message.method == null)
            return

        when(message.method)
        {
            kParticipantJoined -> {
                val typeOfT: Type = object : TypeToken<RpcResponse<CallParticipant>>() {}.type
                val resp: RpcResponse<CallParticipant> = Serializer.deserialize(text, typeOfT) ?: return

                if (resp.params != null)
                    peerDelegate.onParticipantJoined(msg = resp.params)
            }

            kParticipantLeft -> {
                val typeOfT: Type = object : TypeToken<RpcResponse<CallParticipant>>() {}.type
                val resp: RpcResponse<CallParticipant> = Serializer.deserialize(text, typeOfT) ?: return

                if (resp.params != null)
                    peerDelegate.onParticipantLeft(msg = resp.params)
            }

            kParticipantConnectionProblem -> {
                val typeOfT: Type = object : TypeToken<RpcResponse<ParticipantGenericMessage>>() {}.type
                val resp: RpcResponse<ParticipantGenericMessage> = Serializer.deserialize(text, typeOfT) ?: return

                if (resp.params != null)
                    peerDelegate.onParticipantConnectionProblem(msg = resp.params)
            }

            kParticipantConnectionRecovered -> {
                val typeOfT: Type = object : TypeToken<RpcResponse<ParticipantGenericMessage>>() {}.type
                val resp: RpcResponse<ParticipantGenericMessage> = Serializer.deserialize(text, typeOfT) ?: return

                if (resp.params != null)
                    peerDelegate.onParticipantConnectionRecovered(msg = resp.params)
            }

            kIceCandidate -> {
                val typeOfT: Type = object : TypeToken<RpcResponse<RemoteIceCandidate>>() {}.type
                val resp: RpcResponse<RemoteIceCandidate> = Serializer.deserialize(text, typeOfT) ?: return

                if (resp.params != null)
                    peerDelegate.onParticipantIceCandidate(msg = resp.params)
            }

            kMediaRequest -> {
                val typeOfT: Type = object : TypeToken<RpcResponse<RemoteMediaRequest>>() {}.type
                val resp: RpcResponse<RemoteMediaRequest> = Serializer.deserialize(text, typeOfT) ?: return

                if (resp.params != null)
                    peerDelegate.onParticipantRequestedMedia(msg = resp.params)
            }

            kRequestForOffer -> {
                val typeOfT: Type = object : TypeToken<RpcResponse<CallParticipant>>() {}.type
                val resp: RpcResponse<CallParticipant> = Serializer.deserialize(text, typeOfT) ?: return

                if (resp.params != null)
                    peerDelegate.onParticipantRequestedOffer(msg = resp.params)
            }

            kOnPresentationAction -> {
                val typeOfT: Type = object : TypeToken<RpcResponse<ScreenStateMessage>>() {}.type
                val resp: RpcResponse<ScreenStateMessage> = Serializer.deserialize(text, typeOfT) ?: return

                if (resp.params != null)
                    peerDelegate.onScreenStateAction(msg = resp.params)
            }

            kParticipantDeviceStateChanged -> {
                val typeOfT: Type = object : TypeToken<RpcResponse<ParticipantDeviceStateMessage>>() {}.type
                val resp: RpcResponse<ParticipantDeviceStateMessage> = Serializer.deserialize(text, typeOfT) ?: return

                if (resp.params != null)
                    peerDelegate.onParticipantMediaStateChanged(msg = resp.params)
            }
        }
    }

    override fun onClosing(webSocket: WebSocket, code: Int, reason: String?)
    {
        this.webSocket = null
        this.connecting = false
        this.connected = false
    }

    override fun onClosed(webSocket: WebSocket, code: Int, reason: String?)
    {
        this.webSocket = null
        this.connecting = false
        this.connected = false

        peerDelegate.onSocketDisconnected(closing)
        reconnect()
    }

    override fun onFailure(webSocket: WebSocket?, t: Throwable, response: Response?)
    {
        this.webSocket = null
        this.connecting = false
        this.connected = false

        peerDelegate.onSocketDisconnected(closing)
        reconnect()
    }
}