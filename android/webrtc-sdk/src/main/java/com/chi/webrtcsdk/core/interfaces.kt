package com.chi.webrtcsdk.core

import com.chi.webrtcsdk.databinding.ActiveCallParticipantViewBinding
import org.webrtc.VideoTrack



interface SignalSocketDelegate
{
    fun onSocketConnected()
    fun onSocketDisconnected(closing: Boolean)

    fun onAuthenticated(authSession: AuthSession)
    fun onSessionAuthenticationFailed(errorMessage: String)

    fun onRoomJoined(msg: JoinRoomResponse)

    fun onParticipantRequestedOffer(msg: CallParticipant)

    fun onParticipantJoined(msg: CallParticipant)
    fun onParticipantLeft(msg: CallParticipant)
    fun onParticipantConnectionProblem(msg: ParticipantGenericMessage)
    fun onParticipantConnectionRecovered(msg: ParticipantGenericMessage)

    fun onParticipantIceCandidate(msg: RemoteIceCandidate)

    fun onParticipantRequestedMedia(msg: RemoteMediaRequest)

    fun onParticipantMediaStateChanged(msg: ParticipantDeviceStateMessage)

    fun onScreenStateAction(msg: ScreenStateMessage)
}

interface RpcClient
{
    fun <T> sendRpcRequest(method: String, params: T, completion: RpcCompletion)
    fun <T> sendRpcResponse(refId: Int, endPoint: String, result: T, completion: RpcCompletion)
}

interface CallUiDelegate
{
    fun getLocalView(): ActiveCallParticipantViewBinding?

    fun updateParticipant(msg: CallParticipant)

    fun participantJoined(msg: CallParticipant)
    fun participantLeft(id: String)

    fun addVideoTrack(connectionId: String, track: VideoTrack)
    fun userDeviceStateChanged(msg: ParticipantDeviceStateMessage)
    fun userScreenStateChanged(msg: ScreenStateMessage)
    fun endCall()

    fun participantConnectionProblem(id: String)
    fun participantConnectionRecovered(id: String)

    fun stopUI()

    fun autoCallEnded()
    fun updateConnectivityState(isConnected: Boolean)
}

interface CallStateListener
{
    fun callEnded()
}

interface CallUIManagerListener
{
    fun autoCallEnded()
}