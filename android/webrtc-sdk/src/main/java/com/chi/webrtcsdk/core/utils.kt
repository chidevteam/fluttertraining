package com.chi.webrtcsdk.core

import org.webrtc.VideoFrame
import org.webrtc.VideoSink


open class ProxyVideoSink: VideoSink
{
    private var target: VideoSink? = null

    override fun onFrame(frame: VideoFrame)
    {
//        if (target == null) {
//            return;
//        }

        target?.onFrame(frame);
    }


    @Synchronized
    open fun setTarget(target: VideoSink?)
    {
        this.target = target
    }

}