package com.chi.webrtcsdk.core

import android.util.Log
import com.chi.webrtcsdk.Serializer
import com.chi.webrtcsdk.SimpleSdpObserver
import com.google.gson.reflect.TypeToken
import org.webrtc.*
import java.lang.reflect.Type
import org.webrtc.IceCandidate
import org.webrtc.MediaStream


class RemotePeer(
    private val connectionId: String,
    client: RpcClient,
    factory: PeerConnectionFactory,
    private val uiDelegate: CallUiDelegate,
    iceServers: List<PeerConnection.IceServer>
): PeerConnection.Observer
{
    private val rpcClient: RpcClient = client
    private var peerConnection: PeerConnection? = null

    private var remoteVideoTrack: VideoTrack? = null

    private var audioSender: RtpSender? = null
    private var videoSender: RtpSender? = null

    private var initialized: Boolean = false
    private var stopping: Boolean = false

    companion object
    {
        private const val TAG = "PC-R"
    }

    init
    {
        val rtcConfig = PeerConnection.RTCConfiguration(iceServers)
        rtcConfig.iceTransportsType = PeerConnection.IceTransportsType.RELAY
        rtcConfig.continualGatheringPolicy = PeerConnection.ContinualGatheringPolicy.GATHER_CONTINUALLY
        rtcConfig.sdpSemantics = PeerConnection.SdpSemantics.UNIFIED_PLAN

        peerConnection = factory.createPeerConnection(rtcConfig, this)

        Log.e(TAG, "🔔 RemotePeer Created")
    }

    fun start(localMedia: LocalMedia)
    {
        Log.e(TAG, "🔔 RemotePeer STarting")
        val streamId = connectionId

        Log.e(TAG, "🔔 RemotePeer Adding AudioTrack")
        audioSender = peerConnection!!.addTrack(localMedia.audioTrack, arrayListOf(streamId))

        Log.e(TAG, "🔔 RemotePeer Adding VideoTrack")
        //TODO uncomment this for video call
        if (localMedia.videoTrack != null)
            videoSender = peerConnection!!.addTrack(localMedia.videoTrack, arrayListOf(streamId))

        initialized = true
        createAndSendOffer()

        Log.e(TAG, "🔔 RemotePeer Both Tracks Added")
    }

    fun stop()
    {
        stopping = true
        val peer = peerConnection ?: return

        if(audioSender != null)
        {
            peer.removeTrack(audioSender)
            audioSender = null
        }

        if(videoSender != null)
        {
            peer.removeTrack(videoSender)
            videoSender = null
        }

        peer.close()
    }

    fun addIceCandidate(msg: RemoteIceCandidate)
    {
        val candidate = IceCandidate(
            msg.sdpMid,
            msg.sdpMLineIndex,
            msg.candidate
        )

        peerConnection!!.addIceCandidate(candidate)
    }

    fun replaceVideoTrack(localMedia: LocalMedia)
    {
        val streamId = connectionId

        Log.e(TAG, "🔔 RemotePeer Replacing VideoTrack")

        peerConnection?.let {
            initialized = false
            it.removeTrack(videoSender)

            videoSender = it.addTrack(localMedia.videoTrack, arrayListOf(streamId))
            initialized = true
            createAndSendOffer()
        }
    }

    fun reconnectRtc()
    {
        val pc = peerConnection ?: return

        if (pc.iceConnectionState() == PeerConnection.IceConnectionState.FAILED)
        {
            createAndSendOffer(true)
        }
    }

    private fun createAndSendOffer(reset: Boolean = false)
    {
        if(stopping) return

        val mediaConstraints = MediaConstraints()

        if (reset)
        {
            mediaConstraints.mandatory.add(MediaConstraints.KeyValuePair("IceRestart", "true"))
        }

        Log.e(TAG, "🔔 RemotePeer createAndSendOffer")

        peerConnection?.createOffer(object: SdpObserver {
            override fun onCreateSuccess(sessionDescription: SessionDescription) {

                Log.e(TAG, "🔔 RemotePeer Offer Created")
                peerConnection?.setLocalDescription(SimpleSdpObserver(), sessionDescription)

                val req = MediaOfferRequest(
                    sender = connectionId,
                    sdpOffer = sessionDescription.description
                )

                rpcClient.sendRpcRequest(method = "sendOffer", params = req) {
                    onAnswerReceived(it)
                }
            }

            override fun onSetSuccess() {
                Log.e(TAG, "🔔 RemotePeer Offer SET")
            }

            override fun onCreateFailure(p0: String?) {
                Log.e(TAG, "🔔 RemotePeer Offer Create Failed: $p0")
            }

            override fun onSetFailure(p0: String?) {
                Log.e(TAG, "🔔 RemotePeer Offer SET Failed")
            }

        }, mediaConstraints)
    }

    fun onAnswerReceived(result: RpcResult)
    {
        Log.e(TAG, "🔔 RemotePeer onAnswerReceived")

        val typeOfT: Type = object : TypeToken<RpcResponse<MediaOfferResponse>>() {}.type
        val resp: RpcResponse<MediaOfferResponse> = Serializer.deserialize(result.message, typeOfT) ?: return

        val sdp = SessionDescription(SessionDescription.Type.ANSWER, resp.result!!.sdpAnswer)

        peerConnection!!.setRemoteDescription(object: SdpObserver{
            override fun onCreateSuccess(p0: SessionDescription?) {
                Log.e(TAG, "🔔 Remote SDP SET onCreateSuccess")
            }

            override fun onSetSuccess() {
                Log.e(TAG, "🔔 Remote SDP SET")
            }

            override fun onCreateFailure(p0: String?) {
                Log.e(TAG, "🔔 Remote SDP Failed: $p0")
            }

            override fun onSetFailure(p0: String?) {
                Log.e(TAG, "🔔 Remote SDP SET Failed")
            }

        }, sdp)
    }

    fun onOfferReceived(msg: RemoteMediaRequest)
    {
        val sdp = SessionDescription(SessionDescription.Type.OFFER, msg.sdpOffer)

        peerConnection!!.setRemoteDescription(object: SdpObserver{
            override fun onCreateSuccess(p0: SessionDescription?) {
            }

            override fun onSetSuccess() {
                createAndSendAnswer(msg)
            }

            override fun onCreateFailure(p0: String?) {
            }

            override fun onSetFailure(p0: String?) {
            }

        }, sdp)

    }

    fun createAndSendAnswer(msg: RemoteMediaRequest)
    {
        peerConnection!!.createAnswer(object: SdpObserver {

            override fun onCreateSuccess(sessionDescription: SessionDescription) {
                peerConnection!!.setLocalDescription(SimpleSdpObserver(), sessionDescription)

                val result = MediaOfferResponse(sessionDescription.description)

                rpcClient.sendRpcResponse(
                    refId = msg.ref_id,
                    endPoint = msg.endpointName,
                    result = result
                ) {
                }
            }

            override fun onSetSuccess() {
            }

            override fun onCreateFailure(p0: String?) {
            }

            override fun onSetFailure(p0: String?) {
            }

        }, MediaConstraints())
    }


    override fun onSignalingChange(state: PeerConnection.SignalingState) {
        Log.e(TAG, "🔔 RemotePeer onSignalingChanged: $state")
    }

    override fun onIceConnectionChange(state: PeerConnection.IceConnectionState) {
        Log.e(TAG, "🔔 RemotePeer onIceConnectionChanged: $state")
    }

    override fun onIceConnectionReceivingChange(p0: Boolean) {
        Log.e(TAG, "🔔 RemotePeer onIceConnectionReceivingChange")
    }

    override fun onIceGatheringChange(state: PeerConnection.IceGatheringState?) {
        Log.e(TAG, "🔔 RemotePeer onIceGatheringChanged: $state")
    }

    override fun onIceCandidate(candidate: IceCandidate) {
        Log.e(TAG, "🔔 RemotePeer onIceCandidate")

        val req = IceCandidateRequest(
            endpointName = connectionId,
            candidate = candidate.sdp,
            sdpMid = candidate.sdpMid,
            sdpMLineIndex = candidate.sdpMLineIndex
        )

        rpcClient.sendRpcRequest("onIceCandidate", req) {

        }
    }

    override fun onIceCandidatesRemoved(p0: Array<out IceCandidate>?) {
        Log.e(TAG, "🔔 RemotePeer onIceCandidatesRemoved")
    }

    override fun onAddStream(p0: MediaStream?) {
        Log.e(TAG, "🔔 RemotePeer onAddStream")
    }

    override fun onRemoveStream(p0: MediaStream?) {
        Log.e(TAG, "🔔 RemotePeer onRemoveStream")
    }

    override fun onDataChannel(p0: DataChannel?) {
        Log.e(TAG, "🔔 RemotePeer onDataChannel")
    }

    override fun onRenegotiationNeeded()
    {
        Log.e(TAG, "🔔 RemotePeer onRenegotiationNeeded")

        if (initialized)
            createAndSendOffer()
    }

    override fun onAddTrack(receiver: RtpReceiver, streams: Array<out MediaStream>?)
    {
        val kind = receiver.track()?.kind() ?: "N/A"
        Log.e(TAG, "🔔 RemotePeer onAddTrack - $kind")

        if (receiver.track()?.kind() == "video")
        {
            remoteVideoTrack = receiver.track() as VideoTrack?
            uiDelegate.addVideoTrack(connectionId, receiver.track() as VideoTrack)
            Log.e(TAG, "🔔 RemotePeer Video Track Added")
        }
    }
}