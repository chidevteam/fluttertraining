package com.chi.webrtcsdk.core


data class UserMeta (
    var user_id: String,
    var user_name: String,
    var user_image: String?,
    var is_recorder: Boolean?
)

data class AuthSession (
    var auth_token: String,
    var room_id: String,
    var is_recorder: Boolean,

    var connectionId: String,
    var socketUrl: String,
)

data class AuthRequest (
    var room_id: String,
    var user_id: String,
    var user_name: String,
    var invite_token: String?
)

data class AuthResponse(
    var error: String?,
    var session: AuthSession?
)

data class ParticipantStream (
    var id: String,

    var hasAudio: Boolean,
    var hasVideo: Boolean,
    var audioActive: Boolean,
    var videoActive: Boolean,
    var screenActive: Boolean
)

data class ParticipantDeviceState (
    var audioActive: Boolean,
    var videoActive: Boolean,
    var screenActive: Boolean
)

data class CallParticipant (
    var id: String,
    var createdAt: Long?,
    var metadata: UserMeta,

    var streams: List<ParticipantStream>?,
    var deviceState: ParticipantDeviceState?
)

data class JoinRoomRequest (
    var room_id: String,
    var connectionId: String,
    var platform: String = "Android Native",

    var has_audio: Boolean = true,
    var audio_active: Boolean = true,

    var has_video: Boolean = true,
    var video_active: Boolean = true,

    var is_recorder: Boolean = false,

    var secret: String = "",

    var metadata: UserMeta
)

data class CommsIceServer (
    var urls: List<String>,
    var username: String?,
    var credential: String?
)

data class JoinRoomResponse (
    var id: String,
    var created_at: Long,
    var metadata: UserMeta,

    var value: List<CallParticipant>,

    var session_id: String,
    var auth_token: String,
    var ice_servers: List<CommsIceServer>
)

data class SignalMessage (
    var id: String?,
    var method: String?
)

data class RemoteIceCandidate (
    var senderConnectionId: String,
    var endpointName: String,

    var candidate: String,
    var sdpMid: String,
    var sdpMLineIndex: Int
)

data class RemoteMediaRequest (
    var endpointName: String,
    var metadata: UserMeta,

    var ref_id: Int,
    var sdpOffer: String,

    var deviceState: ParticipantDeviceState
)

data class PingRequest(var interval: Int = 5000)

data class DeviceStateChangedRequest(
    var endpointName: String,
    var device: String,
    var state: Boolean
)

data class MediaOfferRequest(
    var sender: String,
    var sdpOffer: String
)

data class MediaOfferResponse(var sdpAnswer: String)

data class IceCandidateRequest(
    var endpointName: String,
    var candidate: String,
    var sdpMid: String?,
    var sdpMLineIndex: Int
)

data class ParticipantDeviceStateMessage(
    var endpointName: String,
    var audio: Boolean,
    var video: Boolean
)

data class ScreenStateMessage(
    var connectionId: String,
    var action: String
)

data class ParticipantGenericMessage(var id: String)

data class OfferRequest(
    var id: String,
    var sender: String
)

data class RpcRequest<T>(
    val id : Int,
    val method: String,
    val params: T
)

data class RpcResponse<T>(
    val id: Int?,
    val method: String?,
    val result: T?,
    val params: T?,

    val endpointName: String?,
    val ref_id: Int?
)

data class RpcResponseHeader(
    val id: Int?,
    val method: String?,
    val endpointName: String?,
    val ref_id: Int?
)

class RpcResult(val message: String)

typealias RpcCompletion = (result: RpcResult) -> Unit

class RpcRequestEntry(
    val request: Any,
    val completion: RpcCompletion
)


data class CallConfig(
    var mainServer: String,
    var roomId: String,
    var callType: String,

    var auth_token: String,

    var my_user_id: String,
    var my_name: String,
    var my_image: String?,

    var inviteToken: String?,
    var viewSessionId: String? = null,
    var is_teleconsult: Boolean = false,
    var tc_config: TeleconsultConfig? = null
)


class RtcAppConfig(config: CallConfig)
{
    val mainServer: String = config.mainServer
    val roomId: String = config.roomId
    val callType: String = config.callType

    var inviteToken: String? = config.inviteToken

    val userMetaData = UserMeta(config.my_user_id, config.my_name, config.my_image, false)

    val authToken: String = config.auth_token
    val viewSessionId: String? = config.viewSessionId
    var is_teleconsult: Boolean = config.is_teleconsult
    var tc_config: TeleconsultConfig? = config.tc_config

    var iceServers: List<CommsIceServer> = ArrayList()
}

data class TeleconsultConfig(
    var enforce_call_duration: Boolean,
    var yellow_alert_delta: Double? = null,
    var red_alert_delta: Double? = null,
    var duration: Double? = null
)